<?php

use yii\helpers\Html;
use common\models\OrdenRetiroGrupal;
use common\models\Remito;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;





/* @var $this yii\web\View */
/* @var $model common\models\Remito */
$idRemitoGrupal = Yii::$app->getRequest()->getQueryParam('ordenderetirogrupal_idordenderetirogrupal');
$idGrup = Yii::$app->getRequest()->getQueryParam('id');

$this->title = Yii::t('app', 'Agregar remitos a Orden Grupal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Remitos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><i class="fa fa-download"></i> <?= Html::encode($this->title) ?></h1>

<div class="x_panel">
    <div class="x_content">
        <?php
        if(isset($idRemitoGrupal)){
            echo $this->render('view', [
                'model' => OrdenRetiroGrupal::findOne($idRemitoGrupal),
                ]);
        }else{
            echo $this->render('view', [
                'model' => OrdenRetiroGrupal::findOne($idGrup),
                ]);
        }
        ?>
    </div>
</div>

<!--Añadir remito -->
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Agregar remito');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

    <?= $this->render('../remito/_form', [
        'model' => $model,
    ]) ?>
    </div>
</div>

