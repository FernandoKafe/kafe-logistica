<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Rendicion;

/**
 * RendiconSearch represents the model behind the search form of `common\models\Rendicion`.
 */
class RendiconSearch extends Rendicion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idrendicion'], 'integer'],
            [['fecha_inicio', 'fecha_fin', 'cliente_idcliente'], 'safe'],
            [['monto'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params,$tipo)
    {
        $query = Rendicion::find();
        $query->joinWith(['clienteIdcliente', 'clienteIdcliente.clienteHasTipos tipos']);
        //$query->joinWith(['clienteIdcliente as cliente']);

        // add conditions that should always apply here

        if($tipo == 0){
            $query->where(['tipos.clientetipo_idclientetipo' => 1]);
        }else if($tipo == 1){
            $query->where(['tipos.clientetipo_idclientetipo' => 2]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idrendicion' => $this->idrendicion,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_fin' => $this->fecha_fin,
            'monto' => $this->monto,
            //'cliente_idcliente' => $this->cliente_idcliente,
        ]);

        $query->andFilterWhere(['like', 'cliente.razonsocial', $this->cliente_idcliente]);

        return $dataProvider;
    }
}
