<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\SucursalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sucursales');
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><i class="fa fa-file"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2>Listado</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php Pjax::begin(); ?>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Crear Sucursal'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'idsucursal',
            [
                'attribute' => 'nombre',
                'label' => 'Nombre Sucursal',
            ],
            [
                'attribute' => 'empresa_idempresa',
                'label' => 'Empresa',
                'format' => 'text', 
                'content' => function($data){
                    return $data->empresaIdempresa->nombre;
                }
            ],
            /*[
                'attribute' => 'direccion',
                'label' => 'Dirección',
                'content' => function($data){
                    return $data->direccion.", ".$data->localidadIdlocalidad->nombre
                    .", ".$data->localidadIdlocalidad->departamentoIddepartamento->nombre
                    .", ".$data->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                }
            ],*/

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
</div>
