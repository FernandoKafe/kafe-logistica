<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TarifarioDomicilio */

$this->title = $model->idtarifario_domicilio;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tarifario Domicilios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarifario-domicilio-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idtarifario_domicilio], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idtarifario_domicilio], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idtarifario_domicilio',
            'reparto',
            'reparto_adicional',
            'nombre',
        ],
    ]) ?>

</div>
