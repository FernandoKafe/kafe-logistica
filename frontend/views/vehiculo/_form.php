<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Vehiculo */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vehiculo-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-4"><?= $form->field($model, 'marca')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-4"><?= $form->field($model, 'modelo')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-4"><?= $form->field($model, 'patente')->textInput(['maxlength' => true]) ?></div>

    <div class="form-group col-md-12">
    <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);?>
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
