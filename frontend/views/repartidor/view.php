<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Repartidor */

$this->title = $model->idrepartidor;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Repartidors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="repartidor-view">
    <h1><i class="fa fa-male"></i> Repartidor: <?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Detalle') ?></h2>
        <div class="clearfix"></div>
    </div>

    <p>
        <?= Html::a('Inicio', ['index'], ['class' => 'btn btn-primary']);?>
        <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->idrepartidor], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idrepartidor',
            'nombre',
            'apellido',
            'nacionalidad',
            'nacimiento',
            'grupo_sanguineo',
            'dni',
            'cuil',
            'email:email',
            'telefono',
            'celular',
            'estudios',
            [
                'attribute' => 'localidad_idlocalidad',
                'value' => function($data){
                    if(isset($data->localidad_idlocalidad)){
                        return $data->localidadIdlocalidad->nombre
                                .", ".$data->localidadIdlocalidad->departamentoIddepartamento->nombre
                                .", ".$data->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                    }else{
                        return " - ";
                    }
                }
            ],
            'direccion',
            'estado_civil',
            'hijos',
            'puesto',
        ],
    ]) ?>

</div>
</div>
