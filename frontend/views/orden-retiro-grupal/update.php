<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OrdenRetiroGrupal */

$this->title = Yii::t('app', 'Actualizar Orden Grupal Nº: ' . $model->numero, [
    'nameAttribute' => '' . $model->numero,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ordenes de Retiro Grupal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idordenderetirogrupal, 'url' => ['view', 'id' => $model->idordenderetirogrupal]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<h1><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario de modificación') ?></h2>
        <div class="clearfix"></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
