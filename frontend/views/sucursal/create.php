<?php

use yii\helpers\Html;
use common\models\Empresa;
use \yii\helpers\ArrayHelper;



/* @var $this yii\web\View */
/* @var $model common\models\Sucursal */

$idEmpresa = Yii::$app->getRequest()->getQueryParam('idempresa');

if(isset($idEmpresa)){
    $empresa = Empresa::findOne($idEmpresa);
    $this->title = Yii::t('app', 'Agregar Sucursal a empresa: ').$empresa->nombre;
}else{
    $this->title = Yii::t('app', 'Crear Sucursal');
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sucursales'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cliente-create">
<h1><i class="fa fa-download"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario') ?></h2>
        <div class="clearfix"></div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
