<?php

namespace frontend\controllers;

use Yii;
use common\models\Zona;
use frontend\models\ZonaSearch;
use common\models\Localidad;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\ZonaHasLocalidad;

/**
 * ZonaController implements the CRUD actions for Zona model.
 */
class ZonaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Zona models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ZonaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Zona model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Zona model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Zona();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['add-localidad', 'id' => $model->idzona]);
        }

        return $this->render('create', [ 
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Zona model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['add-localidad',
            'id' => $model->idzona,
            ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Zona model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Zona model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Zona the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Zona::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Add localidades to an existing zona.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id idcomprobante
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAddLocalidad()
    {
        $model = new ZonaHasLocalidad();

        if ($model->load(Yii::$app->request->post())) {
            foreach($model->localidad_idlocalidad as $lol){
                $modelNew = new ZonaHasLocalidad();
                $modelNew->zona_idzona = $model->zona_idzona;
                $modelNew->localidad_idlocalidad = $lol;
                $localidadZona = ZonaHasLocalidad::find()->where(['localidad_idlocalidad' => $lol])->one();
                if($localidadZona){
                    Yii::$app->session->setflash('error', "La localidad ".$modelNew->localidadIdlocalidad->nombre." ya pertenece a la ".$localidadZona->zonaIdzona->nombre);
                    return $this->render('add-localidad', [
                        'model' => $model,
                    ]);
                }else{
                    $modelNew->save();
                }
            }
            return $this->redirect(['add-localidad',
                    'id' => $model->zona_idzona,
                ]);
        }

        return $this->render('add-localidad', [
            'model' => $model,
        ]);
    }
}
