<?php     

use \yii\helpers\ArrayHelper;
use common\models\RemitoVino;
use common\models\OrdenGrupalVinos;
use common\models\LiquidacionVinoRepartidor;

$remitosVino = [];
if($liquidacionVinos) {
    $ordengrupalvinos = OrdenGrupalVinos::find()->where(['liquidacionvinorepartidor_idliquidacionvinorepartidor' => $liquidacionVinos->idliquidacion_vino_repartidor])->all();
    //echo "<pre>",var_dump($ordenesGrupales),"</pre>";
    $ordenesGrupalesVinosId = ArrayHelper::getColumn($ordengrupalvinos, 'idorden_grupal_vinos');
    $remitosVino = RemitoVino::find()
    ->select('SUM(cant_cajas) as cant_cajas, ordengrupalvinos_idordengrupalvinos')
    ->where(['IN', 'ordengrupalvinos_idordengrupalvinos', $ordenesGrupalesVinosId])
    ->groupBy(['ordengrupalvinos_idordengrupalvinos'])
    ->orderBy(['ordengrupalvinos_idordengrupalvinos' => SORT_ASC])
    ->all();


    $remitosClientes = OrdenGrupalVinos::find()
    ->where(['liquidacionvinorepartidor_idliquidacionvinorepartidor' => $liquidacionVinos->idliquidacion_vino_repartidor])
    ->groupBy(['cliente_idcliente'])
    ->all();

    $arrayCajas = RemitoVino::find()
    ->select('SUM(cant_cajas) as cant_cajas, ordengrupalvinos_idordengrupalvinos, precio_reparto')
    ->joinWith(['ordengrupalvinosIdordengrupalvinos'])
    ->where(['IN', 'ordengrupalvinos_idordengrupalvinos', $ordenesGrupalesVinosId])
    ->groupBy(['cliente_idcliente', 'precio_reparto'])
    ->all();
}

if($remitosVino){ ?>
    <table style="table-layout: fixed; width: 100% ;">
        <thead>
            <tr>
                <th style="width:10%; text-align:center ">Cliente</th>
                <?php for($i = 1; $i <=31; $i++){ ?>
                        <th style="width:4%;  text-align:center "><?= $i ?></th>
                    <?php } ?>
                    <th style="width:5%;  text-align:center">TOTAL</th><!--Resto de celdas de fila cliente-->
            </tr>
        </thead>
        <tbody >
        <?php
        $clienteAnterior = 0;
        foreach($remitosClientes as $rc){
            $totalCajas = 0;?>
            <tr style="background-color: #ddd"><!--Fila cliente-->
                <td style="border-right: 1px solid #ccc;"><strong><?= $rc->clienteIdcliente->razonsocial ?></strong></td><!--Primer celda fila cliente-->
                <?php for($i = 0; $i <= 31; $i++){ ?>
                    <td style="border-right: 1px solid #ccc;width:4%"></td><!--Resto de celdas de fila cliente-->
                <?php } ?>
            </tr>

            <tr style="border-bottom: 1px solid #ccc;">
                <td style="border-right: 1px solid #ccc; width:10%; text-align:center"><strong>Cajas</strong></td>
                <?php 
                for($i = 1; $i <= 32; $i++){ 
                    $flag = 0;
                    foreach($remitosVino as $cc){
                        if(($rc->cliente_idcliente == $cc->ordengrupalvinosIdordengrupalvinos->cliente_idcliente) && ($cc->cant_cajas)){
                            $date = date('d', strtotime($cc->ordengrupalvinosIdordengrupalvinos->fecha));
                            $date = intval($date); 
                            if(($cc->cant_cajas != 0) && ($date == $i)){
                                $totalCajas += $cc->cant_cajas;?>
                                <td style="border-right: 1px solid #ccc;width:4%; text-align:center"><?= $cc->cant_cajas?></td>
                            <?php 
                                $flag = 1;
                                break;
                            }  
                        }
                    }
                    if($flag == 0){  
                        if($i < 32){?>
                        <td style="border-right: 1px solid #ccc;width:4%"></td>
                        <?php }else{ ?>
                            <td style="border-right: 1px solid #ccc;width:5%; text-align:center"><strong><?= $totalCajas ?></strong></td>
                        <?php }
                    } 
                }?>
            </tr>
    <?php   } ?>
        </tbody>
    </table>
    <br>
<?php  
} ?>



