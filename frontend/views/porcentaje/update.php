<?php

use yii\helpers\Html; 

/* @var $this yii\web\View */
/* @var $model common\models\Porcentaje */

$this->title = Yii::t('app', 'Actualizar Porcentaje: ' . $model->cantidad."%", [
    'nameAttribute' => '' . $model->idporcentaje,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Porcentajes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idporcentaje, 'url' => ['view', 'id' => $model->idporcentaje]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<h1><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario de modificación') ?></h2>
        <div class="clearfix"></div>
        </div>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

</div>
