<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "orden_retiro_grupal".
 *
 * @property int $idordenderetirogrupal
 * @property string $fecha
 * @property int $numero
 * @property int $cliente_idcliente
 * @property int $repartidor_idrepartidor
 * @property int $orden_especial
 * @property string $creado
 * 
 * @property Cliente $clienteIdcliente
 * @property Repartidor $repartidorIdrepartidor
 * @property Remito[] $remitos
 */
class OrdenRetiroGrupal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orden_retiro_grupal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero', 'cliente_idcliente', 'repartidor_idrepartidor', 'fecha'], 'required'],
            [['cliente_idcliente', 'repartidor_idrepartidor', 'orden_especial'], 'integer'],
            [['fecha', 'creado'], 'safe'],
            [['numero'], 'unique'],
            [['numero'], 'string', 'max' => 100],
            [['cliente_idcliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['cliente_idcliente' => 'idcliente']],
            [['repartidor_idrepartidor'], 'exist', 'skipOnError' => true, 'targetClass' => Repartidor::className(), 'targetAttribute' => ['repartidor_idrepartidor' => 'idrepartidor']],
        ];
    } 

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idordenderetirogrupal' => Yii::t('app', 'Orden de Retiro Grupal'),
            'fecha' => Yii::t('app', 'Fecha de Retiro Grupal'),
            'numero' => Yii::t('app', 'Nº de Retiro Grupal'),
            'cliente_idcliente' => Yii::t('app', 'Cliente'),
            'repartidor_idrepartidor' => Yii::t('app', 'Repartidor'),
            'orden_especial' => Yii::t('app', 'Orden Especial'),
            'creado' => Yii::t('app', 'Creado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClienteIdcliente()
    {
        return $this->hasOne(Cliente::className(), ['idcliente' => 'cliente_idcliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepartidorIdrepartidor()
    {
        return $this->hasOne(Repartidor::className(), ['idrepartidor' => 'repartidor_idrepartidor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemitos()
    {
        return $this->hasMany(Remito::className(), ['ordenderetirogrupal_idordenderetirogrupal' => 'idordenderetirogrupal']);
    }

    /**
     * @return cliente.nombre
     */
    public function getClienteRazonsocial(){
        $model=$this->cliente_idcliente;
        return $model?$model->razonsocial:'';
    }

    /**
     * Función que controla si el la totalidad de los remitos 
     * de la orden grupal estan liquidado al repartidor */
    public static function getEstadoLiquidacionRepartidorByOrdenId($ordenId){
        //Busca en la orden remitos que NO esten liquidados al repartidor
        $remitosNoLiquidadosRep = Remito::find()->select(['liquidado_repartidor'])->where(['ordenderetirogrupal_idordenderetirogrupal' => $ordenId])->andWhere(['liquidado_repartidor' => 0])->all();
        //Busca en la orden remitos que esten liquidados al repartidor
        $remitosLiquidadosRep = Remito::find()->select(['liquidado_repartidor'])->where(['ordenderetirogrupal_idordenderetirogrupal' => $ordenId])->andWhere(['liquidado_repartidor' => 1])->all();
        //Si la orden tiene remitos liquidados y no liquidados devuelve true        
        if($remitosLiquidadosRep && $remitosNoLiquidadosRep)
            return false;
        else if($remitosLiquidadosRep)
            return true;
        else    
            return false;
    }



    /**
     * Función que controla si el la totalidad de los remitos 
     * de la orden grupal estan liquidado al repartidor */
    public static function getEstadoLiquidacionClienteByOrdenId($ordenId){
        //Busca en la orden remitos que NO esten liquidados a cliente
        $remitosNoLiquidadosCli = Remito::find()->select(['liquidado'])->where(['ordenderetirogrupal_idordenderetirogrupal' => $ordenId])->andWhere(['liquidado' => 0])->all();
        //Busca en la orden remitos que esten liquidados al cliente
        $remitosLiquidadosCli = Remito::find()->select(['liquidado'])->where(['ordenderetirogrupal_idordenderetirogrupal' => $ordenId])->andWhere(['liquidado' => 1])->all();
        //Si la orden tiene remitos liquidados y no liquidados devuelve true
        if($remitosNoLiquidadosCli && $remitosLiquidadosCli)
            return false;
        else if($remitosLiquidadosCli)
            return true;
        else
            return false;
    }
}
