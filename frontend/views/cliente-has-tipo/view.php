<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ClienteHasTipo */

$this->title = $model->cliente_idcliente;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cliente Has Tipos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cliente-has-tipo-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'cliente_idcliente' => $model->cliente_idcliente, 'clientetipo_idclientetipo' => $model->clientetipo_idclientetipo], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'cliente_idcliente' => $model->cliente_idcliente, 'clientetipo_idclientetipo' => $model->clientetipo_idclientetipo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cliente_idcliente',
            'clientetipo_idclientetipo',
        ],
    ]) ?>

</div>
