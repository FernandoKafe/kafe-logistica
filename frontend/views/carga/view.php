<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Carga */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cargas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1>Carga: <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2>Detalle</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

    <p>
        <?= Html::a(Yii::t('app', 'Inicio'), ['index', 'id' => $model->idcarga], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->idcarga], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
        ],
    ]) ?>

</div> 
</div> 
