<?php

namespace frontend\controllers;

use Yii;
use common\models\LiquidacionPorcentajeRepartidor;
use frontend\models\LiquidacionPorcentajeRepartidorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\RemitoPorcentaje;
use common\models\LiquidacionporcentajerepartidorHasRemito;
use common\models\PagosPorcentajesRepartidor;

/**
 * LiquidacionPorcentajeRepartidorController implements the CRUD actions for LiquidacionPorcentajeRepartidor model.
 */
class LiquidacionPorcentajeRepartidorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LiquidacionPorcentajeRepartidor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LiquidacionPorcentajeRepartidorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LiquidacionPorcentajeRepartidor model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LiquidacionPorcentajeRepartidor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LiquidacionPorcentajeRepartidor();
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            //Traer todo los remitos porcentaje asociados al repartidor comprendidos entre fecha_inicio y fecha_fin
            $remitos = RemitoPorcentaje::getRemitosPorcentajeFechaRepartidor($model->repartidor_idrepartidor, $model->fecha_inicio, $model->fecha_fin);
            $flag = 0;
            if($remitos){
                foreach($remitos as $r){
                    $pago = PagosPorcentajesRepartidor::getPagoByPorcentaje(($r->cant_entregados * 100) / $r->cant_pedidos);
                    if(!$pago){
                        Yii::$app->session->setFlash('error', 'Faltan especificar montos de pago en tarifario vinos para el porcentaje:'.(($r->cant_entregados * 100) / $r->cant_pedidos));
                        return $this->redirect(Yii::$app->request->referrer);
                    }else{
                        $flag = 1;
                    }
                }
            }else{
                Yii::$app->session->setflash('error', "No existen remitos a liquidar!");
            }
            
            //Entra al if si existen remitos asociados sino muestra mensaje de error
            if($remitos){
                // Recorre cada remito
                $primerMonto = 0;
                $primerPorcentaje = 0;
                $acumularPorcentaje = 0;
                $fechaAnterior = 0;
                $contador = 1;
                $model->total = 0;
                $faltante = 0;
                $porcentajeAnterior = 0;
                $contadorB = 1;
                $actual = 0;
                $monto = 0;
                $porcentaje_entregas = 0;
                foreach($remitos as $r){
                    // Crea una nueva relación para relacionar remito con liquidación
                    $rel = new LiquidacionporcentajerepartidorHasRemito();
                    // Guarda el ID de la liquidación en la relación
                    $rel->lpr_idlpr = $model->idliquidacion_porcentaje_repartidor;
                    //Guarda el ID del remito en la liquidación
                    $rel->remitoporcentaje_idremitoporcentaje = $r->idremito_porcentaje;
                    //Persiste la relación en la BD
                    $rel->save();
                    //$r->reparto_seleccionado = $pago->monto;
                    if($r->fecha != $remitos[$actual -1]->fecha  && $r->fecha != $remitos[$actual + 1]->fecha ){
                        $r->porcentaje_entregas = $porcentaje_entregas = (($r->cant_entregados * 100) / $r->cant_pedidos);
                        $r->reparto_seleccionado = PagosPorcentajesRepartidor::getPagoByPorcentaje($porcentaje_entregas/($contadorB))->monto;
                        $r->se_paga = 1;
                    }
                    if($r->fecha != $remitos[$actual - 1]->fecha  && $r->fecha === $remitos[$actual + $contadorB]->fecha ) {
                        $r->porcentaje_entregas = $porcentaje_entregas = (($r->cant_entregados * 100) / $r->cant_pedidos);
                        while($r->fecha != $remitos[$actual - 1]->fecha  && $r->fecha == $remitos[$actual + $contadorB]->fecha ){
                            $porcentaje_entregas += (($remitos[$actual + $contadorB]->cant_entregados * 100) / $remitos[$actual + $contadorB]->cant_pedidos);
                            $contadorB++;
                        }

                        $r->porcentaje_entregas = ($porcentaje_entregas / $contadorB);
                        $r->se_paga = 1;
                        $r->reparto_seleccionado = PagosPorcentajesRepartidor::getPagoByPorcentaje($r->porcentaje_entregas)->monto;
                        $contadorB = 1;
                    }
                    if($r->fecha == $remitos[$actual - 1]->fecha){
                        $r->se_paga = 0;
                        $r->porcentaje_entregas = $remitos[$actual - 1]->porcentaje_entregas;
                        $r->reparto_seleccionado = $remitos[$actual - 1]->reparto_seleccionado;
                    }
                    $porcentaje_entregas=0;
                    $actual++;
                    //Entra al IF si el remito actual tiene la misma fecha que el anterior.
                    if($r->se_paga == 1){
                        $model->total += $r->reparto_seleccionado;
                    }

                    $faltante += $r->dinero_faltante;
                    //El remito se marca cómo liquidado al repartidor
                    $r->liquidado_repartidor = 1;
                    // Se persiste el remito en la BD
                    $r->save();
                }
                $model->total = $model->total - $faltante;
                //Se persiste la liquidación en la BD
                $model->creado = date('Y-m-d G:i', time());
                $model->save();
                Yii::$app->session->setflash('success', "Liquidación porcentaje repartidor creada con éxito.");
                return $this->redirect(['view', 'id' => $model->idliquidacion_porcentaje_repartidor]);
            } 
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LiquidacionPorcentajeRepartidor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idliquidacion_porcentaje_repartidor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LiquidacionPorcentajeRepartidor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $rel = LiquidacionporcentajerepartidorHasRemito::find()->where(['lpr_idlpr' => $id])->all();
        if($rel){
            foreach($rel as $r){
                $remito = RemitoPorcentaje::find()->where(['idremito_porcentaje' => $r->remitoporcentaje_idremitoporcentaje])->one();
                if($remito){
                    $remito->liquidado_repartidor = 0;
                    $remito->save();
                }
                $r->delete();
            }
        }

        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the LiquidacionPorcentajeRepartidor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LiquidacionPorcentajeRepartidor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LiquidacionPorcentajeRepartidor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionReport($id) {

        $model = $this->findModel($id);
        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('view', ['model' => $model]);
        
        // setup kartik\mpdf\Pdf component
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;
        
        // return the pdf output as per the destination setting
        return $pdf->render(); 
    }

    /**
     * Creates a new LiquidacionRepartidor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionSummary($id)
    {
        $model = $this->findModel($id);
        $remitos = RemitoPorcentaje::getRemitosLiquidadosByRepartidor($model->repartidor_idrepartidor, $model->fecha_inicio, $model->fecha_fin);

        return $this->render('summary', [ 
            'model' => $model,
            'remitos' => $remitos,
        ]);
    }

    /**BORRAR DESPUES DE ACTUALIZAR
     * Creates a new LiquidacionRepartidor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionArreglar($id)
    {
        $model = $this->findModel($id);
        $remitos = RemitoPorcentaje::getRemitosLiquidadosByRepartidor($model->repartidor_idrepartidor, $model->fecha_inicio, $model->fecha_fin);
        $contador = 1;
        $actual = 0;
        $monto = 0;
        $porcentaje_entregas = 0;

        foreach($remitos as $r) {

            if($r->fecha != $remitos[$actual -1]->fecha  && $r->fecha != $remitos[$actual + 1]->fecha ){
                $r->porcentaje_entregas = $porcentaje_entregas = (($r->cant_entregados * 100) / $r->cant_pedidos);
                $r->reparto_seleccionado = PagosPorcentajesRepartidor::getPagoByPorcentaje($porcentaje_entregas/($contador))->monto;
                $r->se_paga = 1;
            }
            if($r->fecha != $remitos[$actual - 1]->fecha  && $r->fecha == $remitos[$actual + $contador]->fecha ) {
                while($r->fecha != $remitos[$actual - 1]->fecha  && $r->fecha == $remitos[$actual + $contador - 1]->fecha ){
                    $porcentaje_entregas += (($remitos[$actual + $contador - 1]->cant_entregados * 100) / $remitos[$actual + $contador - 1]->cant_pedidos);
                    $contador++;
                }
                $r->porcentaje_entregas = ($porcentaje_entregas / --$contador);
                $r->reparto_seleccionado = PagosPorcentajesRepartidor::getPagoByPorcentaje($r->porcentaje_entregas)->monto;
                $r->se_paga = 1;
                $contador = 1;
            }
            if($r->fecha == $remitos[$actual - 1]->fecha){
                $r->porcentaje_entregas = $remitos[$actual - 1]->porcentaje_entregas;
                $r->reparto_seleccionado = $remitos[$actual - 1]->reparto_seleccionado;
                $r->se_paga = 0;
            }
            $r->save();
            $porcentaje_entregas=0;
            $actual++;
        }

        
        /*return $this->render('view', [
            'model' => $this->findModel($id),
        ]);*/
    }
}
