<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LpcHasRemitoporcentaje */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="lpc-has-remitoporcentaje-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'rp_idrp')->textInput() ?>

    <?= $form->field($model, 'lpc_idlpc')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
