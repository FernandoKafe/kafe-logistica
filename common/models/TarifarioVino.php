<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tarifario_vino}}".
 *
 * @property int $idtarifario_vino
 * @property double $precio_caja
 * @property double $precio_repartidor
 * @property string $nombre
 *
 * @property Cliente[] $clientes
 */
class TarifarioVino extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tarifario_vino}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['precio_caja', 'precio_repartidor', 'nombre'], 'required'],
            [['precio_caja', 'precio_repartidor'], 'number'],
            [['nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idtarifario_vino' => Yii::t('app', 'Idtarifario Vino'),
            'nombre' => Yii::t('app', 'Nombre'),
            'precio_caja' => Yii::t('app', 'Precio x Caja'),
            'precio_repartidor' => Yii::t('app', ' Costo reparto'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientes()
    {
        return $this->hasMany(Cliente::className(), ['tarifariovino_idtarifariovino' => 'idtarifario_vino']);
    }
}
