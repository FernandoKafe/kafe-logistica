<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LpcHasRemitoporcentaje */

$this->title = Yii::t('app', 'Update Lpc Has Remitoporcentaje: ' . $model->idlpc_has_remitoporcentaje, [
    'nameAttribute' => '' . $model->idlpc_has_remitoporcentaje,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lpc Has Remitoporcentajes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idlpc_has_remitoporcentaje, 'url' => ['view', 'id' => $model->idlpc_has_remitoporcentaje]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="lpc-has-remitoporcentaje-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
