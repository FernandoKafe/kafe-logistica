<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RemitoVino;

/**
 * RemitoVinoSearch represents the model behind the search form of `common\models\RemitoVino`.
 */
class RemitoVinoSearch extends RemitoVino
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idremito_vino', 'cant_cajas', 'numero_remito', 'cant_botellas', 'sucursal_idsucursal'], 'integer'],
            [['valor_declarado', 'total_facturacion', 'guia_total', 'valor_caja', 'precio_reparto', 'precio'], 'number'],
            [['guia_numero', 'guia_tipo', 'guia_facturada_a', 'creado'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RemitoVino::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idremito_vino' => $this->idremito_vino,
            'cant_cajas' => $this->cant_cajas,
            'valor_declarado' => $this->valor_declarado,
            'numero_remito' => $this->numero_remito,
            'cant_botellas' => $this->cant_botellas,
            'sucursal_idsucursal' => $this->sucursal_idsucursal,
            'guia_idtransporte' => $this->guia_idtransporte,
            'guia_total' => $this->guia_total,
            'total_facturacion' => $this->total_facturacion,
            'creado' => $this->creado,
            'valor_caja' => $this->valor_caja, 
        ]);

        $query->andFilterWhere(['like', 'guia_numero', $this->guia_numero])
		->andFilterWhere(['like', 'guia_tipo', $this->guia_tipo])
		->andFilterWhere(['like', 'guia_facturada_a', $this->guia_facturada_a]);
        return $dataProvider;
    }
}
