<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

use \common\models\OrdenRetiroGrupal;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\OrdenRetiroGrupalSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Ordenes de Retiro Grupal');
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><i class="fa fa-copy"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2>Listado</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create Orden Retiro Grupal'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel, 
        'moduleId' => 'gridviewk',
        'toolbar' =>  [
            '{export}',
            '{toggleData}',
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
        ],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'idordenderetirogrupal',

            [
                'attribute' => 'idordenderetirogrupal',
                'label' => 'ID',
            ],
            [
                'attribute' => 'numero',
            ],
            'fecha',
            [
                'attribute' => 'cliente_idcliente',
                "label" => 'Cliente',
                'format' => 'text',
                'content' => function($data){
                    return $data->clienteIdcliente->razonsocial;
                }
            ],
            [
                'attribute' => 'repartidor_idrepartidor',
                "label" => 'Repartidor',
                'format' => 'text',
                'content' => function($data){
                    return $data->repartidorIdrepartidor->nombre." ".$data->repartidorIdrepartidor->apellido;
                }
            ],
            [
                'attribute' => 'liquidadoCliente',
                'label' => 'Liquidado cliente',
                'value' => function($data){
                    if($data::getEstadoLiquidacionClienteByOrdenId($data->idordenderetirogrupal))
                        return "Si";
                    else
                        return "No";
                },
                'filter' => ['0' => "No", '1' => "Sí"],
            ],
            [
                'attribute' => 'liquidadoRepartidor',
                'label' => 'Liquidado repartidor',
                'value' => function($data){
                    if($data::getEstadoLiquidacionRepartidorByOrdenId($data->idordenderetirogrupal))
                        return "Si";
                    else 
                        return "No";
                },
                'filter' => ['0' => "No", '1' => "Sí"],
            ],
            [
                'attribute' => 'creado',
                'label' => 'Fecha y Hora creación',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Acciones',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => '{view}{update}{delete}',
                'buttons' => [
                  'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'Ver'),
                        ]);
                  },

                  'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'Actualizar'),
                        ]);
                    },

                    'delete' => function ($url, $model) {
                          return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                      'title' => Yii::t('app', 'Eliminar'),
                                      'data' => [
                                        'confirm' => Yii::t('app', 'Esta seguro de borrar esta orden? Se borraran todos los remitos asociados!'),
                                        'method' => 'post',
                                    ],
                          ]);
                      },

                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='view?repartidor_idrepartidor='.$model->repartidor_idrepartidor
                        .'&fecha='.$model->fecha
                        .'&id='.$model->idordenderetirogrupal
                        .'&ordenderetirogrupal_idordenderetirogrupal='.$model->idordenderetirogrupal;
                        return $url;
                    }
                    if ($action === 'update') {
                        $url ='update?repartidor_idrepartidor='.$model->repartidor_idrepartidor
                        .'&fecha='.$model->fecha
                        .'&id='.$model->idordenderetirogrupal
                        .'&ordenderetirogrupal_idordenderetirogrupal='.$model->idordenderetirogrupal;
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url ='delete?id='.$model->idordenderetirogrupal;
                        return $url;
                    }
                }
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div> 
</div>
