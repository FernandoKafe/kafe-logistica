<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zona".
 *
 * @property int $idzona
 * @property string $nombre
 *
 * @property TarifarioHasTarifa[] $tarifarioHasTarifas
 * @property ZonaHasLocalidad[] $zonaHasLocalidads
 * @property Localidad[] $localidadIdlocalidads
 */
class Zona extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zona';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idzona' => Yii::t('app', 'Idzona'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarifarioHasTarifas()
    {
        return $this->hasMany(TarifarioHasTarifa::className(), ['zona_idzona' => 'idzona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZonaHasLocalidads()
    {
        return $this->hasMany(ZonaHasLocalidad::className(), ['zona_idzona' => 'idzona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalidadIdlocalidads()
    {
        return $this->hasMany(Localidad::className(), ['idlocalidad' => 'localidad_idlocalidad'])->viaTable('zona_has_localidad', ['zona_idzona' => 'idzona']);
    }
}
