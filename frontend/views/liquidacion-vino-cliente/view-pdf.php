<?php

use yii\data\ActiveDataProvider;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use yii\helpers\Html;

use common\models\LiquidacionVinoCliente;
use common\models\RemitoVino;
use common\models\OrdenGrupalVinos;
use common\models\TarifarioVino;
use common\models\Cliente;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionVinoCliente */
$idLiquidacion = Yii::$app->getRequest()->getQueryParam('id');
$clienteLiquidado = $model->cliente_idcliente;

$this->title = "Liquidación vinos: ".$model->nombre." - ".$model->clienteIdcliente->razonsocial;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion Vino Clientes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="liquidacion-view row" style="overflow: hidden;">
    <div class="col-xs-3" style="float: left;">
        <h3>Liquidación: <?= $model->clienteIdcliente->razonsocial?></h3>
        <h3>Periodo: <?= $model->nombre?></h3>
        <h3>CUIT: <?=$model->clienteIdcliente->cuit?></h3>
    </div>
    <div class="col-xs-4" style=" margin: 0 auto 0;">
        <?php echo Html::img('@web/img/logo/LOGO.jpg', ['class' => 'img-responsive', 'width'=>'210', 'style' => 'margin: 0 0;']) ?>
    </div>
    <div class="col-xs-3" style="border: 1px solid #d3d3d3; float:right; margin: 0 15px 0 0;">
            <div class="col-xs-2" style="margin: 20px 0 0 0;">
                <p>Firma: </p>
            </div>
            <div class="col-xs-6" style="margin: 0 0 0 0;">
                <p>____________________</p>
            </div>
            <div class="col-xs-2">
                <p>Aclaración</p>
            </div>     
            <div class="col-xs-6">
                <p>____________________</p>
            </div>
            <div class="col-xs-2">
                <p>Fecha</p>
            </div>
            <div class="col-xs-6">
                <p>_____ / _____ / _____</p>
            </div>
            <div class="col-xs-12" style="margin: 35px 0 0 0;">
                    <?= "<div class='col-md-12 tabla-totales'>TOTAL: ".number_format($model->total_cobrar, 2 , "." ,  "," )."</div>"; ?>
            </div>
        </div>    
</div>
<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_content">
    <?php
    $remitoVino = RemitoVino::find()->joinWith(['ordengrupalvinosIdordengrupalvinos'])->where(['liquidacionvinocliente_idliquidacionvinocliente' => $idLiquidacion]);
        $dataProvider = new ActiveDataProvider([
            'query' => $remitoVino,
            'pagination' => false,
        ]);
        echo GridView::widget([
        'dataProvider' => $dataProvider,
        //'showFooter' => true,
        'moduleId' => 'gridviewk',
        'showPageSummary' => true,
        'columns' => [
            [
                'label' => 'Fecha',
                'value' => function($data){
                    return $data->ordengrupalvinosIdordengrupalvinos->fecha;
                }
            ],
            [
                'label' => 'Nº Planilla',
                'value' => function($data){
                    return $data->numero_remito;
                }
            ],
            'guia_numero',
            'guia_tipo',
            'guia_facturada_a',
            [
                'label' => 'Destinatario',
                'value' => function($data){
                    return $data->sucursalIdsucursal->empresaIdempresa->nombre;
                }
            ],
            [
                'label' => 'Destino',
                'value' => function($data){
                    return  $data->sucursalIdsucursal->localidadIdlocalidad->nombre.", ".$data->sucursalIdsucursal->localidadIdlocalidad->departamentoIddepartamento->nombre.", ".$data->sucursalIdsucursal->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                },
                'footer' => '<strong>TOTAL</strong>',
                'pageSummary' => true,
                'pageSummary' => '<strong>TOTAL</strong>',
            ],
            [
                'attribute' => 'cant_cajas',
                'footer' => $model->cajas,
                'pageSummary' => true,
                'pageSummaryFunc' => GridView::F_SUM,
            ],
            [
                'label' => 'Valor Caja',
                'value' => function($data){
                    return ($data->valor_caja);
                },
                'format' => ['decimal',2],
                'footer' => number_format($model->valor_cajas, 2 , "." ,  "," ),
                'pageSummary' => true,
                'pageSummaryFunc' => GridView::F_SUM,
            ],
            [
                'label' => 'Seguro',
                
                'format' => ['decimal',2],
                'value' => function($data){
                    return (($data->valor_declarado / 2) * 0.01);
                },
                'footer' => number_format($model->total_seguro, 2 , "." ,  "," ),
                'pageSummary' => true,
                'pageSummaryFunc' => GridView::F_SUM,
            ],
            [
                'attribute' => 'guia_total',
                'pageSummary' => true,
                'pageSummaryFunc' => GridView::F_SUM,
            ]
        ],
        ]);
    ?>


<?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-striped table-bordered detail-view tabla-totales' ],
        'attributes' => [
            //'idliquidacion_vino_cliente',
            [
                'attribute' => 'total',
                'label' => 'BULTOS + SEGURO',
                'format' => ['decimal',2],
            ],
            [
                'attribute' => 'total_guias',
                'label' => 'GUÍAS A PAGAR',
                'format' => ['decimal',2],
            ],
            [
                'attribute' => 'total_cobrar',
                'label' => 'TOTAL',
                'format' => ['decimal',2],
            ],
        ],
    ]) ?>
    </div>
</div>