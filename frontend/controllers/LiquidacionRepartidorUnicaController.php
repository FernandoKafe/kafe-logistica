<?php

namespace frontend\controllers;

use Yii;
use frontend\models\LiquidacionRepartidorUnicaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\models\LiquidacionRepartidorUnica;
use common\models\LiquidacionRepartidor;
use common\models\LiquidacionDomicilioRepartidor;
use common\models\LiquidacionPorcentajeRepartidor;
use common\models\LiquidacionVinoRepartidor;

/**
 * LiquidacionRepartidorUnicaController implements the CRUD actions for LiquidacionRepartidorUnica model.
 */
class LiquidacionRepartidorUnicaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LiquidacionRepartidorUnica models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LiquidacionRepartidorUnicaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LiquidacionRepartidorUnica model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LiquidacionRepartidorUnica model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LiquidacionRepartidorUnica();

        if ($model->load(Yii::$app->request->post())) {
            $monthOne = date('m', strtotime($model->fecha_inicio));
            $monthOne = intval($monthOne);
            $monthTwo = date('m', strtotime($model->fecha_fin));
            $monthTwo = intval($monthTwo);
            $yearOne = date('Y', strtotime($model->fecha_inicio));
            $yearOne = intval($yearOne);
            $yearTwo = date('Y', strtotime($model->fecha_inicio));
            $yearTwo = intval($yearTwo);

            if($monthTwo != $monthOne || $yearOne != $yearTwo){
                Yii::$app->session->setflash(
                    'error',
                    "La feha de inicio y fin deber corresponder al mismo mes y año.");
                    return $this->render('create', [
                        'model' => $model,
                    ]);
            }
            
            $model->save();
            $liqComun = new LiquidacionRepartidor();
            $liqDomicilio = new LiquidacionDomicilioRepartidor();
            $liqPorcentaje = new LiquidacionPorcentajeRepartidor();
            $liqVino = new LiquidacionVinoRepartidor();

            //Set liquidación común
            $liqComun->periodo_inicio = $model->fecha_inicio;
            $liqComun->periodo_fin = $model->fecha_fin;
            $liqComun->periodo = $model->nombre;
            $liqComun->repartidor_idrepartidor = $model->repartidor_idrepartidor;

            //Set liquidación porcentaje
            /*$liqPorcentaje->fecha_inicio = $model->fecha_inicio;
            $liqPorcentaje->fecha_fin = $model->fecha_fin;
            $liqPorcentaje->periodo = $model->nombre;
            $liqPorcentaje->repartidor_idrepartidor = $model->repartidor_idrepartidor;
            $liqPorcentaje->estadoliquidacion_idestadoliquidacion = 1;*/

            //Set liquidación vino
            $liqVino->periodo_inicio = $model->fecha_inicio;
            $liqVino->periodo_fin = $model->fecha_fin;
            $liqVino->nombre = $model->nombre;
            $liqVino->repartidor_idrepartidor = $model->repartidor_idrepartidor;

            //Set liquidación domicilio
            $liqDomicilio->fecha_inicio = $model->fecha_inicio;
            $liqDomicilio->fecha_fin = $model->fecha_fin;
            $liqDomicilio->nombre = $model->nombre;
            $liqDomicilio->repartidor_idrepartidor = $model->repartidor_idrepartidor; 


            $objLiqComun = LiquidacionRepartidor::createLiquidacion($liqComun, $model->idliquidacion_unica);
            $objLiqDomicilio = LiquidacionDomicilioRepartidor::createLiquidacion($liqDomicilio, $model->idliquidacion_unica);
            //$objLiqPorcentaje = LiquidacionPorcentajeRepartidor::createLiquidacion($liqPorcentaje, $model->idliquidacion_unica);
            $objLiqVino = LiquidacionVinoRepartidor::createLiquidacion($liqVino, $model->idliquidacion_unica);

            if (is_array($objLiqComun)|| is_array($objLiqDomicilio) || is_array($objLiqVino) ) {
                (is_array($objLiqComun) || is_bool($objLiqComun))? false : LiquidacionRepartidor::deleteLiquidacion($model);
                (is_array($objLiqDomicilio) || is_bool($objLiqDomicilio)) ? false : LiquidacionDomicilioRepartidor::deleteLiquidacion($model);
                (is_array($objLiqVino) || is_bool($objLiqVino)) ? false : LiquidacionVinoRepartidor::deleteLiquidacion($model);;

                $model->delete();
                is_array($objLiqComun) ? Yii::$app->session->setflash( 'error', "Conflicto remito común ID:".$objLiqComun[1].". ".$objLiqComun[2]) : false;
                is_array($objLiqDomicilio) ? Yii::$app->session->setflash( 'error', "Conflicto orden grupal domicilio ID:".$objLiqDomicilio[1].". ".$objLiqDomicilio[2]) : false;
                is_array($objLiqVino) ? Yii::$app->session->setflash( 'error', "Conflicto orden grupal vino ID:".$objLiqVino[1].". ".$objLiqVino[2]) : false;
                
                return $this->render('create', [
                        'model' => $model,
                ]);
            }

            if($objLiqComun || $objLiqDomicilio || /*$objLiqPorcentaje ||*/ $objLiqVino){
                $model->total = 0;
                if($objLiqComun){
                    $model->total += $objLiqComun->total;
                }
                if ($objLiqDomicilio){ 
                    $model->total += $objLiqDomicilio->total;
                }
                /*if($objLiqPorcentaje){
                    $model->total += $objLiqPorcentaje->total;
                }*/
                if($objLiqVino){
                    $model->total += $objLiqVino->total;
                }
                $model->creado = date('Y-m-d G:i', time());
                $model->save(); 
            }else{
                $model->delete();
                Yii::$app->session->setflash(
                    'error',
                    "No se puede generar la liquidación. El repartidor no tiene repartos asociados para la fecha.");
                    return $this->render('create', [
                        'model' => $model,
                    ]);
            }
            Yii::$app->session->setflash('success', "Liquidación repartidor creada con éxito.");
            return $this->redirect(['view', 'id' => $model->idliquidacion_unica]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LiquidacionRepartidorUnica model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) 
        {
            return $this->redirect(
                [
                    'view', 
                    'id' => $model->idliquidacion_unica
                ]
            );
        }

        return $this->render(
            'update', [
                'model' => $model,
            ]
        );
    }

    /**
     * Deletes an existing LiquidacionRepartidorUnica model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        LiquidacionDomicilioRepartidor::deleteLiquidacion($model);
        LiquidacionPorcentajeRepartidor::deleteLiquidacion($model);
        LiquidacionRepartidor::deleteLiquidacion($model);
        LiquidacionVinoRepartidor::deleteLiquidacion($model);

        $model->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the LiquidacionRepartidorUnica model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LiquidacionRepartidorUnica the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LiquidacionRepartidorUnica::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    } 
    
    public function actionReport($id) {

        $model = $this->findModel($id);
        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('view-pdf', ['model' => $model]);

        // setup kartik\mpdf\Pdf component
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;

        // return the pdf output as per the destination setting
        return $pdf->render();
    }
}
