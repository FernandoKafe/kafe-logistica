<?php

use yii\data\ActiveDataProvider;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use yii\helpers\Html;

use common\models\RemitoPorcentaje;
use common\models\LiquidacionPorcentajeCliente;


/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionPorcentajeCliente */

$idLiquidacion = Yii::$app->getRequest()->getQueryParam('id');


$this->title = "Liquidación %: ".$model->clienteIdcliente->razonsocial.", ".$model->periodo;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion % Clientes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-porcentaje-cliente-view">
    <h1>Liquidación: <?= $model->clienteIdcliente->razonsocial?></h1>
    <h1>Periodo: <?= $model->periodo?></h1>
    <h1>CUIT: <?=$model->clienteIdcliente->cuit?></h1>
    <div class="x_panel"> 
        <p>
            <?
                if(!$model->confirmada){
                    echo Html::a(Yii::t('app', 'Eliminar'), ['delete', 'id' => $model->idliquidacion_porcentaje_cliente], ['class' => 'btn btn-danger',  'data-method'=>'post']);
                    echo Html::a(Yii::t('app', 'Recalcular'), ['recalculate', 'id' => $model->idliquidacion_porcentaje_cliente], ['class' => 'btn btn-warning']);
                    echo Html::a(Yii::t('app', 'Confirmar'), ['liquidate', 'id' => $model->idliquidacion_porcentaje_cliente], ['class' => 'btn btn-success']);
                }else{
                    echo Html::a(Yii::t('app', 'Inicio'), ['index', 'id' => $model->idliquidacion_porcentaje_cliente], ['class' => 'btn btn-primary',  'data-method'=>'post']);
                    echo Html::a(Yii::t('app', 'Eliminar'), ['delete', 'id' => $model->idliquidacion_porcentaje_cliente], ['class' => 'btn btn-danger',  'data-method'=>'post']);
                    echo Html::a(Yii::t('app', 'PDF'), ['liquidate', 'id' => $model->idliquidacion_porcentaje_cliente], ['class' => 'btn btn-success']);
                }
            ?>
            <p>
                <h3><strong style="color: red;">IMPORTANTE:</strong> La previa debe recalcularse luego de haberla generado si se modifica o elimina un remito de la misma.</h3>
            </p>
        </p>
    </div>
</div>

<!--Remitos Asociados--> 
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Remitos liquidados');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php
    $remito = RemitoPorcentaje::find()
    ->joinWith('lpcHasRemitoporcentajes')
    ->where(['lpc_idlpc' => $idLiquidacion]);

    if(!isset($model->pedidos) || !isset($model->total_faltante) || !isset($model->total_facturado) || !isset($model->total_devuelto)){
        if(!isset($model->pedidos))
            $model->pedidos = LiquidacionPorcentajeCliente::getSumPedidos($remito->all());
        if(!isset($model->total_faltante))
            $model->total_faltante = LiquidacionPorcentajeCliente::getSumFaltante($remito->all());
        if(!isset($model->total_facturado))
            $model->total_facturado = LiquidacionPorcentajeCliente::getSumDineroFacturado($remito->all()); 
        if(!isset($model->total_devuelto))
            $model->total_devuelto = LiquidacionPorcentajeCliente::getSumDineroDevuelto($remito->all());

        $model->save(false);
    }

    $dataProvider = new ActiveDataProvider([
        'query' => $remito,
    ]);
    $dataProvider->setSort([
        'attributes' => [
            'fecha' => [
                'asc' => ['fecha' => SORT_ASC],
                'desc' => ['fecha' => SORT_DESC],
            ],
            'numero_planilla' => [
                'asc' => ['numero_planilla' => SORT_ASC],
                'desc' => ['numero_planilla' => SORT_DESC],
            ],
        ],
        'defaultOrder' => [
            'fecha' => SORT_ASC,
            'numero_planilla' => SORT_ASC,
        ]
    ]);
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    'moduleId' => 'gridviewk',
    'showFooter' => true,
    'toolbar' =>  [
        '{toggleData}',
    ],
    'showFooter' => true,
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
    ],
    'columns' => [
        [
            'attribute' => 'fecha',
            'label' => 'Fecha',
        ],
        [
            'attribute' => 'porcentaje',
            'value' => function($data){
                if($data->porcentaje != ''){
                    return $data->porcentaje." %";
                }else{
                    return $data->porcentajeIdporcentaje->cantidad." %";
                }
            }
        ],
        [
            'attribute' => 'numero_planilla',
            'label' => 'Nº Planilla',
            'value' => function($data){
                return $data->numero_planilla;
            },
            'pageSummary' => 'SUBTOTAL',
            'footer' => '<strong>TOTAL</strong>',
        ],
        [
            'attribute' => 'cant_pedidos',
            'footer' => $model->pedidos,
        ],
        [
            'attribute' => 'dinero_facturado',
            'format' => ['decimal',2],
            'footer' => number_format($model->total_facturado, 2 , "," ,  "."  )
        ],
        [
            'attribute' => 'dinero_devuelto',
            'format' => ['decimal',2],
            'footer' => number_format($model->total_devuelto, 2 , "," ,  "."  ),
        ],
        [
            'attribute' => 'dinero_rendido',
            'format' => ['decimal',2],
            'footer' => number_format($model->total_rendido, 2 , "," ,  "."  ),
        ],
        [
            'attribute' => 'dinero_faltante',
            'format' => ['decimal',2],
            'footer' => number_format($model->total_faltante, 2 , "," ,  "."  ),
        ],
        [
            'attribute' => 'combustible',
            'format' => ['decimal',2],
            'footer' => number_format($model->combustible, 2 , "," ,  "."  ),
        ],
    ],
    ]); ?>

    <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'label' => 'TOTAL FACTURADO',
                    'attribute' => "total_facturado",
                    'format' => ['decimal',2],
                ],
                [
                    'label' => 'TOTAL DEVUELTO',
                    'attribute' => 'total_devuelto',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'total_rendido',
                    'label' => 'TOTAL RENDIDO',
                    'value' => function($data){
                        return number_format($data->total_rendido, 2 , "," ,  "." )
                        ." (Al 3.5% = ".number_format($data->total_rendido_al_3, 2 , "," ,  "."  )
                        ." - Al 5.5% = ".number_format($data->total_rendido_al_5, 2 , "," ,  "."  )
                        .")";
                    }
                ],
                [
                    'attribute' => 'neto_sin_iva',
                    'label' => 'NETO SIN IVA',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'combustible',
                    'label' => 'COMBUSTIBLE',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'comision',
                    'label' => 'COMISIÓN',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'pallets',
                    'label' => 'TOTAL Pallets',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'subtotal',
                    'label' => 'SUBTOTAL',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'iva',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'total_real',
                    'label' => 'TOTAL REAL',
                    'format' => ['decimal',2],
                ],
                [
                    'label' => 'TOTAL FALTANTE',
                    'attribute' => 'total_faltante',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'total_cobrar',
                    'label' => 'TOTAL COBRAR',
                    'format' => ['decimal',2],
                ],
            ]
        ]) ?>

    </div>
</div>
