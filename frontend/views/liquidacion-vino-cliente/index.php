<?php

use yii\helpers\Html;
use yii\grid\GridView;
use \yii\helpers\ArrayHelper;
use common\models\EstadoLiquidacion;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\LiquidacionVinoClienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Liquidacion Vino Clientes');
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><i class="fa fa-file"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2>Listado</h2>
        <div class="clearfix"></div> 
    </div>
    <div class="x_content">
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Previa Liquidación Vino Cliente'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'idliquidacion_vino_cliente',
            'nombre',
            'periodo_inicio',
            'periodo_fin',
            [
                'attribute'=>'estadoliquidacion_idestadoliquidacion',
                'filter'=>ArrayHelper::map(EstadoLiquidacion::find()->asArray()->all(), 'idestado_liquidacion', 'nombre'),
                'content' => function($data){
                    return $data->estadoliquidacionIdestadoliquidacion->nombre;
                }
            ],
            [
                'attribute' =>'total_cobrar',
                'format' => ['decimal',2],
            ],
            //'estadoliquidacion_idestadoliquidacion',
            //'liquidacionformapago_idliquidacionformapago',
            [
                'attribute' => 'creado',
                'label' => 'Fecha y Hora creación',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
    </div>
