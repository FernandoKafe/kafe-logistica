<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;

use common\models\Empresa;
use \common\models\Provincia;
use \common\models\Departamento;
use \common\models\Localidad;


$idSucursal = Yii::$app->getRequest()->getQueryParam('id');
if(isset($idSucursal)){
    $new = false;
  }else{
    $localidad = new Localidad();
    $new = true; 
  }
$empresa = ArrayHelper::map(Empresa::find()->all(),'idempresa','nombre');
$dataProvincia=ArrayHelper::map(Provincia::find()->orderBy('nombre')->asArray()->all(), 'idprovincia', 'nombre');
$idEmpresa = Yii::$app->getRequest()->getQueryParam('idempresa');
$localidad = new Localidad();

/* @var $this yii\web\View */
/* @var $model common\models\Sucursal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="x_panel">
    <div class="x_content">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idsucursal')->textInput()->label(false)->hiddenInput() ?>

    <?php
    if(isset($idEmpresa)){
      $model->empresa_idempresa = $idEmpresa;
      echo $form->field($model, 'empresa_idempresa')->textInput()->label(false)->hiddenInput();
    } else {
      echo $form->field($model, 'empresa_idempresa')->dropDownList($empresa)->label('Empresa');
    }?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?php
    
            $dataDepartamentos = [];
            $departamento = new Departamento();
            $provincia = new Provincia();
            $dataLocalidades = new Localidad();

            if(!$new && isset($model->localidad_idlocalidad)){
              $provincia->idprovincia = $model->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->idprovincia;
              
              $dataDepartamentos =  ArrayHelper::map(Departamento::find()
                ->select(['iddepartamento as id','nombre as name'])
                ->where(['provincia_idprovincia' => $provincia->idprovincia])
                ->orderBy('nombre ASC')
                ->asArray()
                ->all(), 'id', 'name');
              $departamento->iddepartamento = $model->localidadIdlocalidad->departamentoIddepartamento->iddepartamento;
              
              $dataLocalidades = ArrayHelper::map(Localidad::find()
              ->select(['idlocalidad as id','nombre as name'])
              ->where(['departamento_iddepartamento' => $departamento->iddepartamento])
              ->orderBy('nombre ASC')
              ->asArray()
              ->all(), 'id', 'name');
            }

            echo $form->field($provincia, 'idprovincia')->dropDownList($dataProvincia,
              ['prompt'=>'-Seleccionar-',
                'id' => 'provinciaDestino'])->label('Provincia');

            echo  $form->field($departamento, 'iddepartamento')->widget(DepDrop::classname(), [
              //'data'=> $initLocalidad,
              'data' => $dataDepartamentos,
              'language' => 'es',
              'options' => ['id'=>'departamento'],
              'type' => DepDrop::TYPE_SELECT2,
                  'pluginOptions'=>[
                  'depends' => ['provinciaDestino'],
                  'placeholder' => '- Seleccionar Departamento -',
                  'url' => \yii\helpers\Url::to(['direccion/departamento-provincia']),
                  'loadingText' => 'Cargando Departamentos...'
                ],
            ])->label('<a href="../departamento/create" target="_blank"><i class="fa fa-plus green"></i></a> Departamento');
            
            echo $form->field($model, 'localidad_idlocalidad')->widget(DepDrop::classname(), [
              //'data'=> $initLocalidad,
              'data' => $dataLocalidades,
              'language' => 'es',
              'options' => ['id'=>'localidad'],
              'type' => DepDrop::TYPE_SELECT2,
                'pluginOptions'=>[
                'depends' => ['departamento'],
                'placeholder' => '- Seleccionar Localidad -',
                'url' => \yii\helpers\Url::to(['direccion/departamento-localidad']),
                'loadingText' => 'Cargando Localidades...'
              ],
            ])->label('<a href="../localidad/create" target="_blank"><i class="fa fa-plus green"></i></a> Localidad');
    ?> 

    <?= $form->field($model, 'direccion')->textInput(['maxlength' => true])->label('Dirección') ?>


    <div class="form-group col-md-12">
        <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);?>
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
