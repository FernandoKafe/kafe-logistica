<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LiquidacionrepartidorHasRemito;

/**
 * LiquidacionrepartidorHasRemitoSearch represents the model behind the search form of `common\models\LiquidacionrepartidorHasRemito`.
 */
class LiquidacionrepartidorHasRemitoSearch extends LiquidacionrepartidorHasRemito
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['liquidacionrepartidor_idliquidacionrepartidor', 'remito_idremito', 'total'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LiquidacionrepartidorHasRemito::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'liquidacionrepartidor_idliquidacionrepartidor' => $this->liquidacionrepartidor_idliquidacionrepartidor,
            'remito_idremito' => $this->remito_idremito,
            'total' => $this->total,
        ]);

        return $dataProvider;
    }
}
