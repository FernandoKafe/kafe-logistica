<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Remito;

/**
 * RemitoSearch represents the model behind the search form of `common\models\Remito`.
 */
class RemitoSearch extends Remito
{

    /**
     * busqueda por expediente
     */
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['ordenderetirogrupalIdordenderetirogrupal.cliente_idcliente']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        //Eliminar por los que no voy a filtrar
        return [
            [['idremito', 'numero_remito', 'cantidad_bultos', 'servicio_idservicio', 'flete', 'rol_sucursal', 'formadepago_idformadepago', 'producto_idproducto', 'estado_idestado', 'empresatransporte_idempresatransporte', 'ordenderetirogrupal_idordenderetirogrupal', 'liquidado_repartidor','liquidado', 'terminal', 'contra_pago'], 'integer'],
            [['carga_idcarga', 'cliente_idcliente', 'sucursal_idsucursal', 'remitofecha', 'numero_guia', 'tipo_remito', 'ordenderetirogrupalIdordenderetirogrupal.cliente_idcliente', 'repartidor_idrepartidor', 'creado'], 'safe'],
            [['peso', 'volumen', 'valor_declarado', 'seguro', 'total_contrarembolso', 'total_guia', 'flete_pagado', 'precio', 'precio_adicional', 'precio_reparto', 'precio_reparto_adicional', 'porcentaje_contra'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Remito::find();//->where(['or',['liquidado'=>0],['liquidado_repartidor'=>0]]);
        $query->joinWith(['sucursalIdsucursal as sucursal','sucursalIdsucursal.empresaIdempresa as empresa']);
        $query->joinWith(['ordenderetirogrupalIdordenderetirogrupal.clienteIdcliente as clienteOrden']);
        $query->joinWith(['ordenderetirogrupalIdordenderetirogrupal as ordenderetirogrupal']);
        $query->joinWith(['cargaIdcarga as carga']);
        $query->joinWith(['clienteIdcliente as clienteRemito']);
        $query->joinWith(['repartidorIdrepartidor as repartidor']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            //Eliminar por los que no voy a filtrar
            'idremito' => $this->idremito,
            'numero_remito' => $this->numero_remito,
            'remitofecha' => $this->remitofecha,
            'cantidad_bultos' => $this->cantidad_bultos,
            'peso' => $this->peso,
            'volumen' => $this->volumen,
            'valor_declarado' => $this->valor_declarado,
            'servicio_idservicio' => $this->servicio_idservicio,
            'flete' => $this->flete,
            'rol_sucursal' => $this->rol_sucursal,
            'facturado_a' => $this->facturado_a,
            'seguro' => $this->seguro,
            'total_contrarembolso' => $this->total_contrarembolso,
            'formadepago_idformadepago' => $this->formadepago_idformadepago,
            'producto_idproducto' => $this->producto_idproducto,
            'total_guia' => $this->total_guia,
            'flete_pagado' => $this->flete_pagado,
            'estado_idestado' => $this->estado_idestado,
            'empresatransporte_idempresatransporte' => $this->empresatransporte_idempresatransporte,
            'liquidado_repartidor' => $this->liquidado_repartidor,
            'liquidado' => $this->liquidado,
            'terminal' => $this->terminal,
            'contra_pago' => $this->contra_pago,
            'creado' => $this->creado,
            'precio' => $this->precio, 
            'precio_adicional' => $this->precio_adicional, 
            'precio_reparto' => $this->precio_reparto, 
            'precio_reparto_adicional' => $this->precio_reparto_adicional, 
            'porcentaje_contra' => $this->porcentaje_contra,
        ]);

        $query->andFilterWhere(['like', 'numero_guia', $this->numero_guia])
            ->andFilterWhere(['like', 'tipo_remito', $this->tipo_remito]);

        $query->andFilterWhere(['like', 'CONCAT(empresa.nombre, " - ", sucursal.direccion)', $this->sucursal_idsucursal]);
        $query->andFilterWhere(['like', 'clienteOrden.razonsocial', $this->getAttribute('ordenderetirogrupalIdordenderetirogrupal.cliente_idcliente')]);
        $query->andFilterWhere(['like', 'ordenderetirogrupal.numero', $this->ordenderetirogrupal_idordenderetirogrupal]);
        $query->andFilterWhere(['like', 'carga.nombre', $this->carga_idcarga]);
        $query->andFilterWhere(['like', 'clienteRemito.razonsocial', $this->cliente_idcliente]);
        $query->andFilterWhere(['like', 'CONCAT(repartidor.apellido, ", ", repartidor.nombre)', $this->repartidor_idrepartidor]);

        return $dataProvider; 
    }

}
