<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Rendicion */

$this->title = Yii::t('app', 'Update Rendicion: ' . $model->idrendicion, [
    'nameAttribute' => '' . $model->idrendicion,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rendicions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idrendicion, 'url' => ['view', 'id' => $model->idrendicion]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="rendicion-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
