<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-03-28
 * Time: 13:19
 */
$totales = [
'bultos' => 0,
'volumen' => 0,
'total' => 0,
'neto' => 0
];
?>
<tr>
    <td style='border: solid 1px; text-align: center; font-weight: bold;'>Mes</td>
    <td style='border: solid 1px; text-align: center; font-weight: bold;'>BULTOS</td>
    <td style='border: solid 1px; text-align: center; font-weight: bold;'>EXCEDENTES</td>
    <td style='border: solid 1px; text-align: center; font-weight: bold;'>TOTAL</td>
    <td style='border: solid 1px; text-align: center; font-weight: bold;'>$NETO</td>
</tr>
<?php foreach($post as $elemento){ ?>
    <tr>
        <td>
            <?php
            $mes = strftime("%B", strtotime(date('Y-').$elemento['fecha'].'-01'));
            echo strtoupper($mes);
            $totales['bultos'] += $elemento['bultos'];
            $totales['volumen'] += $elemento['volumen'];
            $totales['total'] += $elemento['volumen'] + $elemento['bultos'];
            $totales['neto'] += $elemento['neto'];
            ?>
        </td>
        <td>
            <?= $elemento['bultos']?>
        </td>
        <td>
            <?= $elemento['volumen']?>
        </td>
        <td>
            <?= $elemento['volumen'] + $elemento['bultos']?>
        </td>
        <td>
            $ <?= $elemento['neto']?>
        </td>
    </tr>
<?php } ?>
<tr>
    <td style='border: solid 1px; text-align: center'><strong>TOTAL</strong></td>
    <td style='border: solid 1px; text-align: center'><strong><?= $totales['bultos']?></strong></td>
    <td style='border: solid 1px; text-align: center'><strong><?= $totales['volumen']?></strong></td>
    <td style='border: solid 1px; text-align: center'><strong><?= $totales['total']?></strong></td>
    <td style='border: solid 1px; text-align: center'><strong><?= $totales['neto']?></strong></td>
</tr>
