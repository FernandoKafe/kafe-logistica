<?php
namespace frontend\controllers;
use Yii;
use common\models\Liquidacion;
use frontend\models\LiquidacionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Remito;
use common\models\OrdenRetiroGrupal;
use common\models\LiquidacionHasRemito;
use common\models\TarifarioHasPrecio;
use common\models\ZonaHasLocalidad;
use common\models\Cheque;

/**
 * LiquidacionController implements the CRUD actions for Liquidacion model.
 */
class LiquidacionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Liquidacion models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LiquidacionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Liquidacion model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Liquidacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Liquidacion();

        if ($model->load(Yii::$app->request->post())) {
            $remitos = Remito::getRemitosFechaCliente($model->cliente_idcliente, $model->periodo_inicio, $model->periodo_fin);
            $flag = 1;
            if(!$remitos){
                Yii::$app->session->setflash('error', "No se puede generar liquidación. No hay remitos para liquidar");
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            foreach ($remitos as $r) {
                if($r->terminal == 0){
                    if(isset($r->carga_idcarga) && isset($r->clienteIdcliente->tarifario_idtarifario) && isset($r->sucursalIdsucursal->localidadIdlocalidad->zonaHasLocalidad->zona_idzona)){
                        $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, $r->sucursalIdsucursal->localidadIdlocalidad->zonaHasLocalidad->zona_idzona);
                    }else{
                        if(!isset($r->carga_idcarga)){
                            Yii::$app->session->setflash('error', "Remito ID: ".$r->idremito." MAL CARGADO. El bulto seleccionado no esta cargado en el tarifario: ".$r->clienteIdcliente->tarifario_idtarifario);
                        }else if(!isset($r->clienteIdcliente->tarifario_idtarifario)){
                            Yii::$app->session->setflash('error', "Remito ID: ".$r->idremito." MAL CARGADO. El cliente: ".$r->clienteIdcliente->razonsocial." no tiene tarifariocomún asignado!");
                        }else{
                            Yii::$app->session->setflash('error', "Remito ID: ".$r->idremito." MAL CARGADO.La sucursal: ".$r->sucursalIdsucursal->nombre.", de la empresa ".$r->sucursalIdsucursal->empresaIdempresa->nombre." No tiene localidad asignada!");
                        }
                        return $this->render('create', [
                            'model' => $model,
                        ]);
                    }
                } else { 
                    $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, 5);
                }
                if(!$tarHasPrecio){
                    $clienteHuerfano = $r->clienteIdcliente->razonsocial;
                    $tarifarioHuerfano = $r->clienteIdcliente->tarifarioIdtarifario->nombre;
                    $cargaHuerfana = $r->cargaIdcarga->nombre;
                    $zonaHuerfana = $r->sucursalIdsucursal->localidadIdlocalidad->zonaHasLocalidad->zonaIdzona->nombre;
                    $flag = -1;
                    break;
                }
            }
            if($flag != -1  && $model->save()){
                $model->bultos = 0;
                $model->volumen = 0;
                $fechaAnterior = null;
                foreach ($remitos as $r) { 
                    if($r->terminal == 0){
                        $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, $r->sucursalIdsucursal->localidadIdlocalidad->zonaHasLocalidad->zona_idzona);
                    } else {
                        $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, 5);
                    }
                    $rel = new LiquidacionHasRemito();
                    $rel->liquidacion_idliquidacion = $model->idliquidacion;
                    $rel->remito_idremito = $r->idremito;
                    $model->bultos += $r->cantidad_bultos;
                    $model->volumen += $r->volumen;
                    if(!isset($r->precio) || !isset($r->precio_adicional) || !isset($r->precio_reparto) || !isset($r->precio_reparto_adicional) || !isset($r->porcentaje_contra))
                    {
                        if(!isset($r->precio))
                            $r->precio = $tarHasPrecio->precio;
                        if(!isset($r->precio_adicional) )
                            $r->precio_adicional = $tarHasPrecio->precio_adicional;
                        if(!isset($r->precio_reparto))
                            $r->precio_reparto = $tarHasPrecio->precio_reparto;
                        if(!isset($r->precio_reparto_adicional))
                            $r->precio_reparto_adicional = $tarHasPrecio->precio_reparto_adicional;
                        if(!isset($r->porcentaje_contra))
                            $r->porcentaje_contra = $tarHasPrecio->porcentaje_contra;
                        $r->save(false);
                    }
                    if($model->clienteIdcliente->tarifarioIdtarifario->tipo == "Tipo A"){
                        if($r->cantidad_bultos >= 1){
                            $model->importe_bultos += $tarHasPrecio->precio;
                        }else{
                            $model->importe_bultos += 0;
                        }
                        $model->importe_bultos += ($r->volumen * $tarHasPrecio->precio_adicional);
                        if($r->cantidad_bultos > 1){
                            $model->importe_bultos += (($r->cantidad_bultos - 1) * $tarHasPrecio->precio_adicional);
                        }
                    }else if($model->clienteIdcliente->tarifarioIdtarifario->tipo == "Tipo B"){
                        if($fechaAnterior == $r->remitofecha){
                            $model->importe_bultos += (($r->cantidad_bultos + $r->volumen) * $tarHasPrecio->precio_adicional);
                        }else{
                            if($r->cantidad_bultos == 0)
                                $model->importe_bultos += ($r->volumen * $tarHasPrecio->precio_adicional);
                            else
                                $model->importe_bultos += ($tarHasPrecio->precio + (($r->cantidad_bultos + $r->volumen - 1) * $tarHasPrecio->precio_adicional));
                        }
                        $fechaAnterior = $r->remitofecha;
                    }

                    if($r->servicio_idservicio == 2){
                        $model->total_contra += (($r->total_contrarembolso * $tarHasPrecio->porcentaje_contra) / 100);
                    }

                    if( ( ($r->tipo_remito == 'A') || ($r->tipo_remito == 'B') || ($r->tipo_remito == 'R') ) && ($r->facturado_a == 'Vertiente')){
                        $model->guias_logistica += ($r->total_guia);
                    }else if( ( ($r->tipo_remito == 'A') || ($r->tipo_remito == 'B') ) && ($r->facturado_a == 'Cliente')){
                        $model->importe_guias += $r->total_guia;
                    }
                    if($r->flete_pagado > 0){
                        $model->total_bultos += -$r->flete_pagado;
                    }
                    $rel->save(false);
                }
                $model->guias_logistica = $model->guias_logistica / 1.21;
                $model->subtotal = $model->importe_bultos + $model->guias_logistica + $model->total_contra;
                $model->total_iva = $model->subtotal * 0.21;
                $model->total_bultos = $model->subtotal + $model->total_iva;
                $model->total = $model->total_bultos + $model->importe_guias;
                $model->creado = date('Y-m-d G:i', time());
                if($model->estadoliquidacion_idestadoliquidacion != 1)
                    $model->fecha_pago = date('Y-m-d G:i', time());
                $model->save();
                Yii::$app->session->setflash('success', "Liquidación común creada con éxito.");
                return $this->redirect(['view', 'id' => $model->idliquidacion]);
            }
            else{
                Yii::$app->session->setflash('error', "No se puede generar liquidación. Revisar que el destino o carga del remito con ID:".$r->idremito);
                //info azul
                //warning amarrillo
                //success verde
                //error rojo

            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }
    /**
     * Creates a new Liquidacion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRecalculate($id)
    {
        $model = $this->findModel($id);
        $model->importe_bultos = 0;
        $model->guias_logistica = 0;
        $model->total_contra = 0;
        $model->subtotal = 0;
        $model->total_iva = 0;
        $model->total_bultos = 0;
        $model->total = 0;
        $model->bultos = 0;
        $model->volumen = 0;
        $model->importe_guias = 0;

        $rel = LiquidacionHasRemito::find()->where(['liquidacion_idliquidacion' => $model->idliquidacion])->all();
        foreach($rel as $l){
            $remito = Remito::find()->where(['idremito' => $l->remito_idremito])->one();
            if(isset($remito)){
                $remito->liquidado = 0;
                $remito->save();
            }
            $l->delete();
        }

            $remitos = Remito::getRemitosFechaCliente($model->cliente_idcliente, $model->periodo_inicio, $model->periodo_fin);
            $flag = 1;
            if(!$remitos){
                Yii::$app->session->setflash('error', "No se puede generar liquidación. No hay remitos para liquidar");
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            foreach ($remitos as $r) {
                if($r->terminal == 0){
                    if(isset($r->carga_idcarga) && isset($r->clienteIdcliente->tarifario_idtarifario) && isset($r->sucursalIdsucursal->localidadIdlocalidad->zonaHasLocalidad->zona_idzona)){
                        $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, $r->sucursalIdsucursal->localidadIdlocalidad->zonaHasLocalidad->zona_idzona);
                    }else{
                        if(!isset($r->carga_idcarga)){
                            Yii::$app->session->setflash('error', "Remito: ".$r->numero_remito." MAL CARGADO. El bulto seleccionado no esta cargado en el tarifario: ".$r->clienteIdcliente->tarifario_idtarifario);
                        }else if(!isset($r->clienteIdcliente->tarifario_idtarifario)){
                            Yii::$app->session->setflash('error', "Remito: ".$r->numero_remito." MAL CARGADO. El cliente: ".$r->clienteIdcliente->razonsocial." no tiene tarifariocomún asignado!");
                        }else{
                            Yii::$app->session->setflash('error', "Remito: ".$r->numero_remito." MAL CARGADO.La sucursal: ".$r->sucursalIdsucursal->nombre.", de la empresa ".$r->sucursalIdsucursal->empresaIdempresa->nombre." No tiene localidad asignada!");
                        }
                        return $this->render('create', [
                            'model' => $model,
                        ]);
                    }
                } else {
                    $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, 5);
                }
                if(!$tarHasPrecio){
                    $clienteHuerfano = $r->clienteIdcliente->razonsocial;
                    $tarifarioHuerfano = $r->clienteIdcliente->tarifarioIdtarifario->nombre;
                    $cargaHuerfana = $r->cargaIdcarga->nombre;
                    $zonaHuerfana = $r->sucursalIdsucursal->localidadIdlocalidad->zonaHasLocalidad->zonaIdzona->nombre;
                    $flag = -1;
                    break;
                }
            }
            if($flag != -1  && $model->save()){
                foreach ($remitos as $r) { 
                    if($r->terminal == 0){
                        $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, $r->sucursalIdsucursal->localidadIdlocalidad->zonaHasLocalidad->zona_idzona);
                    } else {
                        $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, 5);
                    }
                    $rel = new LiquidacionHasRemito();
                    $rel->liquidacion_idliquidacion = $model->idliquidacion;
                    $rel->remito_idremito = $r->idremito;
                    $model->bultos += $r->cantidad_bultos;
                    $model->volumen += $r->volumen;

                    if(!isset($r->precio) || !isset($r->precio_adicional) || !isset($r->precio_reparto) || !isset($r->precio_reparto_adicional) || !isset($r->porcentaje_contra))
                    {
                        if(!isset($r->precio))
                            $r->precio = $tarHasPrecio->precio;
                        if(!isset($r->precio_adicional) )
                            $r->precio_adicional = $tarHasPrecio->precio_adicional;
                        if(!isset($r->precio_reparto))
                            $r->precio_reparto = $tarHasPrecio->precio_reparto;
                        if(!isset($r->precio_reparto_adicional))
                            $r->precio_reparto_adicional = $tarHasPrecio->precio_reparto_adicional;
                        if(!isset($r->porcentaje_contra))
                            $r->porcentaje_contra = $tarHasPrecio->porcentaje_contra;
                        $r->save(false); 
                    }

                    if($model->clienteIdcliente->tarifarioIdtarifario->tipo == "Tipo A"){
                        if($r->cantidad_bultos >= 1){
                            $model->importe_bultos += $tarHasPrecio->precio;
                        }else{
                            $model->importe_bultos += 0;
                        }
                        $model->importe_bultos += ($r->volumen * $tarHasPrecio->precio_adicional);
                        if($r->cantidad_bultos > 1){
                            $model->importe_bultos += (($r->cantidad_bultos - 1) * $tarHasPrecio->precio_adicional);
                        }
                    }else if($model->clienteIdcliente->tarifarioIdtarifario->tipo == "Tipo B"){
                        if($fechaAnterior == $r->remitofecha){
                            //Si la fecha del remito actual es igual a la fecha del remito anterior todos los bultos se cobran como adicional
                            $model->importe_bultos += (($r->cantidad_bultos + $r->volumen) * $tarHasPrecio->precio_adicional);
                        }else{
                            //Si la fecha del remito actual es distinta a la del remito anterior el primer bulto se cobra normal y el resto como adicional
                            if($r->cantidad_bultos == 0)
                                $model->importe_bultos += ($r->volumen * $tarHasPrecio->precio_adicional);
                            else
                                $model->importe_bultos += ($tarHasPrecio->precio + (($r->cantidad_bultos + $r->volumen - 1) * $tarHasPrecio->precio_adicional));
                        }
                        $fechaAnterior = $r->remitofecha; 
                    }

                    if($r->servicio_idservicio == 2){ 
                        $model->total_contra += (($r->total_contrarembolso * $tarHasPrecio->porcentaje_contra) / 100);
                    }

                    if(( ($r->tipo_remito == 'A') || ($r->tipo_remito == 'B') || ($r->tipo_remito == 'R') ) && ($r->facturado_a == 'Vertiente')){
                        $model->guias_logistica += ($r->total_guia);
                    }else if( (($r->tipo_remito == 'A') || ($r->tipo_remito == 'B')) && ($r->facturado_a == 'Cliente') ){
                        $model->importe_guias += ($r->total_guia);
                    }
                    if($r->flete_pagado > 0){
                        $model->total_bultos += -$r->flete_pagado;
                    }
                    $rel->save(false);
                }
                $model->guias_logistica = $model->guias_logistica / 1.21;
                $model->subtotal = $model->importe_bultos + $model->guias_logistica + $model->total_contra;
                $model->total_iva = $model->subtotal * 0.21;
                $model->total_bultos = $model->subtotal + $model->total_iva;
                $model->total = $model->total_bultos + $model->importe_guias;
                $model->creado = date('Y-m-d G:i', time());
                $model->save();
                Yii::$app->session->setflash('success', "Liquidación común creada con éxito.");
                return $this->redirect(['view', 'id' => $model->idliquidacion]);
            }else{
                Yii::$app->session->setflash('error', "No se puede generar liquidación. Revisar el destino del remito con ID:".$r->idremito);
                //info azul
                //warning amarrillo
                //success verde
                //error rojo
            }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Liquidacion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $estado_liquidacion_anterior = $model->estadoliquidacion_idestadoliquidacion;
        
        if(!$model->chequeIdcheque){
            $cheque = new Cheque();
        }else{
            $cheque = $model->chequeIdcheque;
        }

        if ($model->load(Yii::$app->request->post())) {
            if($model->estadoliquidacion_idestadoliquidacion != $estado_liquidacion_anterior && $model->estadoliquidacion_idestadoliquidacion != 1){
                $model->fecha_pago = date('Y-m-d G:i', time());
                if($model->liquidacionformapago_idliquidacionformapago == 1){
                    Yii::$app->session->setflash('error', "Por favor seleccione una forma de pago.");
                    return $this->redirect(['update', 'id' => $model->idliquidacion]);
                }
            }
            if($cheque->load(Yii::$app->request->post()) && $cheque->save()){
                $model->cheque_idcheque = $cheque->idcheque;
            }
            if($cheque->estado == "Rechazado"){
                $model->estadoliquidacion_idestadoliquidacion = 1;
                $model->pago_parcial = 0;
                $model->liquidacionformapago_idliquidacionformapago = 1;
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->idliquidacion]);
        }

        return $this->render('update', [
            'model' => $model,
            'cheque' => $cheque,
        ]);
    }

    /**
     * Deletes an existing Liquidacion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $cheque = Cheque::find()->where(['idcheque' => $model->cheque_idcheque])->one();
        
        $rel = LiquidacionHasRemito::find()->where(['liquidacion_idliquidacion' => $model->idliquidacion])->all();
        foreach($rel as $l){
            $remito = Remito::find()->where(['idremito' => $l->remito_idremito])->one();
            if(isset($remito)){
                $remito->liquidado = 0;
                $remito->save();
            }
            $l->delete();
        }
        $model->delete();
        if(isset($cheque)){
            $cheque->delete();
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Liquidacion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Liquidacion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Liquidacion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionReport($id) {

        $model = $this->findModel($id);
        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('view-pdf', ['model' => $model]);

        // setup kartik\mpdf\Pdf component
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;
        

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    /**
     * Creates a new LiquidacionDomicilioCliente model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateRendicion()
    {
        $model = new Liquidacion();
        if($model->load(Yii::$app->request->post())) {
                return $this->render('rendicion', ['model' => $model]);
        }

        return $this->render('create-rendicion',[
            'model' => $model,
        ]);
    }

    public function actionReportRendicion($fechaRendicion, $clienteRendicion, $fechaFin) {

        $model = new Liquidacion();
        $model->periodo_inicio= $fechaRendicion;
        $model->periodo_fin =$fechaFin;
        $model->cliente_idcliente = $clienteRendicion;
        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('rendicion-pdf', ['model' => $model]);

        // setup kartik\mpdf\Pdf component
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    public function actionLiquidate($id){
        $model = $this->findModel($id);
        $model->confirmada = 1;
        $model->save();
        $remito = Remito::updateRemitosFechaCliente($model->cliente_idcliente, $model->periodo_inicio, $model->periodo_fin);
        return $this->redirect(['report', 'id' => $model->idliquidacion]);
    }

    /**
     * Creates a new LiquidacionDomicilioCliente model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateAnalisis($analisis, $analisisForm = null)
    {
        $model = new Liquidacion();
        if($model->load(Yii::$app->request->post())) {
            if($analisisForm == 1){
                $post = Remito::getSumatoriaRemitosPorClienteAnio($model->cliente_idcliente, $model->periodo_inicio);

                $postDomicilio = Yii::$app->db->createCommand(
                    'SELECT MONTH(orden_grupal_domicilio.fecha) fecha, sum(cantidad) cantidad, sum(monto) monto  FROM guia_domicilio
                    JOIN orden_grupal_domicilio ON guia_domicilio.idorden_grupal_domicilio = orden_grupal_domicilio.idorden_grupal_domicilio
                    WHERE  orden_grupal_domicilio.borrado = 0 AND YEAR(orden_grupal_domicilio.fecha)=:fecha AND orden_grupal_domicilio.cliente_idcliente=:cliente
                    GROUP BY MONTH(orden_grupal_domicilio.fecha)')
                ->bindValue(':cliente', $model->cliente_idcliente)
                ->bindValue(':fecha', $model->periodo_inicio)
                ->queryAll();

                $postPorcentaje = Yii::$app->db->createCommand(
                    'SELECT MONTH(fecha) fecha, sum(cant_pedidos) pedidos, SUM(cant_entregados) entregados FROM remito_porcentaje
                    WHERE YEAR(fecha)=:fecha AND cliente_idcliente=:cliente
                    GROUP BY MONTH(fecha)')
                ->bindValue(':cliente', $model->cliente_idcliente)
                ->bindValue(':fecha', $model->periodo_inicio)
                ->queryAll();

                $postVino = Yii::$app->db->createCommand(
                    'SELECT month(orden_grupal_vinos.fecha) fecha, sum(remito_vino.cant_cajas) cant_cajas
                    FROM kafekafe_vertiente.remito_vino
                    INNER JOIN orden_grupal_vinos on idorden_grupal_vinos = remito_vino.ordengrupalvinos_idordengrupalvinos
                    WHERE YEAR(orden_grupal_vinos.fecha)=:fecha AND orden_grupal_vinos.cliente_idcliente=:cliente GROUP BY month(fecha)')
                ->bindValue(':cliente', $model->cliente_idcliente)
                ->bindValue(':fecha', $model->periodo_inicio)
                ->queryAll();

                if(!$post && !$postPorcentaje && !$postDomicilio && !$postVino){
                    Yii::$app->session->setflash(
                        'error',
                        "No existen remitos para el año especificado.");
                        $model = new Liquidacion();
                        return $this->render('create-analisis', [
                            'model' => $model,
                        ]);
                }
                return $this->renderPartial('view-analisis', ['post' => $post, 'postDomicilio' => $postDomicilio, 'postPorcentaje' => $postPorcentaje, 'postVino' => $postVino, 'model' => $model]);
            }else if($analisisForm == 3){
                $post = Yii::$app->db->createCommand(
                'SELECT 	
                    month(remitofecha) fecha,
                    sum(r.cantidad_bultos) cantidad_bultos,
                    sum(ifnull(r.volumen, 0)) volumen,
                    sum(ifnull(r.volumen, 0) + r.cantidad_bultos) total,
                    z.nombre
                FROM remito r
                INNER JOIN sucursal s
                    ON s.idsucursal = r.sucursal_idsucursal
                INNER JOIN zona_has_localidad zhl
                    ON (zhl.localidad_idlocalidad = case when r.terminal = 1 then 5442 else s.localidad_idlocalidad end)
                INNER JOIN zona z
                    ON zhl.zona_idzona = z.idzona
                WHERE  estado_idestado = 3
                AND cliente_idcliente =:cliente
                AND YEAR(remitofecha) =:fecha
                GROUP BY zona_idzona, MONTH(remitofecha)
                ')
                ->bindValue(':cliente', $model->cliente_idcliente)
                ->bindValue(':fecha', $model->periodo_inicio)
                ->queryAll();

                $postDomicilio = Yii::$app->db->createCommand(
                    'SELECT 
                        month(ogd.fecha) fecha,
                        sum(gd.cantidad) cantidad_bultos,
                        sum(0) volumen,
                        round(sum(gd.monto) , 2) total,
                        z.nombre
                    FROM guia_domicilio gd
                    INNER JOIN orden_grupal_domicilio ogd
                        ON ogd.idorden_grupal_domicilio = gd.ordengrupaldomicilio_idordengrupaldomicilio
                    INNER JOIN zona_has_localidad zhl
                        ON zhl.localidad_idlocalidad = gd.localidad_idlocalidad
                    INNER JOIN zona z
                        ON zhl.zona_idzona = z.idzona
                    WHERE  cliente_idcliente =:cliente
                    AND YEAR(ogd.fecha) =:fecha
                    AND ogd.borrado = 0
                    GROUP BY z.nombre, MONTH(ogd.fecha);
                    ')
                    ->bindValue(':cliente', $model->cliente_idcliente)
                    ->bindValue(':fecha', $model->periodo_inicio)
                    ->queryAll();

                /*$postPorcentaje = Yii::$app->db->createCommand(
                    'SELECT 
                        month(rp.fecha) fecha,
                        sum(rp.cantidad_pedidos) cantidad_bultos,
                        sum(0) volumen,
                        round(sum(rp.dinero_facturado) , 2) total,
                        z.nombre
                    FROM remito_porcentaje rp

                    ');*/
                
                $postVino = Yii::$app->db->createCommand(
                    'SELECT 
                        month(ogv.fecha) fecha,
                        sum(rv.cant_cajas) cantidad_bultos,
                        sum(rv.cant_botellas) volumen,
                        round(sum(CASE WHEN rv.total_facturacion IS NULL THEN 0 ELSE rv.total_facturacion end) , 2) total,
                        z.nombre,
                        z.idzona
                    FROM remito_vino rv
                    INNER JOIN sucursal s
                        ON s.idsucursal = rv.sucursal_idsucursal
                    INNER JOIN orden_grupal_vinos ogv
                        ON ogv.idorden_grupal_vinos = rv.ordengrupalvinos_idordengrupalvinos
                    INNER JOIN zona_has_localidad zhl
                        ON zhl.localidad_idlocalidad = s.localidad_idlocalidad
                    INNER JOIN zona z
                        ON zhl.zona_idzona = z.idzona
                    WHERE  cliente_idcliente =:cliente
                    AND YEAR(ogv.fecha) =:fecha
                    GROUP BY zona_idzona, MONTH(ogv.fecha)
                    ')
                    ->bindValue(':cliente', $model->cliente_idcliente)
                    ->bindValue(':fecha', $model->periodo_inicio)
                    ->queryAll();

                if(!$post && !$postDomicilio && !$postVino){
                    Yii::$app->session->setflash(
                        'error',
                        "No existen remitos para el cliente y año especificados.");
                        $model = new Liquidacion();
                        return $this->render('create-analisis', [
                            'model' => $model,
                        ]);
                }
                return $this->renderPartial('view-analisis-zona', ['post' => $post, 'postDomicilio' => $postDomicilio, 'postPorcentaje' => $postPorcentaje, 'postVino' => $postVino, 'model' => $model]);
            }else if($analisisForm ==4){
                $monthOne = date('m', strtotime($model->periodo_inicio));
                $monthOne = intval($monthOne);
                $monthTwo = date('m', strtotime($model->periodo_fin));
                $monthTwo = intval($monthTwo);
                $firstDay = date('d', strtotime($model->periodo_inicio));
                $firstDay = intval($firstDay);
                $secondDay = date('m', strtotime($model->periodo_fin));
                $secondDay = intval($secondDay);
                $year = date('Y', strtotime($model->periodo_fin));
                $year = intval($year);

                if($monthTwo != $monthOne){
                    Yii::$app->session->setflash(
                        'error',
                        "La feha de inicio y fin deber corresponder al mismo mes.");
                        $model = new Liquidacion();
                        return $this->render('create-analisis', [
                            'model' => $model,
                        ]);
                }
                if($firstDay > $secondDay){
                    Yii::$app->session->setflash(
                        'error',
                        "La feha de DESDE no puede ser mayor que la fecha HASTA.");
                        $model = new Liquidacion();
                        return $this->render('create-analisis', [
                            'model' => $model,
                        ]);
                }
                $post = Yii::$app->db->createCommand(
                    'SELECT day(remitofecha) dia,
                            sum(r.cantidad_bultos) cantidad_bultos,
                            sum(ifnull(r.volumen, 0)) volumen,
                            round(sum((select 
                                case when r.cantidad_bultos >= 1 then 1 * precio else 0 end 
                            from tarifario_has_precio 
                            where tarifario_idtarifario = c.tarifario_idtarifario 
                            and zona_idzona = 
                                (select case when r.terminal = 1 then 5 else zl.zona_idzona end from dual) 
                            and carga_idcarga = r.carga_idcarga) +

                            (select 
                                case when r.cantidad_bultos > 1 then (r.cantidad_bultos - 1 + ifnull(r.volumen, 0)) *  precio_adicional else ifnull(r.volumen, 0) * precio_adicional end 
                            from tarifario_has_precio 
                            where tarifario_idtarifario = c.tarifario_idtarifario 
                            and zona_idzona = 
                                (select case when r.terminal = 1 then 5 else zl.zona_idzona end from dual) 
                            and carga_idcarga = r.carga_idcarga)), 2) as total 
                    FROM remito r
                    inner join cliente c on idcliente = r.cliente_idcliente
                    inner join sucursal on sucursal.idsucursal = sucursal_idsucursal 
                    left join zona_has_localidad zl on zl.localidad_idlocalidad = sucursal.localidad_idlocalidad
                    where remitofecha >=:fechaInicio
                    and remitofecha <=:fechaFin
                    and cliente_idcliente =:cliente
                    and estado_idestado = 3
                    group by day(remitofecha)
                    order by day(remitofecha)
                    ')
                ->bindValue(':cliente', $model->cliente_idcliente)
                ->bindValue(':fechaInicio', $model->periodo_inicio)
                ->bindValue(':fechaFin', $model->periodo_fin)
                ->queryAll();

                $postDomicilio = Yii::$app->db->createCommand(
                    'SELECT day(ogd.fecha) dia,
                            sum(gd.cantidad) cantidad_bultos,
                            sum(0) volumen,
                            round(sum(ifnull(gd.monto, 0)) , 2) total
                    FROM guia_domicilio gd
                    INNER JOIN orden_grupal_domicilio ogd
                        ON ogd.idorden_grupal_domicilio = gd.ordengrupaldomicilio_idordengrupaldomicilio
                    where ogd.fecha >=:fechaInicio
                    and ogd.fecha <=:fechaFin
                    and cliente_idcliente =:cliente
                    group by day(ogd.fecha)
                    order by day(ogd.fecha)
                    ')
                ->bindValue(':cliente', $model->cliente_idcliente)
                ->bindValue(':fechaInicio', $model->periodo_inicio)
                ->bindValue(':fechaFin', $model->periodo_fin)
                ->queryAll();

                $postPorcentaje = Yii::$app->db->createCommand(
                    'SELECT day(rp.fecha) dia,
                            sum(rp.cant_pedidos) cantidad_bultos,
                            sum(0) volumen,
                            round(sum(ifnull(rp.dinero_facturado, 0)), 2) total
                    FROM remito_porcentaje rp
                    where rp.fecha >=:fechaInicio
                    and rp.fecha <=:fechaFin
                    and cliente_idcliente =:cliente
                    group by day(rp.fecha)
                    order by day(rp.fecha)
                    ')
                ->bindValue(':cliente', $model->cliente_idcliente)
                ->bindValue(':fechaInicio', $model->periodo_inicio)
                ->bindValue(':fechaFin', $model->periodo_fin)
                ->queryAll();

                $postVino = Yii::$app->db->createCommand(
                    'SELECT day(ogv.fecha) dia,
                            sum(rv.cant_cajas) cantidad_bultos,
                            sum(ifnull(rv.cant_botellas, 0)) volumen,
							round(sum(CASE WHEN rv.total_facturacion IS NULL THEN 0 ELSE rv.total_facturacion end) , 2) total
                    FROM remito_vino rv
                    INNER JOIN orden_grupal_vinos ogv
                        ON ogv.idorden_grupal_vinos = rv.ordengrupalvinos_idordengrupalvinos
                    where ogv.fecha >=:fechaInicio
                    and ogv.fecha <=:fechaFin
                    and cliente_idcliente =:cliente
                    group by day(ogv.fecha)
                    order by day(ogv.fecha)
                    ')
                ->bindValue(':cliente', $model->cliente_idcliente)
                ->bindValue(':fechaInicio', $model->periodo_inicio)
                ->bindValue(':fechaFin', $model->periodo_fin)
                ->queryAll();





                $previousMonth = date('m', strtotime($model->periodo_inicio));
                $previousMonth = intval($previousMonth);
                if($monthOne == 1){
                    $previousMonth = 12;
                    $previousYear = $year - 1;
                }else{
                    $previousMonth = $monthOne - 1;
                    $previousYear = $year;
                }


                $postAnterior = Yii::$app->db->createCommand(
                    'SELECT 
                    sum(ifnull(volumen, 0) + cantidad_bultos) total, 
                    sum(bulto + adicional) bruto,
                    month(remitofecha) mes,
                    year(remitofecha) anio
                from 
                    (SELECT 
                        remitofecha, 
                        idremito, 
                        remito.carga_idcarga,
                        remito.terminal, 
                        remito.cliente_idcliente, 
                        sucursal.localidad_idlocalidad, 
                        zona_idzona, 
                        cantidad_bultos, 
                        volumen, 

                        (select 
                            case when remito.cantidad_bultos >= 1 then 1 * precio else 0 end 
                        from tarifario_has_precio 
                        where tarifario_idtarifario = c.tarifario_idtarifario 
                        and zona_idzona = 
                            (select case when remito.terminal = 1 then 5 else zl.zona_idzona end from dual) 
                        and carga_idcarga = remito.carga_idcarga) as bulto,

                        (select 
                            case when remito.cantidad_bultos > 1 then (remito.cantidad_bultos - 1 + ifnull(remito.volumen, 0)) *  precio_adicional else remito.volumen * precio_adicional end 
                        from tarifario_has_precio 
                        where tarifario_idtarifario = c.tarifario_idtarifario 
                        and zona_idzona = 
                            (select case when remito.terminal = 1 then 5 else zl.zona_idzona end from dual) 
                        and carga_idcarga = remito.carga_idcarga) as adicional 
                    FROM `remito` 
                inner join cliente c on idcliente = remito.cliente_idcliente
                inner join sucursal on sucursal.idsucursal = sucursal_idsucursal 
                left join zona_has_localidad zl on zl.localidad_idlocalidad = sucursal.localidad_idlocalidad
                WHERE month(remitofecha)=:mes
                AND year(remitofecha)=:anio
                AND estado_idestado = 3
                AND cliente_idcliente=:cliente) as resultado
                    ')
                ->bindValue(':cliente', $model->cliente_idcliente)
                ->bindValue(':mes', $previousMonth)
                ->bindValue(':anio', $previousYear)
                ->queryAll();

                if(!$post && !$postDomicilio && !$postPorcentaje && !$postPorcentaje && !$postVino){
                    Yii::$app->session->setflash(
                        'error',
                        "No existen remitos para el cliente y mes especificados.");
                        $model = new Liquidacion();
                        return $this->render('create-analisis', [
                            'model' => $model,
                        ]);
                }
                return $this->renderPartial('view-analisis-diames', ['post' => $post, 'postDomicilio' => $postDomicilio, 'postPorcentaje' => $postPorcentaje, 'postVino' => $postVino, 'postAnterior' => $postAnterior,  'model' => $model]);
            }else if($analisisForm == 5){
                $datearr = explode('-', $model->periodo_inicio);
                $mes = $datearr[0]."<br>"; //month
                $anio = $datearr[1]."<br>"; //year
                $post = Yii::$app->db->createCommand( 
                    'SELECT c.razonsocial \'CLIENTE\',
                                    l.periodo \'FORMA DE PAGO\',
                                    (case when l.bultos IS NULL THEN (SELECT sum( case when estado_idestado = 3 then (cantidad_bultos + ifnull(volumen, 0)) else 0 end) 
                                    from remito where idremito IN (select remito_idremito from liquidacion_has_remito where liquidacion_idliquidacion = l.idliquidacion)) ELSE (l.bultos + l.volumen) end) BULTOS,
                                    l.importe_bultos \'REPARTO NETO\',
                                    ROUND(l.total_contra, 2) CONTRAREEMBOLSO,
                                    (SELECT sum(case when estado_idestado = 3 then ifnull(flete_pagado, 0) else 0 end)  from remito 
                                    where idremito IN (select remito_idremito from liquidacion_has_remito where liquidacion_idliquidacion = l.idliquidacion)) \'FLETE COBRADO\',
                                    ROUND(l.guias_logistica, 2) \'GUIAS LOGISTICA\',
                                    ROUND(l.total_iva, 2) IVA,
                                    l.importe_guias \'GUIAS CLIENTE\',
                                    ROUND(l.total,2) \'MONTO TOTAL\',
                                    el.nombre ESTADO
                    FROM liquidacion l
                    INNER JOIN liquidacion_forma_pago lfp
                        ON lfp.idliquidacion_forma_pago = l.liquidacionformapago_idliquidacionformapago
                    INNER JOIN estado_liquidacion el
                        ON el.idestado_liquidacion = l.estadoliquidacion_idestadoliquidacion
                    INNER JOIN cliente c
                        ON c.idcliente = l.cliente_idcliente
                    INNER JOIN liquidacion_has_remito lhr
                        ON lhr.liquidacion_idliquidacion = l.idliquidacion
                    INNER JOIN remito r
                        ON r.idremito = lhr.remito_idremito
                    WHERE MONTH(periodo_fin) =:mes
                    AND YEAR(periodo_fin)=:anio
                    AND l.confirmada = 1
                    GROUP BY l.idliquidacion			
                    UNION 
                    (SELECT c.razonsocial \'CLIENTE\',
                                    ldc.nombre \'FORMA DE PAGO\',
                                    ldc.cantidad_bultos BULTOS,
                                    ROUND(ldc.importe_bultos,2) \'REPARTO NETO\',
                                    null CONTRAREEMBOLSO,
                                    null \'FLETE COBRADO\',
                                    null \'GUIAS LOGISTICA\',
                                    ROUND(ldc.total_iva,2) IVA,
                                    null \'GUIAS CLIENTE\',
                                    ROUND(ldc.total,2) \'MONTO TOTAL\',
                                    el.nombre ESTADO
                    FROM liquidacion_domicilio_cliente ldc
                    INNER JOIN cliente c
                        ON c.idcliente = ldc.cliente_idcliente
                    INNER JOIN estado_liquidacion el
                        ON el.idestado_liquidacion = ldc.estadoliquidacion_idestadoliquidacion
                    WHERE MONTH(fecha_fin) =:mes
                    AND YEAR(fecha_fin) =:anio
                    AND ldc.confirmada = 1
                    AND ldc.borrado = 0
                    GROUP BY ldc.idliquidacion_domicilio_cliente)
                    UNION
                    (SELECT c.razonsocial \'CLIENTE\',
                                    lvc.nombre \'FORMA DE PAGO\',
                                    sum(rv.cant_cajas) BULTOS,
                                    lvc.total \'REPARTO NETO\',
                                    null \'CONTRAREEMBOLSO\',
                                    null \'FLETE COBRADO\',
                                    null \'GUIAS LOGISTICA\',
                                    null IVA,
                                    ROUND(lvc.total_guias, 2) \'GUIAS CLIENTE\',
                                    ROUND(lvc.total_cobrar, 2) \'MONTO TOTAL\',
                                    el.nombre ESTADO
                    FROM liquidacion_vino_cliente lvc
                    INNER JOIN cliente c
                        ON c.idcliente = lvc.cliente_idcliente
                    INNER JOIN orden_grupal_vinos ogv
                        ON ogv.liquidacionvinocliente_idliquidacionvinocliente = lvc.idliquidacion_vino_cliente
                    INNER JOIN remito_vino rv
                        ON rv.ordengrupalvinos_idordengrupalvinos = ogv.idorden_grupal_vinos
                    INNER JOIN estado_liquidacion el
                        ON el.idestado_liquidacion = lvc.estadoliquidacion_idestadoliquidacion
                    WHERE MONTH(periodo_fin) =:mes
                    AND YEAR(periodo_fin) =:anio
                    AND lvc.confirmada = 1
                    GROUP BY lvc.idliquidacion_vino_cliente
                            )
                    ORDER BY CLIENTE
                    ')
                ->bindValue(':mes', $mes)
                ->bindValue(':anio', $anio)
                ->queryAll();

                if(!$post){
                    Yii::$app->session->setflash(
                        'error',
                        "No existen planillas y mes especificados.");
                        $model = new Liquidacion();
                        return $this->render('create-analisis', [
                            'model' => $model,
                        ]);
                }
                return $this->renderPartial('view-cobros-pagos', ['post' => $post,  'model' => $model]);
            }else if($analisisForm == 7){
                $year = date('Y', strtotime($model->periodo_inicio));
                $year = intval($year - 1);
                $post = Yii::$app->db->createCommand(
                    'SELECT 
                        c.razonsocial cliente,	
                        sum(r.cantidad_bultos) cantidad_bultos,
                        sum(ifnull(r.volumen, 0) + r.cantidad_bultos - 1) volumen,
                        (select case when r.terminal = 1 then 5 else zhl.zona_idzona end from dual) zona_localidad,
                        z.nombre
                    FROM remito r
                    INNER JOIN cliente c
                        ON c.idcliente = r.cliente_idcliente
                    INNER JOIN sucursal s
                        ON s.idsucursal = r.sucursal_idsucursal
                    INNER JOIN zona_has_localidad zhl
                        ON s.localidad_idlocalidad = zhl.localidad_idlocalidad
                    INNER JOIN zona z
                        ON (select case when r.terminal = 1 then 5 else zhl.zona_idzona end from dual) = z.idzona
                    WHERE  estado_idestado = 3
                    AND YEAR(remitofecha) =:anio
                    GROUP BY zona_localidad, c.razonsocial
                    ORDER BY c.razonsocial, zona_localidad;
                    ')
                ->bindValue(':anio', $year)
                ->queryAll();

                $postDomicilio = Yii::$app->db->createCommand(
                    'SELECT 
                        c.razonsocial cliente,	
                        sum(gd.cantidad) cantidad_bultos,
                        sum(0) volumen,
                        zhl.zona_idzona zona_localidad,
                        z.nombre
                    FROM guia_domicilio gd
                    INNER JOIN orden_grupal_domicilio ogd
                        ON ogd.idorden_grupal_domicilio = gd.ordengrupaldomicilio_idordengrupaldomicilio
                    INNER JOIN cliente c
                        ON c.idcliente = ogd.cliente_idcliente
                    INNER JOIN zona_has_localidad zhl
                        ON zhl.localidad_idlocalidad = gd.localidad_idlocalidad
                    INNER JOIN zona z
                        ON zhl.zona_idzona = z.idzona
                    WHERE YEAR(ogd.fecha) =:anio
                    GROUP BY zona_localidad, c.razonsocial;
                    ')
                ->bindValue(':anio', $year)
                ->queryAll();


                $postVino = Yii::$app->db->createCommand(
                    'SELECT 
                        c.razonsocial cliente,	
                        sum(rv.cant_cajas) cantidad_bultos,
                        sum(rv.cant_botellas) volumen,
                        zhl.zona_idzona zona_localidad,
                        z.nombre
                    FROM remito_vino rv
                    INNER JOIN sucursal s
                        ON s.idsucursal = rv.sucursal_idsucursal
                    INNER JOIN orden_grupal_vinos ogv
                        ON ogv.idorden_grupal_vinos = rv.ordengrupalvinos_idordengrupalvinos
                    INNER JOIN cliente c
                        ON c.idcliente = ogv.cliente_idcliente
                    INNER JOIN zona_has_localidad zhl
                        ON zhl.localidad_idlocalidad = s.localidad_idlocalidad
                    INNER JOIN zona z
                        ON zhl.zona_idzona = z.idzona
                    WHERE YEAR(ogv.fecha) =:anio
                    GROUP BY zona_localidad, c.razonsocial;
                    ')
                ->bindValue(':anio', $year)
                ->queryAll();

                if(!$post && !$postDomicilio && !$postVino){
                    Yii::$app->session->setflash(
                        'error',
                        "No existen remitos para el año especificado.");
                        $model = new Liquidacion();
                        return $this->render('create-analisis', [
                            'model' => $model,
                        ]);
                }
                return $this->renderPartial('view-cliente-zona', ['post' => $post, 'postDomicilio' => $postDomicilio, 'postVino' => $postVino, 'year' => $year ,'model' => $model]);            
            }else if($analisisForm == 8){
                $post = Yii::$app->db->createCommand(
                    'SELECT 
                        l.fecha_pago,
                        c.razonsocial,
                        lfp.nombre,
                        l.total  total
                    from liquidacion l
                    INNER JOIN liquidacion_forma_pago lfp
                    on idliquidacion_forma_pago = l.liquidacionformapago_idliquidacionformapago
                    INNER JOIN cliente c
                    ON c.idcliente = l.cliente_idcliente
                    where fecha_pago >=:fecha_inicio
                    AND fecha_pago <=:fecha_fin
                    UNION
                    SELECT 
                                    l.fecha_pago,
                                    c.razonsocial,
                                    lfp.nombre,
                                    total  total
                    from liquidacion_domicilio_cliente l
                    INNER JOIN liquidacion_forma_pago lfp
                    on idliquidacion_forma_pago = l.liquidacionformapago_idliquidacionformapago  
                    INNER JOIN cliente c
                    ON c.idcliente = l.cliente_idcliente
                    where fecha_pago >=:fecha_inicio
                    AND fecha_pago <=:fecha_fin
                    union
                    SELECT 
                                    l.fecha_pago,
                                    c.razonsocial,
                                    lfp.nombre,
                                    total_cobrar  total
                    from liquidacion_porcentaje_cliente l
                    INNER JOIN liquidacion_forma_pago lfp
                    on idliquidacion_forma_pago = l.liquidacionformapago_idliquidacionformapago 
                    INNER JOIN cliente c
                    ON c.idcliente = l.cliente_idcliente
                    where fecha_pago >=:fecha_inicio
                    AND fecha_pago <=:fecha_fin
                    union
                    SELECT 
                                    l.fecha_pago,
                                    c.razonsocial,
                                    lfp.nombre,
                                    total_cobrar  total
                    from liquidacion_vino_cliente l
                    INNER JOIN liquidacion_forma_pago lfp
                    on idliquidacion_forma_pago = l.liquidacionformapago_idliquidacionformapago 
                    INNER JOIN cliente c
                    ON c.idcliente = l.cliente_idcliente
                    where fecha_pago >=:fecha_inicio
                    AND fecha_pago <=:fecha_fin
                    ORDER BY fecha_pago, razonsocial;
                    ')
                ->bindValue(':fecha_inicio', $model->periodo_inicio)
                ->bindValue(':fecha_fin', $model->periodo_fin)
                ->queryAll();

                if(!$post){
                    Yii::$app->session->setflash(
                        'error',
                        "No existen remitos para el año especificado.");
                        $model = new Liquidacion();
                        return $this->render('create-analisis', [
                            'model' => $model,
                        ]);
                }
                return $this->renderPartial('ingresos', ['post' => $post, 'year' => $year ,'model' => $model]);            
            }
        }

        return $this->render('create-analisis',[
            'model' => $model,
        ]);
    }

    /**
     * Creates a new LiquidacionDomicilioCliente model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateAnalisisZonaCliente($analisis)
    {
    }
}
