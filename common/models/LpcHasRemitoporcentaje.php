<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%lpc_has_remitoporcentaje}}".
 *
 * @property int $idlpc_has_remitoporcentaje
 * @property int $rp_idrp
 * @property int $lpc_idlpc
 *
 * @property LiquidacionPorcentajeCliente $lpcIdlpc
 * @property RemitoPorcentaje $rpIdrp
 */
class LpcHasRemitoporcentaje extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%lpc_has_remitoporcentaje}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rp_idrp', 'lpc_idlpc'], 'required'],
            [['rp_idrp', 'lpc_idlpc'], 'integer'],
            [['lpc_idlpc'], 'exist', 'skipOnError' => true, 'targetClass' => LiquidacionPorcentajeCliente::className(), 'targetAttribute' => ['lpc_idlpc' => 'idliquidacion_porcentaje_cliente']],
            [['rp_idrp'], 'exist', 'skipOnError' => true, 'targetClass' => RemitoPorcentaje::className(), 'targetAttribute' => ['rp_idrp' => 'idremito_porcentaje']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idlpc_has_remitoporcentaje' => Yii::t('app', 'Idlpc Has Remitoporcentaje'),
            'rp_idrp' => Yii::t('app', 'Rp Idrp'),
            'lpc_idlpc' => Yii::t('app', 'Lpc Idlpc'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLpcIdlpc()
    {
        return $this->hasOne(LiquidacionPorcentajeCliente::className(), ['idliquidacion_porcentaje_cliente' => 'lpc_idlpc']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRpIdrp()
    {
        return $this->hasOne(RemitoPorcentaje::className(), ['idremito_porcentaje' => 'rp_idrp']);
    }
}
