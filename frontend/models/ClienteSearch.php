<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Cliente;

/**
 * ClienteSearch represents the model behind the search form of `common\models\Cliente`.
 */
class ClienteSearch extends Cliente
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idcliente', 'telefono', 'condicionfiscal_idcondicionfiscal'], 'integer'],
            [['razonsocial', 'cuit', 'mail', 'domicilio', 'tarifario_idtarifario', 'tarifariovino_idtarifariovino', 'localidad_idlocalidad', 'tarifariodomicilio_idtarifariodomicilio'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cliente::find();

        $query->joinWith(['tarifarioIdtarifario as tarifario']);
        $query->joinWith(['tarifariovinoIdtarifariovino as tarifarioVino']);
        $query->joinWith(['localidadIdlocalidad as localidad']);
        $query->joinWith(['tarifariodomicilioIdtarifariodomicilio as tarifarioDomicilio']);



        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idcliente' => $this->idcliente,
            'condicionfiscal_idcondicionfiscal' => $this->condicionfiscal_idcondicionfiscal,
            'mail' => $this->mail,
            'domicilio' => $this->domicilio,
        ]);

        $query->andFilterWhere(['like', 'razonsocial', $this->razonsocial])
            ->andFilterWhere(['like', 'cuit', $this->cuit])
            ->andFilterWhere(['like', 'telefono', $this->telefono])
            ->andFilterWhere(['like', 'tarifario.nombre', $this->tarifario_idtarifario])
            ->andFilterWhere(['like', 'tarifarioVino.nombre', $this->tarifariovino_idtarifariovino])
            ->andFilterWhere(['like', 'tarifarioDomocilio.nombre', $this->tarifariodomicilio_idtarifariodomicilio]);

        return $dataProvider;
    }
}
