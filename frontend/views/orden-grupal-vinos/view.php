<?php

use yii\data\ActiveDataProvider;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use yii\helpers\Html;
use common\models\RemitoVino;

/* @var $this yii\web\View */
/* @var $model common\models\OrdenGrupalVinos */


$this->title = $model->clienteIdcliente->razonsocial." - ".$model->numero;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orden Grupal Vinos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


$idOrdenGrupal = Yii::$app->getRequest()->getQueryParam('idOrdenGrupal');
if(!isset($idOrdenGrupal)){
    $idOrdenGrupal = Yii::$app->getRequest()->getQueryParam('id'); 
}

?>
<div class="orden-grupal-vinos-view">
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Detalle') ?></h2>
            <div class="clearfix"></div>
        </div>

        <p>
            <?= Html::a('Inicio', ['index'], ['class' => 'btn btn-primary']);?>
            <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->idorden_grupal_vinos], ['class' => 'btn btn-success']) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                //'idorden_grupal_vinos',
                'fecha',
                'numero',
                [
                    'attribute' => 'cliente_idcliente',
                    'value' => function($data){
                        return $data->clienteIdcliente->razonsocial;
                    }
                ],
                [
                    'attribute' => 'repartidor_idrepartidor',
                    'value' => function($data){
                        return $data->repartidorIdrepartidor->apellido.", ".$data->repartidorIdrepartidor->nombre;
                    }
                ],
            ],
        ]) ?>

    </div>
</div>

<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Remitos asociados');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php

    $dataProvider = new ActiveDataProvider([
        'query' => RemitoVino::find()->where(['ordengrupalvinos_idordengrupalvinos' => $idOrdenGrupal])
    ]);

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'moduleId' => 'gridviewk',
        'toolbar' =>  [
            '{export}',
            '{toggleData}',
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
        ],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'idremito_vino',
            'numero_remito',
            'guia_numero',
            'guia_tipo',
            'guia_facturada_a',
            [
                'attribute' => 'sucursal_idsucursal',
                'label' => 'Empresa',
                'value' => function($data){
                    return $data->sucursalIdsucursal->empresaIdempresa->nombre;
                }
            ],
            'cant_cajas',
            'cant_botellas',
            [
                'attribute' => 'valor_declarado',
                'format' => ['decimal',2],
            ],
            'guia_total',
            [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Acciones',
            'headerOptions' => ['style' => 'color:#337ab7'],
            'template' => '{view}{update}{delete}',
            'buttons' => [
              'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'Ver'),
                                'target' => '_blank',
                    ]);
              },

              'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'Actualizar'),
                                'target' => '_blank',
                    ]);
                },

                'delete' => function ($url, $model) {
                      return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                  'title' => Yii::t('app', 'Eliminar'),
                                  'data' => [
                                    'method' => 'post',
                                    'confirm' => Yii::t('app', '¿Eliminar remito vino?'),
                                    ],
                      ]);
                  },

            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    $url ='../remito-vino/view?id='.$model->idremito_vino;
                    return $url;
                }

                if ($action === 'update') {
                    $url ='../remito-vino/update?id='.$model->idremito_vino;
                    return $url;
                }

                if ($action === 'delete') {
                    $idOrdenGrupal = Yii::$app->getRequest()->getQueryParam('idOrdenGrupal');
                    if(!isset($idOrdenGrupal)){
                        $idOrdenGrupal = Yii::$app->getRequest()->getQueryParam('id'); 
                    }
                    $url ='../remito-vino/delete?id='.$model->idremito_vino."&idOrdenGrupalVinos=".$idOrdenGrupal;
                    return $url;
                }
            }
        ],
        ],
    ]); ?>
    </div>
</div>
