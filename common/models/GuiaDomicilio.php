<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%guia_domicilio}}".
 *
 * @property int $idorden_grupal_domicilio
 * @property string $numero_guia
 * @property double $monto
 * @property string $guia_tipo
 * @property string $empresa_destino
 * @property int $cantidad
 * @property int $ordengrupaldomicilio_idordengrupaldomicilio
 * @property int $localidad_idlocalidad
 * @property int $sprinter
 * @property int $cobrar_sprinter
 * @property string $creado
 *
 * @property Localidad $localidadIdlocalidad
 * @property OrdenGrupalDomicilio $ordengrupaldomicilioIdordengrupaldomicilio
 */
class GuiaDomicilio extends \yii\db\ActiveRecord
{
    public $extra;
    public $cliente_idcliente;
    public $precio_reparto;
    public $precio_reparto_adicional;
    public $cantidad_tipob;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%guia_domicilio}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero_guia', 'monto', 'guia_tipo', 'empresa_destino', 'localidad_idlocalidad'], 'required'],
            [['monto'], 'number'],
            [['creado'], 'safe'],
            [['guia_tipo'], 'string'],
            [['cantidad', 'ordengrupaldomicilio_idordengrupaldomicilio', 'localidad_idlocalidad', 'sprinter', 'cobrar_sprinter'], 'integer'],
            [['numero_guia'], 'string', 'max' => 60],
            [['empresa_destino'], 'string', 'max' => 100],
            [['localidad_idlocalidad'], 'exist', 'skipOnError' => true, 'targetClass' => Localidad::className(), 'targetAttribute' => ['localidad_idlocalidad' => 'idlocalidad']],
            [['ordengrupaldomicilio_idordengrupaldomicilio'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenGrupalDomicilio::className(), 'targetAttribute' => ['ordengrupaldomicilio_idordengrupaldomicilio' => 'idorden_grupal_domicilio']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idorden_grupal_domicilio' => Yii::t('app', 'Idorden Grupal Domicilio'),
            'numero_guia' => Yii::t('app', 'Nº Guía'),
            'monto' => Yii::t('app', 'Monto'),
            'guia_tipo' => Yii::t('app', 'Guía Tipo'),
            'empresa_destino' => Yii::t('app', 'Empresa Destino'),
            'cantidad' => Yii::t('app', 'Cantidad de bultos'),
            'ordengrupaldomicilio_idordengrupaldomicilio' => Yii::t('app', 'Ordengrupal domicilio'),
            'localidad_idlocalidad' => Yii::t('app', 'Localidad'),
            'sprinter' => Yii::t('app', 'Sprinter'),
            'cobrar_sprinter' => Yii::t('app', 'Cobrar Sprinter'),
            'creado' => Yii::t('app', 'Creado'), 
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalidadIdlocalidad()
    {
        return $this->hasOne(Localidad::className(), ['idlocalidad' => 'localidad_idlocalidad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdengrupaldomicilioIdordengrupaldomicilio()
    {
        return $this->hasOne(OrdenGrupalDomicilio::className(), ['idorden_grupal_domicilio' => 'ordengrupaldomicilio_idordengrupaldomicilio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getAllGuiasFromOrden($ordenId){
        return self::find()->where(['ordengrupaldomicilio_idordengrupaldomicilio' => $ordenId])->all();
    }
}
