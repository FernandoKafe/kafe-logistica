<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%liquidacionrepartidor_has_remito}}".
 *
 * @property int $liquidacionrepartidor_idliquidacionrepartidor
 * @property int $remito_idremito
 * @property double $total
 *
 * @property LiquidacionRepartidor $liquidacionrepartidorIdliquidacionrepartidor
 * @property Remito $remitoIdremito
 */
class LiquidacionrepartidorHasRemito extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%liquidacionrepartidor_has_remito}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['liquidacionrepartidor_idliquidacionrepartidor', 'remito_idremito', 'total'], 'required'],
            [['liquidacionrepartidor_idliquidacionrepartidor', 'remito_idremito'], 'integer'],
            [['total'], 'number'],
            [['liquidacionrepartidor_idliquidacionrepartidor', 'remito_idremito'], 'unique', 'targetAttribute' => ['liquidacionrepartidor_idliquidacionrepartidor', 'remito_idremito']],
            [['liquidacionrepartidor_idliquidacionrepartidor'], 'exist', 'skipOnError' => true, 'targetClass' => LiquidacionRepartidor::className(), 'targetAttribute' => ['liquidacionrepartidor_idliquidacionrepartidor' => 'idliquidacion_repartidor']],
            [['remito_idremito'], 'exist', 'skipOnError' => true, 'targetClass' => Remito::className(), 'targetAttribute' => ['remito_idremito' => 'idremito']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'liquidacionrepartidor_idliquidacionrepartidor' => Yii::t('app', 'Liquidacionrepartidor Idliquidacionrepartidor'),
            'remito_idremito' => Yii::t('app', 'Remito Idremito'),
            'total' => Yii::t('app', 'Total'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacionrepartidorIdliquidacionrepartidor()
    {
        return $this->hasOne(LiquidacionRepartidor::className(), ['idliquidacion_repartidor' => 'liquidacionrepartidor_idliquidacionrepartidor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemitoIdremito()
    {
        return $this->hasOne(Remito::className(), ['idremito' => 'remito_idremito']);
    }
}
