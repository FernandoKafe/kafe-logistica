<?php
$fileName = "INGRESOS - Semana:".$periodo_inicio;
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=".$fileName.".xls");
header("Pragma: no-cache");
header("Expires: 0");
setlocale(LC_TIME, 'es_ES.UTF-8');
if($post){
    $longitud = count($post);
    //print_r($post);
}if($postAnterior){
    $longitud = count($postAnterior);
    //print_r($postAnterior);
}
//echo date("F", mktime(0, 0, 0, $subelemento, 10));
$fecha = strftime("%B", strtotime($model->periodo_inicio));
$fecha = strtoupper($fecha);
$suma = 0;
$sumaContado = 0;
$sumaTranferencia = 0;
$sumaCheque = 0;
?>
<table xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">  
    <caption style='border: solid 1px'><?= $fileName ?></caption>
    <?php if($post){ ?>
            <tr>
                <td colspan="2" style='border: solid 1px'></td>
                <td colspan="3" style="text-align: center; border: solid 1px"><strong>FORMA DE PAGO</strong></td>
                <td style='border: solid 1px'></td>
                <td></td>

            </tr>
            <tr>
                <td style='border: solid 1px'><strong>FECHA</strong></td>
                <td style='border: solid 1px'><strong>CLIENTE</strong></td>
                <td style='border: solid 1px'><strong>CHEQUE</strong></td>
                <td style='border: solid 1px'><strong>TRANSFERENCIA</strong></td>
                <td style='border: solid 1px'><strong>EFECTIVO</strong></td>
                <td style='border: solid 1px'><strong>MONTO</strong></td>
            </tr>
            <?php foreach($post as $elemento){ ?>
            <tr>
            <?php 
                foreach($elemento as $subelemento){   
                    if(key($elemento) == "fecha_pago"){
                        echo "<td style='border: solid 1px'>".$subelemento."</td>";
                    }  
                    if(key($elemento) == "razonsocial"){
                        echo "<td style='border: solid 1px'>".$subelemento."</td>";
                    }
                    if(key($elemento) == "nombre"){
                        if($elemento[nombre] == "Cheque"){
                            echo "<td style='text-align: center;border: solid 1px'><font color='green'>✓</font></td>";
                            echo "<td style='border: solid 1px'></td>";
                            echo "<td style='border: solid 1px'></td>";
                            $sumaCheque += $elemento[total];
                        }else if($elemento[nombre] == "Transferencia"){
                            echo "<td style='border: solid 1px'></td>";
                            echo "<td style='text-align: center;border: solid 1px'><font color='green'>✓</font></td>";
                            echo "<td style='border: solid 1px'></td>";
                            $sumaTranferencia += $elemento[total];
                        }else if($elemento[nombre] == "Contado"){
                            echo "<td style='border: solid 1px'></td>";
                            echo "<td style='border: solid 1px'></td>";
                            echo "<td style='text-align: center;border: solid 1px;'><font color='green'>✓</font></td>";
                            $sumaContado += $elemento[total];
                        }else{
                            echo "<td style='border: solid 1px'></td>";
                            echo "<td style='border: solid 1px'></td>";
                            echo "<td style='border: solid 1px'></td>";
                        }
                    } 
                    if(key($elemento) == "total"){
                        echo "<td style='border: solid 1px'>".number_format($subelemento, 2 , "." ,  "," )."</td>";
                        $suma += $subelemento;
                    }
                    next($elemento); 
                } ?>
            </tr>
        <?php } ?>
            <tr>
                <td style='border: solid 1px; text-align: center' colspan="5"><strong>TOTAL INGRESOS</strong></td>
                <td style='border: solid 1px'><strong><?= $suma?></strong></td>
            </tr>
        <?php } ?>       
</table>
<br><br><br>
<table xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">  
    <caption style='text-align:center;border: solid 1px'><strong><?= "DETALLE INGRESOS" ?></strong></caption>
    <tr>
        <td style='text-align:center;border: solid 1px'><strong>FORMA</strong></td>
        <td style='text-align:center;border: solid 1px'><strong>IMPORTE</strong></td>
    </tr> 
    <tr>
        <td style='border: solid 1px'><?= "EFECTIVO" ?></td>
        <td style='border: solid 1px'><?= number_format($sumaContado, 2 , "." ,  "," ) ?></td>
    </tr>
    <tr>
        <td style='border: solid 1px'><?="TRANFERENCIA"?></td>
        <td style='border: solid 1px'><?= number_format($sumaTranferencia, 2 , "." ,  "," ) ?></td>
    </tr>
    <tr>
        <td style='border: solid 1px'><?="CHEQUE"?></td>
        <td style='border: solid 1px'><?= number_format($sumaCheque, 2 , "." ,  "," )?></td>
    </tr>
    <tr>
        <td style='border: solid 1px'><strong><?="TOTAL"?></strong></td>
        <td style='border: solid 1px'><strong><?= number_format($suma, 2 , "." ,  "," ) ?></strong></td>
    </tr>    
</table>