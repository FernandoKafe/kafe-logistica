<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\LiquidacionDomicilioRepartidorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="liquidacion-domicilio-repartidor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'idliquidacion_domicilio_repartidor') ?>

    <?= $form->field($model, 'fecha_inicio') ?>

    <?= $form->field($model, 'fecha_fin') ?>

    <?= $form->field($model, 'nombre') ?>

    <?= $form->field($model, 'total') ?>

    <?php // echo $form->field($model, 'repartidor_idrepartidor') ?>

    <?php // echo $form->field($model, 'estadoliquidacion_idestadoliquidacion') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
