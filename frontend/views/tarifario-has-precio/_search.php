<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\TarifarioHasPrecioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tarifario-has-precio-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'carga_idcarga') ?>

    <?= $form->field($model, 'precio') ?>

    <?= $form->field($model, 'zona_idzona') ?>

    <?= $form->field($model, 'tarifario_idtarifario') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
