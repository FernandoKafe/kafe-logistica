<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\GuiaDomicilio */

$this->title = Yii::t('app', 'Actualizar Guía Domicilio: ' . $model->numero_guia);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Guia Domicilios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idorden_grupal_domicilio, 'url' => ['view', 'id' => $model->idorden_grupal_domicilio]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<h1><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario de modificación') ?></h2>
        <div class="clearfix"></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div> 
