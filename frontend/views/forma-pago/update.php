<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FormaPago */

$this->title = Yii::t('app', 'Actualizar Forma de Pago: ' . $model->idformadepago, [
    'nameAttribute' => '' . $model->idformadepago,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Formas de Pago'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idformadepago, 'url' => ['view', 'id' => $model->idformadepago]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<h1><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario de modificación') ?></h2>
        <div class="clearfix"></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
