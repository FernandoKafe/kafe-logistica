<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

use common\models\OrdenGrupalDomicilio;
use common\models\GuiaDomicilio;
use common\models\TarifarioDomicilio;
use common\models\LiquidacionDomicilioCliente;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionDomicilioCliente */
$idLiquidacion = Yii::$app->getRequest()->getQueryParam('id');

$this->title = $model->clienteIdcliente->razonsocial.": ".$model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion Domicilio Clientes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-domicilio-cliente-view">

    <h1>Liquidación: <?= $model->clienteIdcliente->razonsocial?></h1>
    <h1>Periodo: <?= $model->nombre?></h1>
    <h1>CUIT: <?=$model->clienteIdcliente->cuit?></h1>
    <div class="x_panel">
    <div class="x_title">
        <div class="clearfix"></div>
    </div>
    <p>
        <?php
            if(!$model->confirmada){
                echo Html::a(Yii::t('app', 'Eliminar'), ['delete', 'id' => $model->idliquidacion_domicilio_cliente], ['class' => 'btn btn-danger',  'data-method'=>'post']);
                echo Html::a(Yii::t('app', 'Recalcular'), ['recalculate', 'id' => $model->idliquidacion_domicilio_cliente], ['class' => 'btn btn-warning']);
                echo Html::a(Yii::t('app', 'Confirmar'), ['liquidate', 'id' => $model->idliquidacion_domicilio_cliente], ['class' => 'btn btn-success']);
            }else{
                echo Html::a(Yii::t('app', 'Inicio'), ['index', 'id' => $model->idliquidacion_domicilio_cliente], ['class' => 'btn btn-primary',  'data-method'=>'post']);
                echo Html::a(Yii::t('app', 'Eliminar'), ['delete', 'id' => $model->idliquidacion_domicilio_cliente], ['class' => 'btn btn-danger',  'data-method'=>'post']);
                echo Html::a(Yii::t('app', 'PDF'), ['liquidate', 'id' => $model->idliquidacion_domicilio_cliente], ['class' => 'btn btn-success']);
            }
        ?>
    </p>
    <p><h3><strong style="color: red;">IMPORTANTE:</strong> La previa debe recalcularse luego de haberse generado si se modifica o elimina un remito de la misma.</h3></p>

</div>
</div>


<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_title">
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php
        if($model->confirmada){
            $ordenesGrupales = OrdenGrupalDomicilio::getAllOrdenesClienteFechaLiquidados($model->cliente_idcliente, $model->fecha_inicio, $model->fecha_fin, $model->idliquidacion_domicilio_cliente);
            $ordenesGrupalesId = ArrayHelper::getColumn($ordenesGrupales,'idorden_grupal_domicilio');
            $remitos = GuiaDomicilio::find()->where(['IN', 'ordengrupaldomicilio_idordengrupaldomicilio', $ordenesGrupalesId]);
        }else{
            $ordenesGrupales = OrdenGrupalDomicilio::getAllOrdenesClienteFecha($model->cliente_idcliente, $model->fecha_inicio, $model->fecha_fin);
            $ordenesGrupalesId = ArrayHelper::getColumn($ordenesGrupales,'idorden_grupal_domicilio');
            $remitos = GuiaDomicilio::find()->where(['IN', 'ordengrupaldomicilio_idordengrupaldomicilio', $ordenesGrupalesId]);        
        }
        $dataProvider = new ActiveDataProvider([
            'query' => $remitos,
        ]);

        if($model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo A"){
            echo GridView::widget([
            'dataProvider' => $dataProvider,
            'moduleId' => 'gridviewk',
            'toolbar' =>  [
                '{export}',
                '{toggleData}',
            ],
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
            ],
            'showFooter' => true,
            'columns' => [
                [
                    'label' => 'Fecha',
                    'value' => function($data){
                        return $data->ordengrupaldomicilioIdordengrupaldomicilio->fecha;
                    },
                ],
                [
                    'label' => 'Nº Orden Grupal',
                    'value' => function($data){
                        return $data->ordengrupaldomicilioIdordengrupaldomicilio->numero_orden;
                    },
                ],
                [
                    'attribute' => 'empresa_destino',
                    'label' => 'Empresa destino',
                ],
                [
                    'attribute' => 'localidad_idlocalidad',
                    'label' => 'Localidad destino',
                    'value' => function($data){
                        if($data->localidad_idlocalidad){
                            return $data->localidadIdlocalidad->nombre.
                            ", ".
                            $data->localidadIdlocalidad->departamentoIddepartamento->nombre.
                            ", ".
                            $data->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                        }else{
                            return "Guía sin localidad";
                        }
                    },
                ],
                [
                    'attribute' => 'numero_guia',
                    'value' => function($data){
                        return $data->numero_guia."-".$data->guia_tipo;
                    },
                    'footer' => '<strong>TOTAL</strong>',
                ],
                [
                    'attribute' => 'cantidad',
                    'footer' => $model->cantidad_bultos,
                ],  
                [
                    'label' => 'Monto A-R-X-Otras',
                    'value' => function($data){
                        if($data->guia_tipo != "B")
                            return $data->monto; 
                    },
                    'format' => ['decimal', 2],
                    'footer' => number_format ($model->total_ar, 2 , "." ,  "," ),
                ],
                [
                    'label' => 'Monto B',
                    'value' => function($data){
                        if($data->guia_tipo == "B")
                            return $data->monto;
                    },
                    'format' => ['decimal', 2],
                    'footer' => number_format ($model->total_b, 2 , "." ,  "," ),
                ],
            ],
            ]);
        }else if($model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo B"){
            if(!isset($model->rendicion_comun) || !isset($model->rendicion_sprinter) || !isset($model->reparto)) {
                if (!isset($model->rendicion_comun)) 
                    $model->rendicion_comun = LiquidacionDomicilioCliente::getRendicionComunSum($remitos->all());
                
                if (!isset($model->rendicion_sprinter)) 
                    $model->rendicion_sprinter = LiquidacionDomicilioCliente::getRendicionSprinterSum($remitos->all());
                
                if (!isset($model->reparto)) 
                    $model->reparto = LiquidacionDomicilioCliente::getRepartoSum($remitos->all());
                
                $model->save(false);
            }
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'moduleId' => 'gridviewk',
                'toolbar' =>  [
                    '{export}',
                    '{toggleData}',
                ],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                ],
                'showFooter' => true,
                'columns' => [
                    [
                        'label' => 'Fecha',
                        'value' => function($data){
                            return $data->ordengrupaldomicilioIdordengrupaldomicilio->fecha;
                        },
                    ],
                    [
                        'label' => 'Nº Orden Grupal',
                        'value' => function($data){
                            return $data->ordengrupaldomicilioIdordengrupaldomicilio->numero_orden;
                        },
                    ],
                    [
                        'attribute' => 'empresa_destino',
                        'label' => 'Empresa destino',
                    ],
                    [
                        'attribute' => 'localidad_idlocalidad',
                        'label' => 'Localidad destino',
                        'value' => function($data){
                            if($data->localidad_idlocalidad){
                                return $data->localidadIdlocalidad->nombre.
                                ", ".
                                $data->localidadIdlocalidad->departamentoIddepartamento->nombre.
                                ", ".
                                $data->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                            }else{
                                return "Guía sin localidad";
                            }
                        },
                    ],
                    [
                        'attribute' => 'numero_guia',
                        'value' => function($data){
                            return $data->numero_guia;
                        },
                        'footer' => '<strong>TOTAL</strong>',
                        'pageSummary' => '<strong>SUBTOTAL</strong>',
                    ],
                    /*[
                        'label' => 'Repartidor',
                        'value' => function($data){
                            return $data->ordengrupaldomicilioIdordengrupaldomicilio->repartidorIdrepartidor->apellido.
                            ", ".
                            $data->ordengrupaldomicilioIdordengrupaldomicilio->repartidorIdrepartidor->nombre;
                        }
                    ],*/
                    [
                        'attribute' => 'cantidad',
                        'footer' =>  $model->cantidad_bultos,
                    ],  
                    [
                        'attribute' => 'monto',
                        'label' => 'Rendición común',
                        'value' => function($data){
                            if($data->sprinter == 0)
                                return $data->monto;
                        },
                        'format' => ['decimal', 2],
                        'footer' => number_format ($model->rendicion_comun, 2 , "." ,  "," ),
                    ],
                    [
                        'attribute' => 'monto',
                        'label' => 'Rendición sprinter',
                        'value' => function($data){
                            if(($data->sprinter == 1)) 
                                return $data->monto;
                        },
                        'format' => ['decimal', 2],
                        'footer' => number_format ($model->rendicion_sprinter, 2 , "." ,  "," ),
                    ],
                    [
                        'label' => 'Reparto',
                        'value' => function($data){
                            //Hacer calculo en DB
                            $tarifario = TarifarioDomicilio::find()->where(['idtarifario_domicilio' => $data->ordengrupaldomicilioIdordengrupaldomicilio->clienteIdcliente->tarifariodomicilio_idtarifariodomicilio])->one();
                            $remitosOrden = GuiaDomicilio::find()->where(['ordengrupaldomicilio_idordengrupaldomicilio' => $data->ordengrupaldomicilio_idordengrupaldomicilio])->orderBy(['sprinter' => SORT_DESC])->all();
                            $remitosId = ArrayHelper::getColumn($remitosOrden,'idorden_grupal_domicilio');
                            $sprinterArray = ArrayHelper::getColumn($remitosOrden,'sprinter');
                            if($sprinterArray[0] == 1){
                                if($data->idorden_grupal_domicilio == $remitosId[0]){
                                    return $data->ordengrupaldomicilioIdordengrupaldomicilio->precio_sprinter;
                                }else{
                                    return 0;
                                }
                            }else{
                                if(($data->cantidad == 1) && ($data->sprinter == 0)){
                                    return $data->ordengrupaldomicilioIdordengrupaldomicilio->precio_bulto;
                                }else if(($data->cantidad > 1) && ($data->sprinter == 0)){
                                    return ($data->ordengrupaldomicilioIdordengrupaldomicilio->precio_bulto + (($data->cantidad - 1) * $data->ordengrupaldomicilioIdordengrupaldomicilio->precio_bulto_adicional));
                                }else if(($data->sprinter == 1)){
                                    return $data->ordengrupaldomicilioIdordengrupaldomicilio->precio_sprinter;
                                }
                            }
                        },
                        'format' => ['decimal', 2],
                        'footer' => number_format ($model->reparto, 2 , "." ,  "," ),
                    ],
                ],
                ]);
        }
    ?>



    <?php 
    if($model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo A"){
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'neto_b',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'total_ar',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'total_iva',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'total',
                    'format' => ['decimal',2],
                ],

            ], 
        ]);
    }else if($model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo B"){
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'reparto',
                    'label' => 'IMPORTE BULTOS',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'total_iva',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'total',
                    'format' => ['decimal',2],
                ],

            ], 
        ]);
    }
    ?>
    </div>
</div>
