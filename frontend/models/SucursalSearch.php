<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Sucursal;

/**
 * SucursalSearch represents the model behind the search form of `common\models\Sucursal`.
 */
class SucursalSearch extends Sucursal
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idsucursal'], 'integer'],
            [['direccion', 'empresa_idempresa', 'localidad_idlocalidad', 'nombre'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sucursal::find();
        $query->joinWith(['empresaIdempresa as empresa']);
        $query->joinWith(['localidadIdlocalidad.departamentoIddepartamento.provinciaIdprovincia as provincia', 
        'localidadIdlocalidad.departamentoIddepartamento as departamento', 'localidadIdlocalidad as localidad']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idsucursal' => $this->idsucursal,
        ]);

        $query->filterWhere([ 'nombre' => $this->nombre]);
        $query->andFilterWhere(['like', 'empresa.nombre', $this->empresa_idempresa]);
        $query->andFilterWhere(['like', 'CONCAT(provincia.nombre, " - ", departamento.nombre, " - ", localidad.nombre, " - ", direccion )', $this->direccion]);



        return $dataProvider; 
    }
}
