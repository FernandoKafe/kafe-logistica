<?php

use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

use common\models\Cliente;

/* @var $this yii\web\View */
/* @var $model common\models\Remito */
$idCliente = Yii::$app->getRequest()->getQueryParam('id');

$this->title = Yii::t('app', 'Agregar tipo a Cliente');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tipos de cliente'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<?php
    echo $this->render('view', [
        'model' => Cliente::findOne($idCliente),
        ])
?>

<!--Añadir tipo de cliente-->
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Agregar tipo');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

    <?= $this->render('../cliente-has-tipo/_form', [
        'model' => $model,
    ]) ?>
    </div>
</div>

<!--Añadir tarifario-->

<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Agregar tarifario');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php
        $model = Cliente::find()->where(['idcliente' => $idCliente])->one();
        echo $this->render('add-tarifario', [
                'model' => $model,
        ]) ?>
    </div>
</div>
