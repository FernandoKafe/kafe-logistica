<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TarifarioVino */

$this->title = Yii::t('app', 'Update Tarifario Vino: ' . $model->idtarifario_vino, [
    'nameAttribute' => '' . $model->idtarifario_vino,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tarifario Vinos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idtarifario_vino, 'url' => ['view', 'id' => $model->idtarifario_vino]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<h1><i class="fa fa-download"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario modificación') ?></h2>
        <div class="clearfix"></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>