<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\LiquidacionVinoClienteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="liquidacion-vino-cliente-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'idliquidacion_vino_cliente') ?>

    <?= $form->field($model, 'total') ?>

    <?= $form->field($model, 'periodo_inicio') ?>

    <?= $form->field($model, 'periodo_fin') ?>

    <?= $form->field($model, 'nombre') ?>

    <?php // echo $form->field($model, 'estadoliquidacion_idestadoliquidacion') ?>

    <?php // echo $form->field($model, 'liquidacionformapago_idliquidacionformapago') ?>

    <?php // echo $form->field($model, 'cliente_idcliente') ?>

    <?php // echo $form->field($model, 'cheque_idcheque') ?>

    <?php // echo $form->field($model, 'pago_parcial') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
