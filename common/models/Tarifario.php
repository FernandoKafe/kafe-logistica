<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tarifario".
 *
 * @property int $idtarifario
 * @property string $nombre
 * @property string $tipo 
 *
 * @property Cliente[] $clientes
 * @property TarifarioHasPrecio[] $tarifarioHasPrecios
 */
class Tarifario extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tarifario';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],[['tipo'], 'string'], 
            [['nombre'], 'string', 'max' => 45],
            [['nombre'], 'unique'], 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idtarifario' => Yii::t('app', 'Idtarifario'),
            'nombre' => Yii::t('app', 'Nombre'),
            'tipo' => Yii::t('app', 'Tipo'), 
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientes()
    {
        return $this->hasMany(Cliente::className(), ['tarifario_idtarifario' => 'idtarifario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarifarioHasPrecios()
    {
        return $this->hasMany(TarifarioHasPrecio::className(), ['tarifario_idtarifario' => 'idtarifario']);
    }

     /**
     * Funcion que retorna todos los tipos de condicion fiscal
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAllTipo(){
        return self::find()->all();
    } 

    /**
     * Función que busca un tarifario con nombre
     * @param string $q 
     * @return [id, text] Arreglo de identificador y nombre
     */
    public static function getTarifarioCompleto($q){
        $query = self::find();
        $query->select(['idtarifario AS id', 'tarifario.nombre AS text',])
            ->where(['like', 'tarifario.nombre', $q])
            ->limit(20);
        return $query->asArray()->all();
    }

    public static function getZonasTarifarioByLocalidad($localidadId, $tarifarioId){
        return self::find()->joinWith(['tarifarioHasPrecios.zonaIdzona.zonaHasLocalidads'])
        ->where(['zona_has_localidad.localidad_idlocalidad' => $localidadId])
        ->andFilterWhere(['idtarifario' => $tarifarioId])
        ->one();
    }
}
