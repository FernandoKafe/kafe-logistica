<?php

namespace frontend\controllers;

use Yii;
use common\models\GuiaDomicilio;
use frontend\models\GuiaDomicilioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GuiaDomicilioController implements the CRUD actions for GuiaDomicilio model.
 */
class GuiaDomicilioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all GuiaDomicilio models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new GuiaDomicilioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single GuiaDomicilio model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new GuiaDomicilio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new GuiaDomicilio();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->session->setflash('success', "Remito creado con éxito.");
            }
            return $this->redirect(['view', 'id' => $model->idorden_grupal_domicilio]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing GuiaDomicilio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idorden_grupal_domicilio]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing GuiaDomicilio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if($model->ordengrupaldomicilioIdordengrupaldomicilio->liquidacioncliente_idliquidacioncliente || $model->ordengrupaldomicilioIdordengrupaldomicilio->liquidacionrepartidor_idliquidacionrepartidor){
            Yii::$app->session->setflash('error', "No se puede eliminar la guía por que esta liquidada.");
        }else{
            $model->delete();
        }
        return $this->redirect(Yii::$app->request->referrer); 
    }

    /**
     * Finds the GuiaDomicilio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return GuiaDomicilio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = GuiaDomicilio::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
