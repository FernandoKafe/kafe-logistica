<?php

namespace common\models;


use Yii; 

/**
 * This is the model class for table "{{%liquidacion}}".
 *
 * @property int $idliquidacion
 * @property int $cliente_idcliente
 * @property double $guias_logistica
 * @property double $importe_bultos
 * @property double $importe_guias
 * @property double $iva
 * @property string $periodo_inicio
 * @property string $son_pesos
 * @property double $subtotal
 * @property double $total_bultos
 * @property double $total_iva
 * @property double $total
 * @property string $periodo_fin
 * @property string $periodo
 * @property int $estadoliquidacion_idestadoliquidacion
 * @property double $pago_parcial
 * @property int $liquidacionformapago_idliquidacionformapago
 * @property int $cheque_idcheque
 * @property double $total_contra
 * @property string $creado
 * @property int $confirmada
 * @property string $fecha_pago
* @property int $bultos 
* @property int $volumen 
 *
 * @property Cheque $chequeIdcheque
 * @property Cliente $clienteIdcliente
 * @property EstadoLiquidacion $estadoliquidacionIdestadoliquidacion
 * @property LiquidacionFormaPago $liquidacionformapagoIdliquidacionformapago
 * @property LiquidacionHasRemito[] $liquidacionHasRemitos
 */
class Liquidacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%liquidacion}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cliente_idcliente', 'periodo_inicio', 'periodo_fin', 'periodo'], 'required'],
            [['cliente_idcliente', 'estadoliquidacion_idestadoliquidacion', 'liquidacionformapago_idliquidacionformapago', 'confirmada', 'bultos', 'volumen'], 'integer'],
            [['guias_logistica', 'importe_bultos', 'importe_guias', 'iva', 'subtotal', 'total_bultos', 'total_iva', 'total', 'pago_parcial', 'total_contra'], 'number'],
            [['periodo_inicio', 'periodo_fin', 'creado', 'fecha_pago'], 'safe'],
            [['son_pesos'], 'string'],
            [['periodo'], 'string', 'max' => 100],
            [['cliente_idcliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['cliente_idcliente' => 'idcliente']],
            [['estadoliquidacion_idestadoliquidacion', 'cliente_idcliente', 'liquidacionformapago_idliquidacionformapago', 'cheque_idcheque'], 'integer'],
            [['estadoliquidacion_idestadoliquidacion'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoLiquidacion::className(), 'targetAttribute' => ['estadoliquidacion_idestadoliquidacion' => 'idestado_liquidacion']],
            [['liquidacionformapago_idliquidacionformapago'], 'exist', 'skipOnError' => true, 'targetClass' => LiquidacionFormaPago::className(), 'targetAttribute' => ['liquidacionformapago_idliquidacionformapago' => 'idliquidacion_forma_pago']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idliquidacion' => Yii::t('app', 'Idliquidacion'),
            'cliente_idcliente' => Yii::t('app', 'Cliente'),
            'importe_bultos' => Yii::t('app', 'Importe Bultos'),
            'guias_logistica' => Yii::t('app', 'Guías Logística'),
            'importe_guias' => Yii::t('app', 'Importe Guías'),
            'iva' => Yii::t('app', 'IVA'),
            'periodo' => Yii::t('app', 'Nombre Periodo'),
            'periodo_inicio' => Yii::t('app', 'Periodo Inicio'),
            'periodo_fin' => Yii::t('app', 'Periodo Fin'),
            'son_pesos' => Yii::t('app', 'Son Pesos'),
            'subtotal' => Yii::t('app', 'Subtotal'),
            'total_bultos' => Yii::t('app', 'Total Bultos'),
            'total_iva' => Yii::t('app', 'Total IVA'),
            'total' => Yii::t('app', 'TOTAL'),
            'estadoliquidacion_idestadoliquidacion' => Yii::t('app', 'Estado'),
            'pago_parcial' => Yii::t('app', 'Pago Parcial'),
            'liquidacionformapago_idliquidacionformapago' => Yii::t('app', 'Forma de Pago'),
            'total_contra' => Yii::t('app', 'TOTAL contra reembolso'),
            'cheque_idcheque' => Yii::t('app', 'Cheque Idcheque'),
            'creado' => Yii::t('app', 'Creado'),
            'confirmada' => Yii::t('app', 'Confirmada'), 
            'fecha_pago' => Yii::t('app', 'Fecha de pago'),
            'bultos' => Yii::t('app', 'Bultos'),
            'volumen' => Yii::t('app', 'Volumen'),
        ];
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getChequeIdcheque()
    {
        return $this->hasOne(Cheque::className(), ['idcheque' => 'cheque_idcheque']);
    }
    /**
	* @return \yii\db\ActiveQuery
	*/
    public function getLiquidacionformapagoIdliquidacionformapago()
    {
        return $this->hasOne(LiquidacionFormaPago::className(), ['idliquidacion_forma_pago' => 'liquidacionformapago_idliquidacionformapago']); 
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClienteIdcliente()
    {
        return $this->hasOne(Cliente::className(), ['idcliente' => 'cliente_idcliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoliquidacionIdestadoliquidacion()
    {
        return $this->hasOne(EstadoLiquidacion::className(), ['idestado_liquidacion' => 'estadoliquidacion_idestadoliquidacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacionHasRemitos()
    {
        return $this->hasMany(LiquidacionHasRemito::className(), ['liquidacion_idliquidacion' => 'idliquidacion']);
    }

    public static function getTotal($cliente, $fieldName)
    {
        $total = 0;

        foreach ($cliente as $c) {
            $total += $c[$fieldName];
        }

    return $total;
    }

    public static function getBultosSum($remitos) {
        $cant_bultos = 0;
        if($remitos){
            foreach($remitos as $r){
                $cant_bultos += $r->cantidad_bultos;
            }
            return $cant_bultos;
        }    
    }

    public static function getVolumenesSum($remitos) {
        $cant_bultos = 0;
        if($remitos){
            foreach($remitos as $r){
                $cant_bultos += $r->volumen;
            }
            return $cant_bultos;
        }    
    }

    public static function getGuiasClienteSum($remitos) {
        $suma = 0;
        if($remitos){
            foreach($remitos as $r){
                if( (($r->tipo_remito == 'A') || ($r->tipo_remito == 'B')) && ($r->facturado_a == 'Cliente') ){
                    $suma += $r->total_guia; 
                }      
            }
            return $suma;
        }    
    }

    public static function getGuiasLogisticaSum($remitos) {
        $suma = 0;
        if($remitos){
            foreach($remitos as $r){
                if( (($r->tipo_remito == 'A') || ($r->tipo_remito == 'B') || ($r->tipo_remito == 'R')) && ($r->facturado_a == 'Vertiente')){
                    $suma += $r->total_guia;
                }      
            }
            return $suma;
        }    
    }

    public static function getImporteSum($remitos) {
        $fechaAnterior = 0;
        if($remitos){
            foreach($remitos as $r){
                if($r->terminal == 0){
                    $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, $r->sucursalIdsucursal->localidadIdlocalidad->zonaHasLocalidad->zona_idzona);
                }else {
                    $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, 5);
                }
                if($r->clienteIdcliente->tarifarioIdtarifario->tipo == "Tipo A"){
                    if($r->cantidad_bultos >= 1){
                        $suma += $tarHasPrecio->precio;
                    }else{
                        $suma += 0;
                    }
                   $suma += ($r->volumen * $tarHasPrecio->precio_adicional);
                   if($r->cantidad_bultos > 1){
                       $suma += (($r->cantidad_bultos - 1) * $tarHasPrecio->precio_adicional);
                   }
               }else if($r->clienteIdcliente->tarifarioIdtarifario->tipo == "Tipo B"){
                    if($fechaAnterior == $r->remitofecha){
                        $suma += (($r->cantidad_bultos + $r->volumen) * $tarHasPrecio->precio_adicional);
                   }else{
                        if($r->cantidad_bultos == 0)
                            $suma->importe_bultos += ($r->volumen * $tarHasPrecio->precio_adicional);
                        else
                            $suma += ($tarHasPrecio->precio + (($r->cantidad_bultos + $r->volumen - 1) * $tarHasPrecio->precio_adicional));
                   }
                   $fechaAnterior = $r->remitofecha;
               }
            }
        } 
        return $suma; 
    }

    public static function getContraSum($remitos) {
        $suma = 0;
        if($remitos){
            foreach($remitos as $r){
                    $suma += $r->total_contrarembolso; 
            }
            return $suma;
        }    
    }
}
