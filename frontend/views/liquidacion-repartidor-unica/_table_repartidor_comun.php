<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-03-27
 * Time: 12:57
 */
use \yii\helpers\ArrayHelper;
use common\models\Remito;

$remitosClientes = Remito::getRemitosPorLiquidacionByCliente($liquidacionComun->idliquidacion_repartidor);
//echo "<pre>" , var_dump($remitosClientes) , "</pre>";
if($remitosClientes) {
$remitosComunesSum = Remito::getSumatoriaRemitosComunesPorLiquidacionByCliente($liquidacionComun->idliquidacion_repartidor);
    ?>
    <br>
    <table style="table-layout: fixed; width: 100% ;"> 
        <thead>
        <tr>
            <th style="width:10%; text-align:center ">Cliente</th>
            <?php for ($i = 1; $i <= 31; $i++) { ?>
                <th style="width:4%;  text-align:center "><?= $i ?></th>
            <?php } ?>
            <th style="width:5%;  text-align:center">TOTAL</th><!--Resto de celdas de fila cliente-->
        </tr>
        </thead>
        <tbody> 
        <?php
        $clienteAnterior = 0;
        foreach ($remitosClientes as $rc) {
            $totalBultos = 0;
            $totalVolumen = 0;
            $totalTerminal = 0;
            $totalTerminalexc = 0;
            ?>

            <tr style="background-color: #ddd"><!--Fila cliente-->
                <td style="border-right: 1px solid #ccc;width:10%">
                    <strong><?= $rc->clienteIdcliente->razonsocial . " (" . $rc->clienteIdcliente->tarifarioIdtarifario->tipo . ")" ?></strong>
                </td><!--Primer celda fila cliente-->
                <?php for ($i = 0; $i <= 32; $i++) { ?>
                    <td style="border-right: 1px solid #ccc;width:4%"></td><!--Resto de celdas de fila cliente-->
                <?php } ?>
            </tr>

            <tr style="border-bottom: 1px solid #ccc;">
                <td style="border-right: 1px solid #ccc; width:10%; text-align:center"><strong>BULTOS</strong></td>
                <?php
                for ($i = 1; $i <= 32; $i++) {
                    $flag = 0;
                    foreach ($remitosComunesSum as $cc) {

                        if (($rc->cliente_idcliente == $cc->cliente_idcliente) && ($cc->cantidad_bultos)) {
                            $date = date('d', strtotime($cc->remitofecha));
                            $date = intval($date);
                            if (($cc->cantidad_bultos != 0) && ($date == $i)) {
                                $totalBultos += $cc->cantidad_bultos;
                                ?>
                                    <td style="border-right: 1px solid #ccc;width:4%; text-align:center"><?= $cc->cantidad_bultos ?></td>
                                <?php
                                $flag = 1;
                                break;
                            }
                        }
                    }
                    if ($flag == 0) {
                        if ($i < 32) {
                            ?>
                            <td style="border-right: 1px solid #ccc;width:4%"></td>
                            <?php
                        } else if ($i == 32) { ?>
                            <td style="border-right: 1px solid #ccc;width:5%; text-align:center">
                                <strong><?= $totalBultos ?></strong></td>
                            <?php
                        }
                    }
                } ?>
            </tr>

            <tr style="border-bottom: 1px solid #ccc;">
                <td style="border-right: 1px solid #ccc; width:10%; text-align:center"><strong>EXC</strong></td>
                <?php
                for ($i = 1; $i <= 32; $i++) {
                    $flag = 0;
                    foreach ($remitosComunesSum as $cc) {
                        if (($rc->cliente_idcliente == $cc->cliente_idcliente)) {
                            $date = date('d', strtotime($cc->remitofecha));
                            $date = intval($date);
                            if ((($cc->volumen != 0) || ($cc->cantidad_bultos != 0)) && ($date == $i)) {
                                    $totalVolumen += $cc->volumen; ?>
                                    <td style="border-right: 1px solid #ccc;width:4%; text-align:center"><?= $cc->volumen ?></td>
                                <?php
                                $flag = 1;
                                break;
                            }
                        }
                    }
                    if ($flag == 0) {
                        if ($i < 32) {
                            ?>
                            <td style="border-right: 1px solid #ccc;width:4%"></td>
                        <?php } else { ?>
                            <td style="border-right: 1px solid #ccc;width:5%; text-align:center">
                                <strong><?= $totalVolumen ?></strong></td>
                        <?php }
                    }
                } ?>
            </tr>

            <tr style="border-bottom: 1px solid #ccc;">
                <td style="border-right: 1px solid #ccc; width:10%; text-align:center"><strong>TERMINAL</strong></td>
                <?php
                for ($i = 1; $i <= 32; $i++) {
                    $flag = 0;
                    foreach ($remitosComunesSum as $cc) {
                        if (($rc->cliente_idcliente == $cc->cliente_idcliente) && ($cc->terminal)) {
                            $date = date('d', strtotime($cc->remitofecha));
                            $date = intval($date);
                            if (($cc->terminal != 0) && ($date == $i)) {
                                    $totalTerminal += $cc->terminal ?>
                                    <td style="border-right: 1px solid #ccc;width:4%; text-align:center"><?= $cc->terminal ?></td>
                                <?php
                                $flag = 1;
                                break;
                            }
                        }
                    }
                    if ($flag == 0) {
                        if ($i < 32) {
                            ?>
                            <td style="border-right: 1px solid #ccc;width:4%"></td>
                        <?php } else { ?>
                            <td style="border-right: 1px solid #ccc;width:5%; text-align:center">
                                <strong><?= $totalTerminal ?></strong></td>
                        <?php }
                    }
                } ?>
            </tr>

            <tr style="border-bottom: 1px solid #ccc;">
                <td style="border-right: 1px solid #ccc; width:10%; text-align:center"><strong>EXC TER</strong></td>
                <?php
                for ($i = 1; $i <= 32; $i++) {
                    $flag = 0;
                    foreach ($remitosComunesSum as $cc) {
                        if (($rc->cliente_idcliente == $cc->cliente_idcliente) && ($cc->terminal_exc)) {
                            $date = date('d', strtotime($cc->remitofecha));
                            $date = intval($date);
                            if (($cc->terminal != 0) && ($date == $i)) {
                                    $totalTerminalexc += $cc->terminal_exc ?>
                                    <td style="border-right: 1px solid #ccc;width:4%; text-align:center"><?= $cc->terminal_exc ?></td>
                                <?php
                                $flag = 1;
                                break;
                            }
                        }
                    }
                    if ($flag == 0) {
                        if ($i < 32) {
                            ?>
                            <td style="border-right: 1px solid #ccc;width:4%"></td>
                        <?php } else { ?>
                            <td style="border-right: 1px solid #ccc;width:5%; text-align:center">
                                <strong><?= $totalTerminalexc ?></strong></td>
                        <?php }
                    }
                } ?>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    <br>
    <?php
}
    ?>