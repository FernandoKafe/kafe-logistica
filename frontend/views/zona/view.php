<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\ZonaHasLocalidad;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Zona */
$idZona = Yii::$app->getRequest()->getQueryParam('id');


$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zonas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zona-view">
    <h1>Zona: <?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Detalle') ?></h2>
            <div class="clearfix"></div>
        </div>

        <p>
            <?= Html::a(Yii::t('app', 'Inicio'), ['index'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->idzona], ['class' => 'btn btn-success']) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                //'idzona',
                'nombre',
            ],
        ]) ?>
    </div>
</div>

<!--Localidades Asociadas-->
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Localidades asociadas');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php
        $dataProvider = new ActiveDataProvider([
            'query' => ZonaHasLocalidad::find()->where(['zona_idzona' => $idZona]),
        ]);
        $dataProvider->pagination->pageSize=20;
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'localidad_idlocalidad',
            'label' => 'Localidad',
            'value' => function($data){
                return $data->localidadIdlocalidad->nombre 
                ." - ".$data->localidadIdlocalidad->departamentoIddepartamento->nombre
                ." - ".$data->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
            }
        ],
        /*[
            'attribute' => 'empresatransporteIdempresatransporte.razonsocial',
            'label' => 'Empresa de transporte'
        ],*/
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Acciones',
            'headerOptions' => ['style' => 'color:#337ab7'],
            'template' => '{view}{update}{delete}',
            'buttons' => [
              'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'Ver'),
                    ]);
              },

              'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'Actualizar'),
                    ]);
                },

                'delete' => function ($url, $model) {
                      return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                        'title' => Yii::t('app', 'Eliminar'),
                        'data' => [
                            'confirm' => Yii::t('app', '¿Está seguro de querer borrar esta localidad?'),
                            'method' => 'post',
                        ],
                      ]);
                  },

            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    $url ='../zona_has_localidad/view?zona_idzona='.$model->zona_idzona."&localidad_idlocalidad=".$model->localidad_idlocalidad;
                    return $url;
                }

                if ($action === 'update') {
                    $url ='../zona_has_localidad/update?zona_idzona='.$model->zona_idzona."&localidad_idlocalidad=".$model->localidad_idlocalidad;
                    return $url;
                }

                if ($action === 'delete') {
                    $url ='../zona_has_localidad/delete?zona_idzona='.$model->zona_idzona."&localidad_idlocalidad=".$model->localidad_idlocalidad;
                    return $url;
                }
            }
        ],
    ],
    ]); ?>
    </div>
</div>