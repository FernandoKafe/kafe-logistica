<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Liquidacion */

$this->title = Yii::t('app', 'Crear Liquidación');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Crear estado de liquidación'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Formulario') ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div> 
</div> 
