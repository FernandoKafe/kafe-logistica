<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\RemitoVino */

$this->title = $model->idremito_vino;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Remito Vinos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="remito-view">
    <h1><i class="fa fa-file"></i> Remito por porcentajeNº: <?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Detalle') ?></h2>
            <div class="clearfix"></div>
        </div>
        <p>
            <?= Html::a('Inicio', ['index'], ['class' => 'btn btn-primary']);?>
            <?= Html::a(Yii::t('app', 'Actualizar'), ['update',
                'id' => $model->idremito_vino,],
                ['class' => 'btn btn-success']) ?>
                <!--<?//= Html::a('Cargar otro', ['create'], ['class' => 'btn btn-dark']);?>-->
        </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'numero_remito',
            'cant_cajas',
            'cant_botellas',
            [
                'attribute' => 'valor_declarado',
                'format' => ['decimal',2],
            ],
            [
                'attribute' => 'total_facturacion',
                'format' => ['decimal',2],
            ],
        ],
    ]) ?>

</div>
</div>