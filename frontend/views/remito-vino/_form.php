<?php

use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\View;


use common\models\Cliente;
use \common\models\Repartidor;

$script = <<< 'SCRIPT'
    $('#terminal-checkbox').change(function(){
      var x = document.getElementById("formulario_guia");
      if(this.checked){
        x.style.display = "";
      }else{
        x.style.display = "none";
      }
    });
SCRIPT;
$this->registerJs($script, View::POS_END);
/* @var $this yii\web\View */
/* @var $model common\models\RemitoVino */
/* @var $form yii\widgets\ActiveForm */
$idguia = Yii::$app->getRequest()->getQueryParam('guia_numero');

$repartidor = ArrayHelper::map(Repartidor::find()->all(),'idrepartidor','apellido');
?>

<div class="remito-vino-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-2"><?= $form->field($model, 'numero_remito')->textInput() ?></div>

    <div class="col-md-4">
    <?php
        $sucursalRecibe = empty($model->sucursalIdsucursal) ? '' :
        $model->sucursalIdsucursal->empresaIdempresa->nombre
        .", ".$model->sucursalIdsucursal->nombre;

        echo $form->field($model, 'sucursal_idsucursal')->widget(Select2::classname(), [
        'initValueText' => $sucursalRecibe,
        'language' => 'es',
        'options' => ['placeholder' => 'Seleccionar Empresa ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'ajax' => [
            'url' => Url::to(['/sucursal/listado']),
            'dataType' => 'json',
            'data' => new JsExpression('function(params) {return {q:params.term}; }')
            ],
        ],
        ])->label('<a href="../empresa/create" target="_blank"><i class="fa fa-plus green"></i></a> Empresa&nbsp&nbsp(<a href="../sucursal/create" target="_blank"><i class="fa fa-plus green"></i></a>Sucursal)');
        ?>
    </div>

    <div class="col-md-1 col-xs-12"><?= $form->field($model, 'cant_cajas')->textInput() ?></div>
   
   <div class="col-md-1 col-xs-12"><?= $form->field($model, 'cant_botellas')->textInput() ?></div>

    <div class="col-md-4 col-xs-12"><?= $form->field($model, 'valor_declarado')->textInput() ?></div>

    <div class="col-md-2 col-xs-12 hide"><?= $form->field($model, 'total_facturacion')->textInput() ?></div>

    <?php
        if($model->guia_numero == ""){
          $formularioShow = "style='display: none'";
          $checkTerminal = "";
        } else {
          $formularioShow = "";
          $checkTerminal = "checked";
        }
    ?>
    <div class="col-md-3 checkbox">
      <label>
        <input type="checkbox" id="terminal-checkbox" <?= $checkTerminal ?>> Terminal
      </label>
    </div>

    <div class="x_panel" <?php echo $formularioShow?> id="formulario_guia">
        <div class="x_title">
          <h2>Guía</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <div class="col-md-3">
            <?php
              $empresaTransporte = empty($model->guiaIdtransporte) ? '' :
              $model->guiaIdtransporte->razonsocial;

              echo $form->field($model, 'guia_idtransporte')->widget(Select2::classname(), [
                'initValueText' => $empresaTransporte,
                'language' => 'es',
                'options' => ['placeholder' => 'Seleccionar Empresa de Transporte ...'],
                'pluginOptions' => [
                  'allowClear' => true,
                  'minimumInputLength' => 3,
                  'ajax' => [
                    'url' => Url::to(['/empresa-transporte/listado']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) {return {q:params.term}; }')
                  ],
                ],
              ])->label('<a href="../empresa-transporte/create" target="_blank"><i class="fa fa-plus green"></i></a> Empresa de transporte');
            ?>
          </div>

          <div class="col-md-2 col-xs-12"><?= $form->field($model, 'guia_numero')->textInput() ?></div>

          <div class="col-md-2 col-xs-12"><?= $form->field($model, 'guia_tipo')->dropDownList([ 'No Aplica' => 'No Aplica', 'A' => 'A', 'B' => 'B', 'R' => 'R', ])?></div>

          <div class="col-md-2 col-xs-12"><?= $form->field($model, 'guia_total')->textInput() ?></div>

          <div class="col-md-3">
            <?= $form->field($model, 'guia_facturada_a')->dropDownList([
              'Cliente' => 'Cliente',
              'Vertiente' => 'Vertiente', ])
            ?>
          </div>

        </div>
    </div>


    <div class="form-group col-md-12 col-xs-12">
        <?php
            echo Html::a('Volver', ['orden-grupal-vinos/index'], ['class' => 'btn btn-primary']);
            echo Html::submitButton('Guardar', ['class' => 'btn btn-success']);
        ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
