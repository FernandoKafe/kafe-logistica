<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use common\widgets\Alert;
 
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use yiister\gentelella\assets\Asset;


AppAsset::register($this);
Asset::register($this);

$menuEmpresas = [];
$menuEmpresas = 
[
    ['label' => 'Inicio',       'url' => ['/site/index'], "icon" => "fa fa-truck"],
    ['label' => 'Remitos',
        'url' => '#',
        'icon' => 'fas fa-archive',
        'items' => [
        ['label' => 'Común',      'url' => ['/remito/index'], "icon" => "far fa-file"],
        ['label' => 'Domicilio',        'url' => ['/orden-grupal-domicilio/index'], "icon" => "fas fa-home"],
        ['label' => 'Grupal',        'url' => ['/orden-retiro-grupal/index'], "icon" => "fas fa-copy"],
        ['label' => 'Porcentaje',        'url' => ['/remito-porcentaje/index'], "icon" => "fas fa-percent"],
        ['label' => 'Vino',        'url' => ['/orden-grupal-vinos/index'], "icon" => "fas fa-glass"],
        ]
    ],
];

$menuAdministracion = [];
$menuAdministracion = 
[
    ['label' =>
        'Liquidación Cliente',
        "url" => "#",
        "icon" => "money",
        //'options' => ['style' => 'background-color: green;'],
        'items'=>[
            ['label' => 'Liquidación común',        'url' => ['/liquidacion/index']],
            ['label' => 'Liquidación domicilio', 'url' => ['/liquidacion-domicilio-cliente/index']],
            ['label' => 'Liquidación porcentaje', 'url' => ['/liquidacion-porcentaje-cliente/index']],
            ['label' => 'Liquidación vinos', 'url' => ['/liquidacion-vino-cliente/index']],
        ],
    ],
    ['label' =>
        'Liquidación Repartidor',
        "url" => "#",
        "icon" => "money",
        'items'=>[
            ['label' => 'Liquidación única', 'url' => ['/liquidacion-repartidor-unica/index']],
            ['label' => 'Liquidación común', 'url' => ['/liquidacion-repartidor/index']],
            ['label' => 'Liquidación domicilio', 'url' => ['/liquidacion-domicilio-repartidor/index']],
            ['label' => 'Liquidación porcentaje', 'url' => ['/liquidacion-porcentaje-repartidor/index']],
            ['label' => 'Liquidación vinos', 'url' => ['/liquidacion-vino-repartidor/index']],

        ],
    ],
    ['label' => 'Contra Reembolso',    'url' => ['/rendicion/index?tipo=0'], "icon" => "fal fa-file-text-o"],
    ['label' => 'Rendiciones',    'url' => ['/rendicion/index?tipo=1'], "icon" => "fal fa-file"],
    ['label' =>
        'Análisis',
        'url' => ['/liquidacion/create-analisis?analisis=1'],
        "icon" => "fal fa-bar-chart-o",
    ],
    ['label' =>
        'Configuración',
        "url" => "#",
        "icon" => "far fa-cog",
        'items'=>[
            ['label' => 'Cargas',        'url' => ['/carga/index']],
            ['label' => 'Clientes',     'url' => ['/cliente/index'], "icon" => "address-book"],
            ['label' => 'Condiciones Fiscal', 'url' => ['/condicion-fiscal/index']],
            ['label' => 'Departamentos',        'url' => ['/departamento/index']],
            ['label' => 'Empresas',
                'url' => '#',
                'icon' => 'fal fa-building',
                'items' => [
                ['label' => 'Empresas',      'url' => ['/empresa/index'], "icon" => "fal fa-building"],
                ['label' => 'Sucursal',        'url' => ['/sucursal/index'], "icon" => "fas fa-bullseye"],
                ]
            ],
            ['label' => 'Estados de envío', 'url' => ['/estado/index']],
            ['label' => 'Formas de Pago',    'url' => ['/forma-pago/index']],
            ['label' => 'Localidades',        'url' => ['/localidad/index']],
            ['label' => 'Porcentajes',         'url' => ['/porcentaje/index']],
            ['label' => 'Productos',         'url' => ['/producto/index']],
            ['label' => 'Repartidores',   'url' => ['/repartidor/index'], "icon" => "fal fa-male"],
            ['label' => 'Vehículos',        'url' => ['/vehiculo/index']],
            ['label' => 'Servicios',        'url' => ['/servicio/index']],
            ['label' => 'Tarifario común',        'url' => ['/tarifario/index']],
            ['label' => 'Tarifario domicilio',             'url' => ['/tarifario-domicilio/index']],
            ['label' => 'Tarifario porcentaje',        'url' => ['/pagos-porcentajes-repartidor/index']],
            ['label' => 'Tarifario vinos',             'url' => ['/tarifario-vino/index']],
            ['label' => 'Transporte',    'url' => ['/empresa-transporte/index'], "icon" => "fas fa-bus"],
            ['label' => 'Zonas',             'url' => ['/zona/index']],
        ],
    ],
];

$menuItems = [];

if (Yii::$app->User->can('@kafe-logistica/remito/*')) {
    $menuItems = $menuEmpresas;
} else if (Yii::$app->User->can('@kafe-logistica/*')) {
    $menuItems = array_merge($menuEmpresas, $menuAdministracion);
}

?>

<?php $this->beginPage(); ?>

<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta charset="<?= Yii::$app->charset ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="nav-<?= !empty($_COOKIE['menuIsCollapsed']) && $_COOKIE['menuIsCollapsed'] == 'true' ? 'sm' : 'md' ?>" >
<?php $this->beginBody(); ?>
<div class="container body">
    <div class="main_container">

        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0; background-color: #EDEDED;">
                    <a href="index" class="site_title">
                        <?php echo Html::img('@web/img/logo/LOGO-NAV.png', ['class' => 'img-responsive', 'width'=>'70%', 'style' => 'margin: 0 auto; padding: 0']) ?>

                        <span>Vertiente</span>
                    </a>
                </div>
                <div class="clearfix">
                </div>
                <!-- menu prile quick info -->
                <?php if(!Yii::$app->user->isGuest) {?>
                <div class="profile">
                    <div class="profile_pic">
                        <?= Html::img('@web/img/user.png',['class' => 'img-circle profile_img'])?>
                    </div>
                    <div class="profile_info">
                        <span>Bienvenido,</span>
                        <h2><?= Yii::$app->user->identity->getNombreCompleto()?></h2>
                    </div>
                </div>
                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">

                    <div class="menu_section">
                        <h3>General</h3>
                        <?=\yiister\gentelella\widgets\Menu::widget(
                            [
                                "items" => $menuItems
                            ]
                        );
                        ?>
                    </div>
                </div>
                <?php } ?>
                <!-- /menu prile quick info -->
                <br />
                <!-- /sidebar menu -->
                <!-- /menu footer buttons -->
            </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav class="" role="navigation">
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                     <?php if(!Yii::$app->user->isGuest){?>
                         <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <?= Html::img('@web/img/user.png')?><?= Yii::$app->user->identity->getNombreCompleto()?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <?= '<li>'
                                . Html::beginForm(['/site/logout'], 'post')
                                . Html::submitButton(
                                    '<i class="fa fa-sign-out pull-right"></i>Salir',
                                    ['class' => 'btn btn-link logout']
                                )
                                . Html::endForm()
                                . '</li>'?>
                            </ul>
                        </li>
                    <?php }?>
                    </ul>
                </nav>
            </div>

        </div>
        <!-- /top navigation -->
            
        <!-- page content -->
        <div class="right_col" role="main">
            <?php if (isset($this->params['h1'])): ?>
                <div class="page-title">
                    <div class="title_left">
                        <h1><?= $this->params['h1'] ?></h1>
                    </div>
                    <div class="title_right">
                        <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search for...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">Go!</button>
                            </span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <div class="clearfix"></div>
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= \lavrentiev\widgets\toastr\NotificationFlash::widget(['options' => [
                    'positionClass' => \lavrentiev\widgets\toastr\Notification::POSITION_TOP_CENTER,
                    'progressBar' => true
                ]
            ]) ?>
            <?= $content ?>
        </div>
        <!-- /page content -->
        <!-- footer content -->
        <footer>
            <div class="pull-right">
                <a href="https://kafesistemas.com.ar" rel="nofollow" target="_blank">Kafesistemas</a><br />
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>

</div>
<!-- /footer content -->
<?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>