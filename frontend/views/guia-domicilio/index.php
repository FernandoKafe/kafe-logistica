<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\GuiaDomicilioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Guia Domicilios');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="x_panel">
    <div class="x_title">
        <h2>Listado</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a(Yii::t('app', 'Crear Guía Domicilio'), ['create'], ['class' => 'btn btn-success']) ?>
        </p> 

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],

                //'idorden_grupal_domicilio',
                [
                    'attribute' => 'ordengrupaldomicilio_idordengrupaldomicilio',
                    'value' => function($data){
                        return $data->ordengrupaldomicilioIdordengrupaldomicilio->numero_orden;
                    }
                ],
                'numero_guia',
                'guia_tipo',
                'empresa_destino',
                [
                    'attribute' => 'monto',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'creado',
                    'label' => 'Fecha y Hora creación',
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
