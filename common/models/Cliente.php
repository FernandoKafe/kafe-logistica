<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cliente".
 *
 * @property int $idcliente
 * @property string $razonsocial
 * @property string $cuit
 * @property int $telefono
 * @property int $condicionfiscal_idcondicionfiscal
 * @property int $tarifario_idtarifario
 * @property string $mail
 * @property string $domicilio
 * @property int $localidad_idlocalidad
 * @property int $tarifariovino_idtarifariovino
 * @property int $tarifariodomicilio_idtarifariodomicilio
 *
 * @property CondicionFiscal $condicionfiscalIdcondicionfiscal
 * @property Localidad $localidadIdlocalidad
 * @property Tarifario $tarifarioIdtarifario
 * @property TarifarioDomicilio $tarifariodomicilioIdtarifariodomicilio
 * @property TarifarioVino $tarifariovinoIdtarifariovino
 * @property ClienteHasTipo[] $clienteHasTipos
 * @property ClienteTipo[] $clientetipoIdclientetipos
 * @property Liquidacion[] $liquidacions
 * @property LiquidacionDomicilioCliente[] $liquidacionDomicilioClientes
 * @property LiquidacionPorcentajeCliente[] $liquidacionPorcentajeClientes
 * @property LiquidacionVinoCliente[] $liquidacionVinoClientes
 * @property OrdenGrupalDomicilio[] $ordenGrupalDomicilios
 * @property OrdenGrupalVinos[] $ordenGrupalVinos
 * @property OrdenRetiroGrupal[] $ordenRetiroGrupals 
 * @property PlanillaContra[] $planillaContras 
 * @property Remito[] $remitos
 * @property RemitoPorcentaje[] $remitoPorcentajes
 * @property TarifarioporcentajeHasPrecio[] $tarifarioporcentajeHasPrecios
 */
class Cliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cliente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['razonsocial', 'condicionfiscal_idcondicionfiscal', 'tarifario_idtarifario'], 'required'],
            [['telefono', 'condicionfiscal_idcondicionfiscal', 'tarifario_idtarifario', 'localidad_idlocalidad', 'tarifariovino_idtarifariovino', 'tarifariodomicilio_idtarifariodomicilio'], 'integer'],
            [['razonsocial', 'mail', 'domicilio'], 'string', 'max' => 100],
            [['cuit'], 'string', 'max' => 50],
            [['razonsocial'], 'unique'],
            [['condicionfiscal_idcondicionfiscal'], 'exist', 'skipOnError' => true, 'targetClass' => CondicionFiscal::className(), 'targetAttribute' => ['condicionfiscal_idcondicionfiscal' => 'idcondicionfiscal']],
            [['localidad_idlocalidad'], 'exist', 'skipOnError' => true, 'targetClass' => Localidad::className(), 'targetAttribute' => ['localidad_idlocalidad' => 'idlocalidad']],
            [['tarifario_idtarifario'], 'exist', 'skipOnError' => true, 'targetClass' => Tarifario::className(), 'targetAttribute' => ['tarifario_idtarifario' => 'idtarifario']],
            [['tarifariodomicilio_idtarifariodomicilio'], 'exist', 'skipOnError' => true, 'targetClass' => TarifarioDomicilio::className(), 'targetAttribute' => ['tarifariodomicilio_idtarifariodomicilio' => 'idtarifario_domicilio']],
            [['tarifariovino_idtarifariovino'], 'exist', 'skipOnError' => true, 'targetClass' => TarifarioVino::className(), 'targetAttribute' => ['tarifariovino_idtarifariovino' => 'idtarifario_vino']],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcliente' => 'ID Cliente',
            'razonsocial' => 'Razón Social',
            'cuit' => 'CUIT',
            'telefono' => 'Teléfono',
            'condicionfiscal_idcondicionfiscal' => 'Condición Fiscal',
            'tarifario_idtarifario' => 'Tarifario Común',
            'tarifariovino_idtarifariovino' => 'Tarifario Vino',
            'domicilio' => 'Domicilio',
            'mail' => 'e-mail',
            'localidad_idlocalidad' => 'Localidad',
            'tarifariovino_idtarifariovino' => 'Tarifario vino',
		    'tarifariodomicilio_idtarifariodomicilio' => 'Tarifario Domicilio',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanillaContras()
    {
       return $this->hasMany(PlanillaContra::className(), ['cliente_idcliente' => 'idcliente']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenGrupalVinos()
    {
        return $this->hasMany(OrdenGrupalVinos::className(), ['cliente_idcliente' => 'idcliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacionVinoClientes()
    {
        return $this->hasMany(LiquidacionVinoCliente::className(), ['cliente_idcliente' => 'idcliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacions()
    {
        return $this->hasMany(Liquidacion::className(), ['cliente_idcliente' => 'idcliente']);
    }

   /**
    * @return \yii\db\ActiveQuery
    */
   public function getLiquidacionPorcentajeClientes()
   {
       return $this->hasMany(LiquidacionPorcentajeCliente::className(), ['cliente_idcliente' => 'idcliente']);
   }

   /**
    * @return \yii\db\ActiveQuery
    */
   public function getLiquidacionDomicilioClientes()
   {
       return $this->hasMany(LiquidacionDomicilioCliente::className(), ['cliente_idcliente' => 'idcliente']);
   }

   /**
    * @return \yii\db\ActiveQuery
    */
   public function getOrdenGrupalDomicilios()
   {
       return $this->hasMany(OrdenGrupalDomicilio::className(), ['cliente_idcliente' => 'idcliente']);
   }

	/**
	* @return \yii\db\ActiveQuery
	*/
    public function getClienteHasTipos()
    {
        return $this->hasMany(ClienteHasTipo::className(), ['cliente_idcliente' => 'idcliente']);
    }


	/**
	* @return \yii\db\ActiveQuery
	*/
    public function getClientetipoIdclientetipos()
    {
        return $this->hasMany(ClienteTipo::className(), ['idcliente_tipo' => 'clientetipo_idclientetipo'])
        ->viaTable('{{%cliente_has_tipo}}', ['cliente_idcliente' => 'idcliente']);
    }

    /**
	* @return \yii\db\ActiveQuery
	*/
    public function getTarifarioIdtarifario()
    {
        return $this->hasOne(Tarifario::className(), ['idtarifario' => 'tarifario_idtarifario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarifariodomicilioIdtarifariodomicilio()
    {
        return $this->hasOne(TarifarioDomicilio::className(), ['idtarifario_domicilio' => 'tarifariodomicilio_idtarifariodomicilio']);
    }

    /**
	* @return \yii\db\ActiveQuery
	*/
    public function getTarifariovinoIdtarifariovino()
    {
        return $this->hasOne(TarifarioVino::className(), ['idtarifario_vino' => 'tarifariovino_idtarifariovino']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCondicionfiscalIdcondicionfiscal()
    {
        return $this->hasOne(CondicionFiscal::className(), ['idcondicionfiscal' => 'condicionfiscal_idcondicionfiscal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenRetiroGrupals()
    {
        return $this->hasMany(OrdenRetiroGrupal::className(), ['cliente_idcliente' => 'idcliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalidadIdlocalidad()
    {
	   return $this->hasOne(Localidad::className(), ['idlocalidad' => 'localidad_idlocalidad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemitos()
    {
        return $this->hasMany(Remito::className(), ['cliente_idcliente' => 'idcliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemitoPorcentajes()
    {
        return $this->hasMany(RemitoPorcentaje::className(), ['cliente_idcliente' => 'idcliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarifarioporcentajeHasPrecios()
    {
        return $this->hasMany(TarifarioporcentajeHasPrecio::className(), ['cliente_idcliente' => 'idcliente']);
    }

    /**
     * Funcion que busca un cliente por razón social
     * @param string $q Razón social del Cliente
     * @return [id, text] Arreglo de identificador y razon social
     */
    public static function getClienteByRazonsocial($q){
        $query = self::find();
        $query->select(["idcliente as id", "CONCAT(idcliente,' - ', razonsocial) AS text"])
            ->where(['like', 'razonsocial', $q])
            ->orWhere(['idcliente' => $q])
            ->limit(20);
        return $query->asArray()->all();
    }

    /**
     * Función que busca un cliente por razón social
     * @param string $q Razón social del Cliente
     * @return [id, text] Arreglo de identificador y razon social
     */
    public static function getClienteByRazonsocialAndTipo($q, $idclienteTipo){
        $query = self::find();
        $query->select(["idcliente as id", "CONCAT(idcliente,' - ', razonsocial) AS text"])
            ->where(['like', 'razonsocial', $q])
            ->leftJoin('cliente_has_tipo', 'cliente_idcliente = idcliente')
            ->andWhere(['clientetipo_idclientetipo' => $idclienteTipo])
            ->limit(20);
        return $query->asArray()->all();
    }
}
