<?php

use \yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

use common\models\GuiaDomicilio;
use common\models\LiquidacionRepartidor;
use common\models\LiquidacionDomicilioRepartidor;
use common\models\LiquidacionPorcentajeRepartidor;
use common\models\LiquidacionVinoRepartidor;
use common\models\OrdenGrupalDomicilio;
use common\models\OrdenGrupalVinos;
use common\models\Remito;
use common\models\RemitoPorcentaje;
use common\models\RemitoVino;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionRepartidorUnica */

$this->title = $model->idliquidacion_unica;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion Repartidor Unicas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="liquidacion-repartidor-unica-view row" style="overflow: hidden;">
    <div class="col-xs-3" style="float: left;">
        <h1>Repartidor: <?= $model->repartidorIdrepartidor->apellido.", ".$model->repartidorIdrepartidor->nombre ?></h1>
        <h1>Desde: <?= $model->fecha_inicio ?></h1>
        <h1>Hasta: <?= $model->fecha_fin ?></h1>
    </div>
    <div class="col-xs-4" style=" margin: 0 auto 0;">
        <?php echo Html::img('@web/img/logo/LOGO.jpg', ['class' => 'img-responsive', 'width'=>'210', 'style' => 'margin: 0 0;']) ?>
    </div>
    <div class="col-xs-3" style="border: 1px solid #d3d3d3; float:right; margin: 0 15px 0 0;">
            <div class="col-xs-2" style="margin: 20px 0 0 0;">
                <p>Firma: </p>
            </div>
            <div class="col-xs-6" style="margin: 0 0 0 0;">
                <p>________________________________</p>
            </div>
            <div class="col-xs-2">
                <p>Aclaración</p>
            </div>     
            <div class="col-xs-6">
                <p>________________________________</p>
            </div>
            <div class="col-xs-2">
                <p>Fecha</p>
            </div>
            <div class="col-xs-6">
                <p>_______ / _______ / _______</p>
            </div>
            <div class="col-xs-12" style="margin: 35px 0 0 0;">
                    <?= "<div class='col-md-12 tabla-totales'>TOTAL: ".number_format($model->total, 2 , "." ,  "," )."</div>"; ?>
            </div>
        </div>    
</div>

<div class="x_panel">
    <div class="x_title">
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php 
        $liquidacionComun = LiquidacionRepartidor::getAllFromUnica($model->idliquidacion_unica);
        $liquidacionDomicilio = LiquidacionDomicilioRepartidor::getAllFromUnica($model->idliquidacion_unica);
        $liquidacionPorcentaje = LiquidacionPorcentajeRepartidor::getAllFromUnica($model->idliquidacion_unica);
        $liquidacionVinos = LiquidacionVinoRepartidor::getAllFromUnica($model->idliquidacion_unica);
        
        ?>
        
            
        <?//= $this->render('_table_repartidor_comun',['liquidacionComun' => $liquidacionComun])?>
        <?= $this->render('_table_resumen_repartidor_comun',['liquidacionComun' => $liquidacionComun])?>

        <?//= $this->render('_table_repartidor_domicilio',['liquidacionDomicilio' => $liquidacionDomicilio, 'model' => $model])?>
        <?= $this->render('_table_resumen_repartidor_domicilio',['liquidacionDomicilio' => $liquidacionDomicilio, 'model' => $model])?>

        <?//= $this->render('_table_repartidor_porcentaje',['liquidacionPorcentaje' => $liquidacionPorcentaje, 'model' => $model])?>
        <?//= $this->render('_table_resumen_repartidor_porcentaje',['liquidacionPorcentaje' => $liquidacionPorcentaje, 'model' => $model])?>

        <?//= $this->render('_table_repartidor_vino',['liquidacionVinos' => $liquidacionVinos])?>
        <?= $this->render('_table_resumen_repartidor_vino',['liquidacionVinos' => $liquidacionVinos])?>

    </div>
</div> 