<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionHasRemito */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="liquidacion-has-remito-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'liquidacion_idliquidacion')->textInput() ?>

    <?= $form->field($model, 'remito_idremito')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
