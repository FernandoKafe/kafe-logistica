<?php

use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\helpers\Url;

//Clases
use common\models\Cliente;
use common\models\Porcentaje;
use \common\models\Repartidor;

/* @var $this yii\web\View */
/* @var $model common\models\RemitoPorcentaje */
/* @var $form yii\widgets\ActiveForm */
$cliente = ArrayHelper::map(Cliente::find()->all(),'idcliente','razonsocial');
$porcetaje = ArrayHelper::map(Porcentaje::find()->all(), 'idporcentaje', 'cantidad');
$repartidor = ArrayHelper::map(Repartidor::find()->all(),'idrepartidor','apellido');

$idRemito = Yii::$app->getRequest()->getQueryParam('id');
if(!$idRemito){
    $model->noquiso_nopidio = 0;
    $model->cant_anulado = 0;
    $model->cant_sin_dinero = 0;
    $model->cant_regresado = 0;
    $model->cant_faltante = 0;
    $model->cant_mal_armado = 0;
    $model->cant_duplicado = 0;
    $model->otro = 0;
    $model->dinero_facturado = 0;
    $model->dinero_rendido = 0;
    $model->dinero_faltante = 0;
    $model->dinero_devuelto = 0;
}

?>

<div class="remito-porcentaje-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-4 col-xs-12">
        <?= $form->field($model, 'cliente_idcliente')->widget(Select2::classname(), [
                'initValueText' => $cliente,
                'language' => 'es',
                'options' => ['placeholder' => 'Seleccionar Cliente ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 1,
                    'ajax' => [
                        'url' => Url::to(['/cliente/listado-for-tipo']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) {return {q:params.term, id:null, cliente_tipo:3}; }')
                    ],
                ],
            ])->label('<a href="../cliente/create" target="_blank"><i class="fa fa-plus green"></i></a> Cliente');?>
    </div>

    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'numero_planilla')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-2 col-xs-12"><?= $form->field($model, 'porcentaje_idporcentaje')->dropdownList($porcetaje) ?></div>
    
    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'pallets')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-6 col-xs-12">
          <?php
          if(!$idRemito){
            $model->fecha = date("Y-m-d",strtotime("-1 day"));
          }
          echo $form->field($model, 'fecha')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'aaaa-mm-dd'],
                'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd',
                'endDate' => "0d"
                ],
                'language' => Yii::$app->language,
                'type' => DatePicker::TYPE_COMPONENT_PREPEND
            ])->label('Fecha de Remito');
          ?>
    </div>

    <div class="col-md-6 col-xs-12">
        <?php
            $repartidorEntrega = empty($model->repartidorIdrepartidor) ? '' :
            $model->repartidorIdrepartidor->apellido
            .", ".$model->repartidorIdrepartidor->nombre;

            echo $form->field($model, 'repartidor_idrepartidor')->widget(Select2::classname(), [
              'initValueText' => $repartidorEntrega,
              'language' => 'es',
              'options' => ['placeholder' => 'Seleccionar Repartidor ...'],
              'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                  'url' => Url::to(['/repartidor/listado']),
                  'dataType' => 'json',
                  'data' => new JsExpression('function(params) {return {q:params.term}; }')
                ],
              ],
            ])->label('<a href="../repartidor/create" target="_blank"><i class="fa fa-plus green"></i></a> Repartidor');
        ?>
    </div>

    <div class="col-md-2 col-xs-12"><?= $form->field($model, 'cant_pedidos')->textInput() ?></div>

    <div class="col-md-2 col-xs-12"><?= $form->field($model, 'noquiso_nopidio')->textInput() ?></div>

    <div class="col-md-2 col-xs-12"><?= $form->field($model, 'cant_anulado')->textInput() ?></div>

    <div class="col-md-2 col-xs-12"><?= $form->field($model, 'cant_sin_dinero')->textInput() ?></div>

    <div class="col-md-2 col-xs-12"><?= $form->field($model, 'cant_regresado')->textInput() ?></div>

    <div class="col-md-2 col-xs-12"><?= $form->field($model, 'cant_faltante')->textInput() ?></div>

    <div class="col-md-2 col-xs-12"><?= $form->field($model, 'cant_mal_armado')->textInput() ?></div>

    <div class="col-md-2 col-xs-12"><?= $form->field($model, 'cant_duplicado')->textInput() ?></div>

    <div class="col-md-2 col-xs-12"><?= $form->field($model, 'otro')->textInput() ?></div>
    
    <div class="col-md-12"><?= $form->field($model, 'detalle_otro')->textArea() ?></div>

    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'dinero_facturado')->textInput()->label('$Dinero Facturado') ?></div>
    
    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'dinero_devuelto')->textInput()->label('$Dinero Devuelto') ?></div>

    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'dinero_rendido')->textInput()->label('$Dinero Rendido') ?></div>

    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'dinero_faltante')->textInput()->label('$Dinero Faltante') ?></div>

    <div class="col-md-12"><?= $form->field($model, 'observacion_faltante')->textArea() ?></div>

    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'combustible')->textInput()->label('$Combustible') ?></div>

    <div class="form-group col-md-12 col-xs-12">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
