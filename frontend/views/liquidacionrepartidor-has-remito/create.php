<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionrepartidorHasRemito */

$this->title = Yii::t('app', 'Create Liquidacionrepartidor Has Remito');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacionrepartidor Has Remitos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacionrepartidor-has-remito-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
