<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Porcentaje */
/* @var $form yii\widgets\ActiveForm */
$idPorcentaje = Yii::$app->getRequest()->getQueryParam('id');

?>

<div class="porcentaje-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group col-md-12"><?= $form->field($model, 'cantidad')->textInput() ?></div>

    <div class="form-group col-md-12">
      <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);?>

      <?php if(isset($idPorcentaje)){ ?>
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
      <?php }else{ ?>
        <?= Html::submitButton(Yii::t('app', 'Crear'), ['class' => 'btn btn-success']) ?>
      <?php } ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
