<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LiquidacionRepartidorUnica;

/**
 * LiquidacionRepartidorUnicaSearch represents the model behind the search form of `common\models\LiquidacionRepartidorUnica`.
 */
class LiquidacionRepartidorUnicaSearch extends LiquidacionRepartidorUnica
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idliquidacion_unica'], 'integer'],
            [['nombre', 'fecha_inicio', 'fecha_fin', 'estadoliquidacion_idestadoliquidacion', 'repartidor_idrepartidor', 'creado'], 'safe'],
            [['total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LiquidacionRepartidorUnica::find();
        $query->joinWith(['repartidorIdrepartidor as repartidor']);
        $query->joinWith(['estadoliquidacionIdestadoliquidacion as estado']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idliquidacion_unica' => $this->idliquidacion_unica,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_fin' => $this->fecha_fin,
            'total' => $this->total,
            'creado' => $this->creado,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);
        $query->andFilterWhere(['like', 'CONCAT(repartidor.apellido, ", ", repartidor.nombre)', $this->repartidor_idrepartidor]);
        $query->andFilterWhere(['like', 'estado.nombre', $this->estadoliquidacion_idestadoliquidacion]);

        return $dataProvider;
    }
}
