<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\RemitoVinoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Remito Vinos');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="remito-vino-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
    <div class="x_title">
        <h2>Listado</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php Pjax::begin(); ?>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Crear Remito Vino'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'idremito_vino',
            'numero_remito',
            'cant_cajas',
            'valor_declarado',
            'total_facturacion',
            //'devolucion_caja',
            //'observacion:ntext',
            //'sucursal_idsucursal',
            //'precio_unitario',
            //'cant_botellas',
            //'articulo_concepto:ntext',
            [
                'attribute' => 'creado',
                'label' => 'Fecha y Hora creación',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
        </div>
    </div>
</div>
