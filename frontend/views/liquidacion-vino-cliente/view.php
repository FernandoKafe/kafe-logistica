<?php

use yii\data\ActiveDataProvider;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use yii\helpers\Html;

use common\models\LiquidacionVinoCliente;
use common\models\RemitoVino;
use common\models\OrdenGrupalVinos;
use common\models\TarifarioVino;
use common\models\Cliente;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionVinoCliente */
$idLiquidacion = Yii::$app->getRequest()->getQueryParam('id');
$clienteLiquidado = $model->cliente_idcliente;

$this->title = "Liquidación vinos: ".$model->nombre." - ".$model->clienteIdcliente->razonsocial;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion Vino Clientes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-porcentaje-cliente-view">
    <h1>Liquidación: <?= $model->clienteIdcliente->razonsocial?></h1>
    <h1>Periodo: <?= $model->nombre?></h1>
    <h1>CUIT: <?=$model->clienteIdcliente->cuit?></h1>
    <div class="x_panel">
    
        <p>
            <?
                if(!$model->confirmada){
                    echo Html::a(Yii::t('app', 'Eliminar'), ['delete', 'id' => $model->idliquidacion_vino_cliente], ['class' => 'btn btn-danger',  'data-method'=>'post']);
                    echo Html::a(Yii::t('app', 'Recalcular'), ['recalculate', 'id' => $model->idliquidacion_vino_cliente], ['class' => 'btn btn-warning']);
                    echo Html::a(Yii::t('app', 'Confirmar'), ['liquidate', 'id' => $model->idliquidacion_vino_cliente], ['class' => 'btn btn-success']);
                }else{
                    echo Html::a(Yii::t('app', 'Inicio'), ['index', 'id' => $model->idliquidacion_vino_cliente], ['class' => 'btn btn-primary',  'data-method'=>'post']);
                    echo Html::a(Yii::t('app', 'Eliminar'), ['delete', 'id' => $model->idliquidacion_vino_cliente], ['class' => 'btn btn-danger',  'data-method'=>'post']);
                    echo Html::a(Yii::t('app', 'PDF'), ['liquidate', 'id' => $model->idliquidacion_vino_cliente], ['class' => 'btn btn-success']);
                }
            ?>
        </p>
        <p>
            <h3><strong style="color: red;">IMPORTANTE:</strong> La previa debe recalcularse luego de haberla generado si se modifica o elimina un remito de la misma.</h3>
        </p>

    </div>
</div>

<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Remitos liquidados');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php
    if(!$model->confirmada){
        $ordenesGrupales = OrdenGrupalVinos::getOrdenVinosClienteByFecha($model->cliente_idcliente, $model->periodo_inicio, $model->periodo_fin);
        $ordenesGrupalesId = ArrayHelper::getColumn($ordenesGrupales,'idorden_grupal_vinos');
        $remitoVino = RemitoVino::find()->where(['IN', 'ordengrupalvinos_idordengrupalvinos', $ordenesGrupalesId]);
    }else{
        $remitoVino = RemitoVino::find()->joinWith(['ordengrupalvinosIdordengrupalvinos'])->where(['liquidacionvinocliente_idliquidacionvinocliente' => $idLiquidacion]);
    }
        if(!isset($model->cajas) || !isset($model->valor_cajas) || !isset($model->total_seguro)){
            if(!isset($model->cajas))
                $model->cajas = LiquidacionVinoCliente::getCajasSum($remitoVino->all());
            if(!isset($model->valor_cajas))
                $model->valor_cajas = LiquidacionVinoCliente::getValorCaja($remitoVino->all(), $clienteLiquidado);
            if(!isset($model->total_seguro))
                $model->total_seguro = LiquidacionVinoCliente::getSeguroSum($remitoVino->all(), $clienteLiquidado);
            $model->save(false);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $remitoVino,
        ]);
        echo GridView::widget([
        'dataProvider' => $dataProvider,
        'moduleId' => 'gridviewk',
        //'showPageSummary' => true,
        'toolbar' =>  [
            '{export}',
            '{toggleData}',
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
        ],
        'showFooter' => true,
        'columns' => [
            [
                'label' => 'Fecha',
                'value' => function($data){
                    return $data->ordengrupalvinosIdordengrupalvinos->fecha;
                }
            ],
            [
                'label' => 'Nº Planilla',
                'value' => function($data){
                    return $data->numero_remito;
                }
            ],
            'guia_numero',
            'guia_tipo',
            'guia_facturada_a',
            [
                'label' => 'Destinatario',
                'value' => function($data){
                    return $data->sucursalIdsucursal->empresaIdempresa->nombre;
                }
            ],
            [
                'label' => 'Destino',
                'value' => function($data){
                    return  $data->sucursalIdsucursal->localidadIdlocalidad->nombre.", ".$data->sucursalIdsucursal->localidadIdlocalidad->departamentoIddepartamento->nombre.", ".$data->sucursalIdsucursal->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                },
                'pageSummary' => 'SUBTOTAL',
                'footer' => '<strong>TOTAL</strong>',
            ],
            [
                'attribute' => 'cant_cajas',
                'pageSummary' => true,
                'pageSummaryFunc' => GridView::F_SUM,
                'footer' => $model->cajas
            ],
            [
                'label' => 'valor_caja',
                'value' => function($data){
                    if(!isset($data->valor_caja) || !isset($data->precio_reparto)){ 
                        $clienteVino = Cliente::find()->where(['idcliente' => $data->ordengrupalvinosIdordengrupalvinos->cliente_idcliente])->one();
                        $tv = TarifarioVino::find()->where(['idtarifario_vino' => $clienteVino->tarifariovino_idtarifariovino])->one();
                        $remitoCC = RemitoVino::find()->where(['idremito_vino' => $data->idremito_vino])->one();
                        $remitoCC->valor_caja = $tv->precio_caja;
                        $remitoCC->precio_reparto = $tv->precio_repartidor;
                        $remitoCC->save(false);
                    }
                    return ($data->valor_caja);
                },
                'format' => ['decimal',2],
                'label' => 'Valor Caja',
                'pageSummary' => true,
                'pageSummaryFunc' => GridView::F_SUM,
                'footer' => number_format($model->valor_cajas, 2 , "." ,  "," ),
            ],
            [
                'label' => 'Seguro',
                'format' => ['decimal',2],
                'value' => function($data){
                    return (($data->valor_declarado / 2) * 0.01);
                },
                'pageSummary' => true,
                'pageSummaryFunc' => GridView::F_SUM,
                'footer' => number_format($model->total_seguro, 2 , "." ,  "," ),
            ],
            [
                'attribute' => 'guia_total',
                'footer' => $model->total_guias,
                'pageSummary' => true,
                'pageSummaryFunc' => GridView::F_SUM,
            ]


        ],
        ]);
    ?>


<?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idliquidacion_vino_cliente',
            [
                'attribute' => 'total',
                'label' => 'BULTOS + SEGURO',
                'format' => ['decimal',2],
            ],
            [
                'attribute' => 'total_guias',
                'label' => 'GUÍAS A PAGAR',
                'format' => ['decimal',2],
            ],
            [
                'attribute' => 'total_cobrar',
                'label' => 'TOTAL',
                'format' => ['decimal',2],
            ],
        ],
    ]) ?>
    </div>
</div>