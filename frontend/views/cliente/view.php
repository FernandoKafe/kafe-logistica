<?php
use yii\data\ActiveDataProvider;
use common\models\ClienteHasTipo;
use common\models\Cliente;

use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Cliente */
$idCliente = Yii::$app->getRequest()->getQueryParam('id');

$this->title = $model->razonsocial;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Clientes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cliente-view">

    <h1><i class="fa fa-address-book"></i> Cliente: <?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Detalle') ?></h2>
            <div class="clearfix"></div>
        </div>

        <p>
            <?= Html::a('Inicio', ['index'], ['class' => 'btn btn-primary']);?>
            <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->idcliente], ['class' => 'btn btn-success']) ?>
            <!--<?/*= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idcliente], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) */?>-->
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'idcliente',
                'razonsocial',
                [
                    'attribute' => 'localidad_idlocalidad',
                    'value' => function($data){
                        if(isset($data->localidad_idlocalidad)){
                        return $data->localidadIdlocalidad->nombre.
                        " - ".$data->localidadIdlocalidad->departamentoIddepartamento->nombre.
                        " - ".$data->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                        }else{
                            return '';
                        }
                    }
                ],
                'domicilio',
                'cuit',
                'telefono',
                'mail',
                [
                    'label' => 'Condición Fiscal',
                    'value' => function($model){
                        return ($model->condicionfiscal_idcondicionfiscal)?$model->condicionfiscalIdcondicionfiscal->nombre:null;
                    }
                ],
                [
                    'label' => 'Tarifario',
                    'value' => function($model){
                        return $model->tarifarioIdtarifario->nombre;
                    }
                ],
            ],
        ]) ?>
    </div>
</div>

<!--Tipos de cliente asociados-->
<div class="col-md-3">
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Tipos de cliente');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php
        $dataProvider = new ActiveDataProvider([
            'query' => ClienteHasTipo::find()->where(['cliente_idcliente' => $idCliente]),
        ]);
        $dataProvider->pagination->pageSize=20;
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'attribute' => 'clientetipo_idclientetipo',
                    'label' => 'Tipo cliente',
                    'value' => function($data){
                        return $data->clientetipoIdclientetipo->nombre;
                    }
                ],
                /*[
                    'attribute' => 'empresatransporteIdempresatransporte.razonsocial',
                    'label' => 'Empresa de transporte'
                ],*/
            ],
        ]); 
    ?>
    </div>
</div>
</div>

<!--Tarifario común asociados-->
<div class="col-md-3">
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Tarifario común');?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <?php
            $dataProvider = new ActiveDataProvider([
                'query' => Cliente::find()->where(['idcliente' => $idCliente]),
            ]);
            $dataProvider->pagination->pageSize=20;
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'tarifario_idtarifario',
                        'label' => 'Tarifario común',
                        'value' => function($data){
                            if(isset($data->tarifario_idtarifario)){
                                return $data->tarifarioIdtarifario->nombre;
                            }else{
                                return "SIN TARIFARIO";
                            }
                        }
                    ], 
                    /*[
                        'attribute' => 'empresatransporteIdempresatransporte.razonsocial',
                        'label' => 'Empresa de transporte'
                    ],*/
                ],
            ]); 
        ?>
        </div>
    </div>
</div>

<!--Tarifario vino asociados-->
<div class="col-md-3">
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Tarifario vino');?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <?php
            $dataProvider = new ActiveDataProvider([
                'query' => Cliente::find()->where(['idcliente' => $idCliente]),
            ]);
            $dataProvider->pagination->pageSize=20;
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'tarifariovino_idtarifariovino',
                        'label' => 'Tarifario Vino',
                        'value' => function($data){
                            if(isset($data->tarifariovino_idtarifariovino)){
                                return $data->tarifariovinoIdtarifariovino->nombre;
                            }else{
                                return "SIN TARIFARIO";
                            }
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Borrar',
                        'template' => '{delete}',
                        'buttons' => [
                          'delete' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                            'title' => Yii::t('app', 'Borrar'),
                                ]);
                          },
            
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'delete') {
                                $url ='remove-vino?id='.$model->idcliente;
                                return $url;
                            }
                        }
                    ],
                    /*[
                        'attribute' => 'empresatransporteIdempresatransporte.razonsocial',
                        'label' => 'Empresa de transporte'
                    ],*/
                ],
            ]); 
        ?>
        </div>
    </div>
</div>

<!--Tarifario domicilio asociados-->
<div class="col-md-3">
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Tarifario domicilio');?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
        <?php
            $dataProvider = new ActiveDataProvider([
                'query' => Cliente::find()->where(['idcliente' => $idCliente]),
            ]);
            $dataProvider->pagination->pageSize=20;
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'attribute' => 'tarifariodomicilio_idtarifariodomicilio',
                        'label' => 'Tarifario Domicilio',
                        'value' => function($data){
                            if(isset($data->tarifariodomicilio_idtarifariodomicilio)){
                                return $data->tarifariodomicilioIdtarifariodomicilio->nombre;
                            }else{
                                return "SIN TARIFARIO";
                            }
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'header' => 'Borrar',
                        'template' => '{delete}',
                        'buttons' => [
                          'delete' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                            'title' => Yii::t('app', 'Borrar'),
                                ]);
                          },
            
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'delete') {
                                $url ='remove-domicilio?id='.$model->idcliente;
                                return $url;
                            }
                        }
                    ],
                    /*[
                        'attribute' => 'empresatransporteIdempresatransporte.razonsocial',
                        'label' => 'Empresa de transporte'
                    ],*/
                ],
            ]); 
        ?>
        </div>
    </div>
</div>
