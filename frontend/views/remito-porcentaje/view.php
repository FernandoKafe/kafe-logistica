<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\RemitoPorcentaje */

$this->title = $model->idremito_porcentaje;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Remito Porcentajes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="remito-view">
    <h1><i class="fa fa-file"></i> Remito por porcentajeNº: <?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Detalle') ?></h2>
            <div class="clearfix"></div>
        </div>
        <p>
            <?= Html::a('Inicio', ['index'], ['class' => 'btn btn-primary']);?>
            <?= Html::a(Yii::t('app', 'Actualizar'), ['update',
                'id' => $model->idremito_porcentaje,],
                ['class' => 'btn btn-success']) ?>
                <!--<?//= Html::a('Cargar otro', ['create'], ['class' => 'btn btn-dark']);?>-->
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                //'idremito_porcentaje',
                [
                    'attribute' => 'dinero_facturado',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'dinero_rendido',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'dinero_devuelto',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'dinero_faltante',
                    'format' => ['decimal',2],
                ],
                'fecha',
                'numero_planilla',
                [
                    'attribute' => 'porcentaje',
                    'value' => function($data){
                        return $data->porcentaje." %";
                    }
                ],
                'cant_pedidos',
                'cant_entregados',
                'noquiso_nopidio',
                'cant_anulado',
                'cant_sin_dinero',
                'cant_regresado',
                'cant_faltante',
                'cant_mal_armado',
                'cant_duplicado',
                'otro',
                'detalle_otro',
                [
                    'attribute' => 'observacion_faltante',
                ],
                [
                    'attribute' => 'cliente_idcliente',
                    'value' => function($data){
                        return $data->clienteIdcliente->razonsocial;
                    }
                ],
                [
                    'attribute' => 'repartidor_idrepartidor',
                    'value' => function($data){
                        return $data->repartidorIdrepartidor->apellido.", ".$data->repartidorIdrepartidor->nombre;
                    }
                ],
            ],
        ]) ?>

    </div>
</div>

