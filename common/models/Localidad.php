<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "localidad".
 *
 * @property int $idlocalidad
 * @property int $departamento_iddepartamento
 * @property string $nombre
 * @property string $cp
 *
 * @property Departamento $departamentoIddepartamento
 * @property Sucursal[] $sucursals
 * @property ZonaHasLocalidad $zonaHasLocalidad
 */
class Localidad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'localidad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['departamento_iddepartamento', 'nombre'], 'required'],
            [['departamento_iddepartamento'], 'integer'],
            [['nombre', 'cp'], 'string', 'max' => 45],
            [['departamento_iddepartamento'], 'exist', 'skipOnError' => true, 'targetClass' => Departamento::className(), 'targetAttribute' => ['departamento_iddepartamento' => 'iddepartamento']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idlocalidad' => Yii::t('app', 'ID Localidad'),
            'departamento_iddepartamento' => Yii::t('app', 'Departamento'),
            'nombre' => Yii::t('app', 'Nombre Localidad'),
            'cp' => Yii::t('app', 'CP')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartamentoIddepartamento()
    {
        return $this->hasOne(Departamento::className(), ['iddepartamento' => 'departamento_iddepartamento']);
    }

    /**
     * @return departamento.nombre
     */
    public function getDepartamentoNombre(){
        $model=$this->departamentoIddepartamento;
        return $model?$model->nombre:'';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSucursals()
    {
        return $this->hasMany(Sucursal::className(), ['localidad_idlocalidad' => 'idlocalidad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZonaHasLocalidad()
    {
        return $this->hasOne(ZonaHasLocalidad::className(), ['localidad_idlocalidad' => 'idlocalidad']);
    }
}
