<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%liquidacion_domicilio_repartidor}}".
 *
 * @property int $idliquidacion_domicilio_repartidor
 * @property string $fecha_inicio
 * @property string $fecha_fin
 * @property string $nombre
 * @property double $total
 * @property int $repartidor_idrepartidor
 * @property int $estadoliquidacion_idestadoliquidacion
 * @property string $creado
 * @property int liquidacionunica
 *
 * @property EstadoLiquidacion $estadoliquidacionIdestadoliquidacionCopy1
 * @property Repartidor $repartidorIdrepartidor
 */
class LiquidacionDomicilioRepartidor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%liquidacion_domicilio_repartidor}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_inicio', 'fecha_fin', 'nombre', 'repartidor_idrepartidor'], 'required'],
            [['fecha_inicio', 'fecha_fin', 'creado'], 'safe'],
            [['total'], 'number'],
            [['repartidor_idrepartidor', 'estadoliquidacion_idestadoliquidacion', 'liquidacionunica'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['estadoliquidacion_idestadoliquidacion'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoLiquidacion::className(), 'targetAttribute' => ['estadoliquidacion_idestadoliquidacion' => 'idestado_liquidacion']],
            [['repartidor_idrepartidor'], 'exist', 'skipOnError' => true, 'targetClass' => Repartidor::className(), 'targetAttribute' => ['repartidor_idrepartidor' => 'idrepartidor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idliquidacion_domicilio_repartidor' => Yii::t('app', 'Idliquidacion Domicilio Repartidor'),
            'fecha_inicio' => Yii::t('app', 'Fecha Inicio'),
            'fecha_fin' => Yii::t('app', 'Fecha Fin'),
            'nombre' => Yii::t('app', 'Periodo'),
            'total' => Yii::t('app', 'TOTAL'),
            'repartidor_idrepartidor' => Yii::t('app', 'Repartidor'),
            'estadoliquidacion_idestadoliquidacion' => Yii::t('app', 'Estado liquidación'),
            'creado' => Yii::t('app', 'Creado'),
            'liquidacionunica' => Yii::t('app', 'Liquidación única'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoliquidacionIdestadoliquidacion()
    {
        return $this->hasOne(EstadoLiquidacion::className(), ['idestado_liquidacion' => 'estadoliquidacion_idestadoliquidacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepartidorIdrepartidor()
    {
        return $this->hasOne(Repartidor::className(), ['idrepartidor' => 'repartidor_idrepartidor']);
    }

    public static function createLiquidacion($model, $liquidacionUnica){  
        if($model) {
            //Traer todas las ordenes grupales del cliente para la fecha
            $ordenesGruapales = OrdenGrupalDomicilio::getAllOrdenesRepartidorFecha($model->repartidor_idrepartidor, $model->fecha_inicio, $model->fecha_fin);
            if(!empty($ordenesGruapales)){
                foreach($ordenesGruapales as $OG){
                    if(!$OG->clienteIdcliente->tarifariodomicilio_idtarifariodomicilio){
                        $mensaje = "No se puede crear la liquidación. El cliente domicilio ".$OG->clienteIdcliente->razonsocial." no tiene asignado un tarifario domicilio.";
                        Yii::$app->session->setflash('error', $mensaje);
                        return ["error", $OG->idorden_grupal_domicilio, $mensaje];
                    }
                }
                foreach($ordenesGruapales as $OG){
                    $tarifario = TarifarioDomicilio::find()->where(['idtarifario_domicilio' => $OG->clienteIdcliente->tarifariodomicilio_idtarifariodomicilio])->one();
                    $guias = GuiaDomicilio::getAllGuiasFromOrden($OG->idorden_grupal_domicilio);
                    if(!isset($OG->precio_reparto) || !isset($OG->precio_reparto_adicional) || !isset($OG->precio_sprinter) || !isset($OG->precio_bulto) || !isset($OG->precio_bulto_adicional) || !isset($OG->tipo_cliente))
                    {
                        if(!isset($OG->precio_reparto))
                            $OG->precio_reparto = $OG->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->reparto;
                        if(!isset($OG->precio_reparto_adicional) )
                            $OG->precio_reparto_adicional = $OG->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->reparto_adicional;
                        if(!isset($OG->precio_sprinter))
                            $OG->precio_sprinter = $OG->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->sprinter;
                        if(!isset($OG->precio_bulto))
                            $OG->precio_bulto = $OG->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->primer_bulto;
                        if(!isset($OG->precio_bulto_adicional))
                            $OG->precio_bulto_adicional = $OG->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->bulto_adicional;
                        if(!isset($OG->tipo_cliente))
                            $OG->tipo_cliente = $OG->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo;
                    }
                    foreach($guias as $gui){
                        $fechaAnterior = 0;
                        if($gui->ordengrupaldomicilioIdordengrupaldomicilio->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo A"){
                            $model->total +=  $gui->cantidad * $tarifario->reparto;
                        }else if($gui->ordengrupaldomicilioIdordengrupaldomicilio->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo B"){
                            if($fechaAnterior != $gui->ordengrupaldomicilioIdordengrupaldomicilio->fecha){
                                $model->total += $tarifario->reparto + (($gui->cantidad - 1) * $tarifario->reparto_adicional);
                                $fechaAnterior = $gui->ordengrupaldomicilioIdordengrupaldomicilio->fecha;
                            }else{
                                $model->total += ($gui->cantidad * $tarifario->reparto_adicional);
                                $fechaAnterior = $gui->ordengrupaldomicilioIdordengrupaldomicilio->fecha;
                            }
                        }
                    }
                    $model->creado = date('Y-m-d G:i', time());
                    $model->liquidacionunica = $liquidacionUnica;
                    $model->save();
                    $OG->liquidacionrepartidor_idliquidacionrepartidor = $model->idliquidacion_domicilio_repartidor;
                    $OG->save();
                }
            }else{
                $model->delete();
                return false;
            }
            return $model;
        }
    }

    public static function deleteLiquidacion($liquidacionUnica){
        $model = self::find()->where(['fecha_inicio' => $liquidacionUnica->fecha_inicio])
        ->andWhere(['fecha_fin' => $liquidacionUnica->fecha_fin])
        ->andWhere(['nombre' => $liquidacionUnica->nombre])
        ->andWhere(['repartidor_idrepartidor' => $liquidacionUnica->repartidor_idrepartidor])
        ->andWhere(['borrado' => 0])->one();
        if($model){
            $ordenesGruapales = OrdenGrupalDomicilio::getAllOrdenesRepartidorFechaLiquidados($model->repartidor_idrepartidor, $model->fecha_inicio, $model->fecha_fin, $model->idliquidacion_domicilio_repartidor);
            if($ordenesGruapales){
                foreach($ordenesGruapales as $OG){
                    $OG->liquidacionrepartidor_idliquidacionrepartidor = null;
                    $OG->save();
                }
            }

            $model->borrado = 1;
            $model->save();
        }
        return 0;
    }

    public static function getAllFromUnica($idUnica){
        return self::find()->select('idliquidacion_domicilio_repartidor')->where(['liquidacionunica' => $idUnica])->one();
    }
}
