<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Rendicion */

$this->title = Yii::t('app', 'Crear Rendicion');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rendicions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$tipo = Yii::$app->getRequest()->getQueryParam('tipo');
?>
<div class="rendicion-create">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Formulario') ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <?= $this->render('_form', [
                'model' => $model,
                'cliente' => $cliente,
            ]) ?>
        </div>
     </div>
</div>
 