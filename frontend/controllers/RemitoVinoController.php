<?php

namespace frontend\controllers;

use Yii;
use common\models\RemitoVino;
use frontend\models\RemitoVinoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\OrdenGrupalVinos;

/**
 * RemitoVinoController implements the CRUD actions for RemitoVino model.
 */
class RemitoVinoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RemitoVino models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RemitoVinoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RemitoVino model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RemitoVino model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RemitoVino();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'view',
                'id' => $model->idremito_vino,
                ]);
        }


        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RemitoVino model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if(!($model->ordengrupalvinosIdordengrupalvinos->liquidacionvinocliente_idliquidacionvinocliente)
        && !($model->ordengrupalvinosIdordengrupalvinos->liquidacionvinorepartidor_idliquidacionvinorepartidor)){
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->idremito_vino]);
            }
        }else{
            Yii::$app->session->setFlash('error', 'No puede editarse por estar liquidado.');
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RemitoVino model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $idOrdenGrupalVinos = null)
    {
        $model = $this->findModel($id);

        if(!($model->ordengrupalvinosIdordengrupalvinos->liquidacionvinocliente_idliquidacionvinocliente)
        && !($model->ordengrupalvinosIdordengrupalvinos->liquidacionvinorepartidor_idliquidacionvinorepartidor)){
            $model->delete();
            if($idOrdenGrupalVinos != null){
                return $this->redirect(['orden-grupal-vinos/view', 'id' => $idOrdenGrupalVinos]);
            }else{
                return $this->redirect(['index']);
            }
        }else{
            Yii::$app->session->setFlash('error', 'No puede eliminarse por estar liquidado.');
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * Finds the RemitoVino model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RemitoVino the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RemitoVino::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
