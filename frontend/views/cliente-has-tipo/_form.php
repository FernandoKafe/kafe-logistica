<?php

use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Html;

use common\models\ClienteTipo;

/* @var $this yii\web\View */
/* @var $model common\models\ClienteHasTipo */
/* @var $form yii\widgets\ActiveForm */
$idCliente = Yii::$app->getRequest()->getQueryParam('id');
$clienteTipo = ArrayHelper::map(ClienteTipo::find()->orderBy('nombre')->all(), 'idcliente_tipo', 'nombre');
?>

<div class="cliente-has-tipo-form">

    <?php $form = ActiveForm::begin(); ?>

     <?php $model->cliente_idcliente = $idCliente?>

    <?= $form->field($model, 'cliente_idcliente')->textInput()->label(false)->hiddenInput() ?>

    <?= $form->field($model, 'clientetipo_idclientetipo')->dropdownList($clienteTipo) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Agregar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
