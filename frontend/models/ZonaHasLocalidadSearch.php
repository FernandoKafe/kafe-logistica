<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ZonaHasLocalidad;

/**
 * ZonaHasLocalidadSearch represents the model behind the search form of `common\models\ZonaHasLocalidad`.
 */
class ZonaHasLocalidadSearch extends ZonaHasLocalidad
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['zona_idzona', 'localidad_idlocalidad'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ZonaHasLocalidad::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'zona_idzona' => $this->zona_idzona,
            'localidad_idlocalidad' => $this->localidad_idlocalidad,
        ]);

        return $dataProvider;
    }
}
