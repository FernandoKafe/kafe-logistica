<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TarifarioVino;

/**
 * TarifarioVinoSearch represents the model behind the search form of `common\models\TarifarioVino`.
 */
class TarifarioVinoSearch extends TarifarioVino
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idtarifario_vino'], 'integer'],
            [['precio_caja', 'precio_repartidor'], 'number'],
            [['nombre'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TarifarioVino::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idtarifario_vino' => $this->idtarifario_vino,
            'precio_caja' => $this->precio_caja,
            'precio_repartidor' => $this->precio_repartidor,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);

        return $dataProvider;
    }
}
