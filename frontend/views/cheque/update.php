<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Cheque */

$this->title = Yii::t('app', 'Update Cheque: ' . $model->idcheque, [
    'nameAttribute' => '' . $model->idcheque,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cheques'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idcheque, 'url' => ['view', 'id' => $model->idcheque]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="cheque-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
