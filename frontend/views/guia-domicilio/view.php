<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\GuiaDomicilio */

$this->title = $model->numero_guia;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Guia Domicilio'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="guia-domicilio-view">
    <h1>Guía nº: <?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Detalle') ?></h2>
            <div class="clearfix"></div>
        </div>

        <p>
            <?= Html::a('Inicio', ['index'], ['class' => 'btn btn-primary']);?>

            <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->idorden_grupal_domicilio], ['class' => 'btn btn-success']) ?>
            <!--<?/*= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idformadepago], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) */?>-->
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'idorden_grupal_domicilio',
                'numero_guia',
                'monto',
                'guia_tipo',
                'empresa_destino',
                'localidad_idlocalidad',
                'ordengrupaldomicilio_idordengrupaldomicilio',
            ],
        ]) ?> 

    </div>
</div>