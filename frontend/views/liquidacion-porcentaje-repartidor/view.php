<?php

use yii\data\ActiveDataProvider;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\PagosPorcentajesRepartidor;
use common\models\RemitoPorcentaje;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionPorcentajeRepartidor */
$idLiquidacion = Yii::$app->getRequest()->getQueryParam('id');

$this->title = "Liquidación % repartidor: ".$model->repartidorIdrepartidor->apellido.", ".$model->repartidorIdrepartidor->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion Porcentaje Repartidors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-porcentaje-repartidor-view"> 
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
        <div class="x_title">
            <div class="clearfix"></div>
        </div>

        <p>
            <?= Html::a(Yii::t('app', 'Guardar'), ['index'], ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app', 'PDF'), ['report', 'id' => $model->idliquidacion_porcentaje_repartidor], ['class' => 'btn btn-warning']) ?>
            <?= Html::a(Yii::t('app', 'ARREGLAR'), ['arreglar', 'id' => $model->idliquidacion_porcentaje_repartidor], ['class' => 'btn btn-warning']) //BORRAR DESPUES DE ACTUALIZAR?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'idliquidacion_porcentaje_repartidor',
                [
                    'attribute' => 'repartidor_idrepartidor',
                    'value' => function($data){
                        return $data->repartidorIdrepartidor->apellido.", ".$data->repartidorIdrepartidor->nombre;
                    }
                ],
                [
                    'attribute' => 'estadoliquidacion_idestadoliquidacion',
                    'value' => function($data){
                        return $data->estadoliquidacionIdestadoliquidacion->nombre;
                    }
                ],
                'periodo',
                'fecha_inicio',
                'fecha_fin',
                [
                    'attribute' => 'total',
                    'format' => ['decimal',2],
                ]
            ],
        ]) ?>
        <!--<p>
            <?//= Html::a(Yii::t('app', 'Resumen mensual'), ['summary', 'id' => $model->idliquidacion_porcentaje_repartidor], ['class' => 'btn btn-danger']) ?>
        </p>-->

    </div>
</div>


<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_title">
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?
    $fechaAnterior = 0;
    $remito = RemitoPorcentaje::find()->joinWith('liquidacionporcentajerepartidorHasRemitos')->where(['lpr_idlpr' => $idLiquidacion])->orderBy(['fecha' => SORT_ASC, 'numero_planilla' => SORT_ASC]);
    $dataProvider = new ActiveDataProvider([
        'query' => $remito,
    ]);
    $dataProvider->setSort([
        'attributes' => [
            'fecha' => [
                'asc' => ['fecha' => SORT_ASC],
                'desc' => ['fecha' => SORT_DESC],
                'default' => SORT_ASC
            ],
        ],
        'defaultOrder' => [
            'fecha' => SORT_ASC
        ]
    ]);
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    'moduleId' => 'gridviewk',
    'toolbar' =>  [
        '{export}',
        '{toggleData}',
    ],
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
    ],
    'columns' => [
        [
            'attribute' => 'fecha',
            'label' => 'Fecha',
        ],
        [
            'attribute' => 'numero_planilla',
            'label' => 'Nº Plantilla',
            'value' => function($data){
                return $data->numero_planilla;
            }
        ],
        'cant_pedidos',
        'cant_entregados',
        [
            'label' => '% Entregas',
            'attribute' => 'porcentaje_entregas',
            'format' => ['decimal',2]
        ],
        [
            'attribute' => 'dinero_faltante', 
            'format' => ['decimal',2],
        ],
        [
            'label' => 'Se Paga $',
            'attribute' => 'reparto_seleccionado',
            'value' => function($data){
                if($data->se_paga == 1){
                    return $data->reparto_seleccionado;
                }else{
                    return 0;
                }
            },
            'format' => ['decimal',2],
        ]
    ],
    ]); ?>

    </div>
</div>