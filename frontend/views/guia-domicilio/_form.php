<?php

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\helpers\Url;

use \common\models\Provincia;
use \common\models\OrdenGrupalDomicilio;

use \common\models\Departamento;
use \common\models\Localidad;


/* @var $this yii\web\View */
/* @var $model common\models\GuiaDomicilio */
/* @var $form yii\widgets\ActiveForm */ 
$repartidor = ArrayHelper::map(\common\models\Repartidor::find()->all(),'idrepartidor','apellido');
$cliente = empty($model->clienteIdcliente) ? '' : $model->clienteIdcliente->razonsocial;
$idOrdenGrupal = Yii::$app->getRequest()->getQueryParam('idOrdenGrupal');
$dataProvincia=ArrayHelper::map(Provincia::find()->orderBy('nombre')->asArray()->all(), 'idprovincia', 'nombre');

?>

<div class="guia-domicilio-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="hide"><?= $form->field($model, 'ordengrupaldomicilio_idordengrupaldomicilio')->textInput() ?></div>

    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'cantidad')->textInput() ?></div>

<div class="col-md-3 col-xs-12"><?= $form->field($model, 'empresa_destino')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'numero_guia')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'guia_tipo')->dropDownList([ 'No Aplica' => 'No Aplica', 'A' => 'A', 'B' => 'B', 'R' => 'R', 'X' => 'X', ]) ?></div>

    <?php
        $dataDepartamentos = [];
        $dataLocalidades = [];
        $departamento = new Departamento();
        $provincia = new Provincia();
        $provincia->idprovincia = 12;
        $dataDepartamentos = ArrayHelper::map(Departamento::find()->where(['provincia_idprovincia' => 12])->orderBy('nombre')->asArray()->all(), 'iddepartamento', 'nombre');
        if($model->localidad_idlocalidad){
            $provincia->idprovincia = $model->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->idprovincia;
            $dataDepartamentos =  ArrayHelper::map(Departamento::find()
              ->select(['iddepartamento as id','nombre as name'])
              ->where(['provincia_idprovincia' => $provincia->idprovincia])
              ->orderBy('nombre ASC')
              ->asArray()
              ->all(), 'id', 'name');
            $departamento->iddepartamento = $model->localidadIdlocalidad->departamentoIddepartamento->iddepartamento;
            $dataLocalidades = ArrayHelper::map(Localidad::find()
            ->select(['idlocalidad as id','nombre as name'])
            ->where(['departamento_iddepartamento' => $departamento->iddepartamento])
            ->orderBy('nombre ASC')
            ->asArray()
            ->all(), 'id', 'name');
        }
    ?>

    <div class='col-md-3'>
       <?= $form->field($provincia, 'idprovincia')->dropDownList($dataProvincia,
          ['prompt'=>'-Seleccionar-',
            'id' => 'provinciaDestino'])->label('Provincia');
        ?>
    </div>

    <div class='col-md-3'>
            <?= $form->field($departamento, 'iddepartamento')->widget(DepDrop::classname(), [
          //'data'=> $initLocalidad,
          'data' => $dataDepartamentos,
          'language' => 'es',
          'options' => ['id'=>'departamento'],
          'type' => DepDrop::TYPE_SELECT2,
              'pluginOptions'=>[
              'depends' => ['provinciaDestino'],
              'placeholder' => '- Seleccionar Departamento -',
              'url' => \yii\helpers\Url::to(['direccion/departamento-provincia']),
              'loadingText' => 'Cargando Departamentos...'
            ],
        ])->label('<a href="../departamento/create" target="_blank"><i class="fa fa-plus green"></i></a> Departamento');
        ?>
    </div>

    <div class='col-md-3'>
        <?= $form->field($model, 'localidad_idlocalidad')->widget(DepDrop::classname(), [
          //'data'=> $initLocalidad,
          'data' => $dataLocalidades,
          'language' => 'es',
          'options' => ['id'=>'localidad'],
          'type' => DepDrop::TYPE_SELECT2,
            'pluginOptions'=>[
            'depends' => ['departamento'],
            'placeholder' => '- Seleccionar Localidad -',
            'url' => \yii\helpers\Url::to(['direccion/departamento-localidad']),
            'loadingText' => 'Cargando Localidades...'
          ],
        ])->label('<a href="../localidad/create" target="_blank"><i class="fa fa-plus green"></i></a> Localidad');
        ?>
    </div>

    <?php
        if($model->ordengrupaldomicilioIdordengrupaldomicilio->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo A"){
    ?>
            <div class="col-md-3 col-xs-12"><?= $form->field($model, 'monto')->textInput() ?></div>
    <?php
        }else{
    ?>
            <div class="col-md-3 col-xs-12"><?= $form->field($model, 'monto')->textInput()->label('Rendición') ?></div>
            <div class="col-md-12">
                <div class="col-md-3"><?= $form->field($model, 'sprinter')->checkbox()?></div>
            </div>


    <?php
        }
    ?>

    <div class="form-group col-md-12">
        <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);?>
        <?= Html::submitButton(Yii::t('app', 'Agregar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
