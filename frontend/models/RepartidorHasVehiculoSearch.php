<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RepartidorHasVehiculo;

/**
 * RepartidorHasVehiculoSearch represents the model behind the search form of `common\models\RepartidorHasVehiculo`.
 */
class RepartidorHasVehiculoSearch extends RepartidorHasVehiculo
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idtepartidor_has_vehiculo', 'repartidor_idrepartidor', 'vehiculo_idvehiculo'], 'integer'],
            [['desde', 'hasta'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RepartidorHasVehiculo::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idtepartidor_has_vehiculo' => $this->idtepartidor_has_vehiculo,
            'desde' => $this->desde,
            'hasta' => $this->hasta,
            'repartidor_idrepartidor' => $this->repartidor_idrepartidor,
            'vehiculo_idvehiculo' => $this->vehiculo_idvehiculo,
        ]);

        return $dataProvider;
    }
}
