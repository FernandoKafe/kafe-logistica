<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\db\Query;
use frontend\models\LiquidacionHasRemitoSearch;
use common\models\LiquidacionHasRemito;
use common\models\Remito;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use common\models\TarifarioHasPrecio;
use common\models\Liquidacion;


/* @var $this yii\web\View */
/* @var $model common\models\Liquidacion */
$idLiquidacion = Yii::$app->getRequest()->getQueryParam('id');


$this->title = "Liquidación para: ".$model->clienteIdcliente->razonsocial." - Periodo: ".$model->periodo;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidación'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-view row" style="overflow: hidden;">
    <div class="col-xs-3" style="float: left;">
    <h3>Liquidación: <?= $model->clienteIdcliente->razonsocial?></h3>
        <h3>Periodo: <?= $model->periodo?></h3>
        <h3>CUIT: <?=$model->clienteIdcliente->cuit?></h3>
    </div>
    <div class="col-xs-4" style=" margin: 0 auto 0;">
        <?php echo Html::img('@web/img/logo/LOGO.jpg', ['class' => 'img-responsive', 'width'=>'210', 'style' => 'margin: 0 0;']) ?>
    </div>
    <div class="col-xs-3" style="border: 1px solid #d3d3d3; float:right; margin: 0 15px 0 0;">
            <div class="col-xs-2" style="margin: 20px 0 0 0;">
                <p>Firma: </p>
            </div>
            <div class="col-xs-6" style="margin: 0 0 0 0;">
                <p>____________________</p>
            </div>
            <div class="col-xs-2">
                <p>Aclaración:</p>
            </div>     
            <div class="col-xs-6">
                <p>____________________</p>
            </div>
            <div class="col-xs-2">
                <p>Fecha:</p>
            </div>
            <div class="col-xs-6">
                <p>_____ / _____ / _____</p>
            </div>
            <div class="col-xs-12" style="margin: 35px 0 0 0;">
                    <?= "<div class='col-md-12 tabla-totales'>TOTAL: ".number_format($model->total, 2 , "." ,  "," )."</div>"; ?>
            </div>
        </div>    
</div>

<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_title">
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php
    /*$remito = Remito::find()->select('remitofecha, facturado_a, tipo_remito, volumen, terminal,carga_idcarga, sucursal_idsucursal, empresatransporte_idempresatransporte, cliente_idcliente, numero_remito, numero_guia, rol_sucursal, cantidad_bultos, total_guia, ordenderetirogrupal_idordenderetirogrupal, precio, precio_adicional, precio_reparto, precio_reparto_adicional, porcentaje_contra')
    ->joinWith('liquidacionHasRemito')
    ->where(['liquidacion_idliquidacion' => $idLiquidacion]);*/
    $remito = Remito::remitosView($idLiquidacion);
    $dataProvider = new ActiveDataProvider([
        'query' => $remito,
        'pagination' => false,
    ]);
    $dataProvider->setSort([
        'attributes' => [
            'remitofecha' => [
                'asc' => ['remitofecha' => SORT_ASC],
                'desc' => ['remitofecha' => SORT_DESC],
                'default' => SORT_ASC
            ],
        ],
        'defaultOrder' => [
            'remitofecha' => SORT_ASC
        ]
    ]);
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'remitofecha',
            'label' => 'Fecha',
        ],
        [
            'attribute' => 'numero_remito',
            'label' => 'Remito',
            'value' => function($data){
                if($data->numero_remito){
                    return $data->numero_remito;
                }else{
                    return $data->ordenderetirogrupalIdordenderetirogrupal->numero;
                }
            }
        ],
        [
            'attribute' => 'numero_guia',
            'label' => 'Guía',
            'value' => function($data){
                return $data->numero_guia.' - '.$data->tipo_remito;
            }
        ],
        [
            'label' => 'Destinatario',
            'value' => function($data){
                if($data->rol_sucursal == "Remite"){
                    return $data->clienteIdcliente->razonsocial;
                }else{
                    return $data->sucursalIdsucursal->nombre.", ".$data->sucursalIdsucursal->empresaIdempresa->nombre;
                }
            },
        ],
        [
            'attribute' => 'sucursalIdsucursal',
            'label' => 'Destino',
            'value' => function($data){
                if(isset($data->sucursalIdsucursal->localidad_idlocalidad)){
                    return $data->sucursalIdsucursal->localidadIdlocalidad->nombre.", ".
                    $data->sucursalIdsucursal->localidadIdlocalidad->departamentoIddepartamento->nombre.", ".
                    $data->sucursalIdsucursal->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                }else{
                    return "SIN LOCALIDAD ASIGNADA";
                }
            }
        ],
        [
            'attribute' => 'empresatransporte_idempresatransporte',
            'label' => 'Empresa de transporte',
            'value' => function($data){
                if(isset($data->empresatransporteIdempresatransporte->razonsocial)){
                    return $data->empresatransporteIdempresatransporte->razonsocial;
                }else{
                    return "SIN NOMBRE ASIGNADO";
                }
            },
            'footer' => '<strong>TOTAL</strong>',
        ],
        [
            'attribute' => 'cantidad_bultos',
            'label' => 'Cantidad bultos',
            'value' => function($data){
                return $data->cantidad_bultos;
            },
        ],
        [
            'attribute' => 'volumen',
            'label' => 'Cantidad bultos excedentes',
            'value' => function($data){
                return $data->volumen;
            },
        ],
        [
            'label' => 'Importe Bultos',
            'format' => ['decimal',2],
            'value' => function($data){
                $remitoFecha = Remito::testIfFirstOfDate($data->remitofecha, $data->cliente_idcliente);
                if($data->clienteIdcliente->tarifarioIdtarifario->tipo == "Tipo B"){
                    if($remitoFecha->idremito == $data->idremito){
                        if($data->cantidad_bultos == 0)
                            return ($data->volumen * $data->precio_adicional);
                        else
                            return ($data->precio + (($data->cantidad_bultos + $data->volumen - 1) * $data->precio_adicional));
                    }else{
                        if($data->cantidad_bultos == 0)
                            return ($data->volumen * $data->precio_adicional);
                        else
                            return (($data->cantidad_bultos + $data->volumen) * $data->precio_adicional);
                    }
                }else{
                    return $data->precio + (($data->cantidad_bultos + $data->volumen -1) * $data->precio_adicional);
                }
            },
        ],
        /*[
            'label' => 'Importe Bulto',
            'value' => function($data){
                $tarifHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($data->carga_idcarga, $data->ordenderetirogrupalIdordenderetirogrupal->clienteIdcliente->tarifarioIdtarifario, $data->sucursalIdsucursal->localidadIdlocalidad->zonaHasLocalidad->zona_idzona);
                return '$ '.$tarifHasPrecio->precio;
            }
        ],*/
        [
            'attribute' => 'total_guia',
            'label' => 'Guías Logística',
            'format' => ['decimal',2],
            'value' => function($data){
                if(( ($data->tipo_remito == 'A') || ($data->tipo_remito == 'B') || ($data->tipo_remito == 'R') ) && ($data->facturado_a == 'Vertiente')){
                    return $data->total_guia;
                } else {
                    return '';
                }
            },
        ],
        [
            'attribute' => 'total_guia',
            'label' => 'Guías Cliente',
            'format' => ['decimal',2],
            'value' => function($data){
                if( ( ($data->tipo_remito == 'A') || ($data->tipo_remito == 'B') ) && ($data->facturado_a == 'Cliente')){
                    return $data->total_guia;
                } else {
                    return '';
                }
            },
        ],
    ],
    ]); ?>
    <table style="border: solid 0.5px gainsboro; padding: 5px; border-collapse: collapse; width: 100%; margin-bottom: 50px;">
        <thead>
            <tr>
                <th style="border: solid 1px gainsboro; padding: 5px;"></th>
                <th style="border: solid 1px gainsboro; padding: 5px;">TOTAL Bultos</th>
                <th style="border: solid 1px gainsboro; padding: 5px;">TOTAL Volumen</th>
                <th style="border: solid 1px gainsboro; padding: 5px;">TOTAL Importe bultos</th>
                <th style="border: solid 1px gainsboro; padding: 5px;">TOTAL Guías logística</th>
                <th style="border: solid 1px gainsboro; padding: 5px;">TOTAL Guías cliente</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td style="border: solid 1px gainsboro; padding: 5px; font-weight: bold;">TOTALES</td>
                <td style="border: solid 1px gainsboro; padding: 5px;"><?= $model->bultos?></td>
                <td style="border: solid 1px gainsboro; padding: 5px;"><?= $model->volumen?></td>
                <td style="border: solid 1px gainsboro; padding: 5px;"><?= number_format ($model->importe_bultos, 2 , "," ,  "." )?></td>
                <td style="border: solid 1px gainsboro; padding: 5px;"><?= number_format ($model->guias_logistica + (0.21 * $model->guias_logistica), 2 , "," ,  "." )?></td>
                <td style="border: solid 1px gainsboro; padding: 5px;"><?= number_format ($model->importe_guias, 2 , "," ,  "." )?></td>
            </tr>
        </tbody>
    </table>


    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-striped table-bordered detail-view tabla-totales' ],
        'attributes' => [
            //'idliquidacion',
            [
                'attribute' => 'importe_bultos',
                'label' => 'IMPORTES RETIROS/ENTREGA',
                'format' => ['decimal', 2],

            ],
            [
                'attribute' => 'guias_logistica',
                'label' => 'GUÍAS LOGÍSTICA',
                'format' => ['decimal', 2],
            ],
            [
                'attribute' => 'total_contra',
                'label' => 'TOTAL CONTRA REEMBOLSO',
                'format' => ['decimal', 2],

            ],
            [
                'attribute' => 'subtotal',
                'label' => 'SUBTOTAL',
                'format' => ['decimal', 2],
            ],
            [
                'attribute' => 'total_iva',
                'label' => 'IVA 21%',
                'format' => ['decimal', 2],
            ],
            [
                'attribute' => 'importe_guias',
                'label' => 'GUÍAS CLIENTE',
                'format' => ['decimal', 2],
            ],
            [
                'attribute' => 'total',
                'format' => ['decimal', 2],
            ],
        ],
    ]) ?>

    </div>
</div>