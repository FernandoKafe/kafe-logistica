<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LiquidacionHasRemito;

/**
 * LiquidacionHasRemitoSearch represents the model behind the search form of `common\models\LiquidacionHasRemito`.
 */
class LiquidacionHasRemitoSearch extends LiquidacionHasRemito
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idliquidacion_has_remito', 'liquidacion_idliquidacion', 'remito_idremito'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LiquidacionHasRemito::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idliquidacion_has_remito' => $this->idliquidacion_has_remito,
            'liquidacion_idliquidacion' => $this->liquidacion_idliquidacion,
            'remito_idremito' => $this->remito_idremito,
        ]);

        return $dataProvider;
    }
}
