<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

use common\models\OrdenGrupalDomicilio;
use common\models\GuiaDomicilio;
/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionDomicilioRepartidor */

$this->title = "Liquidación domicilio: ".$model->repartidorIdrepartidor->apellido.", ".$model->repartidorIdrepartidor->nombre." - Periodo: ".$model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion Domicilio Repartidors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-domicilio-cliente-view">

<h1><?= Html::encode($this->title) ?></h1>
<div class="x_panel">
<div class="x_title">
    <div class="clearfix"></div>
</div>
<p>
    <?= Html::a(Yii::t('app', 'Guardar'), ['index'], ['class' => 'btn btn-success']) ?>
    <?= Html::a(Yii::t('app', 'PDF'), ['report', 'id' => $model->idliquidacion_domicilio_repartidor], ['class' => 'btn btn-warning']) ?>

</p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idliquidacion_domicilio_repartidor',
            'fecha_inicio',
            'fecha_fin',
            'nombre',
            [
                'attribute' => 'repartidor_idrepartidor',
                'value' => function($data){
                    return $data->repartidorIdrepartidor->apellido.", ".$data->repartidorIdrepartidor->nombre;
                }
            ],
            [
                'attribute' => 'estadoliquidacion_idestadoliquidacion',
                'value' => function($data){
                    return $data->estadoliquidacionIdestadoliquidacion->nombre;
                }
            ],
            [
                'attribute' => 'total',
                'format' => ['decimal', 2],
            ],
        ],
    ]) ?>

    </div>
</div>

<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_title">
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php
    $ordenesGrupales = OrdenGrupalDomicilio::getAllOrdenesRepartidorFechaLiquidados($model->repartidor_idrepartidor, $model->fecha_inicio, $model->fecha_fin, $model->idliquidacion_domicilio_repartidor);
    $ordenesGrupalesId = ArrayHelper::getColumn($ordenesGrupales,'idorden_grupal_domicilio');
    $remitos = GuiaDomicilio::find()->joinWith(['ordengrupaldomicilioIdordengrupaldomicilio'])->where(['IN', 'ordengrupaldomicilio_idordengrupaldomicilio', $ordenesGrupalesId])->orderBy(['fecha' => SORT_ASC]);
    $dataProvider = new ActiveDataProvider([
        'query' => $remitos,
    ]);
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    'moduleId' => 'gridviewk',
    'toolbar' =>  [
        '{export}',
        '{toggleData}',
    ],
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
    ],
    'options' => [
        'style' => 'font-size:10px;'
    ],
    'columns' => [
        [
            'label' => 'fecha',
            'value' => function($data){
                return $data->ordengrupaldomicilioIdordengrupaldomicilio->fecha;
            }
        ],
        [
            'attribute' => 'numero_guia',
            'label' => 'Nº de guía y tipo',
            'value' => function($data){
                return $data->numero_guia.' - '.$data->guia_tipo;
            }
        ],
        'empresa_destino',
        [
            'attribute' => 'localidad_idlocalidad',
            'value' => function($data){
                if(isset($data->empresa->localidad_idlocalidad)){
                    return $data->localidadIdlocalidad->nombre.", ".
                    $data->localidadIdlocalidad->departamentoIddepartamento->nombre.", ".
                    $data->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                }else{
                    return "SIN LOCALIDAD ASIGNADA";
                }
            }
        ],
        [
            'label' => 'Cliente',
            'value' => function($data){
                return $data->ordengrupaldomicilioIdordengrupaldomicilio->clienteIdcliente->razonsocial;
            }
        ],
        [
            'label' => 'Tipo Tarifario',
            'value' => function($data){
                return $data->ordengrupaldomicilioIdordengrupaldomicilio->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo;
            }
        ],
        [
            'attribute' => 'cantidad',
            'label' => 'Bultos',
        ],
        [
            //'attribute' => 'precio_reparto',
            'label' => 'Valor Reparto',
            'format' => ['decimal', 2],
            'value' => function($data) {
                return $data->ordengrupaldomicilioIdordengrupaldomicilio->precio_reparto;
            }
        ],
        [
            //'attribute' => 'precio_reparto_adicional',
            'label' => 'Valor Reparto Adicional',
            'format' => ['decimal', 2],
            'value' => function($data) {
                return $data->ordengrupaldomicilioIdordengrupaldomicilio->precio_reparto_adicional;
            }
        ],
    ],
    ]); ?>

    </div>
</div>