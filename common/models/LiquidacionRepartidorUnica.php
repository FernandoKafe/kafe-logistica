<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%liquidacion_repartidor_unica}}".
 *
 * @property int $idliquidacion_unica
 * @property string $nombre
 * @property string $fecha_inicio
 * @property string $fecha_fin
 * @property double $total
 * @property int $estadoliquidacion_idestadoliquidacion
 * @property int $repartidor_idrepartidor
 * @property string $creado
 *
 * @property EstadoLiquidacion $estadoliquidacionIdestadoliquidacion
 * @property Repartidor $repartidorIdrepartidor
 */
class LiquidacionRepartidorUnica extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%liquidacion_repartidor_unica}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'fecha_inicio', 'fecha_fin', 'repartidor_idrepartidor'], 'required'],
            [['fecha_inicio', 'fecha_fin', 'creado'], 'safe'],
            [['total'], 'number'],
            [['estadoliquidacion_idestadoliquidacion', 'repartidor_idrepartidor'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['estadoliquidacion_idestadoliquidacion'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoLiquidacion::className(), 'targetAttribute' => ['estadoliquidacion_idestadoliquidacion' => 'idestado_liquidacion']],
            [['repartidor_idrepartidor'], 'exist', 'skipOnError' => true, 'targetClass' => Repartidor::className(), 'targetAttribute' => ['repartidor_idrepartidor' => 'idrepartidor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idliquidacion_unica' => Yii::t('app', 'Idliquidacion Unica'),
            'nombre' => Yii::t('app', 'Periodo'),
            'fecha_inicio' => Yii::t('app', 'Fecha Inicio'),
            'fecha_fin' => Yii::t('app', 'Fecha Fin'),
            'total' => Yii::t('app', 'Total'),
            'estadoliquidacion_idestadoliquidacion' => Yii::t('app', 'Estado liquidacion'),
            'repartidor_idrepartidor' => Yii::t('app', 'Repartidor'),
            'creado' => Yii::t('app', 'Creado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoliquidacionIdestadoliquidacion()
    {
        return $this->hasOne(EstadoLiquidacion::className(), ['idestado_liquidacion' => 'estadoliquidacion_idestadoliquidacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepartidorIdrepartidor()
    {
        return $this->hasOne(Repartidor::className(), ['idrepartidor' => 'repartidor_idrepartidor']);
    }
}
