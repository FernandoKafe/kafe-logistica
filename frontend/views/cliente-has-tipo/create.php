<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ClienteHasTipo */

$this->title = Yii::t('app', 'Create Cliente Has Tipo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cliente Has Tipos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cliente-has-tipo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
