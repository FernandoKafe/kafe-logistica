<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionrepartidorHasRemito */

$this->title = Yii::t('app', 'Update Liquidacionrepartidor Has Remito: ' . $model->liquidacionrepartidor_idliquidacionrepartidor, [
    'nameAttribute' => '' . $model->liquidacionrepartidor_idliquidacionrepartidor,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacionrepartidor Has Remitos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->liquidacionrepartidor_idliquidacionrepartidor, 'url' => ['view', 'liquidacionrepartidor_idliquidacionrepartidor' => $model->liquidacionrepartidor_idliquidacionrepartidor, 'remito_idremito' => $model->remito_idremito]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="liquidacionrepartidor-has-remito-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
