<?php

use common\models\LiquidacionDomicilioRepartidor;
use common\models\OrdenGrupalDomicilio;
use common\models\GuiaDomicilio;
use \yii\helpers\ArrayHelper; 

$ordenesGrupales = OrdenGrupalDomicilio::getAllOrdenesRepartidorFechaLiquidados($model->repartidor_idrepartidor, $model->fecha_inicio, $model->fecha_fin, $liquidacionDomicilio->idliquidacion_domicilio_repartidor);
$ordenesGrupalesId = ArrayHelper::getColumn($ordenesGrupales,'idorden_grupal_domicilio');
$remitosDomicilio = GuiaDomicilio::find()
    ->select('  SUM(cantidad) as cantidad,
                count(*) as cantidad_tipob,
                precio_reparto,
                precio_reparto_adicional,
                fecha,
                tipo_cliente as empresa_destino,
                ordengrupaldomicilio_idordengrupaldomicilio,
                cliente_idcliente')
    ->joinWith(['ordengrupaldomicilioIdordengrupaldomicilio'])
    ->where(['IN', 'ordengrupaldomicilio_idordengrupaldomicilio', $ordenesGrupalesId])
    ->groupBy([ 'cliente_idcliente','fecha' ])
    ->all();

//echo "Dom <pre>" , print_r($remitosDomicilio) , "</pre><br>";

$remitosClientes = OrdenGrupalDomicilio::getAllOrdenesRepartidorFechaLiquidadosCliente($model->repartidor_idrepartidor, $model->fecha_inicio, $model->fecha_fin, $liquidacionDomicilio->idliquidacion_domicilio_repartidor);
if($remitosDomicilio){?>
    <table style="table-layout: fixed;" cellspacing="0">
        <thead>
            <tr>
                <td style='border: solid 1px #ccc; text-align:center; padding: 5px;' colspan=6><strong>RESUMEN CLIENTES
                        DOMICILIO</strong></td>
            </tr>
            <tr>
                <th style='border: solid 1px #ccc; padding: 5px;'>Cliente</th>
                <th style='border: solid 1px #ccc; padding: 5px;'>Cantidad</th><!--Resto de celdas de fila cliente-->
                <th style='border: solid 1px #ccc; padding: 5px;'>Adicional</th><!--Resto de celdas de fila cliente-->
                <th style='border: solid 1px #ccc; padding: 5px;'>Reparto</th><!--Resto de celdas de fila cliente-->
                <th style='border: solid 1px #ccc; padding: 5px;'>Sprinter</th><!--Resto de celdas de fila cliente-->
                <th style='border: solid 1px #ccc; padding: 5px;'>TOTAL</th><!--Resto de celdas de fila cliente-->
            </tr>
        </thead>
        <tbody >
            <?php
                $cantidadAcumulada = 0;
                $cantidadAdicionalAcumulada = 0;
            foreach($remitosClientes as $rc) {
                $totalCantidad = 0;
                $precio = $precioAdicional = 0;
                $totalCantidadAdicional = 0;?><!--Fila cliente-->
                <tr>
                    <td style='border: solid 1px #ccc; padding: 5px;'><strong><?= $rc->clienteIdcliente->razonsocial."(".$rc->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo.")" ?></strong></td><!--Primer celda fila cliente-->
                <?php
                foreach($remitosDomicilio as $rd) {
                    if($rc->cliente_idcliente == $rd->cliente_idcliente) {
                        if($rd->empresa_destino == "Tipo A"){
                            $totalCantidad += $rd->cantidad;
                            $precio = $rd->precio_reparto;
                        }
                        if($rd->empresa_destino == "Tipo B"){
                            $totalCantidad += $rd->cantidad_tipob;
                            $totalCantidadAdicional += $rd->cantidad - $rd->cantidad_tipob;
                            $precio = $rd->precio_reparto;
                            $precioAdicional = $rd->precio_reparto_adicional;
                        }
                    }
                } 
                    $cantidadAcumulada += ($totalCantidad * $precio);
                    $cantidadAdicionalAcumulada += ($totalCantidadAdicional * $precioAdicional);
                ?>

                    <td style='border: solid 1px #ccc; padding: 5px;'><?=$totalCantidad." x $".$precio." = $".($totalCantidad * $precio)?></strong></td>
                    <td style='border: solid 1px #ccc; padding: 5px;'><?=$totalCantidadAdicional." x $".$precioAdicional." = $".($totalCantidadAdicional * $precioAdicional)?></strong></td>
                    <td style='border: solid 1px #ccc; padding: 5px;'></td>
                    <td style='border: solid 1px #ccc; padding: 5px;'></td>
                    <td style='border: solid 1px #ccc; padding: 5px;'>$<?=(($totalCantidad * $precio) + ($totalCantidadAdicional * $precioAdicional))?></strong></td>
                </tr>
                <?php
            } ?>
            <tr>
                <td style='border: solid 1px #ccc; padding: 5px; text-align: center;'><strong>TOTAL</strong></td>
                <td style='border: solid 1px #ccc; padding: 5px; text-align: center;'><strong>$<?=$cantidadAcumulada?></strong></td>
                <td style='border: solid 1px #ccc; padding: 5px; text-align: center;'><strong>$<?=$cantidadAdicionalAcumulada?></strong></td>
                <td style='border: solid 1px #ccc; padding: 5px; text-align: center;'><strong></strong></td>
                <td style='border: solid 1px #ccc; padding: 5px; text-align: center;'><strong></strong></td>
                <td style='border: solid 1px #ccc; padding: 5px; text-align: center;'><strong>$<?=($cantidadAdicionalAcumulada + $cantidadAcumulada)?></strong></td>
            </tr>
        </tbody>
    </table>
    <br>
    <br>
<?php  
} ?>