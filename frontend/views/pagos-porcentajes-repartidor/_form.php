<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\PagosPorcentajesRepartidor */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pagos-porcentajes-repartidor-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-4 col-xs-12"><?= $form->field($model, 'extremo_inferior')->textInput() ?></div>

    <div class="col-md-4 col-xs-12"><?= $form->field($model, 'extremo_superior')->textInput() ?></div>

    <div class="col-md-4 col-xs-12"><?= $form->field($model, 'monto')->textInput() ?></div>

    <div class="col-md-12 form-group">
        <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary'])?>
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
