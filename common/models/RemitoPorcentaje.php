<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%remito_porcentaje}}".
 *
 * @property int $idremito_porcentaje
 * @property string $fecha
 * @property string $numero_planilla
 * @property int $cant_pedidos
 * @property int $cant_entregados
 * @property int $noquiso_nopidio
 * @property int $cant_anulado
 * @property int $cant_sin_dinero
 * @property int $cant_regresado
 * @property int $cant_faltante
 * @property int $cant_mal_armado
 * @property int $cant_duplicado
 * @property int $otro
 * @property string $detalle_otro
 * @property double $dinero_facturado
 * @property double $dinero_rendido
 * @property double $dinero_devuelto
 * @property double $dinero_faltante
 * @property string $observacion_faltante
 * @property int $cliente_idcliente
 * @property int $repartidor_idrepartidor
 * @property int $porcentaje_idporcentaje
 * @property int $liquidado_cliente
 * @property int $liquidado_repartidor
 * @property double $combustible
 * @property double $pallets
 * @property double $porcentaje
 * @property string $creado
 * @property string $reparto_uno
 * @property string $reparto_dos
 * @property string $reparto_tres
 * @property string $reparto_cuatro
 * @property string $reparto_seleccionado
 * @property int porcentaje_entregas
 * @property int se_paga
 *
 * @property LiquidacionporcentajerepartidorHasRemito[] $liquidacionporcentajerepartidorHasRemitos
 * @property LpcHasRemitoporcentaje[] $lpcHasRemitoporcentajes
 * @property Cliente $clienteIdcliente
 * @property Porcentaje $porcentajeIdporcentaje
 * @property Repartidor $repartidorIdrepartidor
 */
class RemitoPorcentaje extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%remito_porcentaje}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'creado'], 'safe'],
            [['cant_pedidos', 'cant_entregados', 'noquiso_nopidio', 'cant_anulado', 'cant_sin_dinero', 'cant_regresado', 'cant_faltante', 'cant_mal_armado', 'cant_duplicado', 'otro', 'cliente_idcliente', 'repartidor_idrepartidor', 'porcentaje_idporcentaje', 'liquidado_cliente', 'liquidado_repartidor', 'se_paga'], 'integer'],
            [['detalle_otro', 'observacion_faltante'], 'string'],
            [['dinero_facturado', 'dinero_rendido', 'dinero_devuelto', 'dinero_faltante', 'combustible', 'porcentaje', 'pallets', 'reparto_uno', 'reparto_dos', 'reparto_tres', 'reparto_cuatro', 'reparto_seleccionado', 'porcentaje_entregas'], 'number'],
            [['cliente_idcliente', 'repartidor_idrepartidor', 'porcentaje_idporcentaje', 'cant_pedidos'], 'required'],
            [['numero_planilla'], 'string', 'max' => 50],
            [['cliente_idcliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['cliente_idcliente' => 'idcliente']],
            [['porcentaje_idporcentaje'], 'exist', 'skipOnError' => true, 'targetClass' => Porcentaje::className(), 'targetAttribute' => ['porcentaje_idporcentaje' => 'idporcentaje']],
            [['repartidor_idrepartidor'], 'exist', 'skipOnError' => true, 'targetClass' => Repartidor::className(), 'targetAttribute' => ['repartidor_idrepartidor' => 'idrepartidor']],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idremito_porcentaje' => Yii::t('app', 'Id de remito Porcentaje'),
            'fecha' => Yii::t('app', 'Fecha'),
            'numero_planilla' => Yii::t('app', 'Nº Planilla'),
            'cant_pedidos' => Yii::t('app', 'Pedidos'),
            'cant_entregados' => Yii::t('app', 'Entregados'),
            'noquiso_nopidio' => Yii::t('app', 'No quiso o No pidio'),
            'cant_anulado' => Yii::t('app', 'Cerrados'),
            'cant_sin_dinero' => Yii::t('app', 'Sin Dinero'),
            'cant_regresado' => Yii::t('app', 'Regresar'),
            'cant_faltante' => Yii::t('app', 'Faltantes'),
            'cant_mal_armado' => Yii::t('app', 'Mal Armados'),
            'cant_duplicado' => Yii::t('app', 'Duplicados'),
            'otro' => Yii::t('app', 'Otro'),
            'detalle_otro' => Yii::t('app', 'Detalle Otro'),
            'dinero_facturado' => Yii::t('app', 'Dinero Facturado'),
            'dinero_rendido' => Yii::t('app', 'Dinero Rendido'),
            'dinero_devuelto' => Yii::t('app', 'Dinero Devuelto'),
            'dinero_faltante' => Yii::t('app', 'Dinero Faltante'),
            'observacion_faltante' => Yii::t('app', 'Observación Faltante'),
            'cliente_idcliente' => Yii::t('app', 'Cliente'),
            'repartidor_idrepartidor' => Yii::t('app', 'Repartidor'),
            'porcentaje_idporcentaje' => Yii::t('app', 'Porcentaje'),
            'liquidado_cliente' => Yii::t('app', 'Liquidado Cliente'),
		    'liquidado_repartidor' => Yii::t('app', 'Liquidado Repartidor'),
	        'combustible' => Yii::t('app', 'Combustible'),
	        'porcentaje' => Yii::t('app', 'Porcentaje'),
	        'pallets' => Yii::t('app', 'Pallet'),
            'creado' => Yii::t('app', 'Creado'), 
            'reparto_uno' => Yii::t('app', 'reparto_uno'), 
            'reparto_dos' => Yii::t('app', 'reparto_dos'), 
            'reparto_tres' => Yii::t('app', 'reparto_tres'), 
            'reparto_cuatro' => Yii::t('app', 'reparto_cuatro'), 
            'reparto_seleccionado' => Yii::t('app', 'reparto_seleccionado'),
            'porcentaje_entregas' => Yii::t('app', 'porcentaje_entregas'),
            'se_paga' => Yii::t('app', 'se cobra'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacionporcentajerepartidorHasRemitos()
    {
        return $this->hasMany(LiquidacionporcentajerepartidorHasRemito::className(), ['remitoporcentaje_idremitoporcentaje' => 'idremito_porcentaje']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLpcHasRemitoporcentajes()
    {
        return $this->hasMany(LpcHasRemitoporcentaje::className(), ['rp_idrp' => 'idremito_porcentaje']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClienteIdcliente()
    {
        return $this->hasOne(Cliente::className(), ['idcliente' => 'cliente_idcliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPorcentajeIdporcentaje()
    {
        return $this->hasOne(Porcentaje::className(), ['idporcentaje' => 'porcentaje_idporcentaje']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepartidorIdrepartidor()
    {
        return $this->hasOne(Repartidor::className(), ['idrepartidor' => 'repartidor_idrepartidor']);
    }

    /**
     * @return \yii\db\AvtiveQuery 
     */
    static public function getRemitosPorcentajeFechaCliente($clienteId, $fechaInicio, $fechaFin, $porcentaje){
        return self::find()
        ->where(['cliente_idcliente' => $clienteId])
        ->andWhere(['>=', 'fecha', $fechaInicio])
        ->andWhere(['<=', 'fecha', $fechaFin])
        ->andWhere(['porcentaje' => $porcentaje])
        ->andWhere([ 'liquidado_cliente' => 0])
        //->andWhere(['!=', 'estado_idestado', 4])
        //->andWhere(['formadepago_idformadepago' => 3])
        ->all();
    }

    /**
     * @return \yii\db\AvtiveQuery 
     */
    static public function getRemitosPorcentajeFechaRepartidor($repartidorId, $fechaInicio, $fechaFin){
        return self::find()
        ->where(['repartidor_idrepartidor' => $repartidorId])
        ->andWhere(['>=', 'fecha', $fechaInicio])
        ->andWhere(['<=', 'fecha', $fechaFin])
        ->andWhere([ 'liquidado_repartidor' => 0])
        ->orderBy(['fecha' => SORT_ASC])
        //->andWhere(['!=', 'estado_idestado', 4])
        //->andWhere(['formadepago_idformadepago' => 3])
        ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getPorcentajesFecha($clienteId, $fechaInicio, $fechaFin){
        return self::find()
        ->select('porcentaje')
        ->where(['cliente_idcliente' => $clienteId])
        ->andWhere(['>=', 'fecha', $fechaInicio])
        ->andWhere(['<=', 'fecha', $fechaFin])
        ->andWhere([ 'liquidado_cliente' => 0])
        ->groupBy('porcentaje')
        ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getRemitosLiquidadosByRepartidor($repartidorId, $fechaInicio, $fechaFin){
        return self::find()
        ->where(['repartidor_idrepartidor' => $repartidorId])
        ->andWhere(['>=', 'fecha', $fechaInicio])
        ->andWhere(['<=', 'fecha', $fechaFin])
        ->andWhere([ 'liquidado_repartidor' => 1])
        ->orderBy(['fecha' => SORT_ASC])->all();
    }}
