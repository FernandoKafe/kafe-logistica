<?php

use yii\helpers\Html;
use common\models\Liquidacion;


/* @var $this yii\web\View */
/* @var $model common\models\Liquidacion */

$this->title = Yii::t('app', 'Análisis mensual cantidad de bultos');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-create"><!--1-->

    <div class="x_panel"><!--2-->
        <div class="x_title"><!--3-->
            <h2><?= Yii::t('app', 'Análisis mensual cantidad de bultos') ?></h2>
            <div class="clearfix"></div><!--4/4-->
            <p><strong style="color: red;">Uso:</strong> Seleccionar un cliente y luego un año.</p>
            <p><strong style="color: blue;">Información:</strong> Muestra cantidad de bultos/cajas para cliente y año seleccionado. Información discriminada por meses del año.</p>
        </div><!--3-->
        <div class="x_content"></div><!--5/5-->

        <?= $this->render('_form', [
            'model' => $model,
            'analisisForm' => 1,
        ]) ?>
    </div> 

    <div class="x_panel"><!--2-->
        <div class="x_title"><!--3-->
            <h2><?= Yii::t('app', 'Cantidad de bultos por mes y zona por cliente') ?></h2>
            <div class="clearfix"></div><!--4/4-->
            <p><strong style="color: red;">Uso:</strong> Seleccionar un cliente y luego un año.</p>
            <p><strong style="color: blue;">Información:</strong> Muestra cantidad de bultos/cajas para cliente y año seleccionado. Información discriminada por meses del año y zonas de entrega.</p>
        </div><!--3-->
        <div class="x_content"></div><!--5/5-->

        <?= $this->render('_form', [
            'model' => $model,
            'analisisForm' => 3,
        ]) ?>
    </div> 

    <div class="x_panel"><!--2-->
        <div class="x_title"><!--3-->
            <h2><?= Yii::t('app', 'Cantidad de bultos por día del mes por cliente') ?></h2>
            <div class="clearfix"></div><!--4/4-->
            <p><strong style="color: red;">Uso:</strong> Seleccionar un cliente, un día de inicio y un día de fin.</p>
            <p><strong style="color: orange;">Precaución:</strong> El día de inicio y fin deben pertenecer al mismo mes y año.</p>
            <p><strong style="color: blue;">Información:</strong> Muestra cantidad de bultos/cajas para cliente y rango seleccionado. Información discriminada por día del mes.</p>
        </div><!--3-->
        <div class="x_content"></div><!--5/5-->

        <?= $this->render('_form', [
            'model' => $model,
            'analisisForm' => 4,
        ]) ?>
    </div> 

    <div class="x_panel"><!--2-->
        <div class="x_title"><!--3-->
            <h2><?= Yii::t('app', 'Planilla de cobros y pagos') ?></h2>
            <div class="clearfix"></div><!--4/4-->
            <p><strong style="color: red;">Uso:</strong> Seleccionar mes de un determinado año.</p>
            <p><strong style="color: blue;">Información:</strong> Muestra resumen de liquidaciones del mes seleccionado.</p>
        </div><!--3-->
        <div class="x_content"></div><!--5/5-->

        <?= $this->render('_form', [
            'model' => $model,
            'analisisForm' => 5,
        ]) ?>
    </div> 

    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Análisis de zonas') ?></h2>
            <div class="clearfix"></div>
            <p><strong style="color: red;">Uso:</strong> Seleccionar un año.</p>
            <p><strong style="color: blue;">Información:</strong> Muestra resumen de bultos entregados por zona durante el año seleccionado. Genera una tabla por cada tipo de cliente.</p>
        </div>
        <div class="x_content"></div>

        <?= $this->render('_form', [
            'model' => $model,
            'analisisForm' => 7,
        ]) ?>
    </div>

    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Ingresos') ?></h2>
            <div class="clearfix"></div>
            <p><strong style="color: red;">Uso:</strong> Seleccionar día de inicio y fin.</p>
            <p><strong style="color: blue;">Información:</strong> Muestra un resumen de las liquidaciones comprendidas entre rango de fechas. Información discriminada por cliente y cómo el mismo pagó la liquidación.</p>
        </div>
        <div class="x_content"></div>

        <?= $this->render('_form', [
            'model' => $model,
            'analisisForm' => 8,
        ]) ?>
    </div>
    
</div> 