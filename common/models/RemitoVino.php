<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%remito_vino}}".
 *
 * @property int $idremito_vino
 * @property int $cant_cajas
 * @property double $valor_declarado
 * @property int $numero_remito
 * @property int $cant_botellas
 * @property int $sucursal_idsucursal
 * @property int $guia_idtransporte
 * @property string $guia_numero
 * @property string $guia_tipo
 * @property double $guia_total
 * @property string $guia_facturada_a
 * @property double $total_facturacion
 * @property int $ordengrupalvinos_idordengrupalvinos
 * @property string $creado
 * @property double $valor_caja 
 * @property double $precio_reparto
 * @property double $precio
 *
 * @property OrdenGrupalVinos $ordengrupalvinosIdordengrupalvinos
 * @property Sucursal $sucursalIdsucursal
 * @property EmpresaTransporte $guiaIdtransporte
 */
class RemitoVino extends \yii\db\ActiveRecord
{
    public $cliente;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%remito_vino}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cant_cajas', 'numero_remito', 'cant_botellas', 'sucursal_idsucursal', 'guia_idtransporte', 'ordengrupalvinos_idordengrupalvinos'], 'integer'],
            [['valor_declarado', 'guia_total', 'total_facturacion', 'valor_caja', 'precio_reparto', 'precio'], 'number'],
            [['sucursal_idsucursal'], 'required'],
            [['guia_tipo', 'guia_facturada_a', 'creado'], 'string'],
            [['guia_numero'], 'string', 'max' => 100],
            [['ordengrupalvinos_idordengrupalvinos'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenGrupalVinos::className(), 'targetAttribute' => ['ordengrupalvinos_idordengrupalvinos' => 'idorden_grupal_vinos']],
            [['sucursal_idsucursal'], 'exist', 'skipOnError' => true, 'targetClass' => Sucursal::className(), 'targetAttribute' => ['sucursal_idsucursal' => 'idsucursal']],
            [['guia_idtransporte'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaTransporte::className(), 'targetAttribute' => ['guia_idtransporte' => 'idempresatransporte']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idremito_vino' => Yii::t('app', 'Idremito Vino'),
            'cant_cajas' => Yii::t('app', 'Cajas'),
            'valor_declarado' => Yii::t('app', 'Declarado (TOTAL facturación)'),
            'numero_remito' => Yii::t('app', 'Nº Facturación'),
            'cant_botellas' => Yii::t('app', 'Botellas'),
            'sucursal_idsucursal' => Yii::t('app', 'Sucursal'),
            'guia_idtransporte' => Yii::t('app', 'Transporte'),
            'guia_numero' => Yii::t('app', 'Nº de Guía'),
            'guia_tipo' => Yii::t('app', 'Guia Tipo'),
            'guia_total' => Yii::t('app', 'TOTAL Guía'),
            'guia_facturada_a' => Yii::t('app', 'Guia Facturada A'),
            'total_facturacion' => Yii::t('app', 'TOTAL facturación'),
            'ordengrupalvinos_idordengrupalvinos' => Yii::t('app', 'Orden grupal vinos '),
            'creado' => Yii::t('app', 'Creado'),
            'valor_caja' => Yii::t('app', 'Valor Caja'),
            'precio_reparto' => Yii::t('app', 'Precio Reparto'), 
            'precio' => Yii::t('app', 'Precio'), 
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdengrupalvinosIdordengrupalvinos()
    {
        return $this->hasOne(OrdenGrupalVinos::className(), ['idorden_grupal_vinos' => 'ordengrupalvinos_idordengrupalvinos']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSucursalIdsucursal()
    {
        return $this->hasOne(Sucursal::className(), ['idsucursal' => 'sucursal_idsucursal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuiaIdtransporte()
    {
        return $this->hasOne(EmpresaTransporte::className(), ['idempresatransporte' => 'guia_idtransporte']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getAllRemitosRepartidorFecha($idRepartidor, $fechaInicio, $fechaFin){
        return self::find()->joinWith(['ordengrupalvinos_idordengrupalvinos', 'ordengrupalvinosIdordengrupalvinos.idorden_grupal_vinos'])
        ->where(['cliente_idcliente' => $idRepartidor])
        ->andFilterWhere(['<=', 'fecha', $fechaFin])
        ->andFilterWhere(['>=', 'fecha', $fechaInicio])
        ->andWhere(['is', ['liquidacionvinorepartidor_liquidacionvinorepartidor' => null]])
        ->all(); 
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getAllRemitosClienteFecha($idCliente, $fechaInicio, $fechaFin){
        return self::find()->joinWith(['ordengrupalvinos_idordengrupalvinos', 'ordengrupalvinosIdordengrupalvinos.idordenOrupal_vinos'])
        ->where(['cliente_idcliente' => $idCliente])
        ->andFilterWhere(['<=', 'fecha', $fechaFin])
        ->andFilterWhere(['>=', 'fecha', $fechaInicio])
        ->andWhere(['is', ['liquidacionvinocliente_idliquidacionvinocliente' => null]])
        ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getRemitosByIdLiquidacionCliente($idLiquidacion){
        return self::find()
        ->joinWith(['ordengrupalvinosIdordengrupalvinos'])
        ->andWhere(['ordengrupalvinos_idordengrupalvinos' => $idLiquidacion])->all();
    }
}
