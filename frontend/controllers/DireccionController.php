<?php

namespace frontend\controllers;

use common\models\Departamento;
use common\models\Localidad;

class DireccionController extends \yii\web\Controller
{
   public function actionDepartamento($id)
    {
        $countDepartamentos = Departamento::find()
                ->where(['provincia_idprovincia' => $id])
                ->count();

        $departamentos = Departamento::find()
                ->where(['provincia_idprovincia' => $id])
                ->orderBy('nombre ASC')
                ->all();

        if($countDepartamentos>0){
        	echo "<option value=''>-Seleccionar-</option>";
            foreach($departamentos as $departamento){
                echo "<option value='".$departamento->iddepartamento."'>".$departamento->nombre."</option>";
            }
        }
        else{
            echo "<option>-</option>";
        }

    }

    public function actionLocalidad($id)
    {
        $countLocalidades = Localidad::find()
                ->where(['departamento_iddepartamento' => $id])
                ->count();

        $localidades = Localidad::find()
                ->where(['departamento_iddepartamento' => $id])
                ->orderBy('nombre ASC')
                ->all();

        if($countLocalidades>0){
            foreach($localidades as $localidad){
                echo "<option value='".$localidad->idlocalidad."'>".$localidad->nombre."</option>";
            }
        }
        else{
            echo "<option>-</option>";
        }

    }

    public function actions()
    {
        return \yii\helpers\ArrayHelper::merge(parent::actions(), [
            'departamento-localidad' => [
                'class' => \kartik\depdrop\DepDropAction::className(),
                'outputCallback' => function ($selectedId, $params) {
                    $localidades = Localidad::find()
                        ->select(['idlocalidad as id','nombre as name'])
                        ->where(['departamento_iddepartamento' => $selectedId])
                        ->orderBy('nombre ASC')
                        ->asArray()
                        ->all();
                return $localidades;
                }
            ],
            'departamento-provincia' => [
                'class' => \kartik\depdrop\DepDropAction::className(),
                'outputCallback' => function ($selectedId, $params) {
                    $departamentos = Departamento::find()
                        ->select(['iddepartamento as id','nombre as name'])
                        ->where(['provincia_idprovincia' => $selectedId])
                        ->orderBy('nombre ASC')
                        ->asArray()
                        ->all();
                    return $departamentos;
                }
            ]
        ]);
    }

}
