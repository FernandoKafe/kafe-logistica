<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RepartidorHasVehiculo */

$this->title = Yii::t('app', 'Update Repartidor Has Vehiculo: ' . $model->idtepartidor_has_vehiculo, [
    'nameAttribute' => '' . $model->idtepartidor_has_vehiculo,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Repartidor Has Vehiculos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idtepartidor_has_vehiculo, 'url' => ['view', 'id' => $model->idtepartidor_has_vehiculo]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="repartidor-has-vehiculo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
