<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\CondicionFiscal;
use \yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use common\models\Sucursal;
use common\models\Empresa;
use \common\models\Provincia;
use \common\models\Departamento;
use \common\models\Localidad;




/* @var $this yii\web\View */
/* @var $model common\models\Empresa */
if(isset($sucursal)){
  $new = false;
}else{
  $sucursal = new Sucursal();
  $new = true;
}

$empresa = ArrayHelper::map(Empresa::find()->all(),'idempresa','nombre');
$dataProvincia=ArrayHelper::map(Provincia::find()->orderBy('nombre')->asArray()->all(), 'idprovincia', 'nombre');

?> 

<div class="x_panel">
    <div class="x_content">

    <?php $form = ActiveForm::begin(); ?>

    <?=  $form->errorSummary($model); ?>

      <?= $form->field($model, 'nombre')->widget(\yii\jui\AutoComplete::classname(), [
          'clientOptions' => [
              'source' => ArrayHelper::getColumn(Empresa::find()->orderBy(['nombre'=>SORT_ASC])->all(),'nombre'),
          ],
          'options'=>['class' => 'form-control']
      ]) ?>

    <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

    <?php $sucursal->nombre = "Central";
    echo$form->field($sucursal, 'nombre')->textInput(['maxlength' => true, 'disabled'=>'disabled'])->label("Nombre Sucursal"); 
    ?>

    <!--Sucursal-->
    <?= $form->field($sucursal, 'idsucursal')->textInput()->label(false)->hiddenInput() ?>

    <?php
        $dataDepartamentos = [];
        $dataLocalidades = [];
        $departamento = new Departamento();
        $provincia = new Provincia();

        if(!$new && isset($sucursal->localidad_idlocalidad)){
          $provincia->idprovincia = $sucursal->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->idprovincia;
          $dataDepartamentos =  ArrayHelper::map(Departamento::find()
            ->select(['iddepartamento as id','nombre as name'])
            ->where(['provincia_idprovincia' => $provincia->idprovincia])
            ->orderBy('nombre ASC')
            ->asArray()
            ->all(), 'id', 'name');
          $departamento->iddepartamento = $sucursal->localidadIdlocalidad->departamentoIddepartamento->iddepartamento;
          
          $dataLocalidades = ArrayHelper::map(Localidad::find()
          ->select(['idlocalidad as id','nombre as name'])
          ->where(['departamento_iddepartamento' => $departamento->iddepartamento])
          ->orderBy('nombre ASC')
          ->asArray()
          ->all(), 'id', 'name');
        }

        echo $form->field($provincia, 'idprovincia')->dropDownList($dataProvincia,
          ['prompt'=>'-Seleccionar-',
            'id' => 'provinciaDestino'])->label('Provincia');

        echo  $form->field($departamento, 'iddepartamento')->widget(DepDrop::classname(), [
          //'data'=> $initLocalidad,
          'data' => $dataDepartamentos,
          'language' => 'es',
          'options' => ['id'=>'departamento'],
          'type' => DepDrop::TYPE_SELECT2,
              'pluginOptions'=>[
              'depends' => ['provinciaDestino'],
              'placeholder' => '- Seleccionar Departamento -',
              'url' => \yii\helpers\Url::to(['direccion/departamento-provincia']),
              'loadingText' => 'Cargando Departamentos...'
            ],
        ])->label('<a href="../departamento/create" target="_blank"><i class="fa fa-plus green"></i></a> Departamento');
         
        echo $form->field($sucursal, 'localidad_idlocalidad')->widget(DepDrop::classname(), [
          //'data'=> $initLocalidad,
          'data' => $dataLocalidades,
          'language' => 'es',
          'options' => ['id'=>'localidad'],
          'type' => DepDrop::TYPE_SELECT2,
            'pluginOptions'=>[
            'depends' => ['departamento'],
            'placeholder' => '- Seleccionar Localidad -',
            'url' => \yii\helpers\Url::to(['direccion/departamento-localidad']),
            'loadingText' => 'Cargando Localidades...'
          ],
        ])->label('<a href="../localidad/create" target="_blank"><i class="fa fa-plus green"></i></a> Localidad');
      ?>

    <?= $form->field($sucursal, 'direccion')->textInput(['maxlength' => true])->label('Dirección') ?>

    <div class="form-group">
        <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);?>
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>

