<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use \yii\helpers\ArrayHelper;
use common\models\EstadoLiquidacion;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\LiquidacionDomicilioRepartidorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Liquidación Domicilio Repartidor');
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><i class="fa fa-file"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2>Listado</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <!--<p>
            <?//= Html::a(Yii::t('app', 'Crear Liquidación Domicilio Repartidor'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>-->

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'moduleId' => 'gridviewk',
            'toolbar' =>  [
                '{export}',
                '{toggleData}',
            ],
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
            ],
            'columns' => [
                [
                    'attribute' => 'repartidor_idrepartidor',
                    'value' => function($data){
                        return $data->repartidorIdrepartidor->apellido.", ".$data->repartidorIdrepartidor->nombre;
                    }
                ],
                'nombre',
                [
                    'attribute'=>'estadoliquidacion_idestadoliquidacion',
                    'filter'=>ArrayHelper::map(EstadoLiquidacion::find()->asArray()->all(), 'idestado_liquidacion', 'nombre'),
                    'content' => function($data){
                        return $data->estadoliquidacionIdestadoliquidacion->nombre;
                    }
                ],
                [
                    'attribute' =>'total',
                    'format' => ['decimal', 2],
                ],
                [
                    'attribute' => 'creado',
                    'label' => 'Fecha y Hora creación',
                ],
                //'repartidor_idrepartidor',
                //'estadoliquidacion_idestadoliquidacion',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
