<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LiquidacionVinoCliente;

/**
 * LiquidacionVinoClienteSearch represents the model behind the search form of `common\models\LiquidacionVinoCliente`.
 */
class LiquidacionVinoClienteSearch extends LiquidacionVinoCliente
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idliquidacion_vino_cliente', 'cheque_idcheque', 'confirmada', 'cajas'], 'integer'],
            [['total', 'pago_parcial', 'total_guias', 'total_cobrar', 'valor_cajas', 'total_seguro'], 'number'],
            [['periodo_inicio', 'periodo_fin', 'nombre', 'estadoliquidacion_idestadoliquidacion', 'liquidacionformapago_idliquidacionformapago', 'cliente_idcliente', 'creado', 'fecha_pago'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LiquidacionVinoCliente::find()->where(['confirmada' => 1]);
        $query->joinWith(['estadoliquidacionIdestadoliquidacion as estado']);
        $query->joinWith(['liquidacionformapagoIdliquidacionformapago as forma']);
        $query->joinWith(['clienteIdcliente as cliente']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idliquidacion_vino_cliente' => $this->idliquidacion_vino_cliente,
            'total' => $this->total,
            'periodo_inicio' => $this->periodo_inicio,
            'periodo_fin' => $this->periodo_fin,
            'cheque_idcheque' => $this->cheque_idcheque,
            'pago_parcial' => $this->pago_parcial,
            'total_guias' => $this->total_guias,
            'total_cobrar' => $this->total_cobrar,
            'creado' => $this->creado,
            'confirmada' => $this->confirmada,
            'fecha_pago' => $this->fecha_pago,
            'cajas' => $this->cajas, 
            'valor_cajas' => $this->valor_cajas, 
            'total_seguro' => $this->total_seguro,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
        ->andFilterWhere(['like', 'estado', $this->estadoliquidacion_idestadoliquidacion])
        ->andFilterWhere(['like', 'forma', $this->liquidacionformapago_idliquidacionformapago])
        ->andFilterWhere(['like', 'cliente', $this->cliente_idcliente]);

        return $dataProvider;
    }
}
