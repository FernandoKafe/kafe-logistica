<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "condicion_fiscal".
 *
 * @property int $idcondicionfiscal
 * @property string $nombre
 *
 * @property Cliente[] $clientes
 * @property Empresa[] $empresas
 */
class CondicionFiscal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'condicion_fiscal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcondicionfiscal' => 'Condición Fiscal',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientes()
    {
        return $this->hasMany(Cliente::className(), ['condicionfiscal_idcondicionfiscal' => 'idcondicionfiscal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresas()
    {
        return $this->hasMany(Empresa::className(), ['condicionfiscal_idcondicionfiscal' => 'idcondicionfiscal']);
    }

    /**
     * Funcion que retorna todos los tipos de condicion fiscal
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getAllTipo(){
        return self::find()->all();
    } 
}
