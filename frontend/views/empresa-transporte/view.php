<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\EmpresaTransporte */

$this->title = $model->razonsocial;
$this->params['breadcrumbs'][] = ['label' => 'Empresas de Transporte', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="empresa-transporte-view">
    <h1><i class="fa fa-truck"></i> Empresa de Transporte: <?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Detalle') ?></h2>
        <div class="clearfix"></div>
    </div>

    <p>
        <?= Html::a('Inicio', ['index'], ['class' => 'btn btn-primary']);?>

        <?= Html::a('Actualizar', ['update', 'id' => $model->idempresatransporte], ['class' => 'btn btn-success']) ?>
        <!--<?/*= Html::a('Delete', ['delete', 'id' => $model->idempresatransporte], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) */?>-->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idempresatransporte',
            'razonsocial',
            'cuit',
        ],
    ]) ?>

</div>
</div>
