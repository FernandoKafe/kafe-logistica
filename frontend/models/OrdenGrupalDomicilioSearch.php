<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrdenGrupalDomicilio;

/**
 * OrdenGrupalDomicilioSearch represents the model behind the search form of `common\models\OrdenGrupalDomicilio`.
 */
class OrdenGrupalDomicilioSearch extends OrdenGrupalDomicilio
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idorden_grupal_domicilio', 'borrado', 'liquidacioncliente_idliquidacioncliente', 'liquidacionrepartidor_idliquidacionrepartidor'], 'integer'],
            [['fecha', 'numero_orden', 'repartidor_idrepartidor', 'cliente_idcliente', 'creado', 'tipo_cliente'], 'safe'],
            [['precio_reparto', 'precio_reparto_adicional', 'precio_sprinter', 'precio_bulto', 'precio_bulto_adicional'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrdenGrupalDomicilio::find()->where(['borrado' => 0]); /*->andWhere(['or', ['liquidacioncliente_idliquidacioncliente' => null],['liquidacionrepartidor_idliquidacionrepartidor' => null]]);*/
        $query->joinWith(['clienteIdcliente as cliente']);
        $query->joinWith(['repartidorIdrepartidor as repartidor']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider; 
        }

        if($this->liquidacioncliente_idliquidacioncliente == 2){
            $query->andWhere(['IS NOT', 'liquidacioncliente_idliquidacioncliente', null]);
        }else if($this->liquidacioncliente_idliquidacioncliente == 1){
            $query->andWhere(['liquidacioncliente_idliquidacioncliente' => null]);
        }

        if($this->liquidacionrepartidor_idliquidacionrepartidor == 2){
            $query->andWhere(['IS NOT', 'liquidacionrepartidor_idliquidacionrepartidor', null]);
        }else if($this->liquidacionrepartidor_idliquidacionrepartidor == 1){
            $query->andWhere(['liquidacionrepartidor_idliquidacionrepartidor' => null]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idorden_grupal_domicilio' => $this->idorden_grupal_domicilio,
            'fecha' => $this->fecha,
            'borrado' => $this->borrado,
            'creado' => $this->creado,
            'precio_reparto' => $this->precio_reparto, 
            'precio_reparto_adicional' => $this->precio_reparto_adicional, 
            'precio_sprinter' => $this->precio_sprinter, 
            'precio_bulto' => $this->precio_bulto, 
            'precio_bulto_adicional' => $this->precio_bulto_adicional,
            'tipo_cliente' => $this->tipo_cliente,
        ]);

        $query->andFilterWhere(['like', 'numero_orden', $this->numero_orden]);
        $query->andFilterWhere(['like', 'cliente.razonsocial', $this->cliente_idcliente]);
        $query->andFilterWhere(['like', 'CONCAT(repartidor.apellido, ", ", repartidor.nombre)', $this->repartidor_idrepartidor]);

        return $dataProvider;
    }
}
