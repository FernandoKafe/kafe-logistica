<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use \yii\helpers\ArrayHelper;


use \common\models\Vehiculo;

/* @var $this yii\web\View */
/* @var $model common\models\RepartidorHasVehiculo */
/* @var $form yii\widgets\ActiveForm */
$idRepartidor = Yii::$app->getRequest()->getQueryParam('id');
$vehiculo = Vehiculo::find()->orderBy('marca')->all();
foreach($vehiculo as &$ve){
    $ve->marca = $ve->marca.' - '.$ve->modelo.' - '.$ve->patente;
}

$activities = ArrayHelper::map($vehiculo, 'idvehiculo', 'marca');
// Todo With activities

?>

<div class="repartidor-has-vehiculo-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-4">
          <?php
          echo $form->field($model, 'desde')->widget(DatePicker::classname(), [
            'options' => [
              'placeholder' => 'aaaa-mm-dd',
            ],
            'pluginOptions' => [
              'autoclose'=>true,
              'format' => 'yyyy-mm-dd',
              'endDate' => "0d"
            ],
            'language' => Yii::$app->language,
            'type' => DatePicker::TYPE_COMPONENT_PREPEND
          ])->label('Desde');
          ?>
    </div>

    <div class="col-md-4">
          <?php
          echo $form->field($model, 'hasta')->widget(DatePicker::classname(), [
            'options' => [
              'placeholder' => 'aaaa-mm-dd',
            ],
            'pluginOptions' => [
              'autoclose'=>true,
              'format' => 'yyyy-mm-dd',
            ],
            'language' => Yii::$app->language,
            'type' => DatePicker::TYPE_COMPONENT_PREPEND
          ])->label('Hasta');
          ?>
    </div>

    <?php
        $model->repartidor_idrepartidor = $idRepartidor;
        echo $form->field($model, 'repartidor_idrepartidor')->textInput()->hiddenInput()->label(false);
    ?>

    <div class="col-md-4"><?= $form->field($model, 'vehiculo_idvehiculo')->dropDownList($activities)->label('<a href="../vehiculo/create" target="_blank"><i class="fa fa-plus green"></i></a> Vehículo'); ?></div>

    <div class="form-group col-md-12">
        <?= Html::submitButton(Yii::t('app', 'Asociar', ['idRepartidor' => $idRepartidor]), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
