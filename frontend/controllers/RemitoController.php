<?php

namespace frontend\controllers;

use Yii;
use common\models\Remito;
use frontend\models\RemitoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\OrdenRetiroGrupal;
use common\models\ZonaHasLocalidad;
use common\models\Tarifario;
use common\models\TarifarioHasPrecio;
use common\models\Cheque;
use common\models\LiquidacionHasRemito;
use common\models\LiquidacionrepartidorHasRemito;
use \yii\helpers\ArrayHelper;
use common\models\Estado;
use \common\models\Localidad;
use \common\models\Empresa;
use \common\models\EmpresaTransporte;
use \common\models\Repartidor;
use \common\models\Servicio;
use \common\models\FormaPago;
use \common\models\Producto;
use common\models\Sucursal;
use common\models\Carga;
use \common\models\LiquidacionFormaPago;

/**
 * RemitoController implements the CRUD actions for Remito model.
 */
class RemitoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Remito models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RemitoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $estados = ArrayHelper::map(Estado::find()->asArray()->all(), 'idestado', 'nombre');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'estados' => $estados,
        ]);
    }

    /**
     * Displays a single Remito model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Remito model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Remito();
        $cheque = new Cheque();
        $flag = 1;

        if ($model->load(Yii::$app->request->post()) && $model->validateAll()) {
            $remito = Remito::find()->where(['numero_remito' => $model->numero_remito])->all();
            $precio = TarifarioHasPrecio::getCargaPrecio($model->carga_idcarga, $model->clienteIdcliente->tarifario_idtarifario);
            
            if(!$precio){
                Yii::$app->session->setflash('error', "La carga seleccionada no tiene asociado un precio en el tarifario: ".$model->clienteIdcliente->tarifarioIdtarifario->nombre);
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            if($model->terminal == 0){
                $zona = ZonaHasLocalidad::getZonaByLocalidadId($model->sucursalIdsucursal->localidad_idlocalidad);
                if($zona){
                    $precioZona = TarifarioHasPrecio::getTarifarioHasPrecio($model->carga_idcarga, $model->clienteIdcliente->tarifario_idtarifario, $zona->zona_idzona);
                }else{
                    Yii::$app->session->setflash('error', "La localidad ".$model->sucursalIdsucursal->localidad_idlocalidad." de la sucursal no pertenece a ninguna Zona.");
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            }else{
                $precioZona = TarifarioHasPrecio::getTarifarioHasPrecio($model->carga_idcarga, $model->clienteIdcliente->tarifario_idtarifario, 5);
            }

            if(!$precioZona){
                Yii::$app->session->setflash('error', "La ".$zona->zonaIdzona->nombre." a la que se envía la carga no esta cargada en el tarifario: ".$model->clienteIdcliente->tarifarioIdtarifario->nombre);
                return $this->render('create', [
                    'model' => $model,
                ]);
            }

            if($remito && ($model->numero_remito != null)){
                Yii::$app->session->setflash('error', "Ya existe un remito con Nº: ".$model->numero_remito);
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            if(!$model->sucursalIdsucursal->localidad_idlocalidad){
                Yii::$app->session->setflash('error', "La sucursal ".
                $model->sucursalIdsucursal->nombre.
                " de la empresa ".
                $model->sucursalIdsucursal->empresaIdempresa->nombre.
                " no tiene una localidad asignada");
                return $this->render('create', [
                    'model' => $model,
                ]);
            }else{
                $zonaHasLocalidad = ZonaHasLocalidad::getZonaByLocalidadId($model->sucursalIdsucursal->localidad_idlocalidad);
                $pruebaTarifario = Tarifario::getZonasTarifarioByLocalidad($model->sucursalIdsucursal->localidad_idlocalidad, $model->clienteIdcliente->tarifario_idtarifario);
                if(($zonaHasLocalidad && $pruebaTarifario) || ($model->terminal == 1)){
                    if( $cheque->load(Yii::$app->request->post()) && ($cheque->monto != '')){
                        $cheque->save();
                        $model->cheque_idcheque = $cheque->idcheque;
                    }
                    $model->creado = date('Y-m-d G:i', time());
                    $model->tipo = $model->clienteIdcliente->tarifarioIdtarifario->tipo;
                    if($model->save()){
                        Yii::$app->session->setflash('success', "Remito creado con éxito.");
                    }
                    if($model->ordenderetirogrupal_idordenderetirogrupal == 1){
                        return $this->redirect(['create', 
                                                'ordenderetirogrupal_idordenderetirogrupal' => 1,
                                                'cliente' => $model->cliente_idcliente,
                                                'repartidor_idrepartidor' => $model->repartidor_idrepartidor,
                                                'fecha' => $model->remitofecha]);
                    } else {
                        return $this->redirect(['create',
                            'ordenderetirogrupal_idordenderetirogrupal' => $model->ordenderetirogrupal_idordenderetirogrupal,
                            'repartidor_idrepartidor' => $model->repartidor_idrepartidor,
                            'fecha' => $model->remitofecha,
                            'orden_especial' => $model->orden_especial,
                        ]);
                    }
                }else{
                    if(!$zonaHasLocalidad){
                        Yii::$app->session->setflash('error', "El remito no se puede guardar. La localidad ".
                        $model->sucursalIdsucursal->localidadIdlocalidad->nombre.", ".
                        $model->sucursalIdsucursal->localidadIdlocalidad->departamentoIddepartamento->nombre.", ".
                        $model->sucursalIdsucursal->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre." no esta asiganada a ninguna Zona");
                    }
                    if(!$pruebaTarifario){
                        Yii::$app->session->setflash('error', "El tarifario del Cliente no tiene asociada una Zona que contenga la localidad de envio");
                    }
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
            }
        }
        if($flag){

            $empresa = ArrayHelper::map(Empresa::find()->all(),'idempresa','nombre');
            $repartidor = ArrayHelper::map(Repartidor::find()->all(),'idrepartidor','apellido');
            $servicio = ArrayHelper::map(Servicio::find()->all(),'idservicio','nombre');
            $formaPago = ArrayHelper::map(FormaPago::find()->all(),'idformadepago','nombre');
            $producto = ArrayHelper::map(Producto::find()->all(),'idproducto','nombre');
            $sucursal = ArrayHelper::map(Sucursal::getAllSucursales(), 'idsucursal', 'direccion');
            $estado = ArrayHelper::map(Estado::find()->all(),'idestado', 'nombre');
            $carga = ArrayHelper::map(Carga::find()->all(),'idcarga', 'nombre');
            $contraPago = ArrayHelper::map(LiquidacionFormaPago::find()->all(),'idliquidacion_forma_pago','nombre');

            return $this->render('create', [
                'model' => $model,
                'empresa' => $empresa,
                'repartidor' => $repartidor,
                'servicio' => $servicio,
                'formaPago' => $formaPago,
                'producto' => $producto,
                'sucursal' => $sucursal,
                'estado' => $estado,
                'carga' => $carga,
                'contraPago' => $contraPago,
            ]);
        }
    }

    /**
     * Updates an existing Remito model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if(!$model->chequeIdcheque){
            $cheque = new Cheque();
        }else{
            $cheque = $model->chequeIdcheque;
        }

        if(!($model->liquidado) && !($model->liquidado_repartidor)){
            if ($model->load(Yii::$app->request->post())) {
                $zonaHasLocalidad = ZonaHasLocalidad::getZonaByLocalidadId($model->sucursalIdsucursal->localidad_idlocalidad);
                if($cheque->load(Yii::$app->request->post()) && $cheque->save()){
                    $model->cheque_idcheque = $cheque->idcheque;
                }
                $model->tipo = $model->clienteIdcliente->tarifarioIdtarifario->tipo;
                $model->save();
                return $this->redirect(['view', 'id' => $model->idremito]);
            }
        }else{
            Yii::$app->session->setFlash('error', 'No puede editarse por estar liquidado.');
            return $this->redirect(Yii::$app->request->referrer);
        }

        $empresa = ArrayHelper::map(Empresa::find()->all(),'idempresa','nombre');
        $repartidor = ArrayHelper::map(Repartidor::find()->all(),'idrepartidor','apellido');
        $servicio = ArrayHelper::map(Servicio::find()->all(),'idservicio','nombre');
        $formaPago = ArrayHelper::map(FormaPago::find()->all(),'idformadepago','nombre');
        $producto = ArrayHelper::map(Producto::find()->all(),'idproducto','nombre');
        $sucursal = ArrayHelper::map(Sucursal::getAllSucursales(), 'idsucursal', 'direccion');
        $estado = ArrayHelper::map(Estado::find()->all(),'idestado', 'nombre');
        $carga = ArrayHelper::map(Carga::find()->all(),'idcarga', 'nombre');
        $contraPago = ArrayHelper::map(LiquidacionFormaPago::find()->all(),'idliquidacion_forma_pago','nombre');

        return $this->render('update', [
            'model' => $model,
            'cheque' => $cheque,
            'empresa' => $empresa,
            'repartidor' => $repartidor,
            'servicio' => $servicio,
            'formaPago' => $formaPago,
            'producto' => $producto,
            'sucursal' => $sucursal,
            'estado' => $estado,
            'carga' => $carga,
            'contraPago' => $contraPago,
        ]);
    }

    /**
     * Deletes an existing Remito model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if(!($model->liquidado) && !($model->liquidado_repartidor)){
            $liqHasRemito = LiquidacionHasRemito::find()->where(['remito_idremito' => $id])->one();
            if($liqHasRemito)
                $liqHasRemito->delete();
            $liqRepHasRemito = LiquidacionrepartidorHasRemito::find()->where(['remito_idremito' => $id])->one();
            if($liqRepHasRemito)
                $liqRepHasRemito->delete();
            $model->delete();
            return $this->redirect(['index']);
        }else{
            Yii::$app->session->setFlash('error', 'No puede eliminarse por estar liquidado. Debe eliminar las liquidaciones que contengan este remito.');
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * Finds the Remito model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Remito the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Remito::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Función que remueve un remito de una liquidación.
     * @param integer $idRemito
     * @param integer $idLiquidacion
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionRemoveLiquidado($idRemito, $idLiquidacion)
    {
        $model = $this->findModel($idRemito);
        $model->liquidado = 0;
        $model->save();

        return $this->redirect(['liquidacion/view', 'id' => $idLiquidacion]);
    }

    /**
     * Función que remueve un remito de una liquidación.
     * @param integer $idRemito
     * @param integer $idLiquidacion
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAddLiquidado($idRemito, $idLiquidacion)
    {
        $model = $this->findModel($idRemito);
        $model->liquidado = 1;
        $model->save();

        return $this->redirect(['liquidacion/view', 'id' => $idLiquidacion]);
    }
}
