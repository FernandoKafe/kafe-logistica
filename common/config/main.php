<?php
use kartik\mpdf\Pdf;

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'language' => 'es',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'timeZone' => 'America/Argentina/Mendoza',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\DbManager'
        ],
        'user' => [
            'identityClass' => 'mdm\admin\models\User',
            'loginUrl' => ['site/login'],
        ],
        'formatter' => [
            'decimalSeparator' => ',',
            'thousandSeparator' => '.',
            'currencyCode' => 'ARG',
        ],
        'pdf' => [
                'class' => Pdf::classname(),
                'mode' => Pdf::MODE_BLANK,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_LANDSCAPE,
                'destination' => Pdf::DEST_BROWSER,
                'options' => [
                    'shrink_tables_to_fit' => 0,
                    'defaultFontSize' => 5,
                ],
                // your html content input
                /// 'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.kv-heading-1{font-size:5px}', 
                // set mPDF properties on the fly
                'options' => ['title' => 'Logística Vertiente'],
                // call mPDF methods on the fly
                'methods' => [ 
                    'SetHeader'=>['Logística Vertiente'], 
                    'SetFooter'=>['{PAGENO}'],
                ]
                // refer settings section for all configuration options
        ],
        'pdfP' => [
                'class' => Pdf::classname(),
                'mode' => Pdf::MODE_BLANK,
                'format' => Pdf::FORMAT_A4,
                'orientation' => Pdf::ORIENT_PORTRAIT,
                'destination' => Pdf::DEST_BROWSER,
                'options' => [
                    'shrink_tables_to_fit' => 0,
                    'defaultFontSize' => 5,
                ],
                // your html content input
                ///'content' => $content,
                // format content from your own css file if needed or use the
                // enhanced bootstrap css built by Krajee for mPDF formatting 
                'cssFile' => '@vendor/kartik-v/yii2-mpdf/src/assets/kv-mpdf-bootstrap.min.css',
                // any css to be embedded if required
                'cssInline' => '.tabla-rend{font-size:9px}', 
                // set mPDF properties on the fly
                'options' => ['title' => 'Logística Vertiente'],
                // call mPDF methods on the fly
                'methods' => [ 
                    'SetHeader'=>['Logística Vertiente'], 
                    'SetFooter'=>['{PAGENO}'],
                ]
                // refer settings section for all configuration options
        ],
    ],
    'modules' => [
        'admin' => [
            'class' => 'mdm\admin\Module',
        ],
        'gridviewk' =>  [
            'class' => '\kartik\grid\Module'
            // enter optional module parameters below - only if you need to
            // use your own export download action or custom translation
            // message source
            // 'downloadAction' => 'gridview/export/download',
            // 'i18n' => []
        ]
    ],
    /*'as access' => [
        'class' => 'mdm\admin\components\AccessControl',
        'allowActions' => [
            'site/*',
            'backend/admin/user/login',
            'backend/admin/user/request-password-reset',
        ]
    ]*/
];
