<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Create User'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            ['attribute' => 'name',
             'value' => function($model)
                        {
                            return $model->getNombreCompleto();
                        }
            ],
            'username',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            'dni',
            'email:email',
            //'status',
            //'created_at',
            //'updated_at',
            //'name',
            //'surname',
            //'telephone',
            //'address',

            ['class' => 'yii\grid\ActionColumn',
            'template' => '{view}{update}{delete}{role}',
            'buttons' => [
                'role' => function($url, $model){
                    return Html::a('<span class="glyphicon glyphicon-list-alt"></span>',$url,[
                        'title' => 'roles',
                        'aria-label'=>'roles',
                        'data-pjax'=>0]);
                },],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'role') {
                        $url = \yii\helpers\Url::to(['admin/assignment/view','id' => $model->id]);
                        return $url;
                    }
                    if ($action === 'view') {
                        $url = \yii\helpers\Url::to(['user/view','id' => $model->id]);
                        return $url;
                    }
                    if ($action === 'delete') {
                        $url = \yii\helpers\Url::to(['user/delete','id' => $model->id]);
                        return $url;
                    }
                    if ($action === 'update') {
                        $url = \yii\helpers\Url::to(['user/update','id' => $model->id]);
                        return $url;
                    }
                },
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
