<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\OrdenGrupalDomicilioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orden-grupal-domicilio-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'idorden_grupal_domicilio') ?>

    <?= $form->field($model, 'fecha') ?>

    <?= $form->field($model, 'numero_orden') ?>

    <?= $form->field($model, 'liquidado_cliente') ?>

    <?= $form->field($model, 'liquidado_repartidor') ?>

    <?php // echo $form->field($model, 'cliente_idcliente') ?>

    <?php // echo $form->field($model, 'repartidor_idrepartidor') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
