<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\RemitoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="remito-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'idremito') ?>

    <?= $form->field($model, 'numero_remito') ?>

    <?= $form->field($model, 'remitofecha') ?>

    <?= $form->field($model, 'sucursal_idsucursal') ?>

    <?php // echo $form->field($model, 'cantidad_bultos') ?>

    <?php // echo $form->field($model, 'peso') ?>

    <?php // echo $form->field($model, 'volumen') ?>

    <?php // echo $form->field($model, 'valor_declarado') ?>

    <?php // echo $form->field($model, 'servicio_idservicio') ?>

    <?php // echo $form->field($model, 'flete') ?>

    <?php // echo $form->field($model, 'seguro') ?>

    <?php // echo $form->field($model, 'total_contrarembolso') ?>

    <?php // echo $form->field($model, 'formadepago_idformadepago') ?>

    <?php // echo $form->field($model, 'producto_idproducto') ?>

    <?php // echo $form->field($model, 'repartidor_idrepartidor') ?>

    <?php // echo $form->field($model, 'numero_guia') ?>

    <?php // echo $form->field($model, 'total_guia') ?>

    <?php // echo $form->field($model, 'estado_idestado') ?>

    <?php // echo $form->field($model, 'empresatransporte_idempresatransporte') ?>

    <?php // echo $form->field($model, 'ordenderetirogrupal_idordenderetirogrupal') ?>

    <?php // echo $form->field($model, 'tipo_remito') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
