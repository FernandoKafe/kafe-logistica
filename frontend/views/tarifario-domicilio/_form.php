<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TarifarioDomicilio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tarifario-domicilio-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-6"><?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-6"><?= $form->field($model, 'tipo')->dropDownList([ 'Tipo A' => 'Tipo A', 'Tipo B' => 'Tipo B', ], ['prompt' => '']) ?></div>

    <div class="col-md-2"><?= $form->field($model, 'primer_bulto')->textInput() ?></div>

    <div class="col-md-2"><?= $form->field($model, 'bulto_adicional')->textInput() ?></div>

    <div class="col-md-2"><?= $form->field($model, 'reparto')->textInput() ?></div>

    <div class="col-md-2"><?= $form->field($model, 'reparto_adicional')->textInput() ?></div>

    <div class="col-md-2"><?= $form->field($model, 'sprinter')->textInput() ?></div>

    <div class="form-group col-md-12">
        <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);?>
        <?= Html::submitButton(Yii::t('app', 'Crear'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
