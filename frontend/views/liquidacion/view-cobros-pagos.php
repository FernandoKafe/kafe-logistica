<?php

use yii\helpers\Html;

$fecha = '0000/'.($model->periodo_inicio + 1).'/00';
$fecha = strftime("%B", strtotime($fecha));
$fecha = strtoupper($fecha);
$fileName = "COBROS-y-PAGOS|".$model->clienteIdcliente->razonsocial."-".$fecha."-".date("Y");
$fileName = str_replace(' ', '-', $fileName);
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=".$fileName.".xls");
header("Pragma: no-cache");
header("Expires: 0");
setlocale(LC_TIME, 'es_ES.UTF-8');
if($post){
    $longitud = count($post);
    //echo '<pre>' , print_r($post) , '</pre>';
}
//echo date("F", mktime(0, 0, 0, $subelemento, 10));
?>
<table xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">  
    <caption colspan="11" style='border: solid 1px;'><?= "Planilla cobros y pagos ".$fecha?></caption>
    <?php if($post){ ?>
            <tr>
                <td colspan="11">
                    <?php echo Html::img('@web/img/logo/LOGO.jpg', ['class' => 'img-responsive', 'width'=>'210', 'style' => 'margin: 0 0;']) ?>
                </td>
            </tr>
            <tr>
                <td style='border: solid 1px; font-weight: bold; padding: 5px; white-space: nowrap;'>CLIENTE</td>
                <td style='border: solid 1px; font-weight: bold; padding: 5px; white-space: nowrap;'>FORMA DE PAGO</td>
                <td style='border: solid 1px; font-weight: bold; padding: 5px; white-space: nowrap;'>BULTOS</td>
                <td style='border: solid 1px; font-weight: bold; padding: 5px; white-space: nowrap;'>REPARTO NETO</td>
                <td style='border: solid 1px; font-weight: bold; padding: 5px; white-space: nowrap;'>CONTRAREEMBOLSO</td>
                <td style='border: solid 1px; font-weight: bold; padding: 5px; white-space: nowrap;'>FLETE COBRADO</td>
                <td style='border: solid 1px; font-weight: bold; padding: 5px; white-space: nowrap;'>GUIAS LOGISTICA</td>
                <td style='border: solid 1px; font-weight: bold; padding: 5px; white-space: nowrap;'>IVA</td>
                <td style='border: solid 1px; font-weight: bold; padding: 5px; white-space: nowrap;'>GUIAS CLIENTE</td>
                <td style='border: solid 1px; font-weight: bold; padding: 5px; white-space: nowrap;'>MONTO TOTAL</td>
                <td style='border: solid 1px; font-weight: bold; padding: 5px; white-space: nowrap;'>ESTADO</td>
            </tr>
            <?php foreach($post as $elemento){ ?>
            <tr>
            <?php foreach($elemento as $subelemento){ 
                        if(key($elemento) == "BULTOS"){
                            $bultos += $subelemento;
                        }
                        if(key($elemento) == "REPARTO NETO"){
                            $reparto_neto += $subelemento;
                        }
                        if(key($elemento) == "CONTRAREEMBOLSO"){
                            $contrareembolso += $subelemento;
                        }
                        if(key($elemento) == "FLETE COBRADO"){
                            $flete_cobrado += $subelemento;
                        }
                        if(key($elemento) == "GUIAS LOGISTICA"){
                            $guias_logistica += $subelemento;
                        }
                        if(key($elemento) == "IVA"){
                            $iva += $subelemento;
                        }
                        if(key($elemento) == "GUIAS CLIENTE"){
                            $guias_cliente += $subelemento;
                        }
                        if(key($elemento) == "MONTO TOTAL"){
                            $monto_total += $subelemento;
                        }
                        if(key($elemento) == "FORMA DE PAGO" || key($elemento) == "ESTADO" ){
                            echo '<td style="border: solid 1px; text-align: left; padding: 5px">'.$subelemento;
                        }else if(key($elemento) == "CLIENTE"){
                            echo '<td style="border: solid 1px; text-align: left; font-weight: bold;">'.$subelemento;
                        }else if(key($elemento) == "BULTOS" ){
                            echo '<td style="border: solid 1px; text-align: right;">'.$subelemento;
                        }else{ 
                            echo '<td style="border: solid 1px; text-align: right;">'.number_format ((float)$subelemento, 2 , "." ,  "," );
                        }
                        next($elemento);
                } ?><!--Foreach $elemento-->
            </tr>
        <?php } ?>
            <tr>
                <td style='border: solid 1px'><strong>TOTAL</strong></td>
                <td style='border: solid 1px'><strong></strong></td>
                <td style='border: solid 1px; text-align: right;'><strong><?= $bultos?></strong></td>
                <td style='border: solid 1px; text-align: right;'><strong><?= number_format ($reparto_neto, 2 , "." ,  "," )?></strong></td>
                <td style='border: solid 1px; text-align: right;'><strong><?= number_format ($contrareembolso, 2 , "." ,  "," )?></strong></td>
                <td style='border: solid 1px; text-align: right;'><strong><?= number_format ($flete_cobrado, 2 , "." ,  "," )?></strong></td>
                <td style='border: solid 1px; text-align: right;'><strong><?= number_format ($guias_logistica, 2 , "." ,  "," )?></strong></td>
                <td style='border: solid 1px; text-align: right;'><strong><?= number_format ($iva, 2 , "." ,  "," )?></strong></td>
                <td style='border: solid 1px; text-align: right;'><strong><?= number_format ($guias_cliente, 2 , "." ,  "," )?></strong></td>
                <td style='border: solid 1px; text-align: right;'><strong><?= number_format ($monto_total, 2 , "." ,  "," )?></strong></td>
                <td style='border: solid 1px'><strong></strong></td>
            </tr>
        <?php } ?>       
</table>