<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ZonaHasLocalidad */

$this->title = $model->zona_idzona;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zona Has Localidads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zona-has-localidad-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'zona_idzona' => $model->zona_idzona, 'localidad_idlocalidad' => $model->localidad_idlocalidad], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'zona_idzona',
            'localidad_idlocalidad',
        ],
    ]) ?>

</div>
