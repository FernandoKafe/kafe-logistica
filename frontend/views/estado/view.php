<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Estado */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Estados'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="estado-view">

    <h1>Estado de paquete: <?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Inicio', ['index'], ['class' => 'btn btn-primary'])?>

        <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->idestado], ['class' => 'btn btn-success']) ?>
        <!--<?/*= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idestado], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ])*/ ?>-->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idestado',
            'nombre',
        ],
    ]) ?>

</div>
