<?php

namespace frontend\controllers;

use Yii;
use common\models\LiquidacionVinoCliente;
use frontend\models\LiquidacionVinoClienteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\models\Cheque;
use common\models\OrdenGrupalVinos;
use common\models\RemitoVino;
use common\models\TarifarioVino;

/**
 * LiquidacionVinoClienteController implements the CRUD actions for LiquidacionVinoCliente model.
 */
class LiquidacionVinoClienteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LiquidacionVinoCliente models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LiquidacionVinoClienteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LiquidacionVinoCliente model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

   /**
     * Creates a new LiquidacionVinoCliente model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LiquidacionVinoCliente();
        
        if ($model->load(Yii::$app->request->post())) {
            $tv = TarifarioVino::find()->where(['idtarifario_vino' => $model->clienteIdcliente->tarifariovino_idtarifariovino])->one();
            if (isset($tv)) {
                $ordengrupalvino = OrdenGrupalVinos::getOrdenVinosClienteByFecha($model->cliente_idcliente, $model->periodo_inicio, $model->periodo_fin);
                $model->total = 0;
                $model->total_cobrar = 0;
                $model->total_guias = 0;
                $model->cajas = 0;
                $model->valor_cajas = 0;
                $model->total_seguro = 0;
                if (isset($ordengrupalvino)) {
                    foreach( $ordengrupalvino as $ogv) {
                        $remitos = RemitoVino::find()->where(['ordengrupalvinos_idordengrupalvinos' => $ogv->idorden_grupal_vinos])->all();
                        if (isset($remitos)) {
                            foreach ($remitos as $r) {
                                if(!isset($r->valor_caja) || !isset($r->precio_reparto) || !isset($r->total_facturacion))
                                {
                                    if(!isset($r->valor_caja))
                                        $r->valor_caja = $tv->precio_caja;
                                    if(!isset($r->precio_reparto))
                                        $r->precio_reparto = $tv->precio_repartidor;
                                    if(!isset($r->total_facturacion))
                                        $r->total_facturacion = (($r->cant_cajas * $tv->precio_caja) + (($r->valor_declarado / 2) * 0.01));
                                    
                                    $r->save();
                                }
                                $model->cajas += $r->cant_cajas;
                                $r->valor_caja = (($r->cant_cajas * $tv->precio_caja));
                                $model->valor_cajas += $r->valor_caja;
                                $model->total_seguro += (($r->valor_declarado / 2) * 0.01);
                                $r->total_facturacion = (($r->cant_cajas * $tv->precio_caja) + (($r->valor_declarado / 2) * 0.01));
                                $model->total += $r->total_facturacion;
                                $r->save();
                                if(isset($r->guia_idtransporte)){
                                    if( ( ( ($r->guia_tipo == 'A') || ($r->guia_tipo == 'B') || ($r->guia_tipo == 'R') ) && ($r->guia_facturada_a == 'Vertiente')) || ( ( ($r->guia_tipo == 'A') || ($r->guia_tipo == 'B') ) && ($r->guia_facturada_a == 'Cliente'))){
                                        $model->total_guias += ($r->guia_total); 
                                    }
                                }
                            }
                        }
                        $model->total_cobrar = $model->total + $model->total_guias;
                        $model->creado = date('Y-m-d G:i', time());
                        if($model->estadoliquidacion_idestadoliquidacion != 1)
                            $model->fecha_pago = date('Y-m-d G:i', time());
                        $model->save(false);
                        Yii::$app->session->setflash('success', "Previa liquidación vino cliente creada con éxito.");
                    }
                }else{
                    Yii::$app->session->setflash(
                        'error',
                        "No hay remitos a liquidar para el cliente ".
                        $model->clienteIdcliente->razonsocial);
                        return $this->render('create', ['model' => $model,]);
                }
            }else{
                Yii::$app->session->setflash(
                    'error',
                    "El cliente: ".$model->clienteIdcliente->razonsocial.
                    " no tiene tarifario de vino asociado!");
                    return $this->render('create', ['model' => $model,]);
            }
            return $this->redirect(['view', 'id' => $model->idliquidacion_vino_cliente]);
        }

        return $this->render('create', ['model' => $model,]);
    }

    /**
     * Creates a new LiquidacionVinoCliente model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRecalculate($id)
    {
        $model = $this->findModel($id);
        $tv = TarifarioVino::find()->where(['idtarifario_vino' => $model->clienteIdcliente->tarifariovino_idtarifariovino])->one();
        if (isset($tv)) {
            $ordengrupalvino = OrdenGrupalVinos::getOrdenVinosClienteByFecha($model->cliente_idcliente, $model->periodo_inicio, $model->periodo_fin);
            $model->total = 0;
            $model->total_cobrar = 0;
            $model->total_guias = 0;
            $model->cajas = 0;
            $model->valor_cajas = 0;
            $model->total_seguro = 0;
            if (isset($ordengrupalvino)) {
                foreach( $ordengrupalvino as $ogv) {
                    $remitos = RemitoVino::find()->where(['ordengrupalvinos_idordengrupalvinos' => $ogv->idorden_grupal_vinos])->all();
                    if (isset($remitos)) {
                        foreach ($remitos as $r) {
                            if(!isset($r->valor_caja) || !isset($r->precio_reparto) || !isset($r->total_facturacion))
                            {
                                if(!isset($r->valor_caja))
                                    $r->valor_caja = $tv->precio_caja;
                                if(!isset($r->precio_reparto))
                                    $r->precio_reparto = $tv->precio_repartidor;
                                if(!isset($r->total_facturacion))
                                    $r->total_facturacion = (($r->cant_cajas * $tv->precio_caja) + (($r->valor_declarado / 2) * 0.01));
                                
                                $r->save();
                            }
                            $model->cajas += $r->cant_cajas;
                            $r->valor_caja = (($r->cant_cajas * $tv->precio_caja));
                            $model->valor_cajas += $r->valor_caja;
                            $model->total_seguro += (($r->valor_declarado / 2) * 0.01);
                            $r->total_facturacion = (($r->cant_cajas * $tv->precio_caja) + (($r->valor_declarado / 2) * 0.01));
                            $model->total += $r->total_facturacion;
                            if(isset($r->guia_idtransporte)){
                                if( ( ( ($r->guia_tipo == 'A') || ($r->guia_tipo == 'B') || ($r->guia_tipo == 'R') ) && ($r->guia_facturada_a == 'Vertiente')) || ( ( ($r->guia_tipo == 'A') || ($r->guia_tipo == 'B') ) && ($r->guia_facturada_a == 'Cliente'))){
                                    $model->total_guias += ($r->guia_total);
                                }
                            }
                        }
                    }
                    $model->total_cobrar = $model->total + $model->total_guias;
                    $model->creado = date('Y-m-d G:i', time());
                    $model->save(false);
                    Yii::$app->session->setflash('success', "Liquidación vino cliente creada con éxito."); 
                }
            }else{
                Yii::$app->session->setflash(
                    'error',
                    "No hay remitos a liquidar para el cliente ".
                    $model->clienteIdcliente->razonsocial);
            }
        }else{
            Yii::$app->session->setflash(
                'error',
                "El cliente: ".$model->clienteIdcliente->razonsocial.
                " no tiene tarifario de vino asociado!");
        }
        return $this->redirect(['view', 'id' => $model->idliquidacion_vino_cliente]);
        
    }


    /**
     * Updates an existing LiquidacionVinoCliente model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $estado_liquidacion_anterior = $model->estadoliquidacion_idestadoliquidacion;

        if(!$model->chequeIdcheque){
            $cheque = new Cheque();
        }else{
            $cheque = $model->chequeIdcheque;
        }

        if ($model->load(Yii::$app->request->post())) {
            if($model->estadoliquidacion_idestadoliquidacion != $estado_liquidacion_anterior && $model->estadoliquidacion_idestadoliquidacion != 1){
                $model->fecha_pago = date('Y-m-d G:i', time());
                if($model->liquidacionformapago_idliquidacionformapago == 1){
                    Yii::$app->session->setflash('error', "Por favor seleccione una forma de pago.");
                    return $this->redirect(['update', 'id' => $model->idliquidacion_vino_cliente]);
                }
            }
            if($cheque->load(Yii::$app->request->post()) && $cheque->save()){
                $model->cheque_idcheque = $cheque->idcheque;
            }
            if($cheque->estado == "Rechazado"){
                $model->estadoliquidacion_idestadoliquidacion = 1;
                $model->pago_parcial = 0;
                $model->liquidacionformapago_idliquidacionformapago = 1;
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->idliquidacion_vino_cliente]);
        }

        return $this->render('update', [
            'model' => $model,
            'cheque' => $cheque
        ]);
    }

    /**
     * Deletes an existing LiquidacionVinoCliente model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $ordenes = OrdenGrupalVinos::find()->where(['liquidacionvinocliente_idliquidacionvinocliente' => $model->idliquidacion_vino_cliente])->all();

        if($ordenes){
            foreach($ordenes as $o){
                $o->liquidacionvinocliente_idliquidacionvinocliente = null;
                $o->save();
            }
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LiquidacionVinoCliente model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LiquidacionVinoCliente the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LiquidacionVinoCliente::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionReport($id) {

        $model = $this->findModel($id);
        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('view-pdf', ['model' => $model]);

        // setup kartik\mpdf\Pdf component
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    public function actionLiquidate($id){
        $model = $this->findModel($id);
        $model->confirmada = 1;
        $model->save();
        $ordengrupalvino = OrdenGrupalVinos::getOrdenVinosClienteByFecha($model->cliente_idcliente, $model->periodo_inicio, $model->periodo_fin);
        foreach($ordengrupalvino as $ogv){
            $ogv->liquidacionvinocliente_idliquidacionvinocliente = $model->idliquidacion_vino_cliente;
            $ogv->save(false);
            $remitos = RemitoVino::find()->where(['ordengrupalvinos_idordengrupalvinos' => $ogv->idorden_grupal_vinos])->all();
            foreach ($remitos as $r) {
                $r->total_facturacion = (($r->cant_cajas * $tv->precio_caja) + (($r->valor_declarado / 2) * 0.01));
                $r->save();
            }
        }
        return $this->redirect(['report', 'id' => $model->idliquidacion_vino_cliente]);
    }
}
