<?php

use common\models\LiquidacionDomicilioRepartidor;
use common\models\OrdenGrupalDomicilio;
use common\models\GuiaDomicilio;
use \yii\helpers\ArrayHelper;


$ordenesGrupales = OrdenGrupalDomicilio::getAllOrdenesRepartidorFechaLiquidados($model->repartidor_idrepartidor, $model->fecha_inicio, $model->fecha_fin, $liquidacionDomicilio->idliquidacion_domicilio_repartidor);

$ordenesGrupalesId = ArrayHelper::getColumn($ordenesGrupales,'idorden_grupal_domicilio');
$stringOrdenesId = implode(",", $ordenesGrupalesId);
//echo "Dom <pre>".$stringOrdenesId;
$remitosDomicilio = GuiaDomicilio::find() 
    ->select('  SUM(cantidad) as cantidad,
                count(*) as cantidad_tipob,
                precio_reparto,
                precio_reparto_adicional,
                fecha,
                tipo_cliente as empresa_destino,
                ordengrupaldomicilio_idordengrupaldomicilio,
                cliente_idcliente')
    ->joinWith(['ordengrupaldomicilioIdordengrupaldomicilio'])
    ->where(['IN', 'ordengrupaldomicilio_idordengrupaldomicilio', $ordenesGrupalesId])
    ->groupBy([ 'cliente_idcliente','fecha' ])
    ->all();

$remitosClientes = OrdenGrupalDomicilio::getAllOrdenesRepartidorFechaLiquidadosCliente($model->repartidor_idrepartidor, $model->fecha_inicio, $model->fecha_fin, $liquidacionDomicilio->idliquidacion_domicilio_repartidor);
if($remitosDomicilio){?>
    <table style="table-layout: fixed; width: 100% ;">
        <thead>
            <tr>
                <th style="width:10%; text-align:center ">Cliente</th>
                <?php for($i = 1; $i <=31; $i++){ ?>
                        <th style="width:4%;  text-align:center "><?= $i ?></th>
                    <?php } ?>
                    <th style="width:5%;  text-align:center">TOTAL</th><!--Resto de celdas de fila cliente-->
            </tr>
        </thead>
        <tbody >
        <?php
        $clienteAnterior = 0;
        foreach($remitosClientes as $rc){
            $totalCantidad = 0;?>
            <tr style="background-color: #ddd"><!--Fila cliente-->
                <td style="border-right: 1px solid #ccc;width:10%"><strong><?= $rc->clienteIdcliente->razonsocial." (".$rc->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo.")" ?></strong></td><!--Primer celda fila cliente-->
                <?php for($i = 0; $i <= 31; $i++){ ?>
                    <td style="border-right: 1px solid #ccc;width:4%"></td><!--Resto de celdas de fila cliente-->
                <?php } ?>
            </tr>

            <tr style="border-bottom: 1px solid #ccc;">
                <td style="border-right: 1px solid #ccc; width:10%; text-align:center"><strong>CANTIDAD</strong></td>
                <?php 
                for($i = 1; $i <= 32; $i++){ 
                    $flag = 0;
                    foreach($remitosDomicilio as $cc){
                        if(($rc->cliente_idcliente == $cc->ordengrupaldomicilioIdordengrupaldomicilio->cliente_idcliente) && ($cc->cantidad)){
                            $date = date('d', strtotime($cc->ordengrupaldomicilioIdordengrupaldomicilio->fecha));
                            $date = intval($date); 

                            if(($cc->cantidad != 0) && ($date == $i)){
                                if($cc->empresa_destino == "Tipo A"){
                                $totalCantidad += $cc->cantidad;?>
                                <td style="border-right: 1px solid #ccc;width:4%; text-align:center"><?= $cc->cantidad?></td>
                            <?php 
                                }
                                if($cc->empresa_destino == "Tipo B"){
                                $totalCantidad +=  $cc->cantidad_tipob?>
                                <td style="border-right: 1px solid #ccc;width:4%; text-align:center"><?= $cc->cantidad_tipob ?></td>
                            <?php 
                                }
                                $flag = 1;
                                break;
                            }  
                        }
                    }
                    if($flag == 0){  
                        if($i < 32){?>
                        <td style="border-right: 1px solid #ccc;width:4%"></td>
                        <?php }else{ ?>
                            <td style="border-right: 1px solid #ccc;width:5%; text-align:center"><strong><?= $totalCantidad ?></strong></td>
                        <?php }
                    } 
                }?>
            </tr>
            <tr style="border-bottom: 1px solid #ccc;">
                <td style="border-right: 1px solid #ccc; width:10%; text-align:center"><strong>EXC</strong></td>
                <?php
                $totalExtra = 0;
                for($i = 1; $i <= 32; $i++){ 
                    $flag = 0;
                    foreach($remitosDomicilio as $cc){
                        if(($rc->cliente_idcliente == $cc->ordengrupaldomicilioIdordengrupaldomicilio->cliente_idcliente) && ($cc->cantidad) && ($cc->empresa_destino == "Tipo B")){
                            $date = date('d', strtotime($cc->ordengrupaldomicilioIdordengrupaldomicilio->fecha));
                            $date = intval($date); 
                            if(($date == $i)){
                                $totalExtra+= $cc->cantidad - $cc->cantidad_tipob?>
                                <td style="border-right: 1px solid #ccc;width:4%; text-align:center"><?= ($cc->cantidad - $cc->cantidad_tipob)?></td>
                            <?php 
                                $flag = 1;
                                break;
                            }  
                        }
                    }
                    if($flag == 0){  
                        if($i < 32){?>
                        <td style="border-right: 1px solid #ccc;width:4%"></td>
                        <?php }else{ ?>
                            <td style="border-right: 1px solid #ccc;width:5%; text-align:center"><strong><?= $totalExtra ?></strong></td>
                        <?php }
                    } 
                }?>
            </tr>
    <?php   } ?>
        </tbody>
    </table>
    <br>
<?php  
} ?>