<?php

namespace frontend\controllers;

use Yii;
use frontend\models\LiquidacionDomicilioClienteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\mpdf\Pdf;
use yii\helpers\ArrayHelper;


use common\models\GuiaDomicilio;
use common\models\OrdenGrupalDomicilio;
use common\models\LiquidacionDomicilioCliente;
use common\models\TarifarioDomicilio;
use common\models\Rendicion;
use common\models\Cheque;


/**
 * LiquidacionDomicilioClienteController implements the CRUD actions for LiquidacionDomicilioCliente model.
 */
class LiquidacionDomicilioClienteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LiquidacionDomicilioCliente models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LiquidacionDomicilioClienteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LiquidacionDomicilioCliente model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LiquidacionDomicilioCliente model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LiquidacionDomicilioCliente();

        if($model->load(Yii::$app->request->post())) {
            //Traer todas las ordenes grupales del cliente para la fecha
            $ordenesGrupales = OrdenGrupalDomicilio::getAllOrdenesClienteFecha($model->cliente_idcliente, $model->fecha_inicio, $model->fecha_fin);
            $ordenesGrupalesId = ArrayHelper::getColumn($ordenesGrupales,'idorden_grupal_domicilio');
            if(!empty($ordenesGrupales)){
                $model->save();
                if($model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo A"){
                    foreach($ordenesGrupales as $OG){
                        if(!isset($OG->precio_reparto) || !isset($OG->precio_reparto_adicional) || !isset($OG->precio_sprinter) || !isset($OG->precio_bulto) || !isset($OG->precio_bulto_adicional))
                        {
                            if(!isset($OG->precio_reparto))
                                $OG->precio_reparto = $model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->reparto;
                            if(!isset($OG->precio_reparto_adicional) )
                                $OG->precio_reparto_adicional = $model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->reparto_adicional;
                            if(!isset($OG->precio_sprinter))
                                $OG->precio_sprinter = $model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->sprinter;
                            if(!isset($OG->precio_bulto))
                                $OG->precio_bulto = $model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->primer_bulto;
                            if(!isset($OG->precio_bulto_adicional))
                                $OG->precio_bulto_adicional = $model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->bulto_adicional;
                            
                            $OG->save(false);
                        }
                        $guias = GuiaDomicilio::getAllGuiasFromOrden($OG->idorden_grupal_domicilio);
                        foreach($guias as $gui){
                            $model->cantidad_bultos += $gui->cantidad;
                            if(($gui->guia_tipo == 'B')){
                                $model->total_b += $gui->monto;
                            }else{
                                $model->total_ar += $gui->monto;
                            }
                        }
                    }
                    $model->neto_b = ($model->total_b / 1.21);
                    $model->importe_bultos = $model->neto_b + $model->total_ar;
                    $model->total_iva = $model->importe_bultos * 0.21;
                    $model->total = $model->total_iva + $model->importe_bultos;
                }else if($model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo B"){
                    foreach($ordenesGrupales as $OG){
                        if(!isset($OG->precio_reparto) || !isset($OG->precio_reparto_adicional) || !isset($OG->precio_sprinter) || !isset($OG->precio_bulto) || !isset($OG->precio_bulto_adicional))
                        {
                            if(!isset($OG->precio_reparto))
                                $OG->precio_reparto = $model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->reparto;
                            if(!isset($OG->precio_reparto_adicional) )
                                $OG->precio_reparto_adicional = $model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->reparto_adicional;
                            if(!isset($OG->precio_sprinter))
                                $OG->precio_sprinter = $model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->sprinter;
                            if(!isset($OG->precio_bulto))
                                $OG->precio_bulto = $model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->primer_bulto;
                            if(!isset($OG->precio_bulto_adicional))
                                $OG->precio_bulto_adicional = $model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->bulto_adicional;
                            
                            $OG->save(false);
                        }
                        //En Tipo B los campos de Tipo A cumple la siguiente función:
                        //total_ar almacena la suma de rendido_comun
                        //total_b almacena la suma de rendido_sprinter
                        $tarifario = TarifarioDomicilio::find()->where(['idtarifario_domicilio' => $OG->clienteIdcliente->tarifariodomicilio_idtarifariodomicilio])->one();
                        $guias = GuiaDomicilio::getAllGuiasFromOrden($OG->idorden_grupal_domicilio);
                        foreach($guias as $r){

                            $remitosOrden = GuiaDomicilio::find()->where(['ordengrupaldomicilio_idordengrupaldomicilio' => $r->ordengrupaldomicilio_idordengrupaldomicilio])->orderBy(['sprinter' => SORT_DESC])->all();
                            $tarifario = TarifarioDomicilio::find()->where(['idtarifario_domicilio' => $r->ordengrupaldomicilioIdordengrupaldomicilio->clienteIdcliente->tarifariodomicilio_idtarifariodomicilio])->one();
                            $remitosId = ArrayHelper::getColumn($remitosOrden,'idorden_grupal_domicilio');
                            $sprinterArray = ArrayHelper::getColumn($remitosOrden,'sprinter');
                            
                            if(($remitosOrden[0][sprinter] != 1))
                                $model->rendicion_comun += $r->monto;

                            //Function that return the sum of amounts of guias tipo A
                            if(($remitosOrden[0][sprinter] == 1))
                                $model->rendicion_sprinter += $r->monto;

                                //Function that return the sum of amounts of guias tipo A
                            //Function that return the sum of amounts of guias tipo A
                            if($sprinterArray[0] == 1){
                                if($r->idorden_grupal_domicilio == $remitosId[0]){
                                    $model->reparto += $r->ordengrupaldomicilioIdordengrupaldomicilio->precio_sprinter;
                                }else{
                                    $model->reparto += 0;
                                }
                            }else{
                                if(($r->cantidad == 1) && ($r->sprinter == 0)){
                                    $model->reparto += $r->ordengrupaldomicilioIdordengrupaldomicilio->precio_bulto;
                                }else if(($r->cantidad > 1) && ($r->sprinter == 0)){
                                    $model->reparto += ($r->ordengrupaldomicilioIdordengrupaldomicilio->precio_bulto + (($r->cantidad - 1) * $r->ordengrupaldomicilioIdordengrupaldomicilio->precio_bulto_adicional));
                                }else if(($r->sprinter == 1)){
                                    $model->reparto += $r->ordengrupaldomicilioIdordengrupaldomicilio->precio_sprinter;
                                }
                            }
                            
                            //$model->importe_bultos += LiquidacionDomicilioCliente::getRepartoSum($remitos);

                            if($r->sprinter || ($sprinterFlag && $r->ordengrupaldomicilio_idordengrupaldomicilio == $remitoAnteriorOrden)){
                                if($r->sprinter){
                                    $model->importe_bultos += $tarifario->sprinter;
                                    $sprinterFlag = 1;
                                }else{
                                    $model->importe_bultos += 0;
                                }
                            }else{
                                if(($r->cantidad == 1) && (!$r->sprinter)){
                                    $model->importe_bultos += $tarifario->primer_bulto;
                                }else if(($r->cantidad > 1) && (!$r->sprinter)){
                                    $model->importe_bultos += ($tarifario->primer_bulto + (($r->cantidad - 1) * $tarifario->bulto_adicional));
                                }
                                $sprinterFlag = 0;
                            }
                            $remitoAnteriorOrden = $r->ordengrupaldomicilio_idordengrupaldomicilio;

                            $model->cantidad_bultos += $r->cantidad;
                        }
                    }
                    $model->total_iva = $model->reparto * 0.21;
                    $model->total = $model->total_iva + $model->reparto;
                }

                if($model->estadoliquidacion_idestadoliquidacion != 1)
                    $model->fecha_pago = date('Y-m-d G:i', time());

                $model->creado = date('Y-m-d G:i', time());
                $model->save();
                Yii::$app->session->setflash('success', "Liquidación domicilio cliente creada con éxito.");               
            }else{
                Yii::$app->session->setflash('error', "No se puede crear la liquidación. No hay guias asociadas al cliente.");
                $model->delete();
                return $this->render('create', [
                    'model' => $model,
                ]);            
            }
            return $this->redirect(['view', 'id' => $model->idliquidacion_domicilio_cliente]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new LiquidacionDomicilioCliente model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRecalculate($id)
    {
        $model = $this->findModel($id);
        $model->cantidad_bultos = 0;
        $model->total_b = 0;
        $model->total_ar = 0;
        $model->neto_b = 0;
        $model->importe_bultos = 0;
        $model->total_iva = 0;
        $model->total = 0;
        $model->rendicion_comun = 0;
        $model->rendicion_sprinter = 0;
        $model->reparto = 0;
        
        $ordenesGrupales = OrdenGrupalDomicilio::getAllOrdenesClienteFechaLiquidados($model->cliente_idcliente, $model->fecha_inicio, $model->fecha_fin, $model->idliquidacion_domicilio_cliente);
        if(!empty($ordenesGrupales)){
            foreach($ordenesGrupales as $OG){
                $OG->liquidacioncliente_idliquidacioncliente = null;
                $OG->save();
            }
        }

        //Traer todas las ordenes grupales del cliente para la fecha
        $ordenesGrupales = OrdenGrupalDomicilio::getAllOrdenesClienteFecha($model->cliente_idcliente, $model->fecha_inicio, $model->fecha_fin);
        if(!empty($ordenesGrupales)){
            $model->save();
            if($model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo A"){
                foreach($ordenesGrupales as $OG){
                    $guias = GuiaDomicilio::getAllGuiasFromOrden($OG->idorden_grupal_domicilio);
                    foreach($guias as $gui){
                        //La suma de bultos se hace igual en ambos tipos de guias
                        $model->cantidad_bultos += $gui->cantidad;
                        if(($gui->guia_tipo == 'B')){
                            //Si la guia es tipo B el monto de la guia se suma al total_b de la liquidación
                            $model->total_b += $gui->monto;
                        }else{
                            //Si la guia es tipo A el monto de la guia se suma al total_ar de la liquidación
                            $model->total_ar += $gui->monto;
                        }
                    }
                }
                //El neto_b de la liquidación es igual a
                $model->neto_b = ($model->total_b / 1.21);
                //El importe bultos es la suma de las total_b de guias B y total_ar de guias A
                $model->importe_bultos = $model->neto_b + $model->total_ar;
                //El IVA es el 21% de importe_bultos
                $model->total_iva = $model->importe_bultos * 0.21;
                //El total de la liquidacion es la suma de total_iva más el importe bultos
                $model->total = $model->total_iva + $model->importe_bultos;
            }else if($model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo B"){
                $ordenesGrupalesId = ArrayHelper::getColumn($ordenesGrupales,'idorden_grupal_domicilio');
                $remitos = GuiaDomicilio::find()->where(['IN', 'ordengrupaldomicilio_idordengrupaldomicilio', $ordenesGrupalesId])->all();
                        
                $remitoAnteriorOrden = 0;
                $sprinterFlag = 0;    
                $fechaAnterior = 0;    
                //echo "La suma es: ".$model->importe_bultos."<br>";
                foreach($remitos as $r){
                    $remitosOrden = GuiaDomicilio::find()->where(['ordengrupaldomicilio_idordengrupaldomicilio' => $r->ordengrupaldomicilio_idordengrupaldomicilio])->orderBy(['sprinter' => SORT_DESC])->all();
                    $tarifario = TarifarioDomicilio::find()->where(['idtarifario_domicilio' => $r->ordengrupaldomicilioIdordengrupaldomicilio->clienteIdcliente->tarifariodomicilio_idtarifariodomicilio])->one();
                    $remitosId = ArrayHelper::getColumn($remitosOrden,'idorden_grupal_domicilio');
                    $sprinterArray = ArrayHelper::getColumn($remitosOrden,'sprinter');
                    
                    if(($remitosOrden[0][sprinter] != 1))
                        $model->rendicion_comun += $r->monto;

                    //Function that return the sum of amounts of guias tipo A
                    if(($remitosOrden[0][sprinter] == 1))
                        $model->rendicion_sprinter += $r->monto;

                    //Function that return the sum of amounts of guias tipo A
                    if($sprinterArray[0] == 1){
                        if($r->idorden_grupal_domicilio == $remitosId[0]){
                            $model->reparto += $r->ordengrupaldomicilioIdordengrupaldomicilio->precio_sprinter;
                        }else{
                            $model->reparto += 0;
                        }
                    }else{
                        if(($r->cantidad == 1) && ($r->sprinter == 0)){
                            $model->reparto += $r->ordengrupaldomicilioIdordengrupaldomicilio->precio_bulto;
                        }else if(($r->cantidad > 1) && ($r->sprinter == 0)){
                            $model->reparto += ($r->ordengrupaldomicilioIdordengrupaldomicilio->precio_bulto + (($r->cantidad - 1) * $r->ordengrupaldomicilioIdordengrupaldomicilio->precio_bulto_adicional));
                        }else if(($r->sprinter == 1)){
                            $model->reparto += $r->ordengrupaldomicilioIdordengrupaldomicilio->precio_sprinter;
                        }
                    }
                    $fechaAnterior = $r->ordengrupaldomicilioIdordengrupaldomicilio->fecha;

                    
                    if($r->sprinter || ($sprinterFlag && $r->ordengrupaldomicilio_idordengrupaldomicilio == $remitoAnteriorOrden)){
                        if($r->sprinter){
                            $model->importe_bultos += $tarifario->sprinter;
                            $sprinterFlag = 1;
                        }else{
                            $model->importe_bultos += 0;
                        }
                    }else{
                        if(($r->cantidad == 1) && (!$r->sprinter)){
                            $model->importe_bultos += $tarifario->primer_bulto;
                        }else if(($r->cantidad > 1) && (!$r->sprinter)){
                            $model->importe_bultos += ($tarifario->primer_bulto + (($r->cantidad - 1) * $tarifario->bulto_adicional));
                        }
                        $sprinterFlag = 0;
                    }
                    $remitoAnteriorOrden = $r->ordengrupaldomicilio_idordengrupaldomicilio;

                    $model->cantidad_bultos += $r->cantidad;
                }
                $model->total_iva = $model->reparto * 0.21;
                $model->total = $model->total_iva + $model->reparto;
            }

            $model->creado = date('Y-m-d G:i', time());
            $model->save();
            Yii::$app->session->setflash('success', "Liquidación domicilio cliente creada con éxito.");               
        }else{
            Yii::$app->session->setflash('error', "No se puede crear la liquidación. No hay guias asociadas al cliente.");
            $model->delete();
            return $this->render('create', [
                'model' => $model,
            ]);            
        }
        return $this->redirect(['view', 'id' => $model->idliquidacion_domicilio_cliente]);
    }

    /**
     * Updates an existing LiquidacionDomicilioCliente model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $estado_liquidacion_anterior = $model->estadoliquidacion_idestadoliquidacion;
        
        if(!$model->chequeIdcheque){
            $cheque = new Cheque();
        }else{
            $cheque = $model->chequeIdcheque;
        }

        if ($model->load(Yii::$app->request->post())) {
            if($model->estadoliquidacion_idestadoliquidacion != $estado_liquidacion_anterior && $model->estadoliquidacion_idestadoliquidacion != 1){
                $model->fecha_pago = date('Y-m-d G:i', time());
                if($model->liquidacionformapago_idliquidacionformapago == 1){
                    Yii::$app->session->setflash('error', "Por favor seleccione una forma de pago.");
                    return $this->redirect(['update', 'id' => $model->idliquidacion_domicilio_cliente]);
                }
            }
            if($cheque->load(Yii::$app->request->post()) && $cheque->save()){
                $model->cheque_idcheque = $cheque->idcheque;
            }
            if($cheque->estado == "Rechazado"){
                $model->estadoliquidacion_idestadoliquidacion = 1;
                $model->pago_parcial = 0;
                $model->liquidacionformapago_idliquidacionformapago = 1;
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->idliquidacion_domicilio_cliente]);
        }

        return $this->render('update', [
            'model' => $model,
            'cheque' => $cheque,
        ]);
    }

    /**
     * Deletes an existing LiquidacionDomicilioCliente model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $ordenesGrupales = OrdenGrupalDomicilio::getAllOrdenesClienteFechaLiquidados($model->cliente_idcliente, $model->fecha_inicio, $model->fecha_fin, $model->idliquidacion_domicilio_cliente);
        if(!empty($ordenesGrupales)){
            foreach($ordenesGrupales as $OG){
                $OG->liquidacioncliente_idliquidacioncliente = null;
                $OG->save();
            }
        }

        $model->borrado = 1;
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LiquidacionDomicilioCliente model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LiquidacionDomicilioCliente the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LiquidacionDomicilioCliente::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionReport($id) {

        $model = $this->findModel($id);
        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('view-pdf', ['model' => $model]);

        // setup kartik\mpdf\Pdf component
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    /**
     * Creates a new LiquidacionDomicilioCliente model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreateRendicion($rendicion)
    {
        $model = new LiquidacionDomicilioCliente();
        if($model->load(Yii::$app->request->post())) {
            if($model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo B"){
                $rendicion = new Rendicion();
                $rendicion->fecha_inicio = $model->fecha_inicio;
                $rendicion->fecha_fin = $model->fecha_fin;
                $rendicion->cliente_idcliente = $model->cliente_idcliente;
                $rendicion->save();
                return $this->render('rendicion', ['model' => $model]);
            }else{
                Yii::$app->session->setflash('error', "Las rendiciones son solo para clientes Tipo B.");
                return $this->render('create', [
                    'model' => $model,
                ]);   
            }
        }

        return $this->render('create-rendicion',[
            'model' => $model,
        ]);
    }

    public function actionReportRendicion($fechaRendicion, $clienteRendicion) {

        $model = new LiquidacionDomicilioCliente();
        $model->fecha_inicio = $fechaRendicion;
        $model->cliente_idcliente = $clienteRendicion;
        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('rendicion-pdf', ['model' => $model]);

        // setup kartik\mpdf\Pdf component
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;

        // return the pdf output as per the destination setting
        return $pdf->render();
    }

    public function actionLiquidate($id){
        $model = $this->findModel($id);
        $model->confirmada = 1;
        $model->save();
        $ordenesGrupales = OrdenGrupalDomicilio::updateAllOrdenesClienteFecha($model->cliente_idcliente, $model->fecha_inicio, $model->fecha_fin, $id);
        return $this->redirect(['report', 'id' => $model->idliquidacion_domicilio_cliente]);
    }
}
