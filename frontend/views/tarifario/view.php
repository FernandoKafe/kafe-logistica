<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\TarifarioHasPrecio;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Tarifario */
$idTarifario = Yii::$app->getRequest()->getQueryParam('id');


$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tarifarios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="localidad-view">
    <h1>Tarifario: <?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Detalle') ?></h2>
        <div class="clearfix"></div>
    </div>
    <p>
        <?= Html::a(Yii::t('app', 'Inicio'), ['index'], ['class' => 'btn btn-primary']) ?>

        <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->idtarifario], ['class' => 'btn btn-success']) ?>
        <!--<?/*= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idtarifario], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) */?>-->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idtarifario',
            'nombre',
            'tipo',
        ],
    ]) ?>

</div>
</div>
<!--Precios Asociadas-->
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Zonas asociadas');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php
        $dataProvider = new ActiveDataProvider([
            'query' => TarifarioHasPrecio::find()->where(['tarifario_idtarifario' => $idTarifario]),
        ]);
        $dataProvider->pagination->pageSize=20;
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'zona_idzona',
            'value' => function($data){
                return $data->zonaIdzona->nombre;
            }
        ],
        [
            'attribute' => 'carga_idcarga',
            'value' => function($data){
                return $data->cargaIdcarga->nombre;
            }
        ],
        [
            'attribute' => 'precio',
            'format' => ['decimal',2],
        ],
        [
            'attribute' => 'precio_adicional',
            'format' => ['decimal',2],
        ],
        [
            'attribute' => 'precio_reparto',
            'format' => ['decimal',2],
        ],
        [
            'attribute' => 'precio_reparto_adicional',
            'format' => ['decimal',2],
        ],
        [
            'attribute' => 'porcentaje_contra',
        ],
        /*[
            'attribute' => 'empresatransporteIdempresatransporte.razonsocial', 
            'label' => 'Empresa de transporte'
        ],*/
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Acciones',
            'headerOptions' => ['style' => 'color:#337ab7'],
            'template' => '{view}{update}{delete}',
            'buttons' => [
              'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'Ver'),
                    ]);
              },

              'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'Actualizar'),
                    ]);
                },

                'delete' => function ($url, $model) {
                      return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                  'title' => Yii::t('app', 'Eliminar'),
                                  'data' => [
                                    'confirm' => '¿Seguro desea eliminar ésta entrada?',
                                    'method' => 'post',
                                ],
                      ]);
                  },

            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    $url ='../tarifario-has-precio/view?zona_idzona='.$model->zona_idzona."&carga_idcarga=".$model->carga_idcarga."&tarifario_idtarifario=".$model->tarifario_idtarifario;
                    return $url;
                }

                if ($action === 'update') {
                    $url ='../tarifario-has-precio/update?zona_idzona='.$model->zona_idzona."&carga_idcarga=".$model->carga_idcarga."&tarifario_idtarifario=".$model->tarifario_idtarifario;
                    return $url;
                }

                if ($action === 'delete') {
                  $url ='../tarifario-has-precio/delete?zona_idzona='.$model->zona_idzona."&carga_idcarga=".$model->carga_idcarga."&tarifario_idtarifario=".$model->tarifario_idtarifario;
                  return $url;
                }
            }
        ],
    ],
    ]); ?>

    </div>
</div>