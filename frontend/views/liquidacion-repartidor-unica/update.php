<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionRepartidorUnica */

$this->title = Yii::t('app', 'Update Liquidacion Repartidor Unica: ' . $model->idliquidacion_unica, [
    'nameAttribute' => '' . $model->idliquidacion_unica,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion Repartidor Unicas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idliquidacion_unica, 'url' => ['view', 'id' => $model->idliquidacion_unica]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<h1><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario de modificación') ?></h2>
        <div class="clearfix"></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div> 
