
<?php

use \yii\helpers\ArrayHelper;
use common\models\RemitoPorcentaje;
use common\models\LiquidacionPorcentajeRepartidor;

$remitosPorcentaje = [];
if($liquidacionPorcentaje){

    $remitosClientes = RemitoPorcentaje::find()
    ->select('cliente_idcliente')
    ->joinWith('liquidacionporcentajerepartidorHasRemitos')
    ->where(['lpr_idlpr' => $liquidacionPorcentaje->idliquidacion_porcentaje_repartidor])
    ->groupBy(['cliente_idcliente'])
    ->orderBy(['cliente_idcliente' => SORT_ASC])
    ->all();

    $remitosPorcentaje = RemitoPorcentaje::find()
    ->select('  count(*) as cant_pedidos,
                fecha, 
                porcentaje_entregas as porcentaje,
                cliente_idcliente')
    ->joinWith('liquidacionporcentajerepartidorHasRemitos')
    ->where(['lpr_idlpr' => $liquidacionPorcentaje->idliquidacion_porcentaje_repartidor])
    ->groupBy(['cliente_idcliente', 'fecha' ])
    ->orderBy(['cliente_idcliente' => SORT_ASC, 'porcentaje' => SORT_ASC])
    ->all();

    $remitosPorcentajesValores = RemitoPorcentaje::find()
    ->select('  count(*) as cant_pedidos,
                COUNT(DISTINCT fecha) as reparto_uno,
                sum(dinero_faltante) as dinero_faltante,
                reparto_seleccionado,
                fecha, 
                porcentaje_entregas as porcentaje,
                cliente_idcliente')
    ->joinWith('liquidacionporcentajerepartidorHasRemitos')
    ->where(['lpr_idlpr' => $liquidacionPorcentaje->idliquidacion_porcentaje_repartidor])
    ->groupBy(['cliente_idcliente', 'reparto_seleccionado' ])
    ->orderBy(['cliente_idcliente' => SORT_ASC, 'porcentaje' => SORT_ASC])
    ->all();
}


if($remitosPorcentaje){?>
    <table style="table-layout: fixed;" cellspacing="0">
        <tbody>
            <tr>
                <td style='border: solid 1px #ccc; text-align:center; padding: 5px;' colspan=4><strong>RESUMEN CLIENTES PORCENTAJE</strong></td>
            </tr>
            <tr>
                <td style='border: solid 1px #ccc; padding: 5px;'><strong>Cliente</strong></td>
                <td style='border: solid 1px #ccc; padding: 5px;'><strong>Monto</strong></td>
                <td style='border: solid 1px #ccc; padding: 5px;'><strong>Faltante</strong></td>
                <td style='border: solid 1px #ccc;padding: 5px;'><strong>TOTAL</strong></td>
            </tr>
            <?php
            $montoTotal = 0;
            $montoFaltante = 0;
            $totalTotal = 0;
            foreach($remitosClientes as $rc){ ?>
                <tr>
                    <td style='border: solid 1px #ccc; padding: 5px;'><strong><?= $rc->clienteIdcliente->razonsocial;?></strong></td>
                <?php
                echo "<td style='border: solid 1px #ccc; padding: 5px; text-align:center;'>";
                foreach($remitosPorcentajesValores as $rpv){
                    if($rc->cliente_idcliente == $rpv->cliente_idcliente){
                        $montoTotal += $rpv->reparto_uno * $rpv->reparto_seleccionado;
                        echo $rpv->reparto_uno." x $".number_format ($rpv->reparto_seleccionado, 1 , "." ,  "," )."<br>";
                    }
                }
                echo "</td>";
                echo "<td style='border: solid 1px #ccc;padding: 5px; text-align:center;'>";
                foreach($remitosPorcentajesValores as $rpv){
                    if($rc->cliente_idcliente == $rpv->cliente_idcliente){
                        $montoFaltante += $rpv->dinero_faltante;
                        echo "$".number_format ($rpv->dinero_faltante, 2 , "." ,  "," )."<br>";
                    }
                }
                echo "</td>";
                echo "<td style='border: solid 1px #ccc;padding: 5px; text-align:center;'>";
                foreach($remitosPorcentajesValores as $rpv){
                    if($rc->cliente_idcliente == $rpv->cliente_idcliente){
                        $totalTotal += ($rpv->reparto_uno * $rpv->reparto_seleccionado) - $rpv->dinero_faltante;
                        echo "$".number_format (($rpv->reparto_uno * $rpv->reparto_seleccionado) - $rpv->dinero_faltante, 2 , "." ,  "," )."<br>";
                    }
                }
                echo "</td>";
            }?>
                </tr>
                <tr>
                    <td style='border: solid 1px #ccc; font-wight: bold; text-align: center;padding: 5px;'><strong>TOTAL</strong></td>
                    <td style='border: solid 1px #ccc; font-wight: bold; text-align: center;padding: 5px;'><strong>$<?= $montoTotal ?></strong></td>
                    <td style='border: solid 1px #ccc; font-wight: bold; text-align: center;padding: 5px;'><strong>$<?= $montoFaltante ?></strong></td>
                    <td style='border: solid 1px #ccc; font-wight: bold; text-align: center;padding: 5px;'><strong>$<?= $totalTotal ?></strong></td>
            </tr>
        </tbody>    
    </table>
    <br>
    <br>
<?php  
} ?>