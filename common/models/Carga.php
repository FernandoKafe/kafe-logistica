<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "carga".
 *
 * @property int $idcarga
 * @property string $nombre
 *
 * @property TarifarioHasTarifa[] $tarifarioHasTarifas
 */
class Carga extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'carga';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcarga' => Yii::t('app', 'ID Carga'),
            'nombre' => Yii::t('app', 'Carga'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarifarioHasTarifas()
    {
        return $this->hasMany(TarifarioHasTarifa::className(), ['carga_idcarga' => 'idcarga']);
    }
}
