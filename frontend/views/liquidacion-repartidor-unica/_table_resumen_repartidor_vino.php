<?php     

use \yii\helpers\ArrayHelper;
use common\models\RemitoVino;
use common\models\OrdenGrupalVinos;
use common\models\LiquidacionVinoRepartidor;

$remitosVino = [];
if($liquidacionVinos) {
    $ordengrupalvinos = OrdenGrupalVinos::find()->where(['liquidacionvinorepartidor_idliquidacionvinorepartidor' => $liquidacionVinos->idliquidacion_vino_repartidor])->all();
    //echo "<pre>",var_dump($ordenesGrupales),"</pre>";
    $ordenesGrupalesVinosId = ArrayHelper::getColumn($ordengrupalvinos, 'idorden_grupal_vinos');
    $remitosVino = RemitoVino::find()
    ->select('SUM(cant_cajas) as cant_cajas, ordengrupalvinos_idordengrupalvinos')
    ->where(['IN', 'ordengrupalvinos_idordengrupalvinos', $ordenesGrupalesVinosId])
    ->groupBy(['ordengrupalvinos_idordengrupalvinos'])
    ->orderBy(['ordengrupalvinos_idordengrupalvinos' => SORT_ASC])
    ->all();


    $remitosClientes = OrdenGrupalVinos::find()
    ->where(['liquidacionvinorepartidor_idliquidacionvinorepartidor' => $liquidacionVinos->idliquidacion_vino_repartidor])
    ->groupBy(['cliente_idcliente'])
    ->all();

    $arrayCajas = RemitoVino::find()
    ->select('SUM(cant_cajas) as cant_cajas, ordengrupalvinos_idordengrupalvinos, precio_reparto')
    ->joinWith(['ordengrupalvinosIdordengrupalvinos'])
    ->where(['IN', 'ordengrupalvinos_idordengrupalvinos', $ordenesGrupalesVinosId])
    ->groupBy(['cliente_idcliente', 'precio_reparto'])
    ->all();
}

if($remitosVino){?>
    <table style="table-layout: fixed;" cellspacing="0">
        <tbody>
                <tr>
                    <td style='border: solid 1px #ccc; text-align:center; padding: 5px;' colspan=3><strong>RESUMEN CLIENTES VINOS</strong></td>
                </tr>
                <tr>
                    <td style='border: solid 1px #ccc; padding: 5px;'><strong>Cliente</strong></td>
                    <td style='border: solid 1px #ccc; padding: 5px;'><strong>Cajas</strong></td>
                    <td style='border: solid 1px #ccc;padding: 5px;'><strong>TOTAL</strong></td>
                </tr>
                <?php 
                    $totalAcumulado = 0;
                    $totalCAC = 0;
                foreach($remitosClientes as $cliv){ 
                    $totalReparto = 0;
                    $totalCajas = 0;
                    ?>
                        <tr>
                            <td style='border: solid 1px #ccc; padding: 5px;'><strong><?= $cliv->clienteIdcliente->razonsocial;?></strong></td>
                            <?php 
                                $i = 0;
                                echo "<td style='border: solid 1px #ccc; text-align: center;padding: 5px;'>";
                                foreach($arrayCajas as $ac){
                                    if($cliv->cliente_idcliente == $ac->ordengrupalvinosIdordengrupalvinos->cliente_idcliente && $ac->cant_cajas != 0){
                                        if( $i == 0)
                                            echo $ac->cant_cajas." x $".$ac->precio_reparto." = $".($ac->cant_cajas * $ac->precio_reparto);
                                        else if( $i > 0)
                                            echo "<br>".$ac->cant_cajas." x $".$ac->precio_reparto." = $".($ac->cant_cajas * $ac->precio_reparto);
                                        $i++;
                                        $totalCajas += ($ac->cant_cajas * $ac->precio_reparto);
                                        $totalReparto += $totalCajas;
                                        $totalCAC += $totalCajas;
                                    }
                                }
                                echo "</td>";
                                echo "<td style='border: solid 1px #ccc; font-wight: bold; text-align: center;padding: 5px;'>$".($totalCajas)."</td>";
                                $totalAcumulado += $totalReparto;
                            ?>
                            </tr>
                            <tr>
                                    <td style='border: solid 1px #ccc; font-wight: bold; text-align: center;padding: 5px;'><strong>TOTAL</strong></td>
                                    <td style='border: solid 1px #ccc; font-wight: bold; text-align: center;padding: 5px;'><strong>$<?= $totalCAC ?></strong></td>
                                    <td style='border: solid 1px #ccc; font-wight: bold; text-align: center;padding: 5px;'><strong>$<?= $totalAcumulado ?></strong></td>
                            </tr>
                <?php } ?>
        </tbody>
    </table>
    <br>
    <br>
<?php  
} ?>