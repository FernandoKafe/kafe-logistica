<?php

namespace frontend\controllers;

use Yii;
use common\models\LiquidacionDomicilioRepartidor;
use frontend\models\LiquidacionDomicilioRepartidorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\TarifarioDomicilio;
use common\models\OrdenGrupalDomicilio;
use common\models\GuiaDomicilio;

/**
 * LiquidacionDomicilioRepartidorController implements the CRUD actions for LiquidacionDomicilioRepartidor model.
 */
class LiquidacionDomicilioRepartidorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LiquidacionDomicilioRepartidor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LiquidacionDomicilioRepartidorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LiquidacionDomicilioRepartidor model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LiquidacionDomicilioRepartidor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LiquidacionDomicilioRepartidor();

        if($model->load(Yii::$app->request->post())) {
            //Traer todas las ordenes grupales del cliente para la fecha
            $ordenesGruapales = OrdenGrupalDomicilio::getAllOrdenesRepartidorFecha($model->repartidor_idrepartidor, $model->fecha_inicio, $model->fecha_fin);
            if(!empty($ordenesGruapales)){
                foreach($ordenesGruapales as $OG){
                    if(!$OG->clienteIdcliente->tarifariodomicilio_idtarifariodomicilio){
                        Yii::$app->session->setflash('error', "No se puede crear la liquidación. El cliente ".$OG->clienteIdcliente->razonsocial." no tiene asignado un tarifario de domicilio.");
                        return $this->render('create', [
                            'model' => $model, 
                        ]);
                    } 
                }
                foreach($ordenesGruapales as $OG){
                    $tarifario = TarifarioDomicilio::find()->where(['idtarifario_domicilio' => $OG->clienteIdcliente->tarifariodomicilio_idtarifariodomicilio])->one();
                    $guias = GuiaDomicilio::getAllGuiasFromOrden($OG->idorden_grupal_domicilio);
                    if(!isset($OG->precio_reparto) || !isset($OG->precio_reparto_adicional) || !isset($OG->precio_sprinter) || !isset($OG->precio_bulto) || !isset($OG->precio_bulto_adicional) || !isset($OG->tipo_cliente))
                    {
                        if(!isset($OG->precio_reparto))
                            $OG->precio_reparto = $model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->reparto;
                        if(!isset($OG->precio_reparto_adicional) )
                            $OG->precio_reparto_adicional = $model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->reparto_adicional;
                        if(!isset($OG->precio_sprinter))
                            $OG->precio_sprinter = $model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->sprinter;
                        if(!isset($OG->precio_bulto))
                            $OG->precio_bulto = $model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->primer_bulto;
                        if(!isset($OG->precio_bulto_adicional))
                            $OG->precio_bulto_adicional = $model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->bulto_adicional;
                        if(!isset($OG->tipo_cliente))
                            $OG->tipo_cliente = $model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo;
                    }
                    foreach($guias as $gui){
                        $fechaAnterior = 0;
                        if($gui->ordengrupaldomicilioIdordengrupaldomicilio->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo A"){
                            $model->total +=  $gui->cantidad * $tarifario->reparto;
                        }else if($gui->ordengrupaldomicilioIdordengrupaldomicilio->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo B"){
                            if($fechaAnterior != $gui->ordengrupaldomicilioIdordengrupaldomicilio->fecha){
                                $model->total += $tarifario->reparto + (($gui->cantidad - 1) * $tarifario->reparto_adicional);
                                $fechaAnterior = $gui->ordengrupaldomicilioIdordengrupaldomicilio->fecha;
                            }else{
                                $model->total += ($gui->cantidad * $tarifario->reparto_adicional);
                                $fechaAnterior = $gui->ordengrupaldomicilioIdordengrupaldomicilio->fecha;
                            }
                        }
                    }

                    $model->creado = date('Y-m-d G:i', time());
                    $model->save();
                    $OG->liquidacionrepartidor_idliquidacionrepartidor = $model->idliquidacion_domicilio_repartidor;
                    $OG->save();
                }
            }else{
                Yii::$app->session->setflash('error', "No se puede crear la liquidación. No hay guias asociadas al repartidor.");
                $model->delete();
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            Yii::$app->session->setflash('success', "Liquidación domicilio repartidor creada con éxito.");
            return $this->redirect(['view', 'id' => $model->idliquidacion_domicilio_repartidor]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LiquidacionDomicilioRepartidor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idliquidacion_domicilio_repartidor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LiquidacionDomicilioRepartidor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {

        $model = $this->findModel($id);

        $ordenesGruapales = OrdenGrupalDomicilio::getAllOrdenesRepartidorFechaLiquidados($model->repartidor_idrepartidor, $model->fecha_inicio, $model->fecha_fin, $model->idliquidacion_domicilio_repartidor);
        if(!empty($ordenesGruapales)){
            foreach($ordenesGruapales as $OG){
                $OG->liquidacionrepartidor_idliquidacionrepartidor = null;
                $OG->save();
            }
        } 

        $model->borrado = 1;
        $model->save();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LiquidacionDomicilioRepartidor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LiquidacionDomicilioRepartidor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LiquidacionDomicilioRepartidor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionReport($id) {

        $model = $this->findModel($id);
        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('view', ['model' => $model]);

        // setup kartik\mpdf\Pdf component
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;

        // return the pdf output as per the destination setting
        return $pdf->render();
    }
}
