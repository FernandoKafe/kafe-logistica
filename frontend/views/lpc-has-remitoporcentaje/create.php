<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LpcHasRemitoporcentaje */

$this->title = Yii::t('app', 'Create Lpc Has Remitoporcentaje');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lpc Has Remitoporcentajes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lpc-has-remitoporcentaje-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
