<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\data\ActiveDataProvider;
use common\models\Sucursal;
use yii\grid\GridView;



/* @var $this yii\web\View */
/* @var $model common\models\Empresa */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="condicion-fiscal-view">

<h1><i class="fa fa-building"></i> Empresa: <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Detalle') ?></h2>
        <div class="clearfix"></div>
    </div>

    <p>
        <?= Html::a('Inicio', ['index'], ['class' => 'btn btn-primary']);?>
        <?= Html::a('Actualizar', ['update', 'id' => $model->idempresa], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'nombre',
            'telefono'
        ],
    ]) ?>

</div>
</div>

<!--Sucursales Asociadas-->
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Sucursale asociadas');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php
        $dataProvider = new ActiveDataProvider([
            'query' => Sucursal::find()->where(['empresa_idempresa' => $model->idempresa]),
        ]);
        $dataProvider->pagination->pageSize=20;
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'attribute' => 'nombre',
        ],
        [
            'attribute' => 'direccion',
            'content' => function($data){
                if(isset($data->direccion) && isset($data->localidad_idlocalidad)){
                    return $data->direccion.", ".$data->localidadIdlocalidad->nombre
                            .", ".$data->localidadIdlocalidad->departamentoIddepartamento->nombre
                            .", ".$data->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                }else{
                    return " - ";
                }
            }
        ],
        ],
    ]); ?>

    </div>
</div>
