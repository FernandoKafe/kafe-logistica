<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ZonaHasLocalidad */

$this->title = Yii::t('app', 'Modificar Localidade de Zona: ' . $model->zona_idzona, [
    'nameAttribute' => '' . $model->zona_idzona,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Zona Has Localidads'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->zona_idzona, 'url' => ['view', 'zona_idzona' => $model->zona_idzona, 'localidad_idlocalidad' => $model->localidad_idlocalidad]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<h1><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario de modificación') ?></h2>
        <div class="clearfix"></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
