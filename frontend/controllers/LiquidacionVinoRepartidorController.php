<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\models\Cliente;
use common\models\LiquidacionVinoRepartidor;
use common\models\OrdenGrupalVinos;
use common\models\RemitoVino;
use common\models\TarifarioVino;


use frontend\models\LiquidacionVinoRepartidorSearch;
/**
 * LiquidacionVinoRepartidorController implements the CRUD actions for LiquidacionVinoRepartidor model.
 */
class LiquidacionVinoRepartidorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LiquidacionVinoRepartidor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LiquidacionVinoRepartidorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LiquidacionVinoRepartidor model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LiquidacionVinoRepartidor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LiquidacionVinoRepartidor();

        if ($model->load(Yii::$app->request->post())) { 
            //Obtengo todos las ordenes grupales vinos comprendidad entre periodo inicio y periodo fin sin liquidar del cliente
            $ordengrupalvino = OrdenGrupalVinos::getOrdenVinosRepartidorByFecha($model->repartidor_idrepartidor, $model->periodo_inicio, $model->periodo_fin);
            $model->total = 0;
            if (isset($ordengrupalvino)) {
                foreach( $ordengrupalvino as $ogv) {
                    $cliente = Cliente::find()->where(['idcliente' => $ogv->cliente_idcliente])->one();
                    if(isset($cliente->tarifariovino_idtarifariovino)){
                        $tv = TarifarioVino::find()->where(['idtarifario_vino' => $cliente->tarifariovino_idtarifariovino])->one();
                        if(isset($tv)){
                            //Obtengo todos los remitos asociados a las ordenes grupales sin liquidar
                            $remitos = RemitoVino::find()->where(['ordengrupalvinos_idordengrupalvinos' => $ogv->idorden_grupal_vinos])->all();
                            if (isset($remitos)) {
                                foreach ($remitos as $r) {
                                    if(!isset($r->valor_caja) || !isset($r->precio_reparto) || !isset($r->total_facturacion))
                                    {
                                        if(!isset($r->valor_caja))
                                            $r->valor_caja = $tv->precio_caja;
                                        if(!isset($r->precio_reparto))
                                            $r->precio_reparto = $tv->precio_repartidor;
                                        if(!isset($r->total_facturacion))
                                            $r->total_facturacion = (($r->cant_cajas * $tv->precio_caja) + (($r->valor_declarado / 2) * 0.01));
                                        
                                        $r->save();
                                    }
                                    $model->total += ($r->cant_cajas * $tv->precio_repartidor);
                                }
                            }
                        }else{
                            Yii::$app->session->setflash(
                                'error',
                                "El cliente: ".$model->clienteIdcliente->razonsocial).
                                " no es un cliente de tipo vino o no tiene tarifario de vino asociado!";
                        }
                        $model->creado = date('Y-m-d G:i', time());
                        $model->save();
                        $ogv->liquidacionvinorepartidor_idliquidacionvinorepartidor = $model->idliquidacion_vino_repartidor;
                        $ogv->save(false);
                    }else{
                        Yii::$app->session->setflash(
                            'error',
                            "El cliente: ".$cliente->razonsocial." no es de tipo vino o no tiene asociado un tarifario vino.");
                    }
                }

            }else{
                Yii::$app->session->setflash(
                    'error',
                    "No hay remitos a liquidar para el cliente ".
                    $model->clienteIdcliente->razonsocial);
            }
            Yii::$app->session->setflash('success', "Liquidación vino repartidor creada con éxito.");
            return $this->redirect(['view', 'id' => $model->idliquidacion_vino_repartidor]);
        }

        return $this->render('create', ['model' => $model,]);
    }

    /**
     * Updates an existing LiquidacionVinoRepartidor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idliquidacion_vino_repartidor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LiquidacionVinoRepartidor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $ordenes = OrdenGrupalVinos::find()->where(['liquidacionvinorepartidor_idliquidacionvinorepartidor' => $model->idliquidacion_vino_repartidor])->all();

        if($ordenes){
            foreach($ordenes as $o){
                $o->liquidacionvinorepartidor_idliquidacionvinorepartidor = null;
                $o->save();
            }
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LiquidacionVinoRepartidor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LiquidacionVinoRepartidor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LiquidacionVinoRepartidor::findOne($id)) !== null) { 
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
