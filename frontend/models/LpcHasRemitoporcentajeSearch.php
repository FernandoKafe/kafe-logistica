<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LpcHasRemitoporcentaje;

/**
 * LpcHasRemitoporcentajeSearch represents the model behind the search form of `common\models\LpcHasRemitoporcentaje`.
 */
class LpcHasRemitoporcentajeSearch extends LpcHasRemitoporcentaje
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idlpc_has_remitoporcentaje', 'rp_idrp', 'lpc_idlpc'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LpcHasRemitoporcentaje::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idlpc_has_remitoporcentaje' => $this->idlpc_has_remitoporcentaje,
            'rp_idrp' => $this->rp_idrp,
            'lpc_idlpc' => $this->lpc_idlpc,
        ]);

        return $dataProvider;
    }
}
