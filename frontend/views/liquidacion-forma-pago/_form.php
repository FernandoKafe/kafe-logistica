<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionFormaPago */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="liquidacion-forma-pago-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'idliquidacion_forma_pago')->textInput() ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
