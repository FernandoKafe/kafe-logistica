<?php

namespace common\models;

use Yii; 

/**
 * This is the model class for table "sucursal".
 *
 * @property int $idsucursal
 * @property int $empresa_idempresa
 * @property string $direccion
 * @property int $localidad_idlocalidad
 * @property string $nombre
 *
 * @property Remito[] $remitos
 * @property Remito[] $remitos0
 * @property Empresa $empresaIdempresa
 * @property Localidad $localidadIdlocalidad
 */
class Sucursal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sucursal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['empresa_idempresa', 'nombre'], 'required'],
            [['empresa_idempresa', 'localidad_idlocalidad'], 'integer'],
            [['direccion', 'nombre'], 'string', 'max' => 100],
            [['empresa_idempresa'], 'exist', 'skipOnError' => true, 'targetClass' => Empresa::className(), 'targetAttribute' => ['empresa_idempresa' => 'idempresa']],
            [['localidad_idlocalidad'], 'exist', 'skipOnError' => true, 'targetClass' => Localidad::className(), 'targetAttribute' => ['localidad_idlocalidad' => 'idlocalidad']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idsucursal' => Yii::t('app', 'ID Sucursal'),
            'empresa_idempresa' => Yii::t('app', 'Empresa'),
            'direccion' => Yii::t('app', 'Dirección'),
            'localidad_idlocalidad' => Yii::t('app', 'Localidad'),
            'nombre' => Yii::t('app', 'Nombre Sucursal'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemitos()
    {
        return $this->hasMany(Remito::className(), ['sucursal_idsucursal' => 'idsucursal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresaIdempresa()
    {
        return $this->hasOne(Empresa::className(), ['idempresa' => 'empresa_idempresa']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalidadIdlocalidad()
    {
        return $this->hasOne(Localidad::className(), ['idlocalidad' => 'localidad_idlocalidad']);
    }

    //Funcíon que devuelve el nombre de la sucursal con el nombre de la empresa
    public static function getAllSucursales()
    {
        return self::find()->joinWith(['empresaIdempresa'])
        ->select(['idsucursal', 'concat(empresa.nombre, " - ", direccion) as direccion'])
        ->all();
    }

    //Funcíon la sucursal central de cada empresa
     /**
     * @return \yii\db\ActiveQuery
     */
    public static function getSucursalCentralEmpresa($empresaId)
    {
        return self::find()
        ->where(['empresa_idempresa' => $empresaId])
        ->andWhere(['=', 'nombre', 'Central'])
        ->one();
    }

    /**
     * Funcion que busca un empresa por razón social
     * @param string $q Razón social de la empresa
     * @return [id, text] Arreglo de identificador y razon social
     */
    public static function getSucursalWithEmpresa($q){
        $query = self::find();
        $query->joinWith('empresaIdempresa', false)
            ->select(['idsucursal AS id', 'concat(empresa.nombre, " - ", sucursal.nombre) AS text',])
            ->where(['like', 'empresa.nombre', $q])
            ->orWhere(['like', 'sucursal.nombre', $q])
            ->orderBy(["empresa.nombre" => SORT_ASC, "sucursal.nombre" => SORT_ASC]);
        return $query->asArray()->all();
    }
}
