<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LiquidacionVinoRepartidor;

/**
 * LiquidacionVinoRepartidorSearch represents the model behind the search form of `common\models\LiquidacionVinoRepartidor`.
 */
class LiquidacionVinoRepartidorSearch extends LiquidacionVinoRepartidor
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idliquidacion_vino_repartidor', 'liquidacionunica'], 'integer'],
            [['nombre', 'periodo_inicio', 'periodo_fin', 'estadoliquidacion_idestadoliquidacion', 'repartidor_idrepartidor', 'creado'], 'safe'],
            [['total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LiquidacionVinoRepartidor::find();
        $query->joinWith(['estadoliquidacionIdestadoliquidacion as estado']);
        $query->joinWith(['repartidorIdrepartidor as repartidor']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idliquidacion_vino_repartidor' => $this->idliquidacion_vino_repartidor,
            'periodo_inicio' => $this->periodo_inicio,
            'periodo_fin' => $this->periodo_fin,
            'total' => $this->total,
            'creado' => $this->creado,
            'liquidacionunica' => $this->liquidacionunica,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre]);
        $query->andFilterWhere(['like', 'CONCAT(repartidor.apellido, ", ", repartidor.nombre)', $this->repartidor_idrepartidor]);
        $query->andFilterWhere(['like', 'estado.nombre', $this->estadoliquidacion_idestadoliquidacion]);


        return $dataProvider;
    }
}
