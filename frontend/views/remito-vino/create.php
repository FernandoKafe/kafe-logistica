<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RemitoVino */

$this->title = Yii::t('app', 'Crear Remito Vino');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Remito Vinos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="remito-vino-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario') ?></h2>
        <div class="clearfix"></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?> 

</div>
</div>
