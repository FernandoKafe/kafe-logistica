<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%liquidacion_vino_repartidor}}".
 *
 * @property int $idliquidacion_vino_repartidor
 * @property string $nombre
 * @property string $periodo_inicio
 * @property string $periodo_fin
 * @property double $total
 * @property int $repartidor_idrepartidor
 * @property int $estadoliquidacion_idestadoliquidacion
 * @property string $creado
 * @property int liquidacionunica
 *
 * @property EstadoLiquidacion $estadoliquidacionIdestadoliquidacion
 * @property Repartidor $repartidorIdrepartidor
 * @property OrdenGrupalVinos[] $ordenGrupalVinos
 */
class LiquidacionVinoRepartidor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%liquidacion_vino_repartidor}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['periodo_inicio', 'periodo_fin', 'creado'], 'safe'],
            [['total'], 'number'],
            [['repartidor_idrepartidor'], 'required'],
            [['repartidor_idrepartidor', 'estadoliquidacion_idestadoliquidacion', 'liquidacionunica'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['estadoliquidacion_idestadoliquidacion'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoLiquidacion::className(), 'targetAttribute' => ['estadoliquidacion_idestadoliquidacion' => 'idestado_liquidacion']],
            [['repartidor_idrepartidor'], 'exist', 'skipOnError' => true, 'targetClass' => Repartidor::className(), 'targetAttribute' => ['repartidor_idrepartidor' => 'idrepartidor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idliquidacion_vino_repartidor' => Yii::t('app', 'Idliquidacion Vino Repartidor'),
            'nombre' => Yii::t('app', 'Periodo'),
            'periodo_inicio' => Yii::t('app', 'Desde'),
            'periodo_fin' => Yii::t('app', 'Hasta'),
            'total' => Yii::t('app', 'TOTAL'),
            'repartidor_idrepartidor' => Yii::t('app', 'Repartidor'),
            'estadoliquidacion_idestadoliquidacion' => Yii::t('app', 'Estado liquidacion'),
            'creado' => Yii::t('app', 'Creado'),
            'liquidacionunica' => Yii::t('app', 'Liquidación única'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoliquidacionIdestadoliquidacion()
    {
        return $this->hasOne(EstadoLiquidacion::className(), ['idestado_liquidacion' => 'estadoliquidacion_idestadoliquidacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepartidorIdrepartidor()
    {
        return $this->hasOne(Repartidor::className(), ['idrepartidor' => 'repartidor_idrepartidor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenGrupalVinos()
    {
        return $this->hasMany(OrdenGrupalVinos::className(), ['liquidacionvinorepartidor_idliquidacionvinorepartidor' => 'idliquidacion_vino_repartidor']);
    }

    public static function createLiquidacion($model, $liquidacionUnica){
        if ($model) {
            //Obtengo todos las ordenes grupales vinos comprendidad entre periodo inicio y periodo fin sin liquidar del cliente
            $ordengrupalvino = OrdenGrupalVinos::getOrdenVinosRepartidorByFecha($model->repartidor_idrepartidor, $model->periodo_inicio, $model->periodo_fin);
            $model->total = 0;
            if (isset($ordengrupalvino)) {
                foreach( $ordengrupalvino as $ogv) {
                    $cliente = Cliente::find()->where(['idcliente' => $ogv->cliente_idcliente])->one();
                    if(isset($cliente->tarifariovino_idtarifariovino)){
                        $tv = TarifarioVino::find()->where(['idtarifario_vino' => $cliente->tarifariovino_idtarifariovino])->one();
                        if(isset($tv)){
                            //Obtengo todos los remitos asociados a las ordenes grupales sin liquidar
                            $remitos = RemitoVino::find()->where(['ordengrupalvinos_idordengrupalvinos' => $ogv->idorden_grupal_vinos])->all();
                            if (isset($remitos)) {
                                foreach ($remitos as $r) {
                                    if(!isset($r->valor_caja) || !isset($r->precio_reparto) || !isset($r->total_facturacion))
                                    {
                                        if(!isset($r->valor_caja))
                                            $r->valor_caja = $tv->precio_caja;
                                        if(!isset($r->precio_reparto))
                                            $r->precio_reparto = $tv->precio_repartidor;
                                        if(!isset($r->total_facturacion))
                                            $r->total_facturacion = (($r->cant_cajas * $tv->precio_caja) + (($r->valor_declarado / 2) * 0.01));
                                    }
                                    $model->total += ($r->cant_cajas * $tv->precio_repartidor);
                                    $r->save();
                                }
                            }
                        }else{
                            $mensaje = "El cliente: ".$model->clienteIdcliente->razonsocial. " no es un cliente de tipo vino o no tiene tarifario de vino asociado!";
                            Yii::$app->session->setflash(
                                'error',
                                "El cliente: ".$model->clienteIdcliente->razonsocial).
                                " no es un cliente de tipo vino o no tiene tarifario de vino asociado!";
                            return ["error", $ogv->idorden_grupal_vinos, $mensaje];
                        }
                        $model->creado = date('Y-m-d G:i', time());
                        $model->liquidacionunica = $liquidacionUnica;
                        $model->save(false);
                        $ogv->liquidacionvinorepartidor_idliquidacionvinorepartidor = $model->idliquidacion_vino_repartidor;
                        $ogv->save(false);
                    }else{
                        Yii::$app->session->setflash(
                            'error',
                            "El cliente: ".$cliente->razonsocial."no es de tipo vino o no tiene asociado un tarifario vino.");
                            return "error";
                    }
                }
            }else{
                return false;
            }
            return $model; 
        }
    }

    public static function deleteLiquidacion($liquidacionUnica){
        $model = self::find()->where(['periodo_inicio' => $liquidacionUnica->fecha_inicio])
        ->andWhere(['periodo_fin' => $liquidacionUnica->fecha_fin])
        ->andWhere(['nombre' => $liquidacionUnica->nombre])
        ->andWhere(['repartidor_idrepartidor' => $liquidacionUnica->repartidor_idrepartidor])->one();

        $ordenes = OrdenGrupalVinos::find()->where(['liquidacionvinorepartidor_idliquidacionvinorepartidor' => $model->idliquidacion_vino_repartidor])->all();

        if($ordenes){
            foreach($ordenes as $o){
                $o->liquidacionvinorepartidor_idliquidacionvinorepartidor = null;
                $o->save();
            }
        }
        if($model){
            $model->delete();
            return 0;
        }else{
            return 1;
        }
    }

    public static function getAllFromUnica($idUnica){
        return self::find()->select('idliquidacion_vino_repartidor')->where(['liquidacionunica' => $idUnica])->one();
    }
}
