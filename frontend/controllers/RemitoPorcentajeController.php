<?php

namespace frontend\controllers;

use Yii;
use common\models\RemitoPorcentaje;
use frontend\models\RemitoPorcentajeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RemitoPorcentajeController implements the CRUD actions for RemitoPorcentaje model.
 */
class RemitoPorcentajeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RemitoPorcentaje models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RemitoPorcentajeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RemitoPorcentaje model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RemitoPorcentaje model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RemitoPorcentaje();

        if ($model->load(Yii::$app->request->post())) {
            $model->cant_entregados = $model->cant_pedidos - $model->noquiso_nopidio - $model->cant_anulado - $model->cant_sin_dinero - $model->cant_regresado -$model->cant_faltante - $model->cant_mal_armado - $model->cant_duplicado - $model->otro;
            $model->dinero_rendido = ($model->dinero_facturado - $model->dinero_devuelto);
            $model->porcentaje = $model->porcentajeIdporcentaje->cantidad;
            if($model->pallets != 0){
                $model->porcentaje = 0;
            }
            $model->creado = date('Y-m-d G:i', time());
            $model->save();
            Yii::$app->session->setflash('success', "Remito porcentaje creado con éxito.");
            return $this->redirect(['view', 'id' => $model->idremito_porcentaje]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RemitoPorcentaje model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if(!($model->liquidado_cliente) && !($model->liquidado_repartidor)){
            if ($model->load(Yii::$app->request->post())) {
                $model->cant_entregados = $model->cant_pedidos - $model->noquiso_nopidio - $model->cant_anulado - $model->cant_sin_dinero - $model->cant_regresado -$model->cant_faltante - $model->cant_mal_armado - $model->cant_duplicado - $model->otro;
                $model->dinero_rendido = ($model->dinero_facturado - $model->dinero_devuelto);
                $model->porcentaje = $model->porcentajeIdporcentaje->cantidad;
                if($model->pallets != 0){
                    $model->porcentaje = 0;
                }
                $model->creado = date('Y-m-d G:i', time());
                $model->save();
                return $this->redirect(['view', 'id' => $model->idremito_porcentaje]);
            }
        }else{
            Yii::$app->session->setFlash('error', 'No puede editarse por estar liquidado.');
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RemitoPorcentaje model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if(!($model->liquidado_cliente) && !($model->liquidado_repartidor)){
            $model->delete();
            return $this->redirect(['index']);
        }else{
            Yii::$app->session->setFlash('error', 'No puede eliminarse por estar liquidado.');
            return $this->redirect(Yii::$app->request->referrer);
        }
    }

    /**
     * Finds the RemitoPorcentaje model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RemitoPorcentaje the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RemitoPorcentaje::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
