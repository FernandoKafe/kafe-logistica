<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PagosPorcentajesRepartidor */

$this->title = Yii::t('app', 'Update Pagos Porcentajes Repartidor: ' . $model->idpagos_vinos_repartidor, [
    'nameAttribute' => '' . $model->idpagos_vinos_repartidor,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pagos Porcentajes Repartidors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idpagos_vinos_repartidor, 'url' => ['view', 'id' => $model->idpagos_vinos_repartidor]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<<h1><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario de modificación') ?></h2>
        <div class="clearfix"></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div> 