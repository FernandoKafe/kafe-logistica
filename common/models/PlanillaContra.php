<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%planilla_contra}}".
 *
 * @property int $idplanilla_contra
 * @property double $total
 * @property int $cliente_idcliente
 *
 * @property Cliente $clienteIdcliente
 * @property Remito[] $remitos
 */
class PlanillaContra extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%planilla_contra}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['total'], 'number'],
            [['cliente_idcliente'], 'required'],
            [['cliente_idcliente'], 'integer'],
            [['cliente_idcliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['cliente_idcliente' => 'idcliente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idplanilla_contra' => Yii::t('app', 'Idplanilla Contra'),
            'total' => Yii::t('app', 'Total'),
            'cliente_idcliente' => Yii::t('app', 'Cliente Idcliente'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClienteIdcliente()
    {
        return $this->hasOne(Cliente::className(), ['idcliente' => 'cliente_idcliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemitos()
    {
        return $this->hasMany(Remito::className(), ['planillacontra_idplanillacontra' => 'cliente_idcliente']);
    }
}
