<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\OrdenGrupalDomicilioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Orden Grupal Domicilios');
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><i class="fa fa-copy"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2>Listado</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a(Yii::t('app', 'Crear Orden Grupal Domicilio'), ['create'], ['class' => 'btn btn-success']) ?>
        </p> 

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'moduleId' => 'gridviewk',
            'toolbar' =>  [
                '{export}',
                '{toggleData}', 
            ],
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
            ],
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],

                //'idorden_grupal_domicilio',
                'fecha',
                'numero_orden',
                [
                    'attribute' => 'cliente_idcliente',
                    'value' => function($data){
                        return $data->clienteIdcliente->razonsocial;
                    }
                ],
                [
                    'label' => 'Cliente Tipo',
                    'value' => function($data){
                        return $data->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo;
                    },
                    'filter' => [1 => "Tipo A", 2 => "Tipo B"],
                ],
                [
                    'attribute' => 'repartidor_idrepartidor',
                    'value' => function($data){
                        return $data->repartidorIdrepartidor->apellido.", ".$data->repartidorIdrepartidor->nombre;
                    }
                ],
                [
                    'attribute'=>'liquidacioncliente_idliquidacioncliente',
                    'label' => 'Liquidado Cliente',
                    'content' => function($data){
                        if($data->liquidacioncliente_idliquidacioncliente != null){
                            return "Sí";
                        }else{
                            return "No";
                        }
                    },
                    'filter' => [1 => "No", 2 => "Sí"],
                ],
                [
                    'attribute'=>'liquidacionrepartidor_idliquidacionrepartidor',
                    'label' => 'Liquidado Repartidor',
                    'content' => function($data){
                        if($data->liquidacionrepartidor_idliquidacionrepartidor != null){
                            return "Sí";
                        }else{
                            return "No";
    
                        }
                    },
                    'filter' => [1 => "No", 2 => "Sí"],
                ],
                [
                    'attribute' => 'creado',
                    'label' => 'Fecha y Hora creación',
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div> 
</div>