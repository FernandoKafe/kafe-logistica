<?php

use \yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\db\Query;
use frontend\models\LiquidacionHasRemitoSearch;
use common\models\LiquidacionHasRemito;
use common\models\Remito;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;
use common\models\TarifarioHasPrecio;
use common\models\Liquidacion;
use common\models\OrdenGrupalDomicilio;
use common\models\GuiaDomicilio;
use common\models\LiquidacionDomicilioCliente;

/* @var $this yii\web\View */
/* @var $model common\models\Rendicion */

$this->title = $model->idrendicion;
$tipo = Yii::$app->getRequest()->getQueryParam('tipo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rendiciones'), 'url' => ['index?tipo='.$tipo]];
$this->params['breadcrumbs'][] = $this->title;

$id = Yii::$app->getRequest()->getQueryParam('id');

?>
<div class="rendicion-view">

    <h1>Rendición: <?= $model->clienteIdcliente->razonsocial?></h1>
    <h1>Desde: <?= $model->fecha_inicio?></h1>
    <h1>Hasta: <?= $model->fecha_fin?></h1>
    <h1>CUIT: <?=$model->clienteIdcliente->cuit?></h1>
    <div class="x_panel">
        <div class="x_title">
            <div class="clearfix"></div>
        </div>
        <p>
            <?= Html::a(Yii::t('app', 'PDF'), ['report', 'id' => $model->idrendicion, 'tipo' => $tipo],['class' => 'btn btn-success']) ?>
            <!--<?//= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->idrendicion, 'tipo' => $tipo], ['class' => 'btn btn-primary']) ?>-->
            <?= Html::a(Yii::t('app', 'Eliminar'), ['delete', 'id' => $model->idrendicion, 'tipo' => $tipo], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
            ]) 
            ?>
        </p>
    </div>
</div>

<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_title">
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php
    if($tipo == 0)
    {
        
    $dataProvider->setSort([
        'attributes' => [
            'remitofecha' => [
                'asc' => ['remitofecha' => SORT_ASC],
                'desc' => ['remitofecha' => SORT_DESC],
                'default' => SORT_ASC
            ],
        ],
        'defaultOrder' => [
            'remitofecha' => SORT_ASC
        ]
    ]);
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    'moduleId' => 'gridviewk',
    'toolbar' =>  [
        '{export}',
        '{toggleData}',
    ],
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
    ],
    'showFooter' => true,
    'columns' => [
        [
            'attribute' => 'remitofecha',
            'label' => 'Fecha',
        ],
        [
            'attribute' => 'numero_remito',
            'label' => 'Remito',
            'value' => function($data){
                if($data->numero_remito){
                    return $data->numero_remito;
                }else{
                    return $data->ordenderetirogrupalIdordenderetirogrupal->numero;
                }
            }
        ],
        [
            'attribute' => 'numero_guia',
            'label' => 'Guía',
            'value' => function($data){
                return $data->numero_guia.' - '.$data->tipo_remito;
            }
        ],
        [
            'label' => 'Destinatario',
            'value' => function($data){
                if($data->rol_sucursal == "Remite"){
                    return $data->clienteIdcliente->razonsocial;
                }else{
                    return $data->sucursalIdsucursal->nombre.", ".$data->sucursalIdsucursal->empresaIdempresa->nombre;
                }
            },
        ],
        [
            'attribute' => 'sucursalIdsucursal',
            'label' => 'Destino',
            'value' => function($data){
                if(isset($data->sucursalIdsucursal->localidad_idlocalidad)){
                    return $data->sucursalIdsucursal->localidadIdlocalidad->nombre.", ".
                    $data->sucursalIdsucursal->localidadIdlocalidad->departamentoIddepartamento->nombre.", ".
                    $data->sucursalIdsucursal->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                }else{
                    return "SIN LOCALIDAD ASIGNADA";
                }
            }
        ],
        [
            'attribute' => 'empresatransporte_idempresatransporte',
            'label' => 'Empresa de transporte',
            'value' => function($data){
                if(isset($data->empresatransporteIdempresatransporte->razonsocial)){
                    return $data->empresatransporteIdempresatransporte->razonsocial;
                }else{
                    return "SIN NOMBRE ASIGNADO";
                }
            },
            'footer' => '<strong>TOTAL</strong>',
        ],
        [
            'attribute' => 'cantidad_bultos',
            'label' => 'Cantidad bultos',
            'value' => function($data){
                return $data->cantidad_bultos;
            },
            'footer' => $bultosSum,
        ],
        [
            'attribute' => 'volumen',
            'label' => 'Cantidad bultos excedentes',
            'value' => function($data){
                return $data->volumen;
            },
            'footer' => $volumenesSum,
        ],
        [
            'attribute' => 'total_contrarembolso',
            'footer' => $contraSum,
        ]
    ],
    ]); 
    $totalRendido = number_format ($model->monto, 2 , "." ,  "," );
    echo "<div class='col-md-12'><h2><strong>TOTAL: ".$totalRendido."</strong></h2>";
    }
    else if($tipo == 1)
    {
        ;;
        
        if($model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo B"){
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'moduleId' => 'gridviewk',
                'toolbar' =>  [
                    '{export}',
                    '{toggleData}',
                ],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                ],
                'options' => ['style' => 'font-size:10px;'],
                'showFooter' => true,
                'columns' => [
                    [
                        'label' => 'Fecha',
                        'value' => function($data){
                            return $data->ordengrupaldomicilioIdordengrupaldomicilio->fecha;
                        },
                    ],
                    [
                        'label' => 'Nº Orden Grupal',
                        'value' => function($data){
                            return $data->ordengrupaldomicilioIdordengrupaldomicilio->numero_orden;
                        },
                    ],
                    [
                        'attribute' => 'empresa_destino',
                        'label' => 'Empresa destino',
                    ],
                    [
                        'attribute' => 'localidad_idlocalidad',
                        'label' => 'Localidad destino',
                        'value' => function($data){
                            if($data->localidad_idlocalidad){
                                return $data->localidadIdlocalidad->nombre.
                                ", ".
                                $data->localidadIdlocalidad->departamentoIddepartamento->nombre.
                                ", ".
                                $data->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                            }else{
                                return "Guía sin localidad";
                            }
                        },
                    ],
                    [
                        'attribute' => 'numero_guia',
                        'value' => function($data){
                            return $data->numero_guia;
                        },
                        'footer' => '<strong>TOTAL</strong>',
                    ],
                    [
                        'label' => 'Repartidor',
                        'value' => function($data){
                            return $data->ordengrupaldomicilioIdordengrupaldomicilio->repartidorIdrepartidor->apellido.
                            ", ".
                            $data->ordengrupaldomicilioIdordengrupaldomicilio->repartidorIdrepartidor->nombre;
                        }
                    ],
                    [
                        'attribute' => 'cantidad',
                        'footer' => $sumaBultos,
                    ],  
                    [
                        'attribute' => 'monto',
                        'label' => 'Rendición común',
                        'value' => function($data){
                            /*$remitosOrden = GuiaDomicilio::find()
                                            ->where(['ordengrupaldomicilio_idordengrupaldomicilio' => $data->ordengrupaldomicilio_idordengrupaldomicilio])
                                            ->orderBy(['sprinter' => SORT_DESC])->all();
                            if($remitosOrden[0]['sprinter'] != 1)*/
                            if($data->sprinter != 1)
                                return $data->monto;
                        },
                        'format' => ['decimal', 2],
                        'footer' => number_format ($rendicionComun, 2 , "." ,  "," )
                    ],
                    [
                        'attribute' => 'monto',
                        'label' => 'Rendición sprinter',
                        'value' => function($data){
                            /*$remitosOrden = GuiaDomicilio::find()
                                            ->where(['ordengrupaldomicilio_idordengrupaldomicilio' => $data->ordengrupaldomicilio_idordengrupaldomicilio])
                                            ->orderBy(['sprinter' => SORT_DESC])->all();
                            if($remitosOrden[0]['sprinter'] == 1) */
                            if($data->sprinter == 1)
                                return $data->monto;
                        },
                        'format' => ['decimal', 2],
                        'footer' => number_format($rendicionSprinter, 2 , "." ,  "," )
                    ],
                ],
                ]); 
        }
        echo "<div class='col-md-12'><h2><strong>TOTAL: ".number_format($model->monto, 2 , "." ,  "," )."</strong></h2>";
    }
    ?>