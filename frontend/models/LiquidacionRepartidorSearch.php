<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider; 
use common\models\LiquidacionRepartidor;

/**
 * LiquidacionRepartidorSearch represents the model behind the search form of `common\models\LiquidacionRepartidor`.
 */
class LiquidacionRepartidorSearch extends LiquidacionRepartidor
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idliquidacion_repartidor', 'estadoliquidacion_idestadoliquidacion', 'liquidacionunica'], 'integer'],
            [['total'], 'number'],
            [['periodo', 'periodo_inicio', 'periodo_fin', 'repartidor_idrepartidor', 'creado'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LiquidacionRepartidor::find();
        $query->joinWith(['repartidorIdrepartidor as repartidor']);
        $query->joinWith(['estadoliquidacionIdestadoliquidacion as estado']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idliquidacion_repartidor' => $this->idliquidacion_repartidor,
            'total' => $this->total,
            'periodo_inicio' => $this->periodo_inicio,
            'periodo_fin' => $this->periodo_fin,
            'creado' => $this->creado,
            'liquidacionunica' => $this->liquidacionunica,
        ]);

        $query->andFilterWhere(['like', 'periodo', $this->periodo]);
        $query->andFilterWhere(['like', 'CONCAT(repartidor.apellido, ", ", repartidor.nombre)', $this->repartidor_idrepartidor]);
        $query->andFilterWhere(['like', 'estado.nombre', $this->estadoliquidacion_idestadoliquidacion]);

        return $dataProvider;
    }
}
