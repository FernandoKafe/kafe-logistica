<?php
$fileName = "AÑO-MES-CLIENTE|".$model->clienteIdcliente->razonsocial."_".$model->periodo_inicio;
$fileName = str_replace(' ', '-', $fileName);
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=".$fileName.".xls");
header("Pragma: no-cache");
header("Expires: 0");
setlocale(LC_TIME, 'es_ES.UTF-8');
if($post){
    $longitud = count($post);
    //print_r($post);
}
if($postDomicilio){
    $longitudDomicilio = count($postDomicilio);
    //print_r($postDomicilio);
}
if($postPorcentaje){
    $longitudPorcentaje = count($postPorcentaje);
    //print_r($postPorcentaje);
}
if($postVino){
    $longitudVino = count($postVino);
    //print_r($postVino);
}

$currentMonth = getdate();
?>
<table xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">  
    <caption style='border: solid 1px; text-align: center'><?= $model->clienteIdcliente->razonsocial."-".$model->periodo_inicio?></caption>
    <?php if($post){?>
        <?= $this->render('_table_comun',['post' => $post])?>
        <?php }else if($postDomicilio){?> <!--Foreach $post-->
            <tr>
                <td  style='border: solid 1px; text-align: center; font-weight: bold;'>MES</td>
                <td  style='border: solid 1px; text-align: center; font-weight: bold;'>CANTIDAD</td>
                <td  style='border: solid 1px; text-align: center; font-weight: bold;'>MONTO</td>
            </tr>
            <?php foreach($postDomicilio as $elemento){ ?>
            <tr>
                <?php foreach($elemento as $subelemento){ 
                    if(!($currentMonth[mon] == $elemento[fecha] && $currentMonth[year] == $model->periodo_inicio)){?>
                    <td  style='border: solid 1px; text-align: center'>
                    <?php }   if(key($elemento) == "fecha" && !($currentMonth[mon] == $elemento[fecha] && $currentMonth[year] == $model->periodo_inicio)){
                                $subelemento = '0000/'.($subelemento + 1).'/00';
                                //echo date("F", mktime(0, 0, 0, $subelemento, 10));
                                $mes = strftime("%B", strtotime($subelemento));
                                $mes = strtoupper($mes);
                                echo $mes;
                            }else if(!($currentMonth[mon] == $elemento[fecha] && $currentMonth[year] == $model->periodo_inicio)){
                                if(key($elemento) == "cantidad"){
                                    $cantidad += $subelemento;
                                }
                                if(key($elemento) == "monto"){
                                    $monto += $subelemento;
                                }
                                echo $subelemento;
                            }
                            next($elemento);
                        if(!($currentMonth[mon] == $elemento[fecha] && $currentMonth[year] == $model->periodo_inicio)){?>
                </td>
            <?php }} ?><!--Foreach $elemento-->
            </tr>
        <?php } ?>
            <tr>
                <td  style='border: solid 1px; text-align: center'><strong>TOTAL</strong></td>
                <td  style='border: solid 1px; text-align: center'><strong><?= $cantidad ?></strong></td>
                <td  style='border: solid 1px; text-align: center'><strong>$<?= $monto ?></strong></td>
            </tr>
        <?php }else if($postPorcentaje){?> <!--Foreach $post-->
            <tr>
                <td  style='border: solid 1px; text-align: center; font-weight: bold;'>MES</td>
                <td  style='border: solid 1px; text-align: center; font-weight: bold;'>PEDIDOS</td>
                <td  style='border: solid 1px; text-align: center; font-weight: bold;'>ENTREGADOS</td>
            </tr>
            <?php foreach($postPorcentaje as $elemento){ ?>
            <tr>
                <?php foreach($elemento as $subelemento){ 
                    if(!($currentMonth[mon] == $elemento[fecha] && $currentMonth[year] == $model->periodo_inicio)){?>
                    <td  style='border: solid 1px; text-align: center'>
                    <?php }  if(key($elemento) == "fecha" && !($currentMonth[mon] == $elemento[fecha] && $currentMonth[year] == $model->periodo_inicio)){
                                $subelemento = '0000/'.($subelemento + 1).'/00';
                                //echo date("F", mktime(0, 0, 0, $subelemento, 10));
                                $mes = strftime("%B", strtotime($subelemento));
                                $mes = strtoupper($mes);
                                echo $mes;
                            }else if(!($currentMonth[mon] == $elemento[fecha] && $currentMonth[year] == $model->periodo_inicio)){
                                if(key($elemento) == "pedidos"){
                                    $pedidos += $subelemento;
                                }
                                if(key($elemento) == "entregados"){
                                    $entregados += $subelemento;
                                }
                                echo $subelemento;
                            }
                            next($elemento);
                            if(!($currentMonth[mon] == $elemento[fecha] && $currentMonth[year] == $model->periodo_inicio)){?>
                    </td>
                <?php }} ?><!--Foreach $elemento-->
            </tr>
        <?php } ?>
            <tr>
                <td  style='border: solid 1px; text-align: center'><strong>TOTAL</strong></td>
                <td  style='border: solid 1px; text-align: center'><strong><?= $pedidos ?></strong></td>
                <td  style='border: solid 1px; text-align: center'><strong><?= $entregados ?></strong></td>
            </tr>
        <?php }else if($postVino){?> <!--Foreach $post-->
            <tr>
                <td  style='border: solid 1px; text-align: center; font-weight: bold;'>MES</td>
                <td  style='border: solid 1px; text-align: center; font-weight: bold;'>CAJAS</td>
            </tr>
            <?php foreach($postVino as $elemento){ ?>
            <tr>
                <?php foreach($elemento as $subelemento){ 
                    if(!($currentMonth[mon] == $elemento[fecha] && $currentMonth[year] == $model->periodo_inicio)){?>
                    <td  style='border: solid 1px; text-align: center'>
                    <?php }   if(key($elemento) == "fecha" && !($currentMonth[mon] == $elemento[fecha] && $currentMonth[year] == $model->periodo_inicio)){
                                $subelemento = '0000/'.($subelemento + 1).'/00';
                                //echo date("F", mktime(0, 0, 0, $subelemento, 10));
                                $mes = strftime("%B", strtotime($subelemento));
                                $mes = strtoupper($mes);
                                echo $mes;
                            }else if(!($currentMonth[mon] == $elemento[fecha] && $currentMonth[year] == $model->periodo_inicio)){
                                if(key($elemento) == "cant_cajas"){
                                    $cajas += $subelemento;
                                }
                                echo $subelemento;
                            }
                            next($elemento);
                            if(!($currentMonth[mon] == $elemento[fecha] && $currentMonth[year] == $model->periodo_inicio)){?>
                    </td>
                <?php }} ?><!--Foreach $elemento-->
            </tr>
        <?php } ?>
            <tr>
                <td  style='border: solid 1px; text-align: center'><strong>TOTAL</strong></td>
                <td  style='border: solid 1px; text-align: center'><strong><?= $cajas ?></strong></td>
            </tr>
    <?php } ?>       
</table>