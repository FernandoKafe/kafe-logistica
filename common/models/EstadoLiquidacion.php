<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%estado_liquidacion}}".
 *
 * @property int $idestado_liquidacion
 * @property string $nombre
 *
 * @property Liquidacion[] $liquidacions
 */
class EstadoLiquidacion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%estado_liquidacion}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idestado_liquidacion' => Yii::t('app', 'Idestado Liquidacion'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacions()
    {
        return $this->hasMany(Liquidacion::className(), ['estadoliquidacion_idestadoliquidacion' => 'idestado_liquidacion']);
    }
}
