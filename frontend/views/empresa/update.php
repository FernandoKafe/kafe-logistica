<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Empresa */

$this->title = 'Actualizar Empresa: ' . $model->nombre;
$this->params['breadcrumbs'][] = ['label' => 'Empresas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idempresa, 'url' => ['view', 'id' => $model->idempresa]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<h1><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario de modificación') ?></h2>
        <div class="clearfix"></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
        'sucursal' => $sucursal,
    ]) ?>

</div>
</div>
