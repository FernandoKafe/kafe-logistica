<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\EmpresaTransporte;

/* @var $this yii\web\View */
/* @var $model common\models\EmpresaTransporte */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="empresa-transporte-form">

    <?php $form = ActiveForm::begin(); ?>

      <?= $form->field($model, 'razonsocial')->widget(\yii\jui\AutoComplete::classname(), [
          'clientOptions' => [
              'source' => ArrayHelper::getColumn(EmpresaTransporte::find()->orderBy(['razonsocial'=>SORT_ASC])->all(),'razonsocial'),
          ],
          'options'=>['class' => 'form-control']
      ]) ?>

    <?= $form->field($model, 'cuit')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);?>
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
