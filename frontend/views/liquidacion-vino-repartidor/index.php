<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\LiquidacionVinoRepartidorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Liquidacion Vino Repartidors');
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><i class="fa fa-file"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2>Listado</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

        <!--<p>
            <?//= Html::a(Yii::t('app', 'Crear Liquidación Vino Repartidor'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>-->

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'moduleId' => 'gridviewk',
            'toolbar' =>  [
                '{export}',
                '{toggleData}',
            ],
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
            ],
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'idliquidacion_vino_repartidor',
                [
                    'attribute' => 'repartidor_idrepartidor',
                    'value' => function($data){
                        return $data->repartidorIdrepartidor->apellido.", ".$data->repartidorIdrepartidor->nombre;
                    }
                ],
                [
                    'attribute' => 'estadoliquidacion_idestadoliquidacion',
                    'value' => function($data){
                        return $data->estadoliquidacionIdestadoliquidacion->nombre;
                    }
                ],
                'nombre',
                'periodo_inicio',
                'periodo_fin',
                [
                    'attribute' => 'total',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'creado',
                    'label' => 'Fecha y Hora creación',
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
    </div>
</div>
