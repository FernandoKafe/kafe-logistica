<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Vehiculo */

$this->title = Yii::t('app', 'Actualizar Vehículo: ' . $model->idvehiculo, [
    'nameAttribute' => '' . $model->idvehiculo,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vehículos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idvehiculo, 'url' => ['view', 'id' => $model->idvehiculo]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<h1><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario de modificación') ?></h2>
        <div class="clearfix"></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
