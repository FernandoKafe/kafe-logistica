<?php

$fileName = "CLIENTE-ZONA|".$model->clienteIdcliente->razonsocial."_".$model->periodo_inicio;
$fileName = str_replace(' ', '-', $fileName);
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=".$fileName.".xls");
header("Pragma: no-cache");
header("Expires: 0");
setlocale(LC_TIME, 'es_ES.UTF-8');
if($post){
    $postLength = count($post);
    //echo '<pre>' , print_r($post) , '</pre>';
}
if($postDomicilio){
    $longitudDomicilio = count($postDomicilio);
    //print_r($postDomicilio);
}
if($postPorcentaje){
    $longitudPorcentaje = count($postPorcentaje);
    //print_r($postPorcentaje);
}
if($postVino){
    $longitudVino = count($postVino);
    //print_r($postVino);
}
function in_multy_array($array, $key, $val) {
    foreach ($array as $item)
        if (isset($item[$key]) && $item[$key] == $val)
            return true;
    return false;
}
?>
<table xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">  
    <caption style='border: solid 1px; font-weight: bold'>Cliente - Zona <?= $model->clienteIdcliente->razonsocial."- ".$model->periodo_inicio?></caption>


    <?php if($post){
            $contador= 0;
            $columnas = 0;
            $meses = [];
            ?>    
            <tr>
                <td height="100"></td>
            </tr>
            <tr>
                <td style='border: solid 1px; font-weight: bold'>CLIENTES COMUNES</td>
            </tr>
            <tr>
                <td style='border: solid 1px'></td>
                <?php
                $zonas = [];
                $totalColumns = 0; 
                $clientes = [];
                $filasClientes = 0;
                foreach($post as $elemento){ 
                    foreach($elemento as $subelemento){
                        if(key($elemento) == "nombre"){
                            if(!in_multy_array($zonas, "nombre", $elemento[nombre])){
                                $zonas[$totalColumns][nombre] = $elemento[nombre];
                                $zonas[$totalColumns][idzona] = $elemento[zona_localidad];
                                $totalColumns++;
                            }
                        }
                        if(key($elemento) == "cliente"){
                            if(!in_array($elemento[cliente], $clientes)){
                                $clientes[$filasClientes] = $elemento[cliente];
                                $filasClientes++;
                            }
                        }
                        next($elemento);
                    }
                }
                foreach ($zonas as $key => $row)
                {
                    $idzona[$key] = $row['idzona'];
                }
                array_multisort($idzona, SORT_ASC, $zonas);
                foreach($zonas as $z){?>
                    <td colspan ="2 "style='border: solid 1px; text-align: center'><strong><?= $z[nombre] ?></strong></td>
                <?php 
                }
                ?>
                </tr>
                <tr>
                    <td style='border: solid 1px; font-weight: bold'>Cliente</td>
                    <?php for($i = 0; $i < $totalColumns; $i++){ ?>
                        <td style='border: solid 1px; font-weight: bold'>BULTOS</td>
                        <td style='border: solid 1px; font-weight: bold'>EXCEDENTES</td>
                    <?php } ?>
            </tr>
            
            



            <?php  foreach($clientes as $cliente){ ?>
                <tr> 
                    <td style='border: solid 1px; font-weight: bold'><?= $cliente?></td>  
                <?php $currentColumn = 0;
                //Para cada cliente recorro todo el array post
                for($checkElemento = 0; $checkElemento <= $postLength; $checkElemento++){
                    
                    //Si el cliente del actual elemento de post es igual al cliente en curso, entra en el if
                    if($post[$checkElemento][cliente] == $cliente){
                        //si el nombre de la zona de zonas es distinto al nombre de la zona del elemento actual
                        //entra al if.
                        if(($zonas[$currentColumn][nombre] != $post[$checkElemento][nombre])){
                            $emptyCols = $totalColumns - $currentColumn;
                            while($emptyCols){
                                echo "<td style='border: solid 1px; text-align: center'>0</td>";
                                echo "<td style='border: solid 1px; text-align: center'>0</td>";
                                $emptyCols--;
                                $currentColumn++;
                                if($zonas[$currentColumn][nombre] != $post[$checkElemento][nombre]){
                                    ($emptyCols = true);
                                 } else {
                                     ($emptyCols = false);
                                 }
                            }
                        }
                        if($zonas[$currentColumn][nombre] == $post[$checkElemento][nombre]){ 
                            echo "<td style='border: solid 1px; text-align: center'>".$post[$checkElemento][cantidad_bultos]."</td>";
                            echo "<td style='border: solid 1px; text-align: center'>".$post[$checkElemento][volumen]."</td>";

                            if($post[$checkElemento][cantidad_bultos]){
                                $cantidad_bultos[$zonas[$currentColumn][nombre]] += $post[$checkElemento][cantidad_bultos];
                            }
                            if($post[$checkElemento][volumen]){
                                $volumen[$zonas[$currentColumn][nombre]] += $post[$checkElemento][volumen];
                            }
                        }
                        $currentColumn++;
                    }
                }
                while($currentColumn < $totalColumns){
                    echo "<td style='border: solid 1px; text-align: center'>0</td>";
                    echo "<td style='border: solid 1px; text-align: center'>0</td>";
                    $currentColumn++;
                } ?>
                </tr>
            <?php }
                ?>
            <tr> 
                <td style='border: solid 1px'><strong>TOTAL</strong></td>
                <?php 
                    $i = 0;
                foreach($zonas as $z){ 
                    //echo "cantidad_bultos[\$z[\$i][nombe]: ".$cantidad_bultos[$z]."<br>";
                    ?>
                    <td style='border: solid 1px; text-align: center'><strong><?php 
                    if(isset($cantidad_bultos[$z[nombre]])){
                        echo $cantidad_bultos[$z[nombre]];
                    } else {
                        echo 0;
                    } 
                    ?></strong></td>
                    <td style='border: solid 1px; text-align: center'><strong><?php
                    if(isset($volumen[$z[nombre]])){
                        echo $volumen[$z[nombre]];
                    }else{
                        echo 0;
                    }
                    
                    ?></strong></td>
                    
                <?php  $i++;
                }
                ?>
            </tr>
    <?php } ?>

    <?php if($postDomicilio){
            $contadorD= 0;
            $columnasD = 0;
            $mesesD = [];
            ?>

            <tr>
                <td height="100"></td>
            </tr>
            <tr>
                <td style='border: solid 1px; font-weight: bold'>CLIENTES DOMICILIO</td>
            </tr>
            <tr>
                <td style='border: solid 1px'></td>
                <?php
                $zonasD = [];
                $totalColumnsD = 0; 
                $clientesD = [];
                $filasClientesD = 0;
                foreach($postDomicilio as $elemento){ 
                    foreach($elemento as $subelemento){
                        if(key($elemento) == "nombre"){
                            if(!in_multy_array($zonasD, "nombre", $elemento[nombre])){
                                $zonasD[$totalColumnsD][nombre] = $elemento[nombre];
                                $zonasD[$totalColumnsD][idzona] = $elemento[zona_localidad];
                                $totalColumnsD++;
                            }
                        }
                        if(key($elemento) == "cliente"){
                            if(!in_array($elemento[cliente], $clientesD)){
                                $clientesD[$filasClientesD] = $elemento[cliente];
                                $filasClientesD++;
                            }
                        }
                        next($elemento);
                    }
                }
                foreach ($zonasD as $key => $row)
                {
                    $idzonaD[$key] = $row['idzona'];
                }
                array_multisort($idzonaD, SORT_ASC, $zonasD);
                foreach($zonasD as $z){?>
                    <td colspan ="2 "style='border: solid 1px; text-align: center'><strong><?= $z[nombre] ?></strong></td>
                <?php 
                }
                ?>
                </tr>
                <tr>
                    <td style='border: solid 1px; font-weight: bold'>Cliente</td>
                    <?php for($i = 0; $i < $totalColumnsD; $i++){ ?>
                        <td style='border: solid 1px; font-weight: bold'>BULTOS</td>
                        <td style='border: solid 1px; font-weight: bold'>EXCEDENTES</td>
                    <?php } ?>
            </tr>
            
            <?php  foreach($clientesD as $clienteD){ ?>
                <tr> 
                    <td style='border: solid 1px; font-weight: bold'><?= $clienteD?></td>  
                <?php $currentColumnD = 0;
                //Para cada cliente recorro todo el array postDomicilio
                for($checkElementoD = 0; $checkElementoD <= $longitudDomicilio; $checkElementoD++){
                    
                    //Si el cliente del actual elemento de postDomicilio es igual al cliente en curso, entra en el if
                    if($postDomicilio[$checkElementoD][cliente] == $clienteD){
                        //si el nombre de la zona de zonas es distinto al nombre de la zona del elemento actual
                        //entra al if.
                        if(($zonasD[$currentColumnD][nombre] != $postDomicilio[$checkElementoD][nombre])){
                            $emptyColsD = $totalColumnsD - $currentColumnD;
                            while($emptyColsD){
                                echo "<td style='border: solid 1px; text-align: center'>0</td>";
                                echo "<td style='border: solid 1px; text-align: center'>0</td>";
                                $emptyColsD--;
                                $currentColumnD++;
                                if($zonasD[$currentColumnD][nombre] != $postDomicilio[$checkElementoD][nombre]){
                                    ($emptyColsD = true);
                                 } else {
                                     ($emptyColsD = false);
                                 }
                            }
                        }
                        if($zonasD[$currentColumnD][nombre] == $postDomicilio[$checkElementoD][nombre]){ 
                            echo "<td style='border: solid 1px; text-align: center'>".$postDomicilio[$checkElementoD][cantidad_bultos]."</td>";
                            echo "<td style='border: solid 1px; text-align: center'>".$postDomicilio[$checkElementoD][volumen]."</td>";

                            if($postDomicilio[$checkElementoD][cantidad_bultos]){
                                $cantidad_bultosD[$zonasD[$currentColumnD][nombre]] += $postDomicilio[$checkElementoD][cantidad_bultos];
                            }
                            if($postDomicilio[$checkElementoD][volumen]){
                                $volumenD[$zonasD[$currentColumnD][nombre]] += $postDomicilio[$checkElementoD][volumen];
                            }
                        }
                        $currentColumnD++;
                    }
                }
                while($currentColumnD < $totalColumnsD){
                    echo "<td style='border: solid 1px; text-align: center'>0</td>";
                    echo "<td style='border: solid 1px; text-align: center'>0</td>";
                    $currentColumnD++;
                } ?>
                </tr>
            <?php }
                ?>
            <tr> 
                <td style='border: solid 1px'><strong>TOTAL</strong></td>
                <?php 
                    $iD = 0;
                foreach($zonasD as $z){ 
                    //echo "cantidad_bultos[\$z[\$i][nombe]: ".$cantidad_bultos[$z]."<br>";
                    ?>
                    <td style='border: solid 1px; text-align: center'><strong><?php 
                    if(isset($cantidad_bultosD[$z[nombre]])){
                        echo $cantidad_bultosD[$z[nombre]];
                    } else {
                        echo 0;
                    } 
                    ?></strong></td>
                    <td style='border: solid 1px; text-align: center'><strong><?php
                    if(isset($volumenD[$z[nombre]])){
                        echo $volumenD[$z[nombre]];
                    }else{
                        echo 0;
                    }
                    
                    ?></strong></td>
                    
                <?php  $iD++;
                }
                ?>
            </tr>
    <?php } ?>
    

    <?php if($postVino){
            $contadorV= 0;
            $columnasV = 0;
            $mesesV = [];
            ?>
            <tr>
                <td height="100"></td>
            </tr>
            <tr>
                <td style='border: solid 1px; font-weight: bold'>CLIENTES VINOS</td>
            </tr>
            <tr>
                <td style='border: solid 1px'></td>
                <?php
                $zonasV = [];
                $totalColumnsV = 0; 
                $clientesV = [];
                $filasClientesV = 0;
                foreach($postVino as $elemento){ 
                    foreach($elemento as $subelemento){
                        if(key($elemento) == "nombre"){
                            if(!in_multy_array($zonasV, "nombre", $elemento[nombre])){
                                $zonasV[$totalColumnsV][nombre] = $elemento[nombre];
                                $zonasV[$totalColumnsV][idzona] = $elemento[zona_localidad];
                                $totalColumnsV++;
                            }
                        }
                        if(key($elemento) == "cliente"){
                            if(!in_array($elemento[cliente], $clientesV)){
                                $clientesV[$filasClientesV] = $elemento[cliente];
                                $filasClientesV++;
                            }
                        }
                        next($elemento);
                    }
                }
                foreach ($zonasV as $key => $row)
                {
                    $idzonaV[$key] = $row['idzona'];
                }
                array_multisort($idzonaV, SORT_ASC, $zonasV);
                foreach($zonasV as $z){?>
                    <td colspan ="2 "style='border: solid 1px; text-align: center'><strong><?= $z[nombre] ?></strong></td>
                <?php 
                }
                ?>
                </tr>
                <tr>
                    <td style='border: solid 1px; font-weight: bold'>Cliente</td>
                    <?php for($i = 0; $i < $totalColumnsV; $i++){ ?>
                        <td style='border: solid 1px; font-weight: bold'>CAJAS</td>
                        <td style='border: solid 1px; font-weight: bold'>BOTELLAS</td>
                    <?php } ?>
            </tr>
            
            



            <?php  foreach($clientesV as $clienteV){ ?>
                <tr> 
                    <td style='border: solid 1px; font-weight: bold'><?= $clienteV?></td>  
                <?php $currentColumnV = 0;
                //Para cada cliente recorro todo el array postVino
                for($checkElementoV = 0; $checkElementoV <= $longitudVino; $checkElementoV++){
                    
                    //Si el cliente del actual elemento de postVino es igual al cliente en curso, entra en el if
                    if($postVino[$checkElementoV][cliente] == $clienteV){
                        //si el nombre de la zona de zonas es distinto al nombre de la zona del elemento actual
                        //entra al if.
                        if(($zonasV[$currentColumnV][nombre] != $postVino[$checkElementoV][nombre])){
                            $emptyColsV = $totalColumnsV - $currentColumnV;
                            while($emptyColsV){
                                echo "<td style='border: solid 1px; text-align: center'>0</td>";
                                echo "<td style='border: solid 1px; text-align: center'>0</td>";
                                $emptyColsV--;
                                $currentColumnV++;
                                if($zonasV[$currentColumnV][nombre] != $postVino[$checkElementoV][nombre]){
                                    ($emptyColsV = true);
                                 } else {
                                     ($emptyColsV = false);
                                 }
                            }
                        }
                        if($zonasV[$currentColumnV][nombre] == $postVino[$checkElementoV][nombre]){ 
                            echo "<td style='border: solid 1px; text-align: center'>".$postVino[$checkElementoV][cantidad_bultos]."</td>";
                            echo "<td style='border: solid 1px; text-align: center'>".$postVino[$checkElementoV][volumen]."</td>";

                            if($postVino[$checkElementoV][cantidad_bultos]){
                                $cantidad_bultosV[$zonasV[$currentColumnV][nombre]] += $postVino[$checkElementoV][cantidad_bultos];
                            }
                            if($postVino[$checkElementoV][volumen]){
                                $volumenV[$zonasV[$currentColumnV][nombre]] += $postVino[$checkElementoV][volumen];
                            }
                        }
                        $currentColumnV++;
                    }
                }
                while($currentColumnV < $totalColumnsV){
                    echo "<td style='border: solid 1px; text-align: center'>0</td>";
                    echo "<td style='border: solid 1px; text-align: center'>0</td>";
                    $currentColumnV++;
                } ?>
                </tr>
            <?php }
                ?>
            <tr> 
                <td style='border: solid 1px'><strong>TOTAL</strong></td>
                <?php 
                    $iV = 0;
                foreach($zonasV as $z){ 
                    //echo "cantidad_bultos[\$z[\$i][nombe]: ".$cantidad_bultos[$z]."<br>";
                    ?>
                    <td style='border: solid 1px; text-align: center'><strong><?php 
                    if(isset($cantidad_bultosV[$z[nombre]])){
                        echo $cantidad_bultosV[$z[nombre]];
                    } else {
                        echo 0;
                    } 
                    ?></strong></td>
                    <td style='border: solid 1px; text-align: center'><strong><?php
                    if(isset($volumenV[$z[nombre]])){
                        echo $volumenV[$z[nombre]];
                    }else{
                        echo 0;
                    }
                    
                    ?></strong></td>
                    
                <?php  $iV++;
                }
                ?>
            </tr>
    <?php } ?>
    
</table>