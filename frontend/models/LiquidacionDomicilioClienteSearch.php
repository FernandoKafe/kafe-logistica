<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LiquidacionDomicilioCliente;

/**
 * LiquidacionDomicilioClienteSearch represents the model behind the search form of `common\models\LiquidacionDomicilioCliente`.
 */
class LiquidacionDomicilioClienteSearch extends LiquidacionDomicilioCliente
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idliquidacion_domicilio_cliente', 'estadoliquidacion_idestadoliquidacion', 'liquidacionformapago_idliquidacionformapago', 'cheque_idcheque', 'borrado', 'cantidad_bultos', 'confirmada'], 'integer'],
            [['cliente_idcliente', 'fecha_inicio', 'fecha_fin', 'nombre', 'creado', 'fecha_pago'], 'safe'],
            [['total', 'pago_parcial', 'total_b', 'neto_b', 'total_iva', 'total_ar', 'importe_bultos', 'rendicion_comun', 'rendicion_sprinter', 'reparto'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LiquidacionDomicilioCliente::find()->where(['borrado' => 0])->andWhere(['confirmada' => 1])
        ->joinWith(['clienteIdcliente as cliente']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idliquidacion_domicilio_cliente' => $this->idliquidacion_domicilio_cliente,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_fin' => $this->fecha_fin,
            'total' => $this->total,
            'pago_parcial' => $this->pago_parcial,
            'total_b' => $this->total_b,
            'neto_b' => $this->neto_b,
            'total_iva' => $this->total_iva,
            'total_ar' => $this->total_ar,
            'borrado' => $this->borrado,
            'estadoliquidacion_idestadoliquidacion' => $this->estadoliquidacion_idestadoliquidacion,
            'liquidacionformapago_idliquidacionformapago' => $this->liquidacionformapago_idliquidacionformapago,
            'cheque_idcheque' => $this->cheque_idcheque,
            'cantidad_bultos' => $this->cantidad_bultos,
            'creado' => $this->creado,
            'confirmada' => $this->confirmada,
            'fecha_pago' => $this->fecha_pago,
            'rendicion_comun' => $this->rendicion_comun, 
            'rendicion_sprinter' => $this->rendicion_sprinter, 
            'reparto' => $this->reparto, 
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
        ->andFilterWhere(['like', 'cliente.razonsocial', $this->cliente_idcliente]);

        return $dataProvider;
    }
}
