<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::$app->name . ' - Ingresar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">

    <?php $form = ActiveForm::begin(['id' => 'login-form', 'options'=>['class'=>'form-signin']]); ?>
    </br>
    </br>

    <h3 class="form-signin-heading">
        <?= Html::img('@web/img/logo/LOGO.jpg',['alt' => 'Autogestión GOP', 'class' => 'center-block'])?>
    </h3>
    </br>
    <div class='bootstro' data-bootstro-title="Usuario" data-bootstro-step=1 data-bootstro-content="Si ya estás registrado y activaste tu cuenta desde tu mail, ingresa tu número de documento">
        <label for="documento" class="sr-only">Usuario</label>
        <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder'=>'Nombre de Usuario'])->label(false) ?>
    </div>
    <div class='bootstro' data-bootstro-title="Contraseña" data-bootstro-step=2 data-bootstro-content="A continuación ingresá aquí tu contraseña">
        <label for="password" class="sr-only">Contraseña</label>
        <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Contraseña'])->label(false) ?>
    </div>
    <?= $form->field($model, 'rememberMe')->checkbox() ?>
    <?= Html::submitButton('INGRESAR', ['class' => 'btn btn-lg btn-primary btn-block bootstro', 'name' => 'login-button','data-bootstro-title'=>"Ingresar", 'data-bootstro-step'=>3, 'data-bootstro-content'=>"Por último hacé click en el botón 'INGRESAR'"]) ?>
    <br />
    <p style="text-align:center;">
        <?= Html::a('Recuperá tu contraseña', ['site/request-password-reset'],['data-bootstro-title'=>'Recuperá tu contraseña','class' => 'link-underline bootstro','data-bootstro-step'=>4,'data-bootstro-content'=>'Si ya estás registrado pero no recordás tu contraseña podés recuperarla haciendo click en \'Recuperá tu contraseña\'']) ?>
    </p>
    <?php ActiveForm::end(); ?>
    <!--<div id="help-stick" class="help-stick-close">
        <div class="help-stick-title">Obtené ayuda</div>
        <div class="help-stick-content">
            <button id="iniciar-tutorial" class="btn btn-danger btn-sm">¿CÓMO INGRESAR?</button>
            <br />
            <br />
            Ante cualquier duda, problema o sugerencia sobre el Portal de Obras Privadas puede contactarnos a tráves del siguiente correo: autogestion@lujandecuyo.gob.ar
        </div>
    </div>-->
</div>