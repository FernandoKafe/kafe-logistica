<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Sucursal */

$this->title = Yii::t('app', 'Actualizar Sucursal: ' . $model->idsucursal, [
    'nameAttribute' => '' . $model->idsucursal,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sucursales'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idsucursal, 'url' => ['view', 'id' => $model->idsucursal]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<h1><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario de modificación') ?></h2>
        <div class="clearfix"></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
