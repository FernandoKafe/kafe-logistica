<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

use common\models\OrdenGrupalDomicilio;
use common\models\GuiaDomicilio;
use common\models\TarifarioDomicilio;
use common\models\LiquidacionDomicilioCliente;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionDomicilioCliente */
$idLiquidacion = Yii::$app->getRequest()->getQueryParam('id');

$this->title = $model->clienteIdcliente->razonsocial.": ".$model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion Domicilio Clientes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-view row" style="overflow: hidden;">
    <div class="col-xs-3" style="float: left;">
        <h3>Liquidación: <?= $model->clienteIdcliente->razonsocial?></h3>
        <h3>Periodo: <?= $model->nombre?></h3>
        <h3>CUIT: <?=$model->clienteIdcliente->cuit?></h3>
    </div>
    <div class="col-xs-4" style=" margin: 0 auto 0;">
        <?php echo Html::img('@web/img/logo/LOGO.jpg', ['class' => 'img-responsive', 'width'=>'210', 'style' => 'margin: 0 0;']) ?>
    </div>
    <div class="col-xs-3" style="border: 1px solid #d3d3d3; float:right; margin: 0 15px 0 0;">
            <div class="col-xs-2" style="margin: 20px 0 0 0;">
                <p>Firma: </p>
            </div>
            <div class="col-xs-6" style="margin: 0 0 0 0;">
                <p>________________________________</p>
            </div>
            <div class="col-xs-2">
                <p>Aclaración</p>
            </div>     
            <div class="col-xs-6">
                <p>________________________________</p>
            </div>
            <div class="col-xs-2">
                <p>Fecha</p>
            </div>
            <div class="col-xs-6">
                <p>_______ / _______ / _______</p>
            </div>
            <div class="col-xs-12" style="margin: 35px 0 0 0;">
                    <?= "<div class='col-md-12 tabla-totales'>TOTAL: ".number_format($model->total, 2 , "." ,  "," )."</div>"; ?>
            </div>
        </div>    
</div>


<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_title">
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php
        $ordenesGrupales = OrdenGrupalDomicilio::getAllOrdenesClienteFechaLiquidados($model->cliente_idcliente, $model->fecha_inicio, $model->fecha_fin, $model->idliquidacion_domicilio_cliente);
        $ordenesGrupalesId = ArrayHelper::getColumn($ordenesGrupales,'idorden_grupal_domicilio');
        $remitos = GuiaDomicilio::find()->where(['IN', 'ordengrupaldomicilio_idordengrupaldomicilio', $ordenesGrupalesId]);
        $dataProvider = new ActiveDataProvider([
            'query' => $remitos,
            'pagination' => false,
        ]);

        if($model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo A"){
            echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                [
                    'label' => 'Fecha',
                    'value' => function($data){
                        return $data->ordengrupaldomicilioIdordengrupaldomicilio->fecha;
                    },
                ],
                [
                    'label' => 'Nº Orden Grupal',
                    'value' => function($data){
                        return $data->ordengrupaldomicilioIdordengrupaldomicilio->numero_orden;
                    },
                ],
                [
                    'attribute' => 'empresa_destino',
                    'label' => 'Empresa destino',
                ],
                [
                    'attribute' => 'localidad_idlocalidad',
                    'label' => 'Localidad destino',
                    'value' => function($data){
                        if($data->localidad_idlocalidad){
                            return $data->localidadIdlocalidad->nombre.
                            ", ".
                            $data->localidadIdlocalidad->departamentoIddepartamento->nombre.
                            ", ".
                            $data->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                        }else{
                            return "Guía sin localidad";
                        }
                    },
                ],
                [
                    'attribute' => 'numero_guia',
                    'value' => function($data){
                        return $data->numero_guia."-".$data->guia_tipo;
                    },
                ],
                [
                    'attribute' => 'cantidad',
                ],  
                [
                    'label' => 'Monto A-R-X-Otras',
                    'value' => function($data){
                        if($data->guia_tipo != "B")
                            return $data->monto;
                    },
                    'format' => ['decimal', 2],
                ],
                [
                    'label' => 'Monto B',
                    'value' => function($data){
                        if($data->guia_tipo == "B")
                            return $data->monto;
                    },
                    'format' => ['decimal', 2],
                ],
            ],
            ]);
        }else if($model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo B"){
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    [
                        'label' => 'Fecha',
                        'value' => function($data){
                            return $data->ordengrupaldomicilioIdordengrupaldomicilio->fecha;
                        },
                    ],
                    [
                        'label' => 'Nº Orden Grupal',
                        'value' => function($data){
                            return $data->ordengrupaldomicilioIdordengrupaldomicilio->numero_orden;
                        },
                    ],
                    [
                        'attribute' => 'empresa_destino',
                        'label' => 'Empresa destino',
                    ],
                    [
                        'attribute' => 'localidad_idlocalidad',
                        'label' => 'Localidad destino',
                        'value' => function($data){
                            if($data->localidad_idlocalidad){
                                return $data->localidadIdlocalidad->nombre.
                                ", ".
                                $data->localidadIdlocalidad->departamentoIddepartamento->nombre.
                                ", ".
                                $data->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                            }else{
                                return "Guía sin localidad";
                            }
                        },
                    ],
                    [
                        'attribute' => 'numero_guia',
                        'value' => function($data){
                            return $data->numero_guia;
                        },
                    ],
                    /*[
                        'label' => 'Repartidor',
                        'value' => function($data){
                            return $data->ordengrupaldomicilioIdordengrupaldomicilio->repartidorIdrepartidor->apellido.
                            ", ".
                            $data->ordengrupaldomicilioIdordengrupaldomicilio->repartidorIdrepartidor->nombre;
                        }
                    ],*/
                    [
                        'attribute' => 'cantidad',
                    ],  
                    [
                        'attribute' => 'monto',
                        'label' => 'Rendición común',
                        'value' => function($data){
                            if($data->sprinter == 0)
                                return $data->monto;
                        },
                        'format' => ['decimal', 2],
                    ],
                    [
                        'attribute' => 'monto',
                        'label' => 'Rendición sprinter',
                        'value' => function($data){
                            if(($data->sprinter == 1) && ($data->cobrar_sprinter == 1)) 
                                return $data->monto;
                        },
                        'format' => ['decimal', 2],
                    ],
                    [
                        'label' => 'Reparto',
                        'value' => function($data){
                            $tarifario = TarifarioDomicilio::find()->where(['idtarifario_domicilio' => $data->ordengrupaldomicilioIdordengrupaldomicilio->clienteIdcliente->tarifariodomicilio_idtarifariodomicilio])->one();
                            $remitosOrden = GuiaDomicilio::find()->where(['ordengrupaldomicilio_idordengrupaldomicilio' => $data->ordengrupaldomicilio_idordengrupaldomicilio])->orderBy(['sprinter' => SORT_DESC])->all();
                            $remitosId = ArrayHelper::getColumn($remitosOrden,'idorden_grupal_domicilio');
                            $sprinterArray = ArrayHelper::getColumn($remitosOrden,'sprinter');
                            if($sprinterArray[0] == 1){
                                if($data->idorden_grupal_domicilio == $remitosId[0]){
                                    return $data->ordengrupaldomicilioIdordengrupaldomicilio->precio_sprinter;
                                }else{
                                    return 0;
                                }
                            }else{
                                if(($data->cantidad == 1) && ($data->sprinter == 0)){
                                    return $data->ordengrupaldomicilioIdordengrupaldomicilio->precio_bulto;
                                }else if(($data->cantidad > 1) && ($data->sprinter == 0)){
                                    return ($data->ordengrupaldomicilioIdordengrupaldomicilio->precio_bulto + (($data->cantidad - 1) * $data->ordengrupaldomicilioIdordengrupaldomicilio->precio_bulto_adicional));
                                }else if(($data->sprinter == 1)){
                                    return $data->ordengrupaldomicilioIdordengrupaldomicilio->precio_sprinter;
                                }
                            }
                        },
                        'format' => ['decimal', 2],
                    ],
                ],
                ]);
        }
    ?>



    <?php 
    if($model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo A"){ ?>
        <table style="border:solid 1px Gainsboro; border-collapse: collapse; margin:auto;" width="100%">
            <thead>
                <tr>
                    <th></th>
                    <th style="border:solid 1px Gainsboro; padding: 5px;" align="center">TOTAL BULTOS</th>
                    <th style="border:solid 1px Gainsboro; padding: 5px;" align="center">TOTAL A-R-X Otras</th>
                    <th style="border:solid 1px Gainsboro; padding: 5px;" align="center">TOTAL B</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="border:solid 1px Gainsboro; padding: 5px;" align="center"><strong>TOTALES</strong></td>
                    <td style="border:solid 1px Gainsboro; padding: 5px;" align="center"><?= LiquidacionDomicilioCliente::getBultosSum($remitos->all())?></td>
                    <td style="border:solid 1px Gainsboro; padding: 5px;" align="center"><?=number_format (LiquidacionDomicilioCliente::getMontoTipoOtrasSum($remitos->all()), 2 , "." ,  "," )?></td>
                    <td style="border:solid 1px Gainsboro; padding: 5px;" align="center"><?=number_format (LiquidacionDomicilioCliente::getMontoTipoBSum($remitos->all()), 2 , "." ,  "," )?></td>                </tr>
            </tbody>
        </table>
        <br>
    <?php
        echo DetailView::widget([
            'model' => $model,
            'options' => ['class' => 'table table-striped table-bordered detail-view tabla-totales' ],
            'attributes' => [
                [
                    'attribute' => 'neto_b',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'total_ar',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'total_iva',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'total',
                    'format' => ['decimal',2],
                ],

            ], 
        ]);
    }else if($model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo B"){?>
        <table style="border:solid 1px Gainsboro; border-collapse: collapse; margin:auto" width="100%">
            <thead>
                <tr>
                    <th></th>
                    <th style="border:solid 1px Gainsboro; padding: 5px;" align="center">TOTAL BULTOS</th>
                    <th style="border:solid 1px Gainsboro; padding: 5px;" align="center">TOTAL RENDICIÓN COMÚN</th>
                    <th style="border:solid 1px Gainsboro; padding: 5px;" align="center">TOTAL RENDICIÓN SPRINTER</th>
                    <th style="border:solid 1px Gainsboro; padding: 5px;" align="center">TOTAL REPARTO</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td style="border:solid 1px Gainsboro; padding: 5px;" align="center"><strong>TOTALES</strong></td>
                    <td style="border:solid 1px Gainsboro; padding: 5px;" align="center"><?= $model->cantidad_bultos?></td>
                    <td style="border:solid 1px Gainsboro; padding: 5px;" align="center"><?=number_format ($model->rendicion_comun, 2 , "." ,  "," )?></td>
                    <td style="border:solid 1px Gainsboro; padding: 5px;" align="center"><?=number_format ($model->rendicion_sprinter, 2 , "." ,  "," )?></td>
                    <td style="border:solid 1px Gainsboro; padding: 5px;" align="center"><?=number_format ($model->reparto, 2 , "." ,  "," )?></td>
                </tr>
            </tbody>
        </table>
        <br>
    <?php
        echo DetailView::widget([
            'model' => $model,
            'options' => ['class' => 'table table-striped table-bordered detail-view tabla-totales' ],
            'attributes' => [
                [
                    'attribute' => 'reparto',
                    'label' => 'IMPORTE BULTOS',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'total_iva',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'total',
                    'format' => ['decimal',2],
                ],

            ], 
        ]);
    }
    ?>
    </div>
</div>