<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RepartidorHasVehiculo */

$this->title = Yii::t('app', 'Create Repartidor Has Vehiculo');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Repartidor Has Vehiculos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="repartidor-has-vehiculo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
