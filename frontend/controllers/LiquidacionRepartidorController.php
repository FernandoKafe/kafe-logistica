<?php

namespace frontend\controllers;
use Yii;
use common\models\LiquidacionRepartidor;
use common\models\LiquidacionrepartidorHasRemito;
use common\models\Remito;
use common\models\TarifarioHasPrecio;
use frontend\models\LiquidacionRepartidorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LiquidacionRepartidorController implements the CRUD actions for LiquidacionRepartidor model.
 */
class LiquidacionRepartidorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LiquidacionRepartidor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LiquidacionRepartidorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LiquidacionRepartidor model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LiquidacionRepartidor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LiquidacionRepartidor();
        if ($model->load(Yii::$app->request->post())) {
            $remitos = Remito::getRemitosOrdenFechaByRepartidor($model->repartidor_idrepartidor, $model->periodo_inicio, $model->periodo_fin);
            $flag = 1;
            foreach ($remitos as $r) {
                if($r->terminal == 0){
                    $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, $r->sucursalIdsucursal->localidadIdlocalidad->zonaHasLocalidad->zona_idzona);
                } else {
                    $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, 5);
                }
                if(!$tarHasPrecio){
                    $clienteHuerfano = $r->clienteIdcliente->razonsocial;
                    $tarifarioHuerfano = $r->clienteIdcliente->tarifarioIdtarifario->nombre;
                    $cargaHuerfana = $r->cargaIdcarga->nombre;
                    $zonaHuerfana = $r->sucursalIdsucursal->localidadIdlocalidad->zonaHasLocalidad->zonaIdzona->nombre;
                    $remitoProblema = $r->idremito;
                    $flag = -1;
                    break;
                }
            }
            if($flag != -1  && $model->save()){ 
                $fechaAnterior = $clienteAnterior = null;
                foreach ($remitos as $r) {
                    if($r->terminal == 0){
                        $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, $r->sucursalIdsucursal->localidadIdlocalidad->zonaHasLocalidad->zona_idzona);
                    } else {
                        $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, 5);
                    }
                    $rel = new LiquidacionrepartidorHasRemito();
                    $rel->liquidacionrepartidor_idliquidacionrepartidor = $model->idliquidacion_repartidor;
                    $rel->remito_idremito = $r->idremito;
                    $r->liquidado_repartidor = 1;
                    if(!isset($r->precio) || !isset($r->precio_adicional) || !isset($r->precio_reparto) || !isset($r->precio_reparto_adicional) || !isset($r->porcentaje_contra))
                    {
                        if(!isset($r->precio))
                            $r->precio = $tarHasPrecio->precio;
                        if(!isset($r->precio_adicional) )
                            $r->precio_adicional = $tarHasPrecio->precio_adicional;
                        if(!isset($r->precio_reparto))
                            $r->precio_reparto = $tarHasPrecio->precio_reparto;
                        if(!isset($r->precio_reparto_adicional))
                            $r->precio_reparto_adicional = $tarHasPrecio->precio_reparto_adicional;
                        if(!isset($r->porcentaje_contra))
                            $r->porcentaje_contra = $tarHasPrecio->porcentaje_contra;
                        $r->save(false);
                    }

                    if($r->clienteIdcliente->tarifarioIdtarifario->tipo == "Tipo A"){
                        $rel->total += $tarHasPrecio->precio_reparto;
                        $rel->total += (($r->cantidad_bultos + $r->volumen - 1)  * $tarHasPrecio->precio_reparto_adicional);
                        $fechaAnterior = $r->remitofecha;
                        $clienteAnterior = $r->cliente_idcliente;
                    }else if($r->clienteIdcliente->tarifarioIdtarifario->tipo == "Tipo B"){
                        if($fechaAnterior == $r->remitofecha && $r->cliente_idcliente == $clienteAnterior){
                            $rel->total += (($r->cantidad_bultos + $r->volumen) * $tarHasPrecio->precio_reparto_adicional);
                        }else{
                            $rel->total += ($tarHasPrecio->precio_reparto + (($r->cantidad_bultos + $r->volumen - 1) * $tarHasPrecio->precio_reparto_adicional));
                        }
                        $fechaAnterior = $r->remitofecha;
                        $clienteAnterior = $r->cliente_idcliente;
                    }

                    $r->save(false); 
                    $rel->save(false);
                    $model->total += $rel->total;
                }
                $model->creado = date('Y-m-d G:i', time());
                $model->save(false);
                Yii::$app->session->setflash('success', "Liquidación común repartidor creada con éxito.");
                return $this->redirect(['view', 'id' => $model->idliquidacion_repartidor]);
            }else{
                Yii::$app->session->setflash('error', "Remito ID:".$remitoProblema." Completar datos de ".$zonaHuerfana." en tarifario ".$tarifarioHuerfano."!");
                //info azul
                //warning amarrillo
                //success verde
                //error rojo

            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LiquidacionRepartidor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idliquidacion_repartidor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LiquidacionRepartidor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $liquidacionHasRemito =  LiquidacionrepartidorHasRemito::find()->where(['liquidacionrepartidor_idliquidacionrepartidor' => $id])->all();
        foreach($liquidacionHasRemito as $lhr){
            $post = Yii::$app->db->createCommand('UPDATE remito SET liquidado_repartidor=:liquidado_repartidor WHERE idremito=:idremito')
            ->bindValue(':liquidado_repartidor', 0)
            ->bindValue(':idremito', $lhr->remito_idremito)
            ->execute();
            $lhr->delete();
        }

        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LiquidacionRepartidor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LiquidacionRepartidor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LiquidacionRepartidor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionReport($id) {

        $model = $this->findModel($id);
        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('view', ['model' => $model]);
        // setup kartik\mpdf\Pdf component
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;
        // return the pdf output as per the destination setting
        return $pdf->render(); 
    }

    /**
     * Creates a new LiquidacionRepartidor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionSummary($id)
    {
        $model = $this->findModel($id);
        $remitos = Remito::getRemitosLiquidadosByRepartidor($model->repartidor_idrepartidor, $model->periodo_inicio, $model->periodo_fin);

        return $this->render('summary', [ 
            'model' => $model,
            'remitos' => $remitos,
        ]);
    }

    public function actionReportSummary($id) {

        $model = $this->findModel($id);
        $remitos = Remito::getSumBultosByRepartidorForCliente($model->repartidor_idrepartidor, $model->periodo_inicio, $model->periodo_fin);
        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('summary', ['model' => $model, 'remitos' => $remitos,]);
        
        // setup kartik\mpdf\Pdf component
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;
        // return the pdf output as per the destination setting
        return $pdf->render(); 
    }
}
