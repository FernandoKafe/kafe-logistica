<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\EmpresaTransporte */

$this->title = 'Actualizar Empresa de Transporte: ' . $model->razonsocial;
$this->params['breadcrumbs'][] = ['label' => 'Empresas Transporte', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idempresatransporte, 'url' => ['view', 'id' => $model->idempresatransporte]];
$this->params['breadcrumbs'][] = 'Actualizar';
?>
<h1><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario de modificación') ?></h2>
        <div class="clearfix"></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
