<?php
use yii\data\ActiveDataProvider;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use yii\helpers\Html;

use common\models\RemitoPorcentaje;
use common\models\LiquidacionPorcentajeCliente;


/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionPorcentajeCliente */

$idLiquidacion = Yii::$app->getRequest()->getQueryParam('id');


$this->title = "Liquidación %: ".$model->clienteIdcliente->razonsocial.", ".$model->periodo;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion % Clientes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-view row" style="overflow: hidden;">
    <div class="col-xs-3" style="float: left;">
        <h3>Liquidación: <?= $model->clienteIdcliente->razonsocial?></h3>
        <h3>Periodo: <?= $model->periodo?></h3>
        <h3>CUIT: <?=$model->clienteIdcliente->cuit?></h3>
    </div>
    <div class="col-xs-4" style=" margin: 0 auto 0;">
        <?php echo Html::img('@web/img/logo/LOGO.jpg', ['class' => 'img-responsive', 'width'=>'210', 'style' => 'margin: 0 0;']) ?>
    </div>
    <div class="col-xs-3" style="border: 1px solid #d3d3d3; float:right; margin: 0 15px 0 0;">
            <div class="col-xs-2" style="margin: 20px 0 0 0;">
                <p>Firma: </p>
            </div>
            <div class="col-xs-6" style="margin: 0 0 0 0;">
                <p>________________________________</p>
            </div>
            <div class="col-xs-2">
                <p>Aclaración</p>
            </div>     
            <div class="col-xs-6">
                <p>________________________________</p>
            </div>
            <div class="col-xs-2">
                <p>Fecha</p>
            </div>
            <div class="col-xs-6">
                <p>_______ / _______ / _______</p>
            </div>
            <div class="col-xs-12" style="margin: 35px 0 0 0;">
                    <?= "<div class='col-md-12 tabla-totales'>TOTAL: ".number_format($model->total_cobrar, 2 , "." ,  "," )."</div>"; ?>
            </div>
        </div>    
</div>
<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_content">
    <?php
    $remito = RemitoPorcentaje::find()
                                ->joinWith('lpcHasRemitoporcentajes')
                                ->where(['lpc_idlpc' => $idLiquidacion])
                                ->orderBy([ 'fecha' => SORT_ASC, 'numero_planilla' => SORT_DESC]);
    $dataProvider = new ActiveDataProvider([
        'query' => $remito,
        'pagination' => false,
    ]);
    $dataProvider->setSort([ 
        'attributes' => [
            'fecha' => [
                'asc' => ['fecha' => SORT_ASC],
                'desc' => ['fecha' => SORT_DESC],
            ],
            'numero_planilla' => [
                'asc' => ['numero_planilla' => SORT_ASC],
                'desc' => ['numero_planilla' => SORT_DESC],
            ],
        ],
        'defaultOrder' => [
            'fecha' => SORT_ASC,
            'numero_planilla' => SORT_ASC,
        ]
    ]);
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    'moduleId' => 'gridviewk',
    'showPageSummary' => true,
    'columns' => [
        [
            'attribute' => 'fecha',
            'label' => 'Fecha',
        ],
        [
            'attribute' => 'porcentaje',
            'value' => function($data){
                if($data->porcentaje != ''){
                    return $data->porcentaje." %";
                }else{
                    return $data->porcentajeIdporcentaje->cantidad." %";
                }
            }
        ],
        [
            'attribute' => 'numero_planilla',
            'label' => 'Nº Planilla',
            'value' => function($data){
                return $data->numero_planilla;
            },
            'pageSummary' => '<strong>TOTAL</strong>',
        ],
        [
            'attribute' => 'cant_pedidos',
            'footer' => $model->pedidos,
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
        ],
        [
            'attribute' => 'dinero_facturado',
            'format' => ['decimal',2],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
            'footer' => number_format($model->total_facturado, 2 , "," ,  "." ),
        ],
        [
            'attribute' => 'dinero_devuelto',
            'format' => ['decimal',2],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
            'footer' => number_format($model->total_devuelto, 2 , "," ,  "." ),
        ],
        [
            'attribute' => 'dinero_rendido',
            'format' => ['decimal',2],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
            'footer' => number_format($model->total_rendido, 2 , "," ,  "." ),
        ],
        [
            'attribute' => 'dinero_faltante',
            'format' => ['decimal',2],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
            'footer' => number_format($model->total_faltante, 2 , "," ,  "." ),
        ],
        [
            'attribute' => 'combustible',
            'format' => ['decimal',2],
            'pageSummary' => true,
            'pageSummaryFunc' => GridView::F_SUM,
            'footer' => number_format($model->combustible, 2 , "," ,  "." ),
        ],
    ],
    ]); ?>

    <?= DetailView::widget([
            'model' => $model,
            'options' => ['class' => 'table table-striped table-bordered detail-view tabla-totales' ],
            'attributes' => [
                [
                    'label' => 'TOTAL FACTURADO',
                    'attribute' => 'total_facturado',
                    'format' => ['decimal',2],
                ],
                [
                    'label' => 'TOTAL DEVUELTO',
                    'attribute' => 'total_devuelto',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'total_rendido',
                    'label' => 'TOTAL RENDIDO',
                    'value' => function($data){
                        return number_format($data->total_rendido, 2 , "," ,  "." )
                        ." (Al 3.5% = ".number_format($data->total_rendido_al_3, 2 , "," ,  "."  )
                        ." - Al 5.5% = ".number_format($data->total_rendido_al_5, 2 , "," ,  "."  )
                        .")";
                    }
                ],
                [
                    'attribute' => 'neto_sin_iva',
                    'label' => 'NETO SIN IVA',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'combustible',
                    'label' => 'COMBUSTIBLE',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'comision',
                    'label' => 'COMISIÓN',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'pallets',
                    'label' => 'TOTAL Pallets',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'subtotal',
                    'label' => 'SUBTOTAL',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'iva',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'total_real',
                    'label' => 'TOTAL REAL',
                    'format' => ['decimal',2],
                ],
                [
                    'label' => 'TOTAL FALTANTE',
                    'attribute' => 'total_faltante',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'total_cobrar',
                    'label' => 'TOTAL COBRAR',
                    'format' => ['decimal',2],
                ],
            ]
        ]) ?>

    </div>
</div>