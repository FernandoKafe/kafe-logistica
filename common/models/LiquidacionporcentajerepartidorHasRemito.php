<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%liquidacionporcentajerepartidor_has_remito}}".
 *
 * @property int $idliquidacionporcentajerepartidor_has_remito
 * @property int $lpr_idlpr
 * @property int $remitoporcentaje_idremitoporcentaje
 *
 * @property LiquidacionPorcentajeRepartidor $lprIdlpr
 * @property RemitoPorcentaje $remitoporcentajeIdremitoporcentaje
 */
class LiquidacionporcentajerepartidorHasRemito extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%liquidacionporcentajerepartidor_has_remito}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lpr_idlpr', 'remitoporcentaje_idremitoporcentaje'], 'required'],
            [['lpr_idlpr', 'remitoporcentaje_idremitoporcentaje'], 'integer'],
            [['lpr_idlpr'], 'exist', 'skipOnError' => true, 'targetClass' => LiquidacionPorcentajeRepartidor::className(), 'targetAttribute' => ['lpr_idlpr' => 'idliquidacion_porcentaje_repartidor']],
            [['remitoporcentaje_idremitoporcentaje'], 'exist', 'skipOnError' => true, 'targetClass' => RemitoPorcentaje::className(), 'targetAttribute' => ['remitoporcentaje_idremitoporcentaje' => 'idremito_porcentaje']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idliquidacionporcentajerepartidor_has_remito' => Yii::t('app', 'Idliquidacionporcentajerepartidor Has Remito'),
            'lpr_idlpr' => Yii::t('app', 'Lpr Idlpr'),
            'remitoporcentaje_idremitoporcentaje' => Yii::t('app', 'Remitoporcentaje Idremitoporcentaje'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLprIdlpr()
    {
        return $this->hasOne(LiquidacionPorcentajeRepartidor::className(), ['idliquidacion_porcentaje_repartidor' => 'lpr_idlpr']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemitoporcentajeIdremitoporcentaje()
    {
        return $this->hasOne(RemitoPorcentaje::className(), ['idremito_porcentaje' => 'remitoporcentaje_idremitoporcentaje']);
    }
}
