<?php

use yii\helpers\Html;
use common\models\OrdenGrupalVinos;
use common\models\RemitoVino;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;





/* @var $this yii\web\View */
/* @var $model common\models\Remito */
$idOrdenGrupal = Yii::$app->getRequest()->getQueryParam('idOrdenGrupal');

$this->title = Yii::t('app', 'Agregar remitos a Orden Grupal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Remitos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<h1><i class="fa fa-download"></i> <?= Html::encode($this->title) ?></h1>

<div class="x_panel">
    <div class="x_content">
        <?php
            echo $this->render('view', [
                'model' => OrdenGrupalVinos::findOne($idOrdenGrupal),
                ]);
        ?>
    </div>
</div>

<!--Añadir remito -->
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Agregar remito');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

    <?= $this->render('../remito-vino/_form', [
        'model' => $model,
    ]) ?>
    </div>
</div>