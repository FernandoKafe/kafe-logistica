<?php

namespace frontend\controllers;

use Yii;
use common\models\ClienteHasTipo;
use frontend\models\ClienteHasTipoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ClienteHasTipoController implements the CRUD actions for ClienteHasTipo model.
 */
class ClienteHasTipoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ClienteHasTipo models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClienteHasTipoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ClienteHasTipo model.
     * @param integer $cliente_idcliente
     * @param integer $clientetipo_idclientetipo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cliente_idcliente, $clientetipo_idclientetipo)
    {
        return $this->render('view', [
            'model' => $this->findModel($cliente_idcliente, $clientetipo_idclientetipo),
        ]);
    }

    /**
     * Creates a new ClienteHasTipo model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ClienteHasTipo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cliente_idcliente' => $model->cliente_idcliente, 'clientetipo_idclientetipo' => $model->clientetipo_idclientetipo]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ClienteHasTipo model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $cliente_idcliente
     * @param integer $clientetipo_idclientetipo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cliente_idcliente, $clientetipo_idclientetipo)
    {
        $model = $this->findModel($cliente_idcliente, $clientetipo_idclientetipo);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cliente_idcliente' => $model->cliente_idcliente, 'clientetipo_idclientetipo' => $model->clientetipo_idclientetipo]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ClienteHasTipo model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $cliente_idcliente
     * @param integer $clientetipo_idclientetipo
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cliente_idcliente, $clientetipo_idclientetipo)
    {
        $this->findModel($cliente_idcliente, $clientetipo_idclientetipo)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ClienteHasTipo model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $cliente_idcliente
     * @param integer $clientetipo_idclientetipo
     * @return ClienteHasTipo the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cliente_idcliente, $clientetipo_idclientetipo)
    {
        if (($model = ClienteHasTipo::findOne(['cliente_idcliente' => $cliente_idcliente, 'clientetipo_idclientetipo' => $clientetipo_idclientetipo])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
