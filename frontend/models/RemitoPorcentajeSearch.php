<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RemitoPorcentaje;

/**
 * RemitoPorcentajeSearch represents the model behind the search form of `common\models\RemitoPorcentaje`.
 */
class RemitoPorcentajeSearch extends RemitoPorcentaje
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idremito_porcentaje', 'cant_pedidos', 'cant_entregados', 'noquiso_nopidio', 'cant_anulado', 'cant_sin_dinero', 'cant_regresado', 'cant_faltante', 'cant_mal_armado', 'cant_duplicado', 'otro', 'porcentaje_idporcentaje', 'liquidado_cliente', 'liquidado_repartidor', 'se_paga'], 'integer'],
            [['fecha', 'numero_planilla', 'detalle_otro', 'observacion_faltante', 'cliente_idcliente', 'repartidor_idrepartidor', 'creado'], 'safe'],
            [['dinero_facturado', 'dinero_rendido', 'dinero_devuelto', 'dinero_faltante', 'combustible', 'porcentaje', 'pallets', 'reparto_uno', 'reparto_dos', 'reparto_tres', 'reparto_cuatro', 'reparto_seleccionado', 'porcentaje_entregas'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RemitoPorcentaje::find();/*->where(['or', ['liquidado_cliente' => 0],['liquidado_repartidor' => 0]]);
        $query->joinWith(['clienteIdcliente as cliente']);
        $query->joinWith(['repartidorIdrepartidor as repartidor']);*/



        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idremito_porcentaje' => $this->idremito_porcentaje,
            'fecha' => $this->fecha,
            'cant_pedidos' => $this->cant_pedidos,
            'cant_entregados' => $this->cant_entregados,
            'noquiso_nopidio' => $this->noquiso_nopidio,
            'cant_anulado' => $this->cant_anulado,
            'cant_sin_dinero' => $this->cant_sin_dinero,
            'cant_regresado' => $this->cant_regresado,
            'cant_faltante' => $this->cant_faltante,
            'cant_mal_armado' => $this->cant_mal_armado,
            'cant_duplicado' => $this->cant_duplicado,
            'otro' => $this->otro,
            'dinero_facturado' => $this->dinero_facturado,
            'dinero_rendido' => $this->dinero_rendido,
            'dinero_devuelto' => $this->dinero_devuelto,
            'dinero_faltante' => $this->dinero_faltante,
            'porcentaje_idporcentaje' => $this->porcentaje_idporcentaje,
            'liquidado_cliente' => $this->liquidado_cliente,
            'liquidado_repartidor' => $this->liquidado_repartidor,
            'combustible' => $this->combustible,
            'pallets' => $this->pallets,
            'porcentaje' => $this->porcentaje,
            'creado' => $this->creado,
            'reparto_uno' => $this->reparto_uno, 
            'reparto_dos' => $this->reparto_dos, 
            'reparto_tres' => $this->reparto_tres, 
            'reparto_cuatro' => $this->reparto_cuatro,
            'reparto_seleccionado' => $this->reparto_seleccionado, 
            'porcentaje_entregas' => $this->porcentaje_entregas,
            'se_paga' => $this->se_paga,

        ]);

        $query->andFilterWhere(['like', 'numero_planilla', $this->numero_planilla])
        ->andFilterWhere(['like', 'detalle_otro', $this->detalle_otro])
        ->andFilterWhere(['like', 'observacion_faltante', $this->observacion_faltante])
        ->andFilterWhere(['like', 'cliente.razonsocial', $this->cliente_idcliente])
        ->andFilterWhere(['like', 'CONCAT(repartidor.apellido, ", ", repartidor.nombre)', $this->repartidor_idrepartidor]);


        return $dataProvider;
    }
}
