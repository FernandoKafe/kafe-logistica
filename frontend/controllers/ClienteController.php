<?php

namespace frontend\controllers;

use Yii;
use common\models\Cliente;
use frontend\models\ClienteSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\models\ClienteHasTipo;

/**
 * ClienteController implements the CRUD actions for Cliente model.
 */
class ClienteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cliente models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClienteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cliente model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Cliente model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cliente();
        $clienteHasTipo = new ClienteHasTipo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $clienteHasTipo->cliente_idcliente = $model->idcliente;
            $clienteHasTipo->clientetipo_idclientetipo = 1;
            $clienteHasTipo->save();
            return $this->redirect(['add-tipo', 'id' => $model->idcliente]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Cliente model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['add-tipo', 'id' => $model->idcliente]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Cliente model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if($model->remitos){
            Yii::$app->session->setflash('error', "No se puede eliminar. El cliente tiene remitos comunes a su nombre.");
            return $this->redirect(['index']);
        }
        if($model->ordenRetiroGrupals){
            Yii::$app->session->setflash('error', "No se puede eliminar. El cliente tiene ordenes grupales a su nombre.");
            return $this->redirect(['index']);
        }
        if(count($model->ordenGrupalVinos)>0){
            Yii::$app->session->setflash('error', "No se puede eliminar. El cliente tiene remitos vinos a su nombre.");
            return $this->redirect(['index']);
        }
        if($model->liquidacions){
            Yii::$app->session->setflash('error', "No se puede eliminar. El cliente tiene liquidaciones a su nombre.");
            return $this->redirect(['index']);
        }
        if($model->liquidacionVinoClientes){
            Yii::$app->session->setflash('error', "No se puede eliminar. El cliente tiene liquidaciones vino a su nombre.");
            return $this->redirect(['index']);
        }
        if($model->remitoPorcentajes){
            Yii::$app->session->setflash('error', "No se puede eliminar. El cliente tiene remitos porcentaje a su nombre.");
            return $this->redirect(['index']);
        }
        if($model->ordenGrupalDomicilios){
            Yii::$app->session->setflash('error', "No se puede eliminar. El cliente tiene remitos domicilio a su nombre.");
            return $this->redirect(['index']);
        }
        if($model->liquidacionDomicilioClientes){
            Yii::$app->session->setflash('error', "No se puede eliminar. El cliente tiene liquidaciones a domicilio a su nombre.");
            return $this->redirect(['index']);
        }
        if($model->liquidacionPorcentajeClientes){
            Yii::$app->session->setflash('error', "No se puede eliminar. El cliente tiene liquidaciones porcentaje a su nombre.");
            return $this->redirect(['index']);
        }
        if($model->planillaContras){
            Yii::$app->session->setflash('error', "No se puede eliminar. El cliente tiene planillas contra reembolso a su nombre.");
            return $this->redirect(['index']);
        }
        
        $cliTipos = ClienteHasTipo::find()->where(['cliente_idcliente' => $model->idcliente])->all();
        if($cliTipos){
            foreach($cliTipos as $ct){
                $ct->delete();
            }
        }

        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cliente model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cliente the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cliente::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Listado de clientes para Select2 Ajax
     * @param String|$q Texto de busqueda
     * @param int|$id   Iedntificador de cliente
     * @return string|\yii\web\Response
     */
    public function actionListado($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $out['results'] = Cliente::getClienteByRazonsocial($q);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Cliente::findOne($id)->razonsocial];
        }
        return $out;
    }

    /**
     * Listado de clientes para Select2 Ajax
     * @param String|$q Texto de busqueda
     * @param int|$id   Iedntificador de cliente
     * @return string|\yii\web\Response
     */
    public function actionListadoForTipo($q = null, $id = null, $cliente_tipo = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $out['results'] = Cliente::getClienteByRazonsocialAndTipo($q, $cliente_tipo);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Cliente::findOne($id)->razonsocial];
        }
        return $out;
    }

    /**
     * Add tipos to an existing Cliente.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id idcliente
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAddTipo()
    {
        $model = new ClienteHasTipo();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['add-tipo',
                    'id' => $model->cliente_idcliente,
                ]);
        }
        return $this->render('add-tipo', [
            'model' => $model,
        ]);
    }

    /**
     * Add tipos to an existing Cliente.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id idcliente
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAddTarifario($id)
    {
        $model = $this->findModel($id);
        if($model->load(Yii::$app->request->post()) && $model->save()){
            return $this->redirect(['add-tipo', 'id' => $model->idcliente]);
        }
        //return $this->redirect(['add-tipo', 'id' => $model->idcliente]);
    }

    public function actionRemoveDomicilio($id){
        $model = $this->findModel($id);
        $model->tarifariodomicilio_idtarifariodomicilio = null;
        $model->save();
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionRemoveVino($id){
        $model = $this->findModel($id);
        $model->tarifariovino_idtarifariovino = null;
        $model->save();
        return $this->render('view', [
            'model' => $model,
        ]);
    }
}
