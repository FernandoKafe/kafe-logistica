<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TarifarioHasPrecio */

$this->title = $model->tarifarioIdtarifario->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tarifario Has Precios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarifario-has-precio-view">

    <h1>Precio del tarifario: <?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
    <div class="x_title">
        <h2>Detalle</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <p>
        <?= Html::a(Yii::t('app', 'Inicio'), ['index', 'carga_idcarga' => $model->carga_idcarga, 'zona_idzona' => $model->zona_idzona, 'tarifario_idtarifario' => $model->tarifario_idtarifario], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'carga_idcarga' => $model->carga_idcarga, 'zona_idzona' => $model->zona_idzona, 'tarifario_idtarifario' => $model->tarifario_idtarifario], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'zona_idzona',
                'label' => 'Zona',
                'value' => function($data){
                    return $data->zonaIdzona->nombre;
                }
            ],
            [
                'attribute' => 'carga_idcarga',
                'label' => 'Carga',
                'value' => function($data){
                    return $data->cargaIdcarga->nombre;
                }
            ],
            [
                'attribute' => 'precio',
                'format' => ['decimal',2],
            ],
            [
                'attribute' => 'precio_adicional',
                'format' => ['decimal',2],
            ],
            [
                'attribute' => 'precio_reparto',
                'format' => ['decimal',2],
            ],
            [
                'attribute' => 'precio_reparto_adicional',
                'format' => ['decimal',2],
            ],
            [
                'attribute' => 'porcentaje_contra',
                'format' => ['decimal',2],
            ],

        ],
    ]) ?>

</div>
</div>
</div>
