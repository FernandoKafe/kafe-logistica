<?php

use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use yii\helpers\Html;

use \common\models\Departamento;
use common\models\Empresa;
use \common\models\Provincia;
use common\models\Sucursal;


/* @var $this yii\web\View */
/* @var $model common\models\ZonaHasLocalidad */
/* @var $form yii\widgets\ActiveForm */
$idZona = Yii::$app->getRequest()->getQueryParam('id');
$dataProvincia=ArrayHelper::map(Provincia::find()->orderBy('nombre')->asArray()->all(), 'idprovincia', 'nombre');

?>

<div class="x_panel">
    <div class="x_content">

    <?php $form = ActiveForm::begin(); ?>

    <?php $model->zona_idzona = $idZona?>

    <?= $form->field($model, 'zona_idzona')->textInput()->label(false)->hiddenInput() ?>

    <?php
        echo $form->field(new Provincia(), 'idprovincia')->dropDownList($dataProvincia,
        ['prompt'=>'-Seleccionar-',
          'id' => 'provinciaDestino'])->label('Provincia');

      $dataDepartamentos = [];
      $departamento = new Departamento();

      echo  $form->field($departamento, 'iddepartamento')->widget(DepDrop::classname(), [
        //'data'=> $initLocalidad,
        'data' => $dataDepartamentos,
        'language' => 'es',
        'options' => ['id'=>'departamento'],
        'type' => DepDrop::TYPE_SELECT2,
            'pluginOptions'=>[
            'depends' => ['provinciaDestino'],
            'placeholder' => '- Seleccionar Departamento -',
            'url' => \yii\helpers\Url::to(['direccion/departamento-provincia']),
            'loadingText' => 'Cargando Departamentos...'
          ],
      ])->label('<a href="../departamento/create" target="_blank"><i class="fa fa-plus green"></i></a> Departamento');
      echo $form->field($model, 'localidad_idlocalidad')->widget(DepDrop::classname(), [
        //'data'=> $initLocalidad,
        'language' => 'es',
        'options' => [
          'id'=>'localidad',
          'multiple' => true,
        ],
        'type' => DepDrop::TYPE_SELECT2,
          'pluginOptions'=>[
          'depends' => ['departamento'],
          'placeholder' => '- Seleccionar Localidad -',
          'url' => \yii\helpers\Url::to(['direccion/departamento-localidad']),
          'loadingText' => 'Cargando Localidades...'
        ],
      ])->label('<a href="../localidad/create" target="_blank"><i class="fa fa-plus green"></i></a> Localidad');
      ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Agregar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
