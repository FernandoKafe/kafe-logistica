<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use common\models\Carga;
use common\models\Zona;

$idTarifario = Yii::$app->getRequest()->getQueryParam('id');
if(!isset($idTarifario)){
    $idTarifario = Yii::$app->getRequest()->getQueryParam('tarifario_idtarifario');
}

$carga = ArrayHelper::map(Carga::find()->all(),'idcarga','nombre');
$zona = ArrayHelper::map(Zona::find()->all(),'idzona','nombre');


/* @var $this yii\web\View */
/* @var $model common\models\TarifarioHasPrecio */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="x_panel">
    <div class="x_content">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'zona_idzona')->textInput()->dropDownList($zona)->label('Zona') ?>

    <?= $form->field($model, 'carga_idcarga')->dropDownList($carga)->label('Carga') ?>

    <?php $model->tarifario_idtarifario = $idTarifario ?>
    <?= $form->field($model, 'tarifario_idtarifario')->textInput()->label(false)->hiddenInput() ?>

    <?= $form->field($model, 'precio')->textInput()->label('$Bulto') ?>

    <?= $form->field($model, 'precio_adicional')->textInput()->label('$Bulto adicional') ?>

    <?= $form->field($model, 'precio_reparto')->textInput()->label('$Reparto') ?>

    <?= $form->field($model, 'precio_reparto_adicional')->textInput()->label('$Reparto adicional') ?>

    <?= $form->field($model, 'porcentaje_contra')->textInput()->label('$Porcentaje contra reembolso') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Agregar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
</div>
