<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionDomicilioRepartidor */

$this->title = Yii::t('app', 'Actualizar Liquidación Domicilio Repartidor: ' . $model->repartidorIdrepartidor->apellido." - Periodo: ".$model->nombre);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion Domicilio Repartidors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idliquidacion_domicilio_repartidor, 'url' => ['view', 'id' => $model->idliquidacion_domicilio_repartidor]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<h1><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario de modificación') ?></h2>
        <div class="clearfix"></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
