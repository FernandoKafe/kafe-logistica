<?php

use \yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\db\Query;
use frontend\models\LiquidacionHasRemitoSearch;
use common\models\LiquidacionHasRemito;
use common\models\Remito;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use common\models\TarifarioHasPrecio;
use common\models\Liquidacion;
use common\models\OrdenGrupalDomicilio;
use common\models\GuiaDomicilio;
use common\models\LiquidacionDomicilioCliente;

/* @var $this yii\web\View */
/* @var $model common\models\Rendicion */

$this->title = $model->idrendicion;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rendicions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$tipo = Yii::$app->getRequest()->getQueryParam('tipo');
$id = Yii::$app->getRequest()->getQueryParam('id');


$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidación'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-view row" style="overflow: hidden;">
    <div class="col-xs-3" style="float: left;">
        <h1>Planilla contra reembolso: <?= $model->clienteIdcliente->razonsocial?></h1>
        <h1>Desde: <?= $model->fecha_inicio?></h1>
        <h1>Hasta: <?= $model->fecha_fin?></h1>
        <h1>CUIT: <?=$model->clienteIdcliente->cuit?></h1>
    </div>
    <div class="col-xs-4" style=" margin: 0 auto 0;">
        <?php echo Html::img('@web/img/logo/LOGO.jpg', ['class' => 'img-responsive', 'width'=>'210', 'style' => 'margin: 0 29% 0;']) ?>
    </div>
    <div class="col-xs-3" style="border: 1px solid #d3d3d3; float:right; margin: 0 15px 0 0;">
            <div class="col-xs-2" style="margin: 20px 0 0 0;">
                <p>Firma: </p>
            </div>
            <div class="col-xs-6" style="margin: 0 0 0 0;">
                <p>________________________________</p>
            </div>
            <div class="col-xs-2">
                <p>Aclaración</p>
            </div>     
            <div class="col-xs-6">
                <p>________________________________</p>
            </div>
            <div class="col-xs-2">
                <p>Fecha</p>
            </div>
            <div class="col-xs-6">
                <p>_______ / _______ / _______</p>
            </div>
            <div class="col-xs-12" style="margin: 35px 0 0 0;">
                <?php
                    if($tipo == 0){
                         echo "<div class='col-md-12 tabla-totales'>TOTAL CONTRA REEMBOLSO: ".number_format($model->monto, 2 , "." ,  "," )."</div>";
                    }else if($tipo == 1){
                        echo "<div class='col-md-12 tabla-totales'>TOTAL RENDICIÓN: ".number_format($model->monto, 2 , "." ,  "," )."</div>";
                    } 
                         
                ?>
            </div>
        </div>    
</div>

<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_title">
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php
    if($tipo == 0)
    {
        $remito = Remito::find()->select('remitofecha, total_contrarembolso, facturado_a, tipo_remito, terminal,carga_idcarga, sucursal_idsucursal, empresatransporte_idempresatransporte, cliente_idcliente, numero_remito, numero_guia, rol_sucursal, cantidad_bultos, total_guia, ordenderetirogrupal_idordenderetirogrupal')
    ->where(['>=','remitofecha', $model->fecha_inicio])
    ->andFilterWhere(['<=','remitofecha', $model->fecha_fin])
    ->andFilterWhere(['servicio_idservicio' => 2])
    ->andFilterWhere(['cliente_idcliente' => $model->cliente_idcliente]);
    $dataProvider = new ActiveDataProvider([
        'query' => $remito,
        'pagination' => false,
    ]);
    $dataProvider->setSort([
        'attributes' => [
            'remitofecha' => [
                'asc' => ['remitofecha' => SORT_ASC],
                'desc' => ['remitofecha' => SORT_DESC],
                'default' => SORT_ASC
            ],
        ],
        'defaultOrder' => [
            'remitofecha' => SORT_ASC
        ]
    ]);
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => ['class' => 'tabla-rend' ],
    'showFooter' => true,
    'columns' => [
        [
            'attribute' => 'remitofecha',
            'label' => 'Fecha',
        ],
        [
            'attribute' => 'numero_remito',
            'label' => 'Remito',
            'value' => function($data){
                if($data->numero_remito){
                    return $data->numero_remito;
                }else{
                    return $data->ordenderetirogrupalIdordenderetirogrupal->numero;
                }
            }
        ],
        [
            'attribute' => 'numero_guia',
            'label' => 'Guía',
            'value' => function($data){
                return $data->numero_guia.' - '.$data->tipo_remito;
            }
        ],
        [
            'label' => 'Destinatario',
            'value' => function($data){
                if($data->rol_sucursal == "Remite"){
                    return $data->clienteIdcliente->razonsocial;
                }else{
                    return $data->sucursalIdsucursal->nombre.", ".$data->sucursalIdsucursal->empresaIdempresa->nombre;
                }
            },
        ],
        [
            'attribute' => 'sucursalIdsucursal',
            'label' => 'Destino',
            'value' => function($data){
                if(isset($data->sucursalIdsucursal->localidad_idlocalidad)){
                    return $data->sucursalIdsucursal->localidadIdlocalidad->nombre.", ".
                    $data->sucursalIdsucursal->localidadIdlocalidad->departamentoIddepartamento->nombre.", ".
                    $data->sucursalIdsucursal->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                }else{
                    return "SIN LOCALIDAD ASIGNADA";
                }
            }
        ],
        [
            'attribute' => 'empresatransporte_idempresatransporte',
            'label' => 'Empresa de transporte',
            'value' => function($data){
                if(isset($data->empresatransporteIdempresatransporte->razonsocial)){
                    return $data->empresatransporteIdempresatransporte->razonsocial;
                }else{
                    return "SIN NOMBRE ASIGNADO";
                }
            },
            'footer' => '<strong>TOTAL</strong>',
        ],
        [
            'attribute' => 'cantidad_bultos',
            'label' => 'Cantidad bultos',
            'value' => function($data){
                return $data->cantidad_bultos;
            },
            'footer' => Liquidacion::getBultosSum($remito->all()),
        ],
        [
            'attribute' => 'volumen',
            'label' => 'Cantidad bultos excedentes',
            'value' => function($data){
                return $data->volumen;
            },
            'footer' => Liquidacion::getVolumenesSum($remito->all()),
        ],
        [
            'attribute' => 'total_contrarembolso',
            'footer' => Liquidacion::getContraSum($remito->all()),
        ]
    ],
    ]); 
    $totalRendido = number_format ((Liquidacion::getContraSum($remito->all())), 2 , "." ,  "," );
    echo "<div class='col-md-12'><h2><strong>TOTAL: ".$totalRendido."</strong></h2>";
    }
    else if($tipo == 1)
    {
        $ordenesGrupales = OrdenGrupalDomicilio::getAllOrdenesClienteDia($model->cliente_idcliente, $model->fecha_inicio, $model->fecha_fin);
        $ordenesGrupalesId = ArrayHelper::getColumn($ordenesGrupales,'idorden_grupal_domicilio');
        $remitos = GuiaDomicilio::find()->where(['IN', 'ordengrupaldomicilio_idordengrupaldomicilio', $ordenesGrupalesId])->andWhere(['rendido' => $id]);
        $dataProvider = new ActiveDataProvider([
            'query' => $remitos,
            'pagination' => false,
        ]);
        $remitosArr = $remitos->all();
        $sumaBultos = LiquidacionDomicilioCliente::getBultosSum($remitosArr);
        $rendicionComun = LiquidacionDomicilioCliente::getRendicionComunSum($remitosArr);
        $rendicionSprinter = LiquidacionDomicilioCliente::getRendicionSprinterSum($remitosArr);
        $rendicionTotal = LiquidacionDomicilioCliente::getRendicionTotal($remitosArr);

        if($model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo B"){
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'options' => ['class' => 'tabla-rend' ],
                'showFooter' => true,
                'columns' => [
                    [
                        'label' => 'Fecha',
                        'value' => function($data){
                            return $data->ordengrupaldomicilioIdordengrupaldomicilio->fecha;
                        },
                    ],
                    [
                        'label' => 'Nº Orden Grupal',
                        'value' => function($data){
                            return $data->ordengrupaldomicilioIdordengrupaldomicilio->numero_orden;
                        },
                    ],
                    [
                        'attribute' => 'empresa_destino',
                        'label' => 'Empresa destino',
                    ],
                    [
                        'attribute' => 'localidad_idlocalidad',
                        'label' => 'Localidad destino',
                        'value' => function($data){
                            if($data->localidad_idlocalidad){
                                return $data->localidadIdlocalidad->nombre.
                                ", ".
                                $data->localidadIdlocalidad->departamentoIddepartamento->nombre.
                                ", ".
                                $data->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                            }else{
                                return "Guía sin localidad";
                            }
                        },
                    ],
                    [
                        'attribute' => 'numero_guia',
                        'value' => function($data){
                            return $data->numero_guia;
                        },
                        'footer' => '<strong>TOTAL</strong>',
                    ],
                    /*[
                        'label' => 'Repartidor',
                        'value' => function($data){
                            return $data->ordengrupaldomicilioIdordengrupaldomicilio->repartidorIdrepartidor->apellido.
                            ", ".
                            $data->ordengrupaldomicilioIdordengrupaldomicilio->repartidorIdrepartidor->nombre;
                        }
                    ],*/
                    [
                        'attribute' => 'cantidad',
                        'footer' => $sumaBultos,
                    ],  
                    [
                        'attribute' => 'monto',
                        'label' => 'Rendición común',
                        'value' => function($data){
                            $remitosOrden = GuiaDomicilio::find()->where(['ordengrupaldomicilio_idordengrupaldomicilio' => $data->ordengrupaldomicilio_idordengrupaldomicilio])->orderBy(['sprinter' => SORT_DESC])->all();
                            if($remitosOrden[0][sprinter] != 1)
                                return $data->monto;
                        },
                        'format' => ['decimal', 2],
                        'footer' => number_format ($rendicionComun, 2 , "." ,  "," )
                    ],
                    [
                        'attribute' => 'monto',
                        'label' => 'Rendición sprinter',
                        'value' => function($data){
                            $remitosOrden = GuiaDomicilio::find()->where(['ordengrupaldomicilio_idordengrupaldomicilio' => $data->ordengrupaldomicilio_idordengrupaldomicilio])->orderBy(['sprinter' => SORT_DESC])->all();
                            if($remitosOrden[0][sprinter] == 1) 
                                return $data->monto;
                        },
                        'format' => ['decimal', 2],
                        'footer' => number_format($rendicionSprinter, 2 , "." ,  "," )
                    ],
                ],
                ]); 
        }
        echo "<div class='col-md-12'><h2><strong>TOTAL: ".number_format($model->monto, 2 , "." ,  "," )."</strong></h2>";
    }
    ?>