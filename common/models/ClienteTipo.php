<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%cliente_tipo}}".
 *
 * @property int $idcliente_tipo
 * @property string $nombre
 *
 * @property ClienteHasTipo $clienteHasTipo
 * @property Cliente[] $clienteIdclientes
 */
class ClienteTipo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cliente_tipo}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 60],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcliente_tipo' => Yii::t('app', 'Id Cliente Tipo'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClienteHasTipo()
    {
        return $this->hasOne(ClienteHasTipo::className(), ['clientetipo_idclientetipo' => 'idcliente_tipo']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClienteIdclientes()
    {
        return $this->hasMany(Cliente::className(), ['idcliente' => 'cliente_idcliente'])->viaTable('{{%cliente_has_tipo}}', ['clientetipo_idclientetipo' => 'idcliente_tipo']);
    }
}
