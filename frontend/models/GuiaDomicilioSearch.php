<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\GuiaDomicilio;

/**
 * GuiaDomicilioSearch represents the model behind the search form of `common\models\GuiaDomicilio`.
 */
class GuiaDomicilioSearch extends GuiaDomicilio
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idorden_grupal_domicilio', 'ordengrupaldomicilio_idordengrupaldomicilio', 'cantidad', 'cobrar_sprinter'], 'integer'],
            [['numero_guia', 'guia_tipo', 'empresa_destino', 'localidad_idlocalidad', 'sprinter', 'creado'], 'safe'],
            [['monto'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = GuiaDomicilio::find();
        $query->joinWith(['localidadIdlocalidad as localidad']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idorden_grupal_domicilio' => $this->idorden_grupal_domicilio,
            'monto' => $this->monto,
            'cantidad' => $this->cantidad,
            'ordengrupaldomicilio_idordengrupaldomicilio' => $this->ordengrupaldomicilio_idordengrupaldomicilio,
            'sprinter' => $this->sprinter, 
            'cobrar_sprinter' => $this->cobrar_sprinter,
            'creado' => $this->creado,
        ]);

        $query->andFilterWhere(['like', 'numero_guia', $this->numero_guia])
            ->andFilterWhere(['like', 'guia_tipo', $this->guia_tipo])
            ->andFilterWhere(['like', 'empresa_destino', $this->empresa_destino])
            ->andFilterWhere(['like', 'localidad.nombre', $this->localidad_idlocalidad]);

        return $dataProvider;
    }
}
