<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Tarifario */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tarifario-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="col-md-6"><?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-6"><?= $form->field($model, 'tipo')->dropDownList([ 'Tipo A' => 'Tipo A', 'Tipo B' => 'Tipo B', ], ['prompt' => '']) ?></div>


    <div class="form-group col-md-12">
        <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);?>
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
