<?php

namespace frontend\assets;

use yiister\gentelella\assets\Asset;

/**
 * Main frontend application asset bundle.
 */
class LoginAsset extends Asset
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstro.min.css',
        'css/signin.css'
    ];
    public $js = [
        'js/bootstro.js',
        'js/autogestion.js'
    ];
    public $depends = [
        'yiister\gentelella\assets\ThemeAsset',
        'yiister\gentelella\assets\ExtensionAsset',
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}