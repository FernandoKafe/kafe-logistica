<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\ArrayHelper;

use common\models\OrdenGrupalDomicilio;
use common\models\GuiaDomicilio;
use common\models\TarifarioDomicilio;
use common\models\LiquidacionDomicilioCliente;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionDomicilioCliente */
$this->title = $model->clienteIdcliente->razonsocial.": ".$model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rendición Cliente Domicilio Tipo B'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$ordenesGrupales = OrdenGrupalDomicilio::getAllOrdenesClienteDia($model->cliente_idcliente, $model->fecha_inicio, $model->fecha_fin);
$ordenesGrupalesId = ArrayHelper::getColumn($ordenesGrupales,'idorden_grupal_domicilio');
$remitos = GuiaDomicilio::find()->where(['IN', 'ordengrupaldomicilio_idordengrupaldomicilio', $ordenesGrupalesId]);
$totalRendido = number_format ((LiquidacionDomicilioCliente::getRendicionSprinterSum($remitos->all()) + LiquidacionDomicilioCliente::getRendicionComunSum($remitos->all())), 2 , "." ,  "," );

?>
<div class="liquidacion-view row" style="overflow: hidden;">
    <div class="col-xs-3" style="float: left;">
        <h1>Planilla contra reembolso: <?= $model->clienteIdcliente->razonsocial?></h1>
        <h1>Desde: <?= $model->fecha_inicio?></h1>
        <h1>Hasta: <?= $model->fecha_fin?></h1>
        <h1>CUIT: <?=$model->clienteIdcliente->cuit?></h1>
    </div>
    <div class="col-xs-4" style=" margin: 0 auto 0;">
        <?php echo Html::img('@web/img/logo/LOGO.jpg', ['class' => 'img-responsive', 'width'=>'210', 'style' => 'margin: 0 29% 0;']) ?>
    </div>
    <div class="col-xs-3" style="border: 1px solid #d3d3d3; float:right; margin: 0 15px 0 0;">
            <div class="col-xs-2" style="margin: 20px 0 0 0;">
                <p>Firma: </p>
            </div>
            <div class="col-xs-6" style="margin: 0 0 0 0;">
                <p>________________________________</p>
            </div>
            <div class="col-xs-2">
                <p>Aclaración</p>
            </div>     
            <div class="col-xs-6">
                <p>________________________________</p>
            </div>
            <div class="col-xs-2">
                <p>Fecha</p>
            </div>
            <div class="col-xs-6">
                <p>_______ / _______ / _______</p>
            </div>
            <div class="col-xs-12" style="margin: 35px 0 0 0;">
                    <?= "<div class='col-md-12 tabla-totales'>TOTAL: ".$totalRendido."</div>"; ?>
            </div>
        </div>    
</div>

<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_title">
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php
        $dataProvider = new ActiveDataProvider([
            'query' => $remitos,
            'pagination' => false,
        ]);

        if($model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo B"){
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'options' => ['class' => 'tabla-rend' ],
                'showFooter' => true,
                'columns' => [
                    [
                        'label' => 'Fecha',
                        'value' => function($data){
                            return $data->ordengrupaldomicilioIdordengrupaldomicilio->fecha;
                        },
                    ],
                    [
                        'label' => 'Nº Orden Grupal',
                        'value' => function($data){
                            return $data->ordengrupaldomicilioIdordengrupaldomicilio->numero_orden;
                        },
                    ],
                    [
                        'attribute' => 'empresa_destino',
                        'label' => 'Empresa destino',
                    ],
                    [
                        'attribute' => 'localidad_idlocalidad',
                        'label' => 'Localidad destino',
                        'value' => function($data){
                            if($data->localidad_idlocalidad){
                                return $data->localidadIdlocalidad->nombre.
                                ", ".
                                $data->localidadIdlocalidad->departamentoIddepartamento->nombre.
                                ", ".
                                $data->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                            }else{
                                return "Guía sin localidad";
                            }
                        },
                    ],
                    [
                        'attribute' => 'numero_guia',
                        'value' => function($data){
                            return $data->numero_guia;
                        },
                        'footer' => '<strong>TOTAL</strong>',
                    ],
                    /*[
                        'label' => 'Repartidor',
                        'value' => function($data){
                            return $data->ordengrupaldomicilioIdordengrupaldomicilio->repartidorIdrepartidor->apellido.
                            ", ".
                            $data->ordengrupaldomicilioIdordengrupaldomicilio->repartidorIdrepartidor->nombre;
                        }
                    ],*/
                    [
                        'attribute' => 'cantidad',
                        'footer' => LiquidacionDomicilioCliente::getBultosSum($remitos->all()),
                    ],  
                    [
                        'attribute' => 'monto',
                        'label' => 'Rendición común',
                        'value' => function($data){
                            if($data->sprinter == 0)
                                return $data->monto;
                        },
                        'format' => ['decimal', 2],
                        'footer' => number_format (LiquidacionDomicilioCliente::getRendicionComunSum($remitos->all()), 2 , "." ,  "," )
                    ],
                    [
                        'attribute' => 'monto',
                        'label' => 'Rendición sprinter',
                        'value' => function($data){
                            if(($data->sprinter == 1) && ($data->cobrar_sprinter == 1)) 
                                return $data->monto;
                        },
                        'format' => ['decimal', 2],
                        'footer' => number_format (LiquidacionDomicilioCliente::getRendicionSprinterSum($remitos->all()), 2 , "." ,  "," )
                    ],
                ],
                ]);
        }
    ?>
    </div>
</div>
