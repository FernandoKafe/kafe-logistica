<?php

use \yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\web\View;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;

use common\models\EstadoLiquidacion;
use common\models\Repartidor;

$script = <<< 'SCRIPT'
    $('#liquidacionvinorepartidor-estadoliquidacion_idestadoliquidacion').change(function(){
      if(this.value != 2){
        $('#liquidacionvinorepartidor-total').attr('disabled', 'disabled');
      }else{
        $('#liquidacionvinorepartidor-total').removeAttr('disabled');
      }
    });
SCRIPT;
$this->registerJs($script, View::POS_END);
/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionRepartidorUnica */
/* @var $form yii\widgets\ActiveForm */
$repartidores=ArrayHelper::map(Repartidor::find()->orderBy('apellido')->asArray()->all(), 'idrepartidor', 'nombre');
$estado = ArrayHelper::map(EstadoLiquidacion::find()->all(),'idestado_liquidacion','nombre');
?>

<div class="liquidacion-repartidor-unica-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-3">
        <?php
        $repartidorEntrega = empty($model->repartidorIdrepartidor) ? '' :
        $model->repartidorIdrepartidor->apellido
        .", ".$model->repartidorIdrepartidor->nombre;

        echo $form->field($model, 'repartidor_idrepartidor')->widget(Select2::classname(), [
            'initValueText' => $repartidorEntrega,
            'language' => 'es',
            'options' => ['placeholder' => 'Seleccionar Repartidor ...'],
            'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 3,
            'ajax' => [
                'url' => Url::to(['/repartidor/listado']),
                'dataType' => 'json',
                'data' => new JsExpression('function(params) {return {q:params.term}; }')
            ],
            ],
        ])->label('<a href="../repartidor/create" target="_blank"><i class="fa fa-plus green"></i></a> Repartidor');
        ?>
    </div>

    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-3  col-xs-12">
      <?=
      $form->field($model, 'fecha_inicio')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'aaaa-mm-dd'],
        'pluginOptions' => [
          'autoclose'=>true,
          'format' => 'yyyy-mm-dd'
        ],
        'language' => Yii::$app->language,
        'type' => DatePicker::TYPE_COMPONENT_PREPEND
      ])->label('Desde');
      ?>
    </div>

    <div class="col-md-3  col-xs-12">
      <?=
      $form->field($model, 'fecha_fin')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'aaaa-mm-dd'],
        'pluginOptions' => [
          'autoclose'=>true,
          'format' => 'yyyy-mm-dd',
          'endDate' => "0d"
        ],
        'language' => Yii::$app->language,
        'type' => DatePicker::TYPE_COMPONENT_PREPEND
      ])->label('Hasta');
      ?>
    </div>

    <?php if(isset($idLiquidacionrepartidor)){ ?>   
      <?php
        if($model->estadoliquidacion_idestadoliquidacion == 3){
          $disabledEstado = true;
        }else{
          $disabledEstado = false;
        }

        if($model->estadoliquidacion_idestadoliquidacion != 2){
          $disabledParcial = true;
        }else{
          $disabledParcial = false;
        }
      ?>
      <div class="col-md-3 col-xs-12"><?= $form->field($model, 'estadoliquidacion_idestadoliquidacion')->dropdownList($estado, ['options' => [1 => ['disabled' => $disabledEstado], 2 => ['disabled' => $disabledEstado]]]) ?></div>
    <?php }?>

    <div class="form-group col-md-12">
      <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);?>

      <?php if(isset($idLiquidacionrepartidor)){ ?>
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
      <?php }else{ ?>
        <?= Html::submitButton(Yii::t('app', 'Crear'), ['class' => 'btn btn-success']) ?>
      <?php } ?>
    </div>

    <?php ActiveForm::end(); ?>

</div> 
