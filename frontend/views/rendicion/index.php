<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\RendiconSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$tipo = Yii::$app->getRequest()->getQueryParam('tipo');
if($tipo == 0){
    $this->title = Yii::t('app', 'Contra reembolsos');
}else if($tipo == 1){
    $this->title = Yii::t('app', 'Rendiciones');
}
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><i class="fa fa-file"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2>Listado</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php Pjax::begin(); ?>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>
    <p>
        <?php
        if($tipo == 0){
            echo Html::a(Yii::t('app', 'Crear Contra Reembolso'), ['create', 'tipo' => $tipo], ['class' => 'btn btn-success']); 
        }else if($tipo == 1){
            echo Html::a(Yii::t('app', 'Crear Rendición'), ['create', 'tipo' => $tipo], ['class' => 'btn btn-success']); 
        }?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'idrendicion',
            [
                'attribute' => 'cliente_idcliente',
                'value' => function($data){
                    return $data->clienteIdcliente->razonsocial;
                }
            ],
            'fecha_inicio',
            'fecha_fin',
            [
                'attribute' => 'monto',
                'format' => ['decimal', 2],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Acciones',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => '{view}{update}{delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                        'title' => Yii::t('app', 'Ver'),
                            ]);
                    },
        
                    'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'Actualizar'),
                        ]);
                    },
                    
                    'delete' => function ($url, $model) {
                            return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                        'title' => Yii::t('app', 'Ver'),
                            ]);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                    'title' => Yii::t('app', 'Eliminar'),
                                    'data' => [
                                      'confirm' => Yii::t('app', 'Esta seguro de borrar la rendición?'),
                                      'method' => 'post',
                                  ],
                        ]);
                    },
    
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='view?id='.$model->idrendicion.'&tipo='.$_GET['tipo'];
                        return $url;
                    }
    
                    if ($action === 'update') {
                        $url ='update?id='.$model->idrendicion.'&tipo='.$_GET['tipo'];
                        return $url;
                    }
    
                    if ($action === 'delete') {
                        $url ='delete?id='.$model->idrendicion.'&tipo='.$_GET['tipo'];
                        return $url;
                    }
                }
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
</div>
