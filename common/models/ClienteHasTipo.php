<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%cliente_has_tipo}}".
 *
 * @property int $cliente_idcliente
 * @property int $clientetipo_idclientetipo
 *
 * @property Cliente $clienteIdcliente
 * @property ClienteTipo $clientetipoIdclientetipo
 */
class ClienteHasTipo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cliente_has_tipo}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cliente_idcliente', 'clientetipo_idclientetipo'], 'required'],
            [['cliente_idcliente', 'clientetipo_idclientetipo'], 'integer'],
            [['cliente_idcliente', 'clientetipo_idclientetipo'], 'unique', 'targetAttribute' => ['cliente_idcliente', 'clientetipo_idclientetipo']],
            [['cliente_idcliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['cliente_idcliente' => 'idcliente']],
            [['clientetipo_idclientetipo'], 'exist', 'skipOnError' => true, 'targetClass' => ClienteTipo::className(), 'targetAttribute' => ['clientetipo_idclientetipo' => 'idcliente_tipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cliente_idcliente' => Yii::t('app', 'Cliente'),
            'clientetipo_idclientetipo' => Yii::t('app', 'Tipo de Cliente'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClienteIdcliente()
    {
        return $this->hasOne(Cliente::className(), ['idcliente' => 'cliente_idcliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientetipoIdclientetipo()
    {
        return $this->hasOne(ClienteTipo::className(), ['idcliente_tipo' => 'clientetipo_idclientetipo']);
    }
}
