<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%liquidacion_has_remito}}".
 *
 * @property int $idliquidacion_has_remito
 * @property int $liquidacion_idliquidacion
 * @property int $remito_idremito
 *
 * @property Liquidacion $liquidacionIdliquidacion
 * @property Remito $remitoIdremito
 */
class LiquidacionHasRemito extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%liquidacion_has_remito}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['liquidacion_idliquidacion', 'remito_idremito'], 'required'],
            [['liquidacion_idliquidacion', 'remito_idremito'], 'integer'],
            [['liquidacion_idliquidacion'], 'exist', 'skipOnError' => true, 'targetClass' => Liquidacion::className(), 'targetAttribute' => ['liquidacion_idliquidacion' => 'cliente_idcliente']],
            [['remito_idremito'], 'exist', 'skipOnError' => true, 'targetClass' => Remito::className(), 'targetAttribute' => ['remito_idremito' => 'idremito']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idliquidacion_has_remito' => Yii::t('app', 'Idliquidacion Has Remito'),
            'liquidacion_idliquidacion' => Yii::t('app', 'Liquidacion Idliquidacion'),
            'remito_idremito' => Yii::t('app', 'Remito Idremito'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacionIdliquidacion()
    {
        return $this->hasOne(Liquidacion::className(), ['cliente_idcliente' => 'liquidacion_idliquidacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemitoIdremito()
    {
        return $this->hasOne(Remito::className(), ['idremito' => 'remito_idremito']);
    }
}
