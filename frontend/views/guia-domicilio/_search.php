<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\GuiaDomicilioSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="guia-domicilio-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'idorden_grupal_domicilio') ?>

    <?= $form->field($model, 'numero_guia') ?>

    <?= $form->field($model, 'monto') ?>

    <?= $form->field($model, 'guia_tipo') ?>

    <?= $form->field($model, 'empresa_destino') ?>

    <?php // echo $form->field($model, 'ordengrupaldomicilio_idordengrupaldomicilio') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
