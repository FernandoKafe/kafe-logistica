<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\LiquidacionDomicilioClienteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="liquidacion-domicilio-cliente-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'idliquidacion_domicilio_cliente') ?>

    <?= $form->field($model, 'fecha_inicio') ?>

    <?= $form->field($model, 'fecha_fin') ?>

    <?= $form->field($model, 'total') ?>

    <?= $form->field($model, 'nombre') ?>

    <?php // echo $form->field($model, 'pago_parcial') ?>

    <?php // echo $form->field($model, 'cliente_idcliente') ?>

    <?php // echo $form->field($model, 'estadoliquidacion_idestadoliquidacion') ?>

    <?php // echo $form->field($model, 'liquidacionformapago_idliquidacionformapago') ?>

    <?php // echo $form->field($model, 'cheque_idcheque') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
