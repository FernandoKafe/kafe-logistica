<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TarifarioHasPrecio */

$this->title = Yii::t('app', 'Actualizar precio de Tarifario: ' . $model->tarifarioIdtarifario->nombre, [
    'nameAttribute' => '' . $model->carga_idcarga,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tarifario Has Precios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->carga_idcarga, 'url' => ['view', 'carga_idcarga' => $model->carga_idcarga, 'zona_idzona' => $model->zona_idzona, 'tarifario_idtarifario' => $model->tarifario_idtarifario]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<h1><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario de modificación') ?></h2>
        <div class="clearfix"></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
