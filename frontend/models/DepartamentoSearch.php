<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Departamento;

/**
 * DepartamentoSearch represents the model behind the search form of `common\models\Departamento`.
 */
class DepartamentoSearch extends Departamento
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iddepartamento'], 'integer'],
            [['nombre', 'provincia_idprovincia'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Departamento::find();
        $query->joinWith(['provinciaIdprovincia as provincia']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'iddepartamento' => $this->iddepartamento,
        ]);

        $query->andFilterWhere(['like', 'departamento.nombre', $this->nombre])
        ->andFilterWhere(['like', 'provincia.nombre', $this->provincia_idprovincia]);


        return $dataProvider;
    }
}
