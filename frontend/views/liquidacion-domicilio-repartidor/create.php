<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionDomicilioRepartidor */

$this->title = Yii::t('app', 'Crear Liquidación Domicilio Repartidor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidación Domicilio Repartidor'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-create">
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Encabezado') ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content"></div>
        <h1><?= Html::encode($this->title) ?></h1>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>