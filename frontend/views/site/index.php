<?php

/* @var $this yii\web\View */

$this->title = 'KAFE Logistica';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Bienvenido!</h1>

        <p class="lead">Usted ha accedido satisfactoriamente a la aplicación.</p>
        
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                 
                <h2>Areas</h2>

                <p>
                    Nuestro sistema cuenta con las siguientes areas de consulta
                </p>

                <p><a class="btn btn-default" href="">Documentación &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Funcionalidades</h2>

                <p>Usted podra desarrollar las siguientes actividades</p>

                <p><a class="btn btn-default" href="">Funcionalidades &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Soporte</h2>

                <p>Si usted tiene alguna consulta contactenos.</p>

                <p><a class="btn btn-default" href="">Soporte &raquo;</a></p>
            </div>
        </div>

    </div>

</div>
