<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TarifarioDomicilioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Tarifarios Domicilio');
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><i class="fa fa-file"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2>Listado</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php Pjax::begin(); ?>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Crear Tarifario'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'nombre',
                'label' => 'Tarifario nombre:',
            ],
            'tipo',
            [
                'attribute' => 'primer_bulto',
                'format' => ['decimal', 2],
            ],
            [
                'attribute' => 'bulto_adicional',
                'format' => ['decimal', 2],
            ],
            [
                'attribute' => 'reparto',
                'format' => ['decimal', 2],
            ],
            [
                'attribute' => 'reparto_adicional',
                'format' => ['decimal', 2],
            ],
            [
                'attribute' => 'sprinter',
                'format' => ['decimal', 2],
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div> 
</div> 
