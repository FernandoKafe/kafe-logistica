<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use \yii\helpers\ArrayHelper;
use common\models\EstadoLiquidacion;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\LiquidacionPorcentajeClienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Liquidación por % para Clientes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-porcentaje-cliente-index">
<h1><i class="fa fa-file"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2>Listado</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a(Yii::t('app', 'Previa Liquidación Porcentaje Cliente'), ['create'], ['class' => 'btn btn-success']) ?>

        </p> 

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'moduleId' => 'gridviewk',
            'toolbar' =>  [
                '{export}',
                '{toggleData}',
            ],
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
            ],
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],

                //'idliquidacion_porcentaje_cliente',
                [
                    'attribute' => 'cliente_idcliente',
                    'value' => function($data){
                        return $data->clienteIdcliente->razonsocial;
                    }
                ],
                'periodo',
                'periodo_inicio',
                'periodo_fin',
                [
                    'attribute'=>'estadoliquidacion_idestadoliquidacion',
                    'filter'=>ArrayHelper::map(EstadoLiquidacion::find()->asArray()->all(), 'idestado_liquidacion', 'nombre'),
                    'content' => function($data){
                        return $data->estadoliquidacionIdestadoliquidacion->nombre;
                    }
                ],
                [
                    'attribute' => 'total_cobrar',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'combustible',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'creado',
                    'label' => 'Fecha y Hora creación',
                ],
                //'porcentaje_idporcentaje',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
