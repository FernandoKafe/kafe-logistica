<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionHasRemito */

$this->title = Yii::t('app', 'Update Liquidacion Has Remito: ' . $model->idliquidacion_has_remito, [
    'nameAttribute' => '' . $model->idliquidacion_has_remito,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion Has Remitos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idliquidacion_has_remito, 'url' => ['view', 'id' => $model->idliquidacion_has_remito]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="liquidacion-has-remito-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
