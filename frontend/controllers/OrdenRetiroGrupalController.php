<?php

namespace frontend\controllers;

use Yii;
use common\models\OrdenRetiroGrupal;
use common\models\Remito;
use frontend\models\OrdenRetiroGrupalSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use common\models\LiquidacionHasRemito;

/**
 * OrdenRetiroGrupalController implements the CRUD actions for OrdenRetiroGrupal model.
 */
class OrdenRetiroGrupalController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrdenRetiroGrupal models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdenRetiroGrupalSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrdenRetiroGrupal model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OrdenRetiroGrupal model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OrdenRetiroGrupal();

        if ($model->load(Yii::$app->request->post())) {
            $orden = OrdenRetiroGrupal::find()->where(['numero' => $model->numero])->all();
            if($orden){
                Yii::$app->session->setflash('error', "Ya exíste una orden grupal con número ".$model->numero.".");
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            $previa = OrdenRetiroGrupal::find()->where(['numero' => $model->numero]);
            if($previa){
                $model->creado = date('Y-m-d G:i', time());
                $model->save();
                return $this->redirect([
                    'add-remito',
                    'ordenderetirogrupal_idordenderetirogrupal' => $model->idordenderetirogrupal,
                    'repartidor_idrepartidor' => $model->repartidor_idrepartidor,
                    'fecha' => $model->fecha,
                    'cliente' => $model->cliente_idcliente,
                    'first' => true ]);
            }
            Yii::$app->session->setflash('error', "Ya exíste la orden grupal que se quiere crear.");
            return $this->render('create', [
                'model' => $model,
            ]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing OrdenRetiroGrupal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $remitos = Remito::find()->where(['ordenderetirogrupal_idordenderetirogrupal' => $id])->all();
        $idClienteAnterior = $model->cliente_idcliente;

        if($remitos){
            foreach($remitos as $r){
                if($r->liquidado || $r->liquidado_repartidor){
                    Yii::$app->session->setflash(
                        'error',
                        "La Orden Grupal posee remitos liquidados! No puede editarse");
                        return $this->redirect(Yii::$app->request->referrer);
                }
            }
        }     

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($clienteAnterior != $model->cliente_idcliente && $remitos){
                foreach($remitos as $r){
                    $r->cliente_idcliente = $model->cliente_idcliente;
                    $r->remitofecha = $model->fecha;
                    $r->save();
                }
            }
            return $this->redirect(['add-remito',
            'id' => $model->idordenderetirogrupal,
            'ordenderetirogrupal_idordenderetirogrupal' => $model->idordenderetirogrupal,
            'repartidor_idrepartidor' => $model->repartidor_idrepartidor,
            'fecha' => $model->fecha,
            'orden_especial' => $model->orden_especial,
            'cliente' => $model->cliente_idcliente,
            ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OrdenRetiroGrupal model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $remitos = Remito::find()->where(['ordenderetirogrupal_idordenderetirogrupal' => $id])->all();

        if($remitos){
            foreach($remitos as $r){
                if($r->liquidado || $r->liquidado_repartidor){
                    Yii::$app->session->setflash(
                        'error',
                        "La Orden Grupal posee remitos liquidados! No puede borrarse");
                        return $this->redirect(Yii::$app->request->referrer);
                }
            }
            foreach($remitos as $r){
                    $r->delete();
            }
        }
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the OrdenRetiroGrupal model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrdenRetiroGrupal the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrdenRetiroGrupal::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    static function findAllRemitos($id){
        $dataProvider = new ActiveDataProvider([
            'query' => Remito::find()->where(['ordenderetirogrupal_idordenderetirogrupal' => $id])
        ]);
    }

    /**
     * Add Remitos to an existing OrdenRetiroGrupal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id idcomprobante
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAddRemito()
    {
        $model = new Remito();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['add-remito',
                    'ordenderetirogrupal_idordenderetirogrupal' => $model->ordenderetirogrupal_idordenderetirogrupal,
                    'repartidor_idrepartidor' => $model->repartidor_idrepartidor,
                    'fecha' => $model->remitofecha,
                    'cliente' => $model->cliente_idcliente,
                ]);
        }

        return $this->render('add-remito', [
            'model' => $model,
        ]);
    } 

    /**
     * Add Remitos to an existing OrdenRetiroGrupal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id idcomprobante
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDeleteRemito($id)
    {
        $model = Remito::find()->where(['idremito' => $id])->one();

        if(!($model->liquidado) && !($model->liquidado_repartidor)){
            $model->delete();
            return $this->redirect(Yii::$app->request->referrer);
        }else{
            Yii::$app->session->setFlash('error', 'No puede eliminarse por estar liquidado.');
            return $this->redirect(Yii::$app->request->referrer);
        }

        return $this->render('add-remito', [
            'model' => $model,
        ]);
    } 
}
