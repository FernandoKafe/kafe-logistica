<?php


use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\View;

use \common\models\Cheque;
use \common\models\Cliente;
use \common\models\EstadoLiquidacion;
use \common\models\LiquidacionFormaPago;
use common\models\Porcentaje;

$script = <<< 'SCRIPT'
    $('#liquidacionporcentajecliente-estadoliquidacion_idestadoliquidacion').change(function(){
      if(this.value == 2 || this.value == 4){
        $('#liquidacionporcentajecliente-pago_parcial').removeAttr('disabled');
      }else{
        $('#liquidacionporcentajecliente-pago_parcial').attr('disabled', 'disabled');
      }
    });

    $('#liquidacionporcentajecliente-liquidacionformapago_idliquidacionformapago').change(function(){
      var x = document.getElementById("cheque-div");
      if(this.value == 3){
        x.style.display = "block";
      }else{
        x.style.display = "none";
      }
    });

    $('#liquidacionporcentajecliente-estadoliquidacion_idestadoliquidacion').change(function(){
      var x = document.getElementById("liquidacion_forma_pago");
      if(this.value != 1){
        x.style.display = "block";
      }else{
        x.style.display = "none";
      }
    });
SCRIPT;
$this->registerJs($script, View::POS_END);
/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionPorcentajeCliente */
/* @var $form yii\widgets\ActiveForm */
$idLiquidacion = Yii::$app->getRequest()->getQueryParam('id');
$cliente = ArrayHelper::map(Cliente::find()->all(),'idcliente','razonsocial');
$estado = ArrayHelper::map(EstadoLiquidacion::find()->all(),'idestado_liquidacion','nombre');
$formaPago = ArrayHelper::map(LiquidacionFormaPago::find()->all(),'idliquidacion_forma_pago','nombre');

?>

<div class="liquidacion-porcentaje-cliente-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="hide"><?=  $form->errorSummary($model); ?></div>

    <div class="col-md-3 col-xs-12">
        <?= $form->field($model, 'cliente_idcliente')->widget(Select2::classname(), [
                'initValueText' => $cliente,
                'language' => 'es',
                'options' => ['placeholder' => 'Seleccionar Cliente ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 1,
                    'ajax' => [
                        'url' => Url::to(['/cliente/listado-for-tipo']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) {return {q:params.term, id:null, cliente_tipo:3}; }')
                    ],
                ],
            ])->label('<a href="../cliente/create" target="_blank"><i class="fa fa-plus green"></i></a> Cliente');?>
    </div>

    <div class="col-md-3  col-xs-12"><?= $form->field($model, 'periodo')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-3  col-xs-12">
      <?=
      $form->field($model, 'periodo_inicio')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'aaaa-mm-dd'],
        'pluginOptions' => [
          'autoclose'=>true,
          'format' => 'yyyy-mm-dd'
        ],
        'language' => Yii::$app->language,
        'type' => DatePicker::TYPE_COMPONENT_PREPEND
      ])->label('Desde');
      ?>
    </div>

    <div class="col-md-3  col-xs-12">
      <?=
      $form->field($model, 'periodo_fin')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'aaaa-mm-dd'],
        'pluginOptions' => [
          'autoclose'=>true,
          'format' => 'yyyy-mm-dd',
          'endDate' => "0d"
        ],
        'language' => Yii::$app->language,
        'type' => DatePicker::TYPE_COMPONENT_PREPEND
      ])->label('Hasta');
      ?>
    </div>

    <?php
      if(isset($idLiquidacion)){
        if($model->estadoliquidacion_idestadoliquidacion == 3){
          $disabledEstado = true;
        }else{
          $disabledEstado = false;
        }
        if($model->estadoliquidacion_idestadoliquidacion == 2 || $model->estadoliquidacion_idestadoliquidacion == 4){
          $disabledParcial = false;
        }else{
          $disabledParcial = true;
        }
    ?>
    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'estadoliquidacion_idestadoliquidacion')->dropdownList($estado, ['options' => [1 => ['disabled' => $disabledEstado], 2 => ['disabled' => $disabledEstado]]]) ?></div>

    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'pago_parcial')->textInput(['disabled'=>$disabledParcial])->label('Entrega monto') ?></div>

    <?php
      if($model->estadoliquidacion_idestadoliquidacion != 1){
        $pagoShow = '';
      } else {
        $pagoShow = "style='display:none'";
      }
      echo "<div class='col-md-3 col-xs-12' ".$pagoShow." id='liquidacion_forma_pago'>".$form->field($model, 'liquidacionformapago_idliquidacionformapago')->dropdownList($formaPago)."</div>" ?>

    <?php
      if($model->liquidacionformapago_idliquidacionformapago != 3){
        $chequeShow = "style='display:none'";
      } else {
        $chequeShow = '';
      }

      echo "<div class='cheque-form' ".$chequeShow." id='cheque-div'>"; ?>

      <div class="x_panel">
        <div class="x_title">
          <h2><?= Yii::t('app', 'Formulario cheque') ?></h2>
          <div class="clearfix"></div>
        </div>

        <?php
          if(!isset($cheque)){
            $cheque = new Cheque();
          }

        ?>

        <div class="col-md-6 col-xs-12"><?= $form->field($cheque, 'numero')->textInput(['maxlength' => true]) ?></div>

        <div class="col-md-6 col-xs-12"><?= $form->field($cheque, 'banco')->textInput(['maxlength' => true]) ?></div>

        <div class="col-md-4 col-xs-12"><?= $form->field($cheque, 'monto')->textInput() ?></div>

        <div class="col-md-4 col-xs-12">
        <?=
          $form->field($cheque, 'fecha')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'aaaa-mm-dd'],
            'pluginOptions' => [
              'autoclose'=>true,
              'format' => 'yyyy-mm-dd'
            ],
            'language' => Yii::$app->language,
            'type' => DatePicker::TYPE_COMPONENT_PREPEND
          ]);
        ?>
        </div>

        <div class="col-md-4 col-xs-12"><?= $form->field($cheque, 'estado')->dropDownList([ 'Cobrado' => 'Cobrado', 'Rechazado' => 'Rechazado', ], ['prompt' => '']) ?></div>

        <div class="col-md-12 col-xs-12"><?= $form->field($cheque, 'observacion')->textarea(['rows' => 3]) ?></div>

        </div>
      </div>
    </div>
    <?php } ?>

    <div class="form-group col-md-12">
      <?php
        echo Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);
        if(isset($idLiquidacion)){
          echo Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']);
        }else{
          echo Html::submitButton(Yii::t('app', 'Crear'), ['class' => 'btn btn-success']);
        } ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
