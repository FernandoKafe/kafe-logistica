<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\db\Query;
use frontend\models\LiquidacionHasRemitoSearch;
use common\models\LiquidacionHasRemito;
use common\models\Remito;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;
use common\models\TarifarioHasPrecio;
use common\models\Liquidacion;


/* @var $this yii\web\View */
/* @var $model common\models\Liquidacion */
$idLiquidacion = Yii::$app->getRequest()->getQueryParam('id');


$this->title = "Liquidación para: ".$model->clienteIdcliente->razonsocial." - Periodo: ".$model->periodo;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidación'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-domicilio-cliente-view">


<div class="x_panel">
    <div class="x_title">
        <div class="clearfix"></div>
        <h1>Planilla contra reembolso: <?= $model->clienteIdcliente->razonsocial?></h1>
        <h1>Desde: <?= $model->periodo_inicio?></h1>
        <h1>Hasta: <?= $model->periodo_fin?></h1>
        <h1>CUIT: <?=$model->clienteIdcliente->cuit?></h1>
        </div>
        <p>
            <?
                Html::a(Yii::t('app', 'Crear Contra Reembolso'), ['rendicion/create', 
                /*'fecha_inicio' => $model->periodo_inicio,
                'fecha_fin' => $model->periodo_fin,
                'cliente' => $model->cliente_idcliente,
'tipo' => 0*/],
                 ['class' => 'btn btn-success']) 
            ?>
        </p>
    </div>
</div>
<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_title">
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php
    $remito = Remito::find()->select('remitofecha, total_contrarembolso, facturado_a, tipo_remito, terminal,carga_idcarga, sucursal_idsucursal, empresatransporte_idempresatransporte, cliente_idcliente, numero_remito, numero_guia, rol_sucursal, cantidad_bultos, total_guia, ordenderetirogrupal_idordenderetirogrupal')
    ->where(['>=','remitofecha', $model->periodo_inicio])
    ->andFilterWhere(['<=','remitofecha', $model->periodo_fin])
    ->andFilterWhere(['servicio_idservicio' => 2])
    ->andFilterWhere(['cliente_idcliente' => $model->cliente_idcliente]);
    $dataProvider = new ActiveDataProvider([
        'query' => $remito,
    ]);
    $dataProvider->setSort([
        'attributes' => [
            'remitofecha' => [
                'asc' => ['remitofecha' => SORT_ASC],
                'desc' => ['remitofecha' => SORT_DESC],
                'default' => SORT_ASC
            ],
        ],
        'defaultOrder' => [
            'remitofecha' => SORT_ASC
        ]
    ]);
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    'moduleId' => 'gridviewk',
    'toolbar' =>  [
        '{export}',
        '{toggleData}',
    ],
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
    ],
    'showFooter' => true,
    'columns' => [
        [
            'attribute' => 'remitofecha',
            'label' => 'Fecha',
        ],
        [
            'attribute' => 'numero_remito',
            'label' => 'Remito',
            'value' => function($data){
                if($data->numero_remito){
                    return $data->numero_remito;
                }else{
                    return $data->ordenderetirogrupalIdordenderetirogrupal->numero;
                }
            }
        ],
        [
            'attribute' => 'numero_guia',
            'label' => 'Guía',
            'value' => function($data){
                return $data->numero_guia.' - '.$data->tipo_remito;
            }
        ],
        [
            'label' => 'Destinatario',
            'value' => function($data){
                if($data->rol_sucursal == "Remite"){
                    return $data->clienteIdcliente->razonsocial;
                }else{
                    return $data->sucursalIdsucursal->nombre.", ".$data->sucursalIdsucursal->empresaIdempresa->nombre;
                }
            },
        ],
        [
            'attribute' => 'sucursalIdsucursal',
            'label' => 'Destino',
            'value' => function($data){
                if(isset($data->sucursalIdsucursal->localidad_idlocalidad)){
                    return $data->sucursalIdsucursal->localidadIdlocalidad->nombre.", ".
                    $data->sucursalIdsucursal->localidadIdlocalidad->departamentoIddepartamento->nombre.", ".
                    $data->sucursalIdsucursal->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                }else{
                    return "SIN LOCALIDAD ASIGNADA";
                }
            }
        ],
        [
            'attribute' => 'empresatransporte_idempresatransporte',
            'label' => 'Empresa de transporte',
            'value' => function($data){
                if(isset($data->empresatransporteIdempresatransporte->razonsocial)){
                    return $data->empresatransporteIdempresatransporte->razonsocial;
                }else{
                    return "SIN NOMBRE ASIGNADO";
                }
            },
            'footer' => '<strong>TOTAL</strong>',
        ],
        [
            'attribute' => 'cantidad_bultos',
            'label' => 'Cantidad bultos',
            'value' => function($data){
                return $data->cantidad_bultos;
            },
            'footer' => Liquidacion::getBultosSum($remito->all()),
        ],
        [
            'attribute' => 'volumen',
            'label' => 'Cantidad bultos excedentes',
            'value' => function($data){
                return $data->volumen;
            },
            'footer' => Liquidacion::getVolumenesSum($remito->all()),
        ],
        [
            'attribute' => 'total_contrarembolso',
            'footer' => Liquidacion::getContraSum($remito->all()),
        ]
    ],
    ]); 
    $totalRendido = number_format ((Liquidacion::getContraSum($remito->all())), 2 , "." ,  "," );
    echo "<div class='col-md-12'><h2><strong>TOTAL: ".$totalRendido."</strong></h2>";
    ?>
    

    </div>
</div>