<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\LpcHasRemitoporcentaje */

$this->title = $model->idlpc_has_remitoporcentaje;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Lpc Has Remitoporcentajes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lpc-has-remitoporcentaje-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idlpc_has_remitoporcentaje], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idlpc_has_remitoporcentaje], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idlpc_has_remitoporcentaje',
            'rp_idrp',
            'lpc_idlpc',
        ],
    ]) ?>

</div>
