<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use \yii\helpers\ArrayHelper;
use yii\web\View;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\helpers\Url;
use common\models\Repartidor;
use common\models\EstadoLiquidacion;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionDomicilioRepartidor */
/* @var $form yii\widgets\ActiveForm */
$repartidor = ArrayHelper::map(Repartidor::find()->all(),'idrepartidor','apellido');
$idLiquidacion = Yii::$app->getRequest()->getQueryParam('id');
$estadoLiquidacion = ArrayHelper::map(EstadoLiquidacion::find()->all(),'idestado_liquidacion','nombre');

?>

<div class="liquidacion-domicilio-repartidor-form">

    <?php $form = ActiveForm::begin(); ?> 

    <div class="hide"><?=  $form->errorSummary($model); ?></div>

    <!--<?//= $form->field($model, 'idliquidacion')->textInput() ?>-->
    <div class="col-md-3 col-xs-12">
        <?= $form->field($model, 'repartidor_idrepartidor')->widget(Select2::classname(), [
              'initValueText' => $repartidor,
              'language' => 'es',
              'options' => ['placeholder' => 'Seleccionar Repartidor ...'],
              'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                  'url' => Url::to(['/repartidor/listado']),
                  'dataType' => 'json',
                  'data' => new JsExpression('function(params) {return {q:params.term}; }')
                ],
              ],
            ])->label('<a href="../repartidor/create" target="_blank"><i class="fa fa-plus green"></i></a> Repartidor');
        ?>
    </div>

    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'nombre')->textInput() ?></div>

    <div class="col-md-3  col-xs-12">
    <?=
    $form->field($model, 'fecha_inicio')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'aaaa-mm-dd'],
        'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd'
        ],
        'language' => Yii::$app->language,
        'type' => DatePicker::TYPE_COMPONENT_PREPEND
    ])->label('Desde');
    ?>
    </div>

    <div class="col-md-3  col-xs-12">
    <?=
    $form->field($model, 'fecha_fin')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'aaaa-mm-dd'],
        'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd'
        ],
        'language' => Yii::$app->language,
        'type' => DatePicker::TYPE_COMPONENT_PREPEND
    ])->label('Hasta');
    ?>
    </div>

    <?php
      if($idLiquidacion)
        echo "<div class='col-md-13'".$form->field($model, 'estadoliquidacion_idestadoliquidacion')->dropDownList($estadoLiquidacion)."</div>";
    ?>

    <div class="form-group col-md-12">
      <?php
        echo Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);
        if(isset($idLiquidacion)){
          echo Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']);
        }else{
          echo Html::submitButton(Yii::t('app', 'Crear'), ['class' => 'btn btn-success']);
        } ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
