<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RemitoVino */

$this->title = Yii::t('app', 'Actualizar Remito Vino Nº: ' . $model->idremito_vino, [
    'nameAttribute' => '' . $model->idremito_vino,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Remito Vinos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idremito_vino, 'url' => ['view', 'id' => $model->idremito_vino]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="remito-porcentaje-update">
    <h1><?= $this->title?></h1>
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Formulario de modificación') ?></h2>
            <div class="clearfix"></div>
            </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
