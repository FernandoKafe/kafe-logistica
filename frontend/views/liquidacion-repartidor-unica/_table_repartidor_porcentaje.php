
<?php
use \yii\helpers\ArrayHelper;
use common\models\RemitoPorcentaje;
use common\models\LiquidacionPorcentajeRepartidor;

$remitosPorcentaje = [];
if($liquidacionPorcentaje){

    $remitosClientes = RemitoPorcentaje::find()
    ->select('cliente_idcliente')
    ->joinWith('liquidacionporcentajerepartidorHasRemitos')
    ->where(['lpr_idlpr' => $liquidacionPorcentaje->idliquidacion_porcentaje_repartidor])
    ->groupBy(['cliente_idcliente'])
    ->orderBy(['cliente_idcliente' => SORT_ASC])
    ->all();

    $remitosPorcentaje = RemitoPorcentaje::find()
    ->select('  count(*) as cant_pedidos,
                fecha, 
                porcentaje_entregas as porcentaje,
                cliente_idcliente')
    ->joinWith('liquidacionporcentajerepartidorHasRemitos')
    ->where(['lpr_idlpr' => $liquidacionPorcentaje->idliquidacion_porcentaje_repartidor])
    ->groupBy(['cliente_idcliente', 'fecha' ])
    ->orderBy(['cliente_idcliente' => SORT_ASC, 'porcentaje' => SORT_ASC])
    ->all();

    $remitosPorcentajesValores = RemitoPorcentaje::find()
    ->select('  count(*) as cant_pedidos,
                COUNT(DISTINCT fecha) as reparto_uno,
                sum(dinero_faltante) as dinero_faltante,
                reparto_seleccionado,
                fecha, 
                porcentaje_entregas as porcentaje,
                cliente_idcliente')
    ->joinWith('liquidacionporcentajerepartidorHasRemitos')
    ->where(['lpr_idlpr' => $liquidacionPorcentaje->idliquidacion_porcentaje_repartidor])
    ->groupBy(['cliente_idcliente', 'reparto_seleccionado' ])
    ->orderBy(['cliente_idcliente' => SORT_ASC, 'porcentaje' => SORT_ASC])
    ->all();
}

//echo "<pre>" , var_dump($remitoPorcentaje) , "</pre>";

if($remitosPorcentaje){?>
    <table style="table-layout: fixed; width: 100% ;">
        <thead>
            <tr>
                <th style="width:10%; text-align:center ">Cliente</th>
                <?php for($i = 1; $i <=31; $i++){ ?>
                        <th style="width:4%;  text-align:center "><?= $i ?></th>
                    <?php } ?>
                    <th style="width:5%;  text-align:center">TOTAL</th><!--Resto de celdas de fila cliente-->
            </tr>
        </thead>
        <tbody >
        <?php
        foreach($remitosClientes as $rc){
            $totalPedidos = 0;?>
            <tr style="background-color: #ddd"><!--Fila cliente-->
                <td style="border-right: 1px solid #ccc;width:10%"><strong><?= $rc->clienteIdcliente->razonsocial ?></strong></td><!--Primer celda fila cliente-->
                <?php for($i = 0; $i <= 31; $i++){ ?>
                    <td style="border-right: 1px solid #ccc;width:4%"></td><!--Resto de celdas de fila cliente-->
                <?php } ?>
            </tr>

            <tr style="border-bottom: 1px solid #ccc;">
                <td style="border-right: 1px solid #ccc; width:10%; text-align:center"><strong>Cant. Remitos</strong></td>
                <?php
                for($i = 1; $i <= 32; $i++){ 
                    $flag = 1;
                    $contador = 0;
                    foreach($remitosPorcentaje as $rp){
                        if(($rc->cliente_idcliente == $rp->cliente_idcliente) && ($rp->cant_pedidos)){
                            $date = date('d', strtotime($rp->fecha));
                            $date = intval($date); 
                            if(($rp->cant_pedidos != 0) && ($date == $i)){
                                $totalPedidos += $rp->cant_pedidos;
                                ?>
                                    <td style="border-right: 1px solid #ccc;width:4%; text-align:center"><?= $rp->cant_pedidos?></td>
                            <?php
                                $flag = 0;
                                //break;
                            }  
                        }
                        $contador++;
                    }
                    if($flag){  
                        if($i < 32){?>
                        <td style="border-right: 1px solid #ccc;width:4%"></td>
                        <?php }else{ ?>
                            <td style="border-right: 1px solid #ccc;width:5%; text-align:center"><strong><?= $totalPedidos ?></strong></td>
                        <?php }
                    } 
                }?>
            </tr>
            <tr style="border-bottom: 1px solid #ccc;">
                <td style="border-right: 1px solid #ccc; width:10%; text-align:center"><strong>% promedio diario</strong></td>
                <?php
                for($i = 1; $i <= 32; $i++){ 
                    $flag = 1;
                    $contador = 0;
                    $porcentajeEntrega = 0;
                    $remitos = 0;
                    foreach($remitosPorcentaje as $rp){
                        if(($rc->cliente_idcliente == $rp->cliente_idcliente) && ($rp->cant_pedidos)){
                            $remitos++;
                            $date = date('d', strtotime($rp->fecha));
                            $date = intval($date); 
                            if(($rp->cant_pedidos != 0) && ($date == $i)){
                                $totalPedidos += $rp->cant_pedidos;
                                ?>
                                    <td style="border-right: 1px solid #ccc;width:4%; text-align:center; transform:rotate(45deg)"><?= number_format ($rp->porcentaje, 2 , "." ,  "," )?></td>
                            <?php
                                $flag = 0;
                                //break;
                            }  
                        }
                        $remitos = 0;
                        $contador++;
                    }
                    if($flag){  
                        if($i < 32){?>
                        <td style="border-right: 1px solid #ccc;width:4%"></td>
                        <?php }else{ ?>
                            <td style="border-right: 1px solid #ccc;width:5%; text-align:center">-</td>
                        <?php }
                    } 
                }?>
            </tr>
    <?php   } ?>
        </tbody>
    </table>
    <br>
<?php  
} ?>