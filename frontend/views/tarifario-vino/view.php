<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TarifarioVino */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Tarifario Vinos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarifario-has-precio-view">

<h1>Detalles tarifario vino: <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2>Detalle</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <p>
            <?= Html::a(Yii::t('app', 'Inicio'), ['index'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->idtarifario_vino], ['class' => 'btn btn-success']) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'precio_caja',
                    'format' => ['decimal',2],
                ],
                [
                    'attribute' => 'precio_repartidor',
                    'format' => ['decimal',2],
                ],
                'nombre',
            ],
        ]) ?>

    </div>
</div>
