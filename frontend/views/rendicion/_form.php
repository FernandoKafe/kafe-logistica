<?php

use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Rendicion */
/* @var $form yii\widgets\ActiveForm */
$tipo = Yii::$app->getRequest()->getQueryParam('tipo');
?>

<div class="rendicion-form"> 

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="col-md-4">
        <?php
        if($tipo == 0)
        {
            echo $form->field($model, 'cliente_idcliente')->widget(Select2::classname(), [
                'initValueText' => $cliente,
                'language' => 'es',
                'options' => ['placeholder' => 'Seleccionar Cliente ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 1,
                    'ajax' => [
                    'url' => Url::to(['/cliente/listado-for-tipo']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) {return {q:params.term, id:null, cliente_tipo:1}; }')
                ],
                ],
                ])->label('<a href="../cliente/create" target="_blank"><i class="fa fa-plus green"></i></a> Cliente'); 
        }else if($tipo == 1)
        {
            echo $form->field($model, 'cliente_idcliente')->widget(Select2::classname(), [
                'initValueText' => $cliente,
                'language' => 'es',
                'options' => ['placeholder' => 'Seleccionar Cliente ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 1,
                    'ajax' => [
                    'url' => Url::to(['/cliente/listado-for-tipo']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) {return {q:params.term, id:null, cliente_tipo:2}; }')
                ],
                ],
                ])->label('<a href="../cliente/create" target="_blank"><i class="fa fa-plus green"></i></a> Cliente');
        }
        ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'fecha_inicio')->widget(DatePicker::classname(), [
                'options' => [
                'placeholder' => 'aaaa-mm-dd',
                ],
                'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd',
                'endDate' => "0d"
                ],
                'language' => Yii::$app->language,
                'type' => DatePicker::TYPE_COMPONENT_PREPEND
            ])->label('Desde');
        ?>
    </div>

    <div class="col-md-4">
        <?= $form->field($model, 'fecha_fin')->widget(DatePicker::classname(), [
                'options' => [
                'placeholder' => 'aaaa-mm-dd',
                ],
                'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd',
                'endDate' => "0d"
                ],
                'language' => Yii::$app->language,
                'type' => DatePicker::TYPE_COMPONENT_PREPEND
            ])->label('Hasta');
        ?>
    </div>

    <div class="form-group col-md-12">
        <?= Html::submitButton(Yii::t('app', 'Crear'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
