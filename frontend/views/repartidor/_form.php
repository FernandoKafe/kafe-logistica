<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Zona;
use yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use kartik\date\DatePicker;

use \common\models\Provincia;
use \common\models\Departamento;
use \common\models\Localidad;
use \common\models\Repartidor;


/* @var $this yii\web\View */
/* @var $model common\models\Repartidor */
/* @var $form yii\widgets\ActiveForm */
$dataProvincia=ArrayHelper::map(Provincia::find()->orderBy('nombre')->asArray()->all(), 'idprovincia', 'nombre');
$provincia = new Provincia();
$dataDepartamentos = [];
$departamento = new Departamento();

if($model->localidad_idlocalidad){
  $provincia->idprovincia = $model->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->idprovincia;
  
  $dataDepartamentos =  ArrayHelper::map(Departamento::find()
    ->select(['iddepartamento as id','nombre as name'])
    ->where(['provincia_idprovincia' => $provincia->idprovincia])
    ->orderBy('nombre ASC')
    ->asArray()
    ->all(), 'id', 'name');
  $departamento->iddepartamento = $model->localidadIdlocalidad->departamentoIddepartamento->iddepartamento;
  
  $dataLocalidades = ArrayHelper::map(Localidad::find()
  ->select(['idlocalidad as id','nombre as name'])
  ->where(['departamento_iddepartamento' => $departamento->iddepartamento])
  ->orderBy('nombre ASC')
  ->asArray()
  ->all(), 'id', 'name');
  $localidad = $model->localidadIdlocalidad->nombre;
}
?>

<div class="repartidor-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-4">  
      <?= $form->field($model, 'nombre')->widget(\yii\jui\AutoComplete::classname(), [
          'clientOptions' => [
              'source' => ArrayHelper::getColumn(Repartidor::find()->orderBy(['nombre'=>SORT_ASC])->all(),'nombre'),
          ],
          'options'=>['class' => 'form-control']
      ]) ?>
    </div>

    <div class="col-md-4">
      <?= $form->field($model, 'apellido')->widget(\yii\jui\AutoComplete::classname(), [
          'clientOptions' => [
              'source' => ArrayHelper::getColumn(Repartidor::find()->orderBy(['nombre'=>SORT_ASC])->all(),'apellido'),
          ],
          'options'=>['class' => 'form-control']
      ]) ?>
    </div>

    <div class="col-md-4"><?= $form->field($model, 'dni')->textInput() ?></div>

    <div class="col-md-4  col-xs-12">
      <?=
      $form->field($model, 'nacimiento')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'aaaa-mm-dd'],
        'pluginOptions' => [
          'autoclose'=>true,
          'format' => 'yyyy-mm-dd'
        ],
        'language' => Yii::$app->language,
        'type' => DatePicker::TYPE_COMPONENT_PREPEND
        ])->label('Nacimiento');
      ?>
    </div>  
    
    <div class="col-md-4"><?= $form->field($model, 'nacionalidad')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-4"><?= $form->field($model, 'grupo_sanguineo')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-4"><?= $form->field($model, 'direccion')->textInput(['maxlength' => true]) ?></div>

    <?php

        echo "<div class='col-md-4'>".$form->field($provincia, 'idprovincia')->dropDownList($dataProvincia,
          ['prompt'=>'-Seleccionar-',
            'id' => 'provinciaDestino'])->label('Provincia')."</div>";

        echo  "<div class='col-md-4'>".$form->field($departamento, 'iddepartamento')->widget(DepDrop::classname(), [
          //'data'=> $initLocalidad,
          'data' => $dataDepartamentos,
          'language' => 'es',
          'options' => ['id'=>'departamento'],
          'type' => DepDrop::TYPE_SELECT2,
              'pluginOptions'=>[
              'depends' => ['provinciaDestino'],
              'placeholder' => '- Seleccionar Departamento -',
              'url' => \yii\helpers\Url::to(['direccion/departamento-provincia']),
              'loadingText' => 'Cargando Departamentos...'
            ],
        ])->label('<a href="../departamento/create" target="_blank"><i class="fa fa-plus green"></i></a> Departamento')."</div>";
         echo "<div class='col-md-4'>".$form->field($model, 'localidad_idlocalidad')->widget(DepDrop::classname(), [
          'data'=> $dataLocalidades,
          'language' => 'es',
          'options' => ['id'=>'localidad'],
          'type' => DepDrop::TYPE_SELECT2,
            'pluginOptions'=>[
            'depends' => ['departamento'],
            'placeholder' => '- Seleccionar Localidad -',
            'url' => \yii\helpers\Url::to(['direccion/departamento-localidad']),
            'loadingText' => 'Cargando Localidades...'
          ],
        ])->label('<a href="../localidad/create" target="_blank"><i class="fa fa-plus green"></i></a> Localidad')."</div>";
      ?>

    <div class="col-md-4"><?= $form->field($model, 'hijos')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-4"><?= $form->field($model, 'estado_civil')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-4"><?= $form->field($model, 'cuil')->textInput() ?></div>

    <div class="col-md-4"><?= $form->field($model, 'telefono')->textInput() ?></div>

    <div class="col-md-4"><?= $form->field($model, 'celular')->textInput() ?></div>

    <div class="col-md-4"><?= $form->field($model, 'estudios')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-4"><?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-4"><?= $form->field($model, 'puesto')->textInput(['maxlength' => true]) ?></div>

    <div class="form-group col-md-12">
        <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);?>
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
