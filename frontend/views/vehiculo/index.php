<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\VehiculoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
?>

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
    <div class="x_title">
        <h2>Listado</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Crear Vehículo'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider, 
        'filterModel' => $searchModel,
        'columns' => [
            //'idvehiculo',
            'marca',
            'modelo',
            'patente',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
</div>