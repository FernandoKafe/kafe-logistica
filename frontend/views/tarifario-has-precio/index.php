<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TarifarioHasPrecioSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Preciso de los tarifarios');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tarifario-has-precio-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'tarifario_idtarifario',
                'value' => function($data){
                    return $data->tarifarioIdtarifario->nombre;
                }
            ],
            [
                'attribute' => 'zona_idzona',
                'value' => function($data){
                    return $data->zonaIdzona->nombre;
                }
            ],
            [
                'attribute' => 'carga_idcarga',
                'value' => function($data){
                    return $data->cargaIdcarga->nombre;
                }
            ],
            'precio',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
