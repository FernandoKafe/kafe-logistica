<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\OrdenGrupalVinosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Orden Grupal Vinos');
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><i class="fa fa-copy"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2>Listado</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Crear Orden Grupal Vinos'), ['create'], ['class' => 'btn btn-success']) ?>
    </p> 

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'moduleId' => 'gridviewk',
        'toolbar' =>  [
            '{export}',
            '{toggleData}',
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
        ],
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'idorden_grupal_vinos',
            'numero',
            'fecha',
            [
                'attribute' => 'remito_numero',
                'label' => 'Nº de facturación'
            ],
            [
                'attribute' => 'guia',
                'label' => 'Nº de guía'
            ],
            'empresa',
            [
                'attribute' => 'cliente_idcliente',
                'value' => function($data){
                    return $data->clienteIdcliente->razonsocial;
                }
            ],
            [
                'attribute' => 'repartidor_idrepartidor',
                'value' => function($data){
                    return $data->repartidorIdrepartidor->apellido.", ".$data->repartidorIdrepartidor->nombre;
                }
            ],
            [
                'attribute'=>'liquidacionvinocliente_idliquidacionvinocliente',
                'label' => 'Liquidado Cliente',
                'content' => function($data){
                    if($data->liquidacionvinocliente_idliquidacionvinocliente != null){
                        return "Sí";
                    }else{
                        return "No";
                    }
                },
                'filter' => [0 => "No", 1 => "Sí"],
            ],
            [
                'attribute'=>'liquidacionvinorepartidor_idliquidacionvinorepartidor',
                'label' => 'Liquidado Repartidor',
                'content' => function($data){
                    if($data->liquidacionvinorepartidor_idliquidacionvinorepartidor != null){
                        return "Sí";
                    }else{
                        return "No";
                    }
                },
                'filter' => [0 => "No", 1 => "Sí"],
            ],
            [
                'attribute' => 'creado',
                'label' => 'Fecha y Hora creación',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div> 
</div>
