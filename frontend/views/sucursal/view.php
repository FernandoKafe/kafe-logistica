<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Sucursal */

$this->title = $model->empresaIdempresa->nombre.", ".$model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sucursales'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="servicio-view">
    <h1><i class="fa fa-bullseye"></i> Sucursal: <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2>Detalle</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

    <p>
        <?= Html::a('Inicio', ['index'], ['class' => 'btn btn-primary']);?>
        <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->idsucursal], ['class' => 'btn btn-success']) ?>
        <!--<?/*= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idsucursal], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) */?>-->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            /*[
                'attribute' => 'idsucursal',
                'label' => 'ID Sucursal'
            ],*/
            [
                'attribute' => 'empresaIdempresa.nombre',
                'label' => 'Empresa'
            ],
            'nombre',
            'direccion',
            [
                'attribute' => 'localidadIdlocalidad.nombre',
                'label' => 'Localidad'
            ],
            [
                'attribute' => 'localidad_idlocalidad',
                'label' => 'Departamento',
                'value' => function($model){
                    if(isset($model->localidad_idlocalidad))
                        return $model->localidadIdlocalidad->departamentoIddepartamento->nombre;
                    else
                        return " - ";
                }
            ],
            [
                'attribute' => 'localidad_idlocalidad',
                'label' => 'Provincia',
                'value' => function($model){
                    if(isset($model->localidad_idlocalidad))
                        return $model->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                    else
                        return " - ";
                }
            ]
        ],
    ]) ?>

</div>
</div>
</div>

