<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrdenRetiroGrupal;

/**
 * OrdenRetiroGrupalSearch represents the model behind the search form of `common\models\OrdenRetiroGrupal`.
 */
class OrdenRetiroGrupalSearch extends OrdenRetiroGrupal
{
    //Declaración de nueva variabel
    public $liquidadoRepartidor;
    public $liquidadoCliente;
    
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idordenderetirogrupal', 'liquidadoRepartidor', 'liquidadoCliente'], 'integer'],
            [['fecha', 'cliente_idcliente', 'repartidor_idrepartidor', 'orden_especial', 'numero', 'creado'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrdenRetiroGrupal::find();/*->andWhere([
        'OR',
        '0 < (SELECT COUNT(*) FROM remito WHERE liquidado = 0 AND ordenderetirogrupal_idordenderetirogrupal = orden_retiro_grupal.idordenderetirogrupal)',
        '0 = (SELECT COUNT(*) FROM remito WHERE ordenderetirogrupal_idordenderetirogrupal = orden_retiro_grupal.idordenderetirogrupal)',
        '0 < (SELECT COUNT(*) FROM remito WHERE liquidado_repartidor = 0 AND ordenderetirogrupal_idordenderetirogrupal = orden_retiro_grupal.idordenderetirogrupal)']);

        $query->andWhere('0 < (SELECT COUNT(*) FROM remito WHERE ordenderetirogrupal_idordenderetirogrupal = orden_retiro_grupal.idordenderetirogrupal)');*/

        $query->joinWith(['clienteIdcliente as cliente']);
        $query->joinWith(['repartidorIdrepartidor as repartidor']);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) { 
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($this->liquidadoRepartidor === '1' ){
            $query->andWhere('0 = (SELECT COUNT(*) FROM remito WHERE liquidado_repartidor = 0 AND ordenderetirogrupal_idordenderetirogrupal = orden_retiro_grupal.idordenderetirogrupal)');
        }else if($this->liquidadoRepartidor === '0' ){
            $query->andWhere('0 < (SELECT COUNT(*) FROM remito WHERE liquidado_repartidor = 0 AND ordenderetirogrupal_idordenderetirogrupal = orden_retiro_grupal.idordenderetirogrupal)');
        }
        if($this->liquidadoCliente === '1' ){
            $query->andWhere('0 = (SELECT COUNT(*) FROM remito WHERE liquidado = 0 AND ordenderetirogrupal_idordenderetirogrupal = orden_retiro_grupal.idordenderetirogrupal)');
        }else if($this->liquidadoCliente === '0' ){
            $query->andWhere('0 < (SELECT COUNT(*) FROM remito WHERE liquidado = 0 AND ordenderetirogrupal_idordenderetirogrupal = orden_retiro_grupal.idordenderetirogrupal)');
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idordenderetirogrupal' => $this->idordenderetirogrupal,
            'fecha' => $this->fecha,
            'numero' => $this->numero,
            'orden_especial' => $this->orden_especial,
            'creado' =>  $this->creado,
        ]);

        $query->andFilterWhere(['like', 'cliente.razonsocial', $this->cliente_idcliente]);
        $query->andFilterWhere(['like', 'CONCAT(repartidor.apellido, " - ", repartidor.nombre)', $this->repartidor_idrepartidor]);


        return $dataProvider;
    }
}
