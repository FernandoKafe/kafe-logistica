<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionFormaPago */

$this->title = Yii::t('app', 'Create Liquidacion Forma Pago');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion Forma Pagos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-forma-pago-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
