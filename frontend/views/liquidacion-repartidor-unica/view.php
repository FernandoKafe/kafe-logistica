<?php

use \yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

use common\models\LiquidacionRepartidor;
use common\models\LiquidacionDomicilioRepartidor;
use common\models\LiquidacionPorcentajeRepartidor;
use common\models\LiquidacionVinoRepartidor;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionRepartidorUnica */

function mezclar($a, $b, $c){
    for($i = 0; $i <= count($a); $i++){

    }
}

$this->title = $model->idliquidacion_unica;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion Repartidor Unicas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-repartidor-unica-view">
    <h1>Repartidor: <?= $model->repartidorIdrepartidor->apellido.", ".$model->repartidorIdrepartidor->nombre ?></h1>
    <h1>Periodo: <?= $model->nombre ?></h1>
    <div class="x_panel">
    
        <p>
            <?= Html::a(Yii::t('app', 'Guardar'), ['index'], ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app', 'PDF'), ['report', 'id' => $model->idliquidacion_unica], ['class' => 'btn btn-warning']) ?>

        </p>
    </div>
</div>
<div class="x_panel">
    <div class="x_title">
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'fecha_inicio',
                'fecha_fin',
                [
                    'attribute' => 'estadoliquidacion_idestadoliquidacion',
                    'value' => function($data){
                        return $data->estadoliquidacionIdestadoliquidacion->nombre;
                    }
                ],
                [
                    'attribute' => 'total',
                    'format' => ['decimal', 2], 
                ],
            ],
        ]); 
        
        $liquidacionComun = LiquidacionRepartidor::getAllFromUnica($model->idliquidacion_unica);
        //$liquidacionDomicilio = LiquidacionDomicilioRepartidor::getAllFromUnica($model->idliquidacion_unica);
        //$liquidacionPorcentaje = LiquidacionPorcentajeRepartidor::getAllFromUnica($model->idliquidacion_unica);
        //$liquidacionVinos = LiquidacionVinoRepartidor::getAllFromUnica($model->idliquidacion_unica);
        //echo "comun <pre>" , print_r($liquidacionComun) ,"</pre><br>";
        //echo "Dom <pre>" , print_r($liquidacionDomicilio) , "</pre><br>";
        //echo "% <pre>" , print_r($liquidacionPorcentaje) , "</pre><br>";
        //echo "Vinos <pre>" , print_r($liquidacionVinos) , "</pre><br>";
?>

<?= $this->render('_table_repartidor_comun',['liquidacionComun' => $liquidacionComun])?>
<?= $this->render('_table_resumen_repartidor_comun',['liquidacionComun' => $liquidacionComun])?>

<?php /* $this->render('_table_repartidor_domicilio',['liquidacionDomicilio' => $liquidacionDomicilio, 'model' => $model])?>
<?= $this->render('_table_resumen_repartidor_domicilio',['liquidacionDomicilio' => $liquidacionDomicilio, 'model' => $model])?>

<?//= $this->render('_table_repartidor_porcentaje',['liquidacionPorcentaje' => $liquidacionPorcentaje, 'model' => $model])?>
<?//= $this->render('_table_resumen_repartidor_porcentaje',['liquidacionPorcentaje' => $liquidacionPorcentaje, 'model' => $model])?>

<?= $this->render('_table_repartidor_vino',['liquidacionVinos' => $liquidacionVinos])?>
<?= $this->render('_table_resumen_repartidor_vino',['liquidacionVinos' => $liquidacionVinos]) */?>


