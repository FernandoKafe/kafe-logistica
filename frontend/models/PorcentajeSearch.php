<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Porcentaje;

/**
 * PorcentajeSearch represents the model behind the search form of `common\models\Porcentaje`.
 */
class PorcentajeSearch extends Porcentaje
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idporcentaje'], 'integer'],
            [['cantidad'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Porcentaje::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idporcentaje' => $this->idporcentaje,
            'cantidad' => $this->cantidad,
        ]);

        return $dataProvider;
    }
}
