<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionHasRemito */

$this->title = $model->idliquidacion_has_remito;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion Has Remitos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-has-remito-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->idliquidacion_has_remito], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idliquidacion_has_remito], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idliquidacion_has_remito',
            'liquidacion_idliquidacion',
            'remito_idremito',
        ],
    ]) ?>

</div>
