<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\models\LiquidacionrepartidorHasRemitoSearch;
use common\models\LiquidacionrepartidorHasRemito;
use common\models\Remito;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use common\models\TarifarioHasPrecio;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionRepartidor */
$idLiquidacion = Yii::$app->getRequest()->getQueryParam('id');

$this->title = $model->repartidorIdrepartidor->apellido.", ".$model->repartidorIdrepartidor->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidación Repartidor'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="localidad-view">
    <h1>Repartidor: <?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Detalle') ?></h2>
        <div class="clearfix"></div>
    </div>

    <p>
        <?= Html::a(Yii::t('app', 'Inicio'), ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'PDF'), ['report-summary', 'id' => $model->idliquidacion_repartidor], ['class' => 'btn btn-warning']) ?>
    </p>

        <table>
            <tr>
                <th class="column-title col-md-2 col-xs-8">Día</th>
                <th class="column-title col-md-2 col-xs-8">Cliente</th>
                <th class="column-title col-md-2 col-xs-8">Tipo Cliente</th>
                <th class="column-title col-md-2 col-xs-8">Bultos</th>
                <!--<th scope="col">Comisión reparto</th>-->
                <th class="column-title col-md-2 col-xs-8">Excedentes</th>
                <th class="column-title col-md-2 col-xs-8">Terminal</th>
                <!--<th scope="col">Comisión extra</th>-->
                <!--<th scope="col">Comisión total bultos</th>-->
                <!--<th scope="col">Comisión total volumen</th>-->
            </tr>
            <tbody>
            <?php 
                $fechaAnterior = 0;
                $total_liquidacion = 0;
                $total_bultos = 0;
                $bultos_excedentes = 0;
                $terminal = 0;
                $clienteActual = 0;
                $clienteAnterior = 1;
                $first = 0;
                $sum_bultos = 0;
                $sum_excedentes = 0;
                $sum_terminal = 0;
                foreach ($remitos as $r) {
                    if($clienteAnterior == 1)
                        $clienteAnterior =$r->cliente_idcliente;
                    if($clienteActual != $r->cliente_idcliente)
                        $clienteActual = $r->cliente_idcliente;
                    if(($first != 0) && ($fechaAnterior != $r->remitofecha)) { 
                        
                        $sum_bultos += $total_bultos;
                        $sum_excedentes += $bultos_excedentes;
                        $sum_terminal += $terminal;
                        ?>
                        <tr class="headings">
                            <td aling="center" class="column-title col-md-2 col-xs-8">
                                <?= $fechaAnterior?>
                            </td>
                            <td aling="center" class="column-title col-md-2 col-xs-8">
                                <?= $objetoCliente->razonsocial ?>
                            </td>
                            <td aling="center" class="column-title col-md-2 col-xs-8">
                                <?= $objetoCliente->tarifarioIdtarifario->tipo ?>
                            </td>
                            <td aling="center" class="column-title col-md-2 col-xs-8">
                                <?= $total_bultos;?>
                            </td>
                            <td aling="center" class="column-title col-md-2 col-xs-8">
                                <?= $bultos_excedentes; ?>
                            </td>
                            <td aling="center" class="column-title col-md-2 col-xs-8">
                                <?= $terminal;?>
                            </td>
                        </tr>   
            <?php   }
                    if($fechaAnterior != $r->remitofecha)
                    { 
                        $fechaAnterior = 0;
                        $total_liquidacion = 0;
                        $total_bultos = 0;
                        $bultos_excedentes = 0;
                        $terminal = 0;    
                    }
                    if($r->terminal == 0){
                        $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, $r->sucursalIdsucursal->localidadIdlocalidad->zonaHasLocalidad->zona_idzona);
                    } else {
                        $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, 5);
                    }
                    // Entra al if si el cliente es de Tipo A, o si el Cliente es de Tipo B y son remitos de distintas fechas, y si el cliente del remito es el mismo que el cliente actual
                    if((($r->clienteIdcliente->tarifarioIdtarifario->tipo == 'Tipo A') || (($r->clienteIdcliente->tarifarioIdtarifario->tipo == 'Tipo B') && ($r->remitofecha != $fecha_anterior))) && ($r->cliente_idcliente == $clienteActual)){
                        if($r->terminal == 1)
                        {
                            $terminal += $r->cantidad_bultos;
                            $total_bultos++;
                        }
                        else
                        {
                            $total_bultos++;
                        }
                        $bultos_excedentes += ($r->volumen + $r->cantidad_bultos - 1);
                        $fechaAnterior = $r->remitofecha;
                        $total += $tarHasPrecio->precio_reparto;
                        $total += (($r->cantidad_bultos + $r->volumen - 1)  * $tarHasPrecio->precio_reparto_adicional);
                    }
                    else if((($r->clienteIdcliente->tarifarioIdtarifario->tipo == 'Tipo B') && ($r->remitofecha == $fecha_anterior)))
                    {
                        $bultos_excedentes += ($r->volumen + $r->cantidad_bultos);
                        $fechaAnterior = $r->remitofecha;
                        $total += $tarHasPrecio->precio_reparto;
                        $total += (($r->cantidad_bultos + $r->volumen) * $tarHasPrecio->precio_reparto_adicional);
                    }
                    $clienteAnterior = $clienteActual;
                    $objetoCliente = $r->clienteIdcliente;
                    $first = 1;
                } ?>
                <tr class="headings">
                    <td aling="center" class="column-title col-md-2 col-xs-8">
                        <?= $fechaAnterior?>
                    </td>
                    <td aling="center" class="column-title col-md-2 col-xs-8">
                        <?= $objetoCliente->razonsocial ?>
                    </td>
                    <td aling="center" class="column-title col-md-2 col-xs-8">
                        <?= $objetoCliente->tarifarioIdtarifario->tipo ?>
                    </td>
                    <td aling="center" class="column-title col-md-2 col-xs-8">
                        <?= $total_bultos;?>
                    </td>
                    <td aling="center" class="column-title col-md-2 col-xs-8">
                        <?= $bultos_excedentes; ?>
                    </td>
                    <td aling="center" class="column-title col-md-2 col-xs-8">
                        <?= $terminal;?>
                    </td>
                </tr>   
            </tbody>
        </table>
        <div class="x_title">
            <div class="clearfix"></div>
        </div>
        <h2>Cantidad de Bultos: <?= $sum_bultos ?> </h2>
        <h2>Cantidad de Excedentes: <?= $sum_excedentes ?> </h2>
        <h2>Cantidad a Terminal: <?= $sum_terminal ?> </h2>

        <div class="x_title">
            <div class="clearfix"></div>
        </div>
        <h3><strong>
            <?= "TOTAL $ ".$total ?>
        </strong></h3>
</div>
</div>