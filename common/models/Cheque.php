<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%cheque}}".
 *
 * @property int $idcheque
 * @property string $numero
 * @property string $banco
 * @property double $monto
 * @property string $observacion
 * @property string $fecha
 * @property string $estado
 *
 * @property Liquidacion[] $liquidacions
 * @property LiquidacionPorcentajeCliente[] $liquidacionPorcentajeClientes
 * @property LiquidacionVinoCliente[] $liquidacionVinoClientes
 */
class Cheque extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%cheque}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['monto'], 'number'],
            [['observacion', 'estado'], 'string'],
            [['fecha'], 'safe'],
            [['numero', 'banco'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idcheque' => Yii::t('app', 'Idcheque'),
            'numero' => Yii::t('app', 'Numero'),
            'banco' => Yii::t('app', 'Banco'),
            'monto' => Yii::t('app', 'Monto'),
            'observacion' => Yii::t('app', 'Observacion'),
            'fecha' => Yii::t('app', 'Fecha'),
            'estado' => Yii::t('app', 'Estado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacions()
    {
        return $this->hasMany(Liquidacion::className(), ['cheque_idcheque' => 'idcheque']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacionPorcentajeClientes()
    {
        return $this->hasMany(LiquidacionPorcentajeCliente::className(), ['cheque_idcheque' => 'idcheque']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacionVinoClientes()
    {
        return $this->hasMany(LiquidacionVinoCliente::className(), ['cheque_idcheque' => 'idcheque']);
    }
}
