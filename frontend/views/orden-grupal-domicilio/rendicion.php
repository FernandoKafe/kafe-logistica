<?php

use yii\data\ActiveDataProvider;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use yii\helpers\Html;

use common\models\GuiaDomicilio;

/* @var $this yii\web\View */
/* @var $model common\models\OrdenGrupalDomicilio */
$idOrdenGrupal = Yii::$app->getRequest()->getQueryParam('idOrdenGrupal');
if(!isset($idOrdenGrupal)){
    $idOrdenGrupal = Yii::$app->getRequest()->getQueryParam('id'); 
}

$this->title = $model->idorden_grupal_domicilio;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orden Grupal Domicilios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orden-grupal-vinos-view">
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Detalle') ?></h2>
            <div class="clearfix"></div>
        </div>

        <p>
            <?= Html::a('Inicio', ['index'], ['class' => 'btn btn-primary']);?>
            <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->idorden_grupal_domicilio], ['class' => 'btn btn-success']) ?>
            <?= Html::a('Rendición díaria', ['rendicion'], ['class' => 'btn btn-primary']);?>

        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                //'idorden_grupal_domicilio',
                'numero_orden',
                'fecha',
                [
                    'attribute' => 'liquidacioncliente_idliquidacioncliente',
                    'label' => 'Liquidado cliente',
                    'value' => function($data){
                        if($data->liquidacioncliente_idliquidacioncliente){
                            return "Si";
                        }else{
                            return "No";
                        }
                    }
                ],
                [
                    'attribute' => 'liquidacionrepartidor_idliquidacionrepartidor',
                    'label' => 'Liquidado repartidor',
                    'value' => function($data){
                        if($data->liquidacionrepartidor_idliquidacionrepartidor){
                            return "Si";
                        }else{
                            return "No";
                        }
                    }
                ],
                [
                    'attribute' => 'cliente_idcliente',
                    'value' => function($data){
                        return $data->clienteIdcliente->razonsocial;
                        }
                ],
                [
                    'attribute' => 'repartidor_idrepartidor',
                    'value' => function($data){
                        return $data->repartidorIdrepartidor->apellido.", ".$data->repartidorIdrepartidor->nombre;
                        }
                ],
            ],
        ]) ?>

    </div>
</div>

<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Remitos asociados');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php

    $dataProvider = new ActiveDataProvider([
        'query' => GuiaDomicilio::find()->where(['ordengrupaldomicilio_idordengrupaldomicilio' => $idOrdenGrupal])
    ]);

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'moduleId' => 'gridviewk',
        'toolbar' =>  [
            '{export}',
            '{toggleData}',
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
        ],
        'columns' => [
            [
                'label' => 'Nº Orden Grupal',
                'value' => function($data){
                    return $data->ordengrupaldomicilioIdordengrupaldomicilio->numero_orden;
                }
            ],
            'cantidad',
            'empresa_destino',
            [
                'attribute' => 'localidad_idlocalidad',
                'value' => function($data){
                    return $data->localidadIdlocalidad->nombre.", ".$data->localidadIdlocalidad->departamentoIddepartamento->nombre.", ".$data->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                }
            ],
            'numero_guia',
            'guia_tipo',
            [
                'attribute' => 'monto',
                'format' => ['decimal',2],
            ],
            [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Acciones',
            'headerOptions' => ['style' => 'color:#337ab7'],
            'template' => '{view}{update}{delete}',
            'buttons' => [
              'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'Ver'),
                                'target' => '_blank',
                    ]);
              },

              'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'Actualizar'),
                                'target' => '_blank',
                    ]);
                },

                'update' => function ($url, $model) {
                      return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                  'title' => Yii::t('app', 'Eliminar'),
                                  'target' => '_blank',
                      ]);
                  },

            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    $url ='../guia-domicilio/view?id='.$model->idorden_grupal_domicilio;
                    return $url;
                }

                if ($action === 'update') {
                    $url ='../guia-domicilio/update?id='.$model->idorden_grupal_domicilio;
                    return $url;
                }

                if ($action === 'delete') {
                    $url ='../guia-domicilio/delete?id='.$model->idorden_grupal_domicilio;
                    return $url;
                }
            }
        ],
        ],
    ]); ?>
    </div>
</div>
