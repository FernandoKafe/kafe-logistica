<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%orden_grupal_domicilio}}".
 *
 * @property int $idorden_grupal_domicilio
 * @property string $fecha
 * @property string $numero_orden
 * @property int $borrado
 * @property int $liquidacioncliente_idliquidacioncliente
 * @property int $liquidacionrepartidor_idliquidacionrepartidor
 * @property int $cliente_idcliente
 * @property int $repartidor_idrepartidor  
 * @property string $creado
 * @property double $precio_reparto 
 * @property double $precio_reparto_adicional 
 * @property double $precio_sprinter 
 * @property double $precio_bulto 
 * @property double $precio_bulto_adicional 
 * @property string $tipo_cliente 
 *
 * @property GuiaDomicilio[] $guiaDomicilios
 * @property Cliente $clienteIdcliente
 * @property Repartidor $repartidorIdrepartidor
 * @property LiquidacionDomicilioCliente $liquidacionclienteIdliquidacioncliente
 * @property LiquidacionDomicilioRepartidor $liquidacionrepartidorIdliquidacionrepartidor
 */
class OrdenGrupalDomicilio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%orden_grupal_domicilio}}'; 
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'numero_orden', 'cliente_idcliente', 'repartidor_idrepartidor'], 'required'],
            [['fecha', 'creado'], 'safe'],
            [['precio_reparto', 'precio_reparto_adicional', 'precio_sprinter', 'precio_bulto', 'precio_bulto_adicional'], 'number'], 
            [['borrado', 'liquidacioncliente_idliquidacioncliente', 'liquidacionrepartidor_idliquidacionrepartidor', 'cliente_idcliente', 'repartidor_idrepartidor'], 'integer'],
            [['numero_orden'], 'string', 'max' => 60],
            [['tipo_cliente'], 'string', 'max' => 45], 
            [['cliente_idcliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['cliente_idcliente' => 'idcliente']],
            [['liquidacioncliente_idliquidacioncliente'], 'exist', 'skipOnError' => true, 'targetClass' => LiquidacionDomicilioCliente::className(), 'targetAttribute' => ['liquidacioncliente_idliquidacioncliente' => 'idliquidacion_domicilio_cliente']], 
		    [['liquidacionrepartidor_idliquidacionrepartidor'], 'exist', 'skipOnError' => true, 'targetClass' => LiquidacionDomicilioRepartidor::className(), 'targetAttribute' => ['liquidacionrepartidor_idliquidacionrepartidor' => 'idliquidacion_domicilio_repartidor']],
            [['repartidor_idrepartidor'], 'exist', 'skipOnError' => true, 'targetClass' => Repartidor::className(), 'targetAttribute' => ['repartidor_idrepartidor' => 'idrepartidor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idorden_grupal_domicilio' => Yii::t('app', 'Idorden Grupal Domicilio'),
            'fecha' => Yii::t('app', 'Fecha'),
            'numero_orden' => Yii::t('app', 'Nº Orden'),
            'liquidado_cliente' => Yii::t('app', 'Liquidado Cliente'),
            'liquidado_repartidor' => Yii::t('app', 'Liquidado Repartidor'),
            'borrado' => Yii::t('app', 'Borrado'),
            'cliente_idcliente' => Yii::t('app', 'Cliente'),
            'repartidor_idrepartidor' => Yii::t('app', 'Repartidor'),
            'creado' => Yii::t('app', 'Creado'),
            'precio_reparto' => Yii::t('app', 'Precio Reparto'), 
            'precio_reparto_adicional' => Yii::t('app', 'Precio Reparto Adicional'), 
            'precio_sprinter' => Yii::t('app', 'Precio Sprinter'), 
            'precio_bulto' => Yii::t('app', 'Precio Bulto'), 
            'precio_bulto_adicional' => Yii::t('app', 'Precio Bulto Adicional'),
            'tipo_cliente' => Yii::t('app', 'Tipo Cliente'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacionrepartidorIdliquidacionrepartidor()
	{
		return $this->hasOne(LiquidacionDomicilioRepartidor::className(), ['idliquidacion_domicilio_repartidor' => 'liquidacionrepartidor_idliquidacionrepartidor']);
	}
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacionclienteIdliquidacioncliente()
	{
		return $this->hasOne(LiquidacionDomicilioCliente::className(), ['idliquidacion_domicilio_cliente' => 'liquidacioncliente_idliquidacioncliente']);
	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGuiaDomicilios()
    {
        return $this->hasMany(GuiaDomicilio::className(), ['ordengrupaldomicilio_idordengrupaldomicilio' => 'idorden_grupal_domicilio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClienteIdcliente()
    {
        return $this->hasOne(Cliente::className(), ['idcliente' => 'cliente_idcliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepartidorIdrepartidor()
    {
        return $this->hasOne(Repartidor::className(), ['idrepartidor' => 'repartidor_idrepartidor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getAllOrdenesClienteFecha($clienteId, $fechaInicio, $fechaFin){
        return self::find()->where(['cliente_idcliente' => $clienteId])
        ->andWhere(['>=', 'fecha', $fechaInicio])
        ->andWhere(['<=', 'fecha', $fechaFin])
        ->andWhere(['liquidacioncliente_idliquidacioncliente' => null])
        ->all();
    }

    /**
     * @return \yii\db\AvtiveQuery 
     */
    static public function updateAllOrdenesClienteFecha($clienteId, $fechaInicio, $fechaFin, $idLiquidacion){

        $params = [':liquidacion' => $idLiquidacion, ':cliente' => $clienteId, ':initDate' => $fechaInicio, ':endDate' => $fechaFin, ':estadoA' => null];
        Yii::$app->db->createCommand('UPDATE orden_grupal_domicilio SET liquidacioncliente_idliquidacioncliente=:liquidacion WHERE cliente_idcliente=:cliente AND fecha>=:initDate  AND fecha<=:endDate AND liquidacioncliente_idliquidacioncliente is :estadoA')->bindValues($params)->execute();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getAllOrdenesRepartidorFecha($repartidorId, $fechaInicio, $fechaFin){
        return self::find()->where(['repartidor_idrepartidor' => $repartidorId])
        ->andFilterWhere(['>=', 'fecha', $fechaInicio])
        ->andFilterWhere(['<=', 'fecha', $fechaFin])
        ->andWhere(['liquidacionrepartidor_idliquidacionrepartidor' => null])
        ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getAllOrdenesClienteFechaLiquidados($clienteId, $fechaInicio, $fechaFin, $idLiquidacion){
        return self::find()
        ->where(['cliente_idcliente' => $clienteId])
        ->andFilterWhere(['>=', 'fecha', $fechaInicio])
        ->andFilterWhere(['<=', 'fecha', $fechaFin])
        ->andWhere(['liquidacioncliente_idliquidacioncliente' => $idLiquidacion])
        ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getAllOrdenesRepartidorFechaLiquidados($repartidorId, $fechaInicio, $fechaFin, $idLiquidacion){
        return self::find()->where(['repartidor_idrepartidor' => $repartidorId])
        ->andFilterWhere(['>=', 'fecha', $fechaInicio])
        ->andFilterWhere(['<=', 'fecha', $fechaFin])
        ->andWhere(['liquidacionrepartidor_idliquidacionrepartidor' => $idLiquidacion])
        ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getAllOrdenesRepartidorFechaLiquidadosCliente($repartidorId, $fechaInicio, $fechaFin, $idLiquidacion){
        return self::find()->where(['repartidor_idrepartidor' => $repartidorId])
        ->andFilterWhere(['>=', 'fecha', $fechaInicio])
        ->andFilterWhere(['<=', 'fecha', $fechaFin])
        ->andWhere(['liquidacionrepartidor_idliquidacionrepartidor' => $idLiquidacion])
        ->groupBy(['cliente_idcliente'])
        ->all();
    }

    /**
     * @return \yii\db\ActiveQuery 
     */
    public static function getAllOrdenesClienteDia($clienteId, $fechaInicio, $fechaFin){
            return self::find()->where(['cliente_idcliente' => $clienteId])
            ->andWhere(['>=','fecha', $fechaInicio])
            ->andWhere(['<=','fecha', $fechaFin])
            ->all();
    }
}
