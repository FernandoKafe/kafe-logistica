<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RemitoPorcentaje */

$this->title = Yii::t('app', 'Crear Remito por Porcentaje');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Remito Porcentajes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="remito-porcentaje-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario') ?></h2>
        <div class="clearfix"></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div> 
</div> 
