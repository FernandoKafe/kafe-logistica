<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Producto */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Productos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="producto-view">
    <h1>Producto: <?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Detalle') ?></h2>
        <div class="clearfix"></div>
    </div>
    <p>
        <?= Html::a('Inicio', ['index'], ['class' => 'btn btn-primary']);?>
        <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->idproducto], ['class' => 'btn btn-success']) ?>
        <!--<?/*= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->idproducto], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) */?>-->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idproducto',
            'nombre',
        ],
    ]) ?>

</div>
</div>
