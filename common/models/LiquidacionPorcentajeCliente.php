<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%liquidacion_porcentaje_cliente}}".
 *
 * @property int $idliquidacion_porcentaje_cliente
 * @property string $periodo
 * @property double $total_rendido
 * @property double $neto_sin_iva
 * @property double $combustible
 * @property double $comision
 * @property double $subtotal
 * @property double $iva
 * @property double $total_real
 * @property double $total_cobrar
 * @property double $pago_parcial
 * @property string $periodo_inicio
 * @property string $periodo_fin
 * @property int $cliente_idcliente
 * @property int $estadoliquidacion_idestadoliquidacion
 * @property int $cheque_idcheque
 * @property int $liquidacionformapago_idliquidacionformapago
 * @property double $pallets
 * @property string $creado
 * @property int $confirmada
 * @property string $fecha_pago
 * @property int $pedidos 
 * @property double $total_facturado 
 * @property double $total_devuelto
 * @property double $total_faltante 
 * @property double $total_rendido_al_3 
 * @property double $total_rendido_al_5 
 *
 * @property Cheque $chequeIdcheque
 * @property Cliente $clienteIdcliente
 * @property EstadoLiquidacion $estadoliquidacionIdestadoliquidacion
 * @property LiquidacionFormaPago $liquidacionformapagoIdliquidacionformapago
 * @property Porcentaje $porcentajeIdporcentaje
 * @property LpcHasRemitoporcentaje[] $lpcHasRemitoporcentajes
 */
class LiquidacionPorcentajeCliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%liquidacion_porcentaje_cliente}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['periodo', 'cliente_idcliente'], 'required'],
            [['total_rendido', 'neto_sin_iva', 'combustible', 'comision', 'subtotal', 'iva', 'total_real', 'total_cobrar', 'pago_parcial', 'pallets', 'total_facturado', 'total_devuelto', 'total_faltante', 'total_rendido_al_3', 'total_rendido_al_5'], 'number'],
            [['periodo_inicio', 'periodo_fin', 'fecha_pago'], 'safe'],
            [['cliente_idcliente', 'estadoliquidacion_idestadoliquidacion', 'cheque_idcheque', 'liquidacionformapago_idliquidacionformapago', 'confirmada'], 'integer'],
            [['periodo'], 'string', 'max' => 100],
            [['cliente_idcliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['cliente_idcliente' => 'idcliente']],
            [['cheque_idcheque'], 'exist', 'skipOnError' => true, 'targetClass' => Cheque::className(), 'targetAttribute' => ['cheque_idcheque' => 'idcheque']], 
		    [['estadoliquidacion_idestadoliquidacion'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoLiquidacion::className(), 'targetAttribute' => ['estadoliquidacion_idestadoliquidacion' => 'idestado_liquidacion']], 
	        [['liquidacionformapago_idliquidacionformapago'], 'exist', 'skipOnError' => true, 'targetClass' => LiquidacionFormaPago::className(), 'targetAttribute' => ['liquidacionformapago_idliquidacionformapago' => 'idliquidacion_forma_pago']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idliquidacion_porcentaje_cliente' => Yii::t('app', 'Idliquidacion Porcentaje Cliente'),
            'periodo' => Yii::t('app', 'Periodo'),
            'total_rendido' => Yii::t('app', 'TOTAL Rendido'),
            'neto_sin_iva' => Yii::t('app', 'Neto Sin IVA'),
            'combustible' => Yii::t('app', 'Combustible'),
            'comision' => Yii::t('app', 'Comision'),
            'subtotal' => Yii::t('app', 'Subtotal'),
            'iva' => Yii::t('app', 'IVA'),
            'total_real' => Yii::t('app', 'TOTAL Real'),
            'total_cobrar' => Yii::t('app', 'TOTAL Cobrar'),
            'periodo_inicio' => Yii::t('app', 'Periodo Inicio'),
            'periodo_fin' => Yii::t('app', 'Periodo Fin'),
            'cliente_idcliente' => Yii::t('app', 'Cliente'),
            'estadoliquidacion_idestadoliquidacion' => Yii::t('app', 'Estado'),
		    'cheque_idcheque' => Yii::t('app', 'Cheque'),
            'liquidacionformapago_idliquidacionformapago' => Yii::t('app', 'Forma de pago'),
            'pago_parcial' => Yii::t('app', 'Pago parcial'),
            'pallets' => Yii::t('app', 'Pallets'),
            'creado' => Yii::t('app', 'Creado'),
            'confirmada' => Yii::t('app', 'Confirmada'),
            'fecha_pago' => Yii::t('app', 'Fecha de pago'),
            'pedidos' => Yii::t('app', 'Pedidos'),
            'total_facturado' => Yii::t('app', 'TOTAL FACTURADO'),
            'total_devuelto' => Yii::t('app', 'TOTAL DEVUELTO'),
            'total_faltante' => Yii::t('app', 'TOTAL FALTANTE'),
            'total_rendido_al_3' => Yii::t('app', 'Rendido al 3.5'),
            'total_rendido_al_5' => Yii::t('app', 'Rendido al 5.5'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChequeIdcheque()
    {
        return $this->hasOne(Cheque::className(), ['idcheque' => 'cheque_idcheque']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClienteIdcliente()
    {
        return $this->hasOne(Cliente::className(), ['idcliente' => 'cliente_idcliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoliquidacionIdestadoliquidacion()
    {
        return $this->hasOne(EstadoLiquidacion::className(), ['idestado_liquidacion' => 'estadoliquidacion_idestadoliquidacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacionformapagoIdliquidacionformapago()
    {
        return $this->hasOne(LiquidacionFormaPago::className(), ['idliquidacion_forma_pago' => 'liquidacionformapago_idliquidacionformapago']); 
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLpcHasRemitoporcentajes()
    {
        return $this->hasMany(LpcHasRemitoporcentaje::className(), ['lpc_idlpc' => 'idliquidacion_porcentaje_cliente']);
    }

    public static function getSumFaltante($remitos){
        $suma = 0;
        foreach($remitos as $r){
            $suma += $r->dinero_faltante;
        }
        return $suma;
    }

    public static function getSumDineroDevuelto($remitos){
        $suma = 0;
        foreach($remitos as $r){
            $suma += $r->dinero_devuelto;
        }
        return $suma;
    }

    public static function getSumDineroRendido($remitos){
        $suma = 0;
        foreach($remitos as $r){
            $suma += $r->dinero_rendido;
        }
        return $suma;
    }

    public static function getSumDineroFacturado($remitos){
        $suma = 0;
        foreach($remitos as $r){
            $suma += $r->dinero_facturado;
        }
        return $suma;
    }

    public static function getSumCombustible($remitos){
        $suma = 0;
        foreach($remitos as $r){
            $suma += $r->combustible;
        }
        return $suma;
    }

    public static function getSumPedidos($remitos){
        $suma = 0;
        foreach($remitos as $r){
            $suma += $r->cant_pedidos;
        }
        return $suma;
    }
}
