<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Repartidor */

$this->title = Yii::t('app', 'Actualizar Repartidor: ' . $model->apellido.", ".$model->nombre, [
    'nameAttribute' => '' . $model->idrepartidor,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Repartidores'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idrepartidor, 'url' => ['view', 'id' => $model->idrepartidor]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Actualizar');
?>
<div class="repartidor-create">
<h1><i class="fa fa-download"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario') ?></h2>
        <div class="clearfix"></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
