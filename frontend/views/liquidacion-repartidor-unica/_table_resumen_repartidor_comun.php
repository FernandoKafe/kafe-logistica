<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-03-26
 * Time: 12:50
 */
use common\models\Remito;

$arrayTodoBultos = Remito::getCantidadRemitosLiquidacionComun($liquidacionComun->idliquidacion_repartidor);

if($arrayTodoBultos) {
    ?>

    <table style="table-layout: fixed ;" cellspacing="0">
        <tbody>
        <tr>
            <td style='border: solid 1px #ccc; text-align:center; padding: 5px;' colspan=6><strong>RESUMEN CLIENTES
                    COMUNES</strong></td>
        </tr>
        <tr>
            <td style='border: solid 1px #ccc; padding: 5px;'><strong>Cliente</strong></td>
            <td style='border: solid 1px #ccc; padding: 5px;'><strong>Bultos</strong></td>
            <td style='border: solid 1px #ccc;padding: 5px;'><strong>Exc</strong></td>
            <td style='border: solid 1px #ccc; padding: 5px;'><strong>Terminal</strong></td>
            <td style='border: solid 1px #ccc; padding: 5px;'><strong>Exc Terminal</strong></td>
            <td style='border: solid 1px #ccc;padding: 5px;'><strong>TOTAL</strong></td>
        </tr>
        <?php
        $auxCliente = null;
        $bultosCliente = inicioBultos();
        $totales = inicioTotales();
        foreach ($arrayTodoBultos as $ar) {
            if ($auxCliente != null && $auxCliente->idcliente != $ar->cliente_idcliente) {
                $totales = printRow($bultosCliente, $auxCliente, $totales);
                $bultosCliente = inicioBultos();
            }
            $auxCliente = $ar->clienteIdcliente;
            if ($ar->cantidad_bultos > 0) {
                $bultosCliente['bultos'][] = ['precio' => $ar->precio_reparto, 'cantidad' => $ar->cantidad_bultos];
            }
            if ($ar->volumen > 0) {
                $bultosCliente['volumen'][] = ['precio' => $ar->precio_reparto_adicional, 'cantidad' => $ar->volumen];
            }
            if ($ar->bultos_terminal > 0) {
                $bultosCliente['bultos_terminal'][] = ['precio' => $ar->precio_reparto, 'cantidad' => $ar->bultos_terminal];
            }
            if ($ar->excedentes_terminal > 0) {
                $bultosCliente['excedentes_terminal'][] = ['precio' => $ar->precio_reparto_adicional, 'cantidad' => $ar->excedentes_terminal];
            }
        }
        if ($auxCliente != null) {
            $totales = printRow($bultosCliente, $auxCliente, $totales);
        } ?>
        </tbody>
        <tfoot>
        <tr>
            <th style='border: solid 1px #ccc; padding: 5px; text-align: center; font-weight: bold'>
                TOTALES
            </th>
            <th style='border: solid 1px #ccc; padding: 5px; text-align: center; font-weight: bold'>
                $ <?= $totales['bultos'] ?></th>
            <th style='border: solid 1px #ccc;padding: 5px; text-align: center; font-weight: bold'>
                $ <?= $totales['volumen'] ?></th>
            <th style='border: solid 1px #ccc; padding: 5px; text-align: center; font-weight: bold'>
                $ <?= $totales['bultos_terminal'] ?></th>
            <th style='border: solid 1px #ccc; padding: 5px; text-align: center; font-weight: bold'>
                $ <?= $totales['excedentes_terminal'] ?></th>
            <th style='border: solid 1px #ccc;padding: 5px; text-align: center; font-weight: bold'>
                $ <?= $totales['total'] ?></th>
        </tr>
        </tfoot> 
    </table>
    <br>
    <br>

    <?php
}
function printRow($bultosCliente,$auxCliente,$totales){
    $total = 0;
    ?>
    <tr>
        <td style='border: solid 1px #ccc;padding: 5px;'><strong><?= $auxCliente->idcliente.' - '.$auxCliente->razonsocial;?></strong></td>
        <?php foreach ($bultosCliente as $key => $bc){?>
            <td style='border: solid 1px #ccc; text-align: center;padding: 5px;'>
                <?php
                $subtotal = 0;
                for($i=0;$i < count($bc);$i++){
                    echo $bc[$i]['cantidad']." x $".$bc[$i]['precio'].' = $'.$bc[$i]['cantidad'] * $bc[$i]['precio'].'<br>';
                    $total += $bc[$i]['cantidad'] * $bc[$i]['precio'];
                    $subtotal += $bc[$i]['cantidad'] * $bc[$i]['precio'];
                }
                $totales[$key] += $subtotal;
                ?>
            </td>
        <?php } ?>
        <td style='border: solid 1px #ccc; text-align: center;padding: 5px;'>
            $ <?= $total?>
        </td>
    </tr>
<?php
    $totales['total'] += $total;
    return $totales;
}

function inicioBultos(){
    $bultosCliente['bultos'] = [];
    $bultosCliente['volumen'] = [];
    $bultosCliente['bultos_terminal'] = [];
    $bultosCliente['excedentes_terminal'] = [];
    return $bultosCliente;
}
function inicioTotales(){
    $totales['bultos'] = 0;
    $totales['volumen'] = 0;
    $totales['bultos_terminal'] = 0;
    $totales['excedentes_terminal'] = 0;
    $totales['total'] = 0;
    return $totales;
}

?>