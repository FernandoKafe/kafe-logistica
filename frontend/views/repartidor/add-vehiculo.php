<?php

use yii\helpers\Html;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

use common\models\Repartidor;
use common\models\RepartidorHasVehiculo;

/* @var $this yii\web\View */
/* @var $model common\models\Remito */
$idRepartidor = Yii::$app->getRequest()->getQueryParam('id');

$this->title = Yii::t('app', 'Agregar Vehículo a Repartidor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vehículos de crepartidor'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<?php
    echo $this->render('view', [
        'model' => Repartidor::findOne($idRepartidor),
        ])
?>

<!--Tipos de cliente asociados-->
<div class="">
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Vehículos asociados');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content col-md-12">
    <?php
        $dataProvider = new ActiveDataProvider([
            'query' => RepartidorHasVehiculo::find()->where(['repartidor_idrepartidor' => $idRepartidor]),
        ]);
        $dataProvider->pagination->pageSize=20;
        echo GridView::widget([
            'dataProvider' => $dataProvider,
            'columns' => [
                'desde',
                'hasta',
                [
                    'label' => 'Vehículo',
                    'value' => function($data){
                        return $data->vehiculoIdvehiculo->marca." - ".$data->vehiculoIdvehiculo->modelo." - ".$data->vehiculoIdvehiculo->patente;
                    }
                ],
                /*[
                    'attribute' => 'empresatransporteIdempresatransporte.razonsocial',
                    'label' => 'Empresa de transporte'
                ],*/
            ],
        ]); 
    ?>
    </div>
</div>
</div>

<!--Añadir tipo de cliente-->
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Agregar vehículo');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

    <?= $this->render('../repartidor-has-vehiculo/_form', [
        'model' => $model,
    ]) ?>
    </div>
</div>
