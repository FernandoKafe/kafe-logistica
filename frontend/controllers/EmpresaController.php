<?php

namespace frontend\controllers;

use Yii;
use common\models\Empresa;
use common\models\Sucursal;
use common\models\Remito;
use common\models\RemitoVino;

use frontend\models\EmpresaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * EmpresaController implements the CRUD actions for Empresa model.
 */
class EmpresaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Empresa models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EmpresaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Empresa model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Empresa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Empresa();
        $sucursal = new Sucursal();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $sucursal->empresa_idempresa = $model->idempresa;
            $sucursal->nombre = "Central";
            $sucursal->load(Yii::$app->request->post());
            $sucursal->save();
            return $this->redirect(['view', 'id' => $model->idempresa]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Empresa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $sucursal = Sucursal::getSucursalCentralEmpresa($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $sucursal->load(Yii::$app->request->post()) && $sucursal->save();
            return $this->redirect(['view', 'id' => $model->idempresa]);
        }

        return $this->render('update', [
            'model' => $model,
            'sucursal' => $sucursal,
        ]);
    }

    /**
     * Deletes an existing Empresa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $sucursales = Sucursal::find()->where(['empresa_idempresa' => $model->idempresa])->all();
        if($sucursales){
            foreach($sucursales as $s){
                $remito = Remito::find()->where(['sucursal_idsucursal' => $s->idsucursal])->all();
                $remitoVino = RemitoVino::find()->where(['sucursal_idsucursal' => $s->idsucursal])->all();
                if($remito || $remitoVino){
                    Yii::$app->session->setflash('error', "No se puede eliminar la empresa porque sus sucursales tienen remitos asociados.");
                    return $this->redirect(['index']);
                }
            }
            foreach($sucursales as $s){
                $s->delete();
            }
        }
        
        $model->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Empresa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Empresa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Empresa::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
