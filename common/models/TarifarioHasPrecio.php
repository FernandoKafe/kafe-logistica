<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tarifario_has_precio".
 *
 * @property int $carga_idcarga
 * @property double $precio
 * @property double $precio_adicional
 * @property int $zona_idzona
 * @property int $tarifario_idtarifario
 * @property double $precio_reparto
 * @property double $precio_reparto_adicional
 * @property double $porcentaje_contra
 *
 * @property Carga $cargaIdcarga
 * @property Tarifario $tarifarioIdtarifario
 * @property Zona $zonaIdzona
 */
class TarifarioHasPrecio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tarifario_has_precio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['carga_idcarga', 'precio', 'zona_idzona', 'tarifario_idtarifario', 'precio_reparto', 'precio_adicional', 'precio_reparto_adicional', 'porcentaje_contra'], 'required'],
            [['carga_idcarga', 'zona_idzona', 'tarifario_idtarifario'], 'integer'],
            [['precio', 'precio_reparto', 'precio_adicional', 'precio_reparto_adicional', 'porcentaje_contra'], 'number'],
            [['carga_idcarga', 'zona_idzona', 'tarifario_idtarifario'], 'unique', 'targetAttribute' => ['carga_idcarga', 'zona_idzona', 'tarifario_idtarifario']],
            [['carga_idcarga'], 'exist', 'skipOnError' => true, 'targetClass' => Carga::className(), 'targetAttribute' => ['carga_idcarga' => 'idcarga']],
            [['tarifario_idtarifario'], 'exist', 'skipOnError' => true, 'targetClass' => Tarifario::className(), 'targetAttribute' => ['tarifario_idtarifario' => 'idtarifario']],
            [['zona_idzona'], 'exist', 'skipOnError' => true, 'targetClass' => Zona::className(), 'targetAttribute' => ['zona_idzona' => 'idzona']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'zona_idzona' => Yii::t('app', 'Zona'),
            'carga_idcarga' => Yii::t('app', 'Carga'),
            'precio' => Yii::t('app', 'Bulto'),
            'tarifario_idtarifario' => Yii::t('app', 'Tarifario'),
            'precio_reparto' => 'Reparto',
            'precio_adicional' => 'Bulto adicional',
            'precio_reparto_adicional' => 'Reparto adicional',
            'porcentaje_contra' => 'Porcentaje contra reembolso',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCargaIdcarga()
    {
        return $this->hasOne(Carga::className(), ['idcarga' => 'carga_idcarga']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTarifarioIdtarifario()
    {
        return $this->hasOne(Tarifario::className(), ['idtarifario' => 'tarifario_idtarifario']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZonaIdzona()
    {
        return $this->hasOne(Zona::className(), ['idzona' => 'zona_idzona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getTarifarioHasPrecio($cargaId, $tarifarioId, $zonaId){
        return self::find()->where(['carga_idcarga' => $cargaId, 'tarifario_idtarifario' => $tarifarioId, 'zona_idzona' => $zonaId])->one();
    } 

    /**
     * @return \yii\db\AvtiveQuery
     */
    static public function getCargaPrecio($cargaId, $tarifarioId){
        return self::find()
        ->where(['carga_idcarga' => $cargaId])
        ->andWhere(['tarifario_idtarifario' => $tarifarioId])
        ->one();
    }
}
