<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\RemitoPorcentajeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="remito-porcentaje-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'idremito_porcentaje') ?>

    <?= $form->field($model, 'fecha') ?>

    <?= $form->field($model, 'numero_planilla') ?>

    <?= $form->field($model, 'cant_pedidos') ?>

    <?= $form->field($model, 'cant_entregados') ?>

    <?php // echo $form->field($model, 'noquiso_nopidio') ?>

    <?php // echo $form->field($model, 'cant_anulado') ?>

    <?php // echo $form->field($model, 'cant_sin_dinero') ?>

    <?php // echo $form->field($model, 'cant_regresado') ?>

    <?php // echo $form->field($model, 'cant_faltante') ?>

    <?php // echo $form->field($model, 'cant_mal_armado') ?>

    <?php // echo $form->field($model, 'cant_duplicado') ?>

    <?php // echo $form->field($model, 'dinero_facturado') ?>

    <?php // echo $form->field($model, 'dinero_rendido') ?>

    <?php // echo $form->field($model, 'dinero_devuelto') ?>

    <?php // echo $form->field($model, 'cliente_idcliente') ?>

    <?php // echo $form->field($model, 'repartidor_idrepartidor') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
