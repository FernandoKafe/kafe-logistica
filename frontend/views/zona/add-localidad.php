<?php

use yii\helpers\Html;
use common\models\ZonaHasLocalidad;
use common\models\Zona;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Remito */
$idZona = Yii::$app->getRequest()->getQueryParam('id');

$this->title = Yii::t('app', 'Agregar localidad a Zona');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Localidades'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><i class="fa fa-download"></i> <?= Html::encode($this->title) ?></h1>

<div class="x_panel">
    <div class="x_content">
        <?php
            echo $this->render('view', [
                'model' => Zona::findOne($idZona),
                ])
        ?>
    </div>
</div>

<!--Añadir localidad -->
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Agregar Localidad');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

    <?= $this->render('../zona-has-localidad/_form', [
        'model' => $model,
    ]) ?>
    </div>
</div>

