<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

use common\models\OrdenGrupalDomicilio;
use common\models\GuiaDomicilio;
use common\models\TarifarioDomicilio;
use common\models\LiquidacionDomicilioCliente;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionDomicilioCliente */
$this->title = $model->clienteIdcliente->razonsocial;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Rendición Cliente Domicilio Tipo B'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="liquidacion-domicilio-cliente-view">

    <h1>Rendición: <?= $model->clienteIdcliente->razonsocial?></h1>
    <h1>Desde: <?= $model->fecha_inicio?></h1>
    <h1>Hasta: <?= $model->fecha_fin?></h1>
    <h1>CUIT: <?=$model->clienteIdcliente->cuit?></h1>
    <div class="x_panel">
        <div class="x_title">
            <div class="clearfix"></div>
        </div>
        <p>
            <?php
                echo Html::a(Yii::t('app', 'Liquidaciones domicilio'), ['index'], ['class' => 'btn btn-primary']); 
                echo Html::a(Yii::t('app', 'Confirmar'), ['rendicion/create', 
                'fecha_inicio' => $model->fecha_inicio,
                'fecha_fin' => $model->fecha_fin,
                'cliente' => $model->cliente_idcliente,
                'tipo' => 1],
                 ['class' => 'btn btn-success']) 
            ?>
        </p>

    </div>
</div>


<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_title">
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php
        $ordenesGrupales = OrdenGrupalDomicilio::getAllOrdenesClienteDia($model->cliente_idcliente, $model->fecha_inicio, $model->fecha_fin);
        $ordenesGrupalesId = ArrayHelper::getColumn($ordenesGrupales,'idorden_grupal_domicilio');
        $remitos = GuiaDomicilio::find()->where(['IN', 'ordengrupaldomicilio_idordengrupaldomicilio', $ordenesGrupalesId])->where(['rendido' => 0]);
        $dataProvider = new ActiveDataProvider([
            'query' => $remitos,
        ]);

        if($model->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo == "Tipo B"){
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'moduleId' => 'gridviewk',
                'toolbar' =>  [
                    '{export}',
                    '{toggleData}',
                ],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                ],
                'options' => ['style' => 'font-size:10px;'],
                'showFooter' => true,
                'columns' => [
                    [
                        'label' => 'Fecha',
                        'value' => function($data){
                            return $data->ordengrupaldomicilioIdordengrupaldomicilio->fecha;
                        },
                    ],
                    [
                        'label' => 'Nº Orden Grupal',
                        'value' => function($data){
                            return $data->ordengrupaldomicilioIdordengrupaldomicilio->numero_orden;
                        },
                    ],
                    [
                        'attribute' => 'empresa_destino',
                        'label' => 'Empresa destino',
                    ],
                    [
                        'attribute' => 'localidad_idlocalidad',
                        'label' => 'Localidad destino',
                        'value' => function($data){
                            if($data->localidad_idlocalidad){
                                return $data->localidadIdlocalidad->nombre.
                                ", ".
                                $data->localidadIdlocalidad->departamentoIddepartamento->nombre.
                                ", ".
                                $data->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                            }else{
                                return "Guía sin localidad";
                            }
                        },
                    ],
                    [
                        'attribute' => 'numero_guia',
                        'value' => function($data){
                            return $data->numero_guia;
                        },
                        'footer' => '<strong>TOTAL</strong>',
                    ],
                    /*[
                        'label' => 'Repartidor',
                        'value' => function($data){
                            return $data->ordengrupaldomicilioIdordengrupaldomicilio->repartidorIdrepartidor->apellido.
                            ", ".
                            $data->ordengrupaldomicilioIdordengrupaldomicilio->repartidorIdrepartidor->nombre;
                        }
                    ],*/
                    [
                        'attribute' => 'cantidad',
                        'footer' => LiquidacionDomicilioCliente::getBultosSum($remitos->all()),
                    ],  
                    [
                        'attribute' => 'monto',
                        'label' => 'Rendición común',
                        'value' => function($data){
                            if($data->sprinter == 0)
                                return $data->monto;
                        },
                        'format' => ['decimal', 2],
                        'footer' => number_format (LiquidacionDomicilioCliente::getRendicionComunSum($remitos->all()), 2 , "." ,  "," )
                    ],
                    [
                        'attribute' => 'monto',
                        'label' => 'Rendición sprinter',
                        'value' => function($data){
                            if(($data->sprinter == 1) && ($data->cobrar_sprinter == 1)) 
                                return $data->monto;
                        },
                        'format' => ['decimal', 2],
                        'footer' => number_format (LiquidacionDomicilioCliente::getRendicionSprinterSum($remitos->all()), 2 , "." ,  "," )
                    ],
                ],
                ]);
        }
        $totalRendido = number_format ((LiquidacionDomicilioCliente::getRendicionSprinterSum($remitos->all()) + LiquidacionDomicilioCliente::getRendicionComunSum($remitos->all())), 2 , "." ,  "," );
        echo "<div class='col-md-12'><h2><strong>TOTAL: ".$totalRendido."</strong></h2>";
    ?>