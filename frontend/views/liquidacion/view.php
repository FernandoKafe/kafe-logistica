<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\db\Query;
use frontend\models\LiquidacionHasRemitoSearch;
use common\models\LiquidacionHasRemito;
use common\models\Remito;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;
use common\models\TarifarioHasPrecio;
use common\models\Liquidacion;


/* @var $this yii\web\View */
/* @var $model common\models\Liquidacion */
$idLiquidacion = Yii::$app->getRequest()->getQueryParam('id');


$this->title = "Liquidación para: ".$model->clienteIdcliente->razonsocial." - Periodo: ".$model->periodo;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidación'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-view">

   
<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_title">
        <div class="clearfix"></div>
        <h1>Previa Liquidación: <?= $model->clienteIdcliente->razonsocial?></h1>
        <h1>Periodo: <?= $model->periodo?></h1>
        <h1>CUIT: <?=$model->clienteIdcliente->cuit?></h1>
            <div class="x_title"></div>
            <p>
                <? 
                if(!$model->confirmada){
                    echo Html::a(Yii::t('app', 'Eliminar'), ['delete', 'id' => $model->idliquidacion], ['class' => 'btn btn-danger',  'data-method'=>'post']);
                    echo Html::a(Yii::t('app', 'Recalcular'), ['recalculate', 'id' => $model->idliquidacion], ['class' => 'btn btn-warning']);
                    echo Html::a(Yii::t('app', 'Confirmar'), ['liquidate', 'id' => $model->idliquidacion], ['class' => 'btn btn-success']);
                }else{
                    echo Html::a(Yii::t('app', 'Inicio'), ['index', 'id' => $model->idliquidacion], ['class' => 'btn btn-primary',  'data-method'=>'post']);
                    echo Html::a(Yii::t('app', 'Eliminar'), ['delete', 'id' => $model->idliquidacion], ['class' => 'btn btn-danger',  'data-method'=>'post']);
                    echo Html::a(Yii::t('app', 'PDF'), ['liquidate', 'id' => $model->idliquidacion], ['class' => 'btn btn-success']);
                }
                ?>
            
            </p>
            <p><h3><strong style="color: red;">IMPORTANTE:</strong> La previa debe recalcularse luego de haberla generado si se modifica o elimina un remito de la misma.</h3></p>

    <div class="x_content">
    <?php
    $remito = Remito::remitosView($idLiquidacion);
    if(is_null($model->bultos) || is_null($model->volumen) || is_null($model->guias_logistica) || is_null($model->importe_guias)) {
        if(is_null($model->bultos)){
            $model->bultos = Liquidacion::getBultosSum($remito->all());
        }
        if(is_null($model->volumen)){
            $model->volumen = Liquidacion::getVolumenesSum($remito->all());
        }
        if(is_null($model->guias_logistica)){
            $model->guias_logistica = number_format (Liquidacion::getGuiasLogisticaSum($remito->all()), 2 , "." ,  "," );
        }
        if(is_null($model->importe_guias)){
            $model->importe_guias = number_format (Liquidacion::getGuiasClienteSum($remito->all()), 2 , "." ,  "," );
        }
        $model->save(false);
    }
    $dataProvider = new ActiveDataProvider([
        'query' => $remito,
    ]);
    $dataProvider->setSort([
        'attributes' => [
            'remitofecha' => [
                'asc' => ['remitofecha' => SORT_ASC],
                'desc' => ['remitofecha' => SORT_DESC],
                'default' => SORT_ASC
            ],
        ],
        'defaultOrder' => [
            'remitofecha' => SORT_ASC
        ]
    ]);
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    'moduleId' => 'gridviewk',
    'toolbar' =>  [
        '{export}',
        '{toggleData}',
    ],
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
    ],
    'showFooter' => true,
    'columns' => [
        [
            'attribute' => 'remitofecha',
            'label' => 'Fecha',
        ],
        [
            'attribute' => 'numero_remito',
            'label' => 'Remito',
            'value' => function($data){
                if($data->numero_remito){
                    return $data->numero_remito;
                }else{
                    return $data->ordenderetirogrupalIdordenderetirogrupal->numero;
                }
            }
        ],
        [
            'attribute' => 'numero_guia',
            'label' => 'Guía',
            'value' => function($data){
                return $data->numero_guia.' - '.$data->tipo_remito;
            }
        ],
        [
            'label' => 'Destinatario',
            'value' => function($data){
                if($data->rol_sucursal == "Remite"){
                    return $data->clienteIdcliente->razonsocial;
                }else{
                    return $data->sucursalIdsucursal->nombre.", ".$data->sucursalIdsucursal->empresaIdempresa->nombre;
                }
            },
        ],
        [
            'attribute' => 'sucursalIdsucursal',
            'label' => 'Destino',
            'value' => function($data){
                if(isset($data->sucursalIdsucursal->localidad_idlocalidad)){
                    return $data->sucursalIdsucursal->localidadIdlocalidad->nombre.", ".
                    $data->sucursalIdsucursal->localidadIdlocalidad->departamentoIddepartamento->nombre.", ".
                    $data->sucursalIdsucursal->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                }else{
                    return "SIN LOCALIDAD ASIGNADA";
                }
            }
        ],
        [
            'attribute' => 'empresatransporte_idempresatransporte',
            'label' => 'Empresa de transporte',
            'value' => function($data){
                if(isset($data->empresatransporteIdempresatransporte->razonsocial)){
                    return $data->empresatransporteIdempresatransporte->razonsocial;
                }else{
                    return "SIN NOMBRE ASIGNADO";
                }
            },
            'footer' => '<strong>TOTAL</strong>',
        ],
        [
            'attribute' => 'cantidad_bultos',
            'label' => 'Cantidad bultos',
            'value' => function($data){
                return $data->cantidad_bultos;
            },
            'footer' => $model->bultos,
        ],
        [
            'attribute' => 'volumen',
            'label' => 'Cantidad bultos excedentes',
            'value' => function($data){
                return $data->volumen;
            },
            'footer' => $model->volumen,
        ],
        [
            'label' => 'Importe Bultos',
            'format' => ['decimal',2],
            'value' => function($data){
                $remitoFecha = Remito::testIfFirstOfDate($data->remitofecha, $data->cliente_idcliente);
                if($data->clienteIdcliente->tarifarioIdtarifario->tipo == "Tipo B"){
                    if($remitoFecha->idremito == $data->idremito){
                        if($data->cantidad_bultos == 0)
                            return ($data->volumen * $data->precio_adicional);
                        else
                            return ($data->precio + (($data->cantidad_bultos + $data->volumen - 1) * $data->precio_adicional));
                    }else{
                        if($data->cantidad_bultos == 0)
                            return ($data->volumen * $data->precio_adicional);
                        else
                            return (($data->cantidad_bultos + $data->volumen) * $data->precio_adicional);
                    }
                }else{
                    if($data->cantidad_bultos == 0) {
                        return (($data->cantidad_bultos + $data->volumen -1) * $data->precio_adicional);
                    }else{
                        return $data->precio + (($data->cantidad_bultos + $data->volumen -1) * $data->precio_adicional);
                    }
                }
            },
            'footer' => number_format ($model->importe_bultos, 2 , "." ,  "," ),
        ],
        [
            'attribute' => 'total_guia',
            'label' => 'Guías Logística',
            'format' => ['decimal',2],
            'value' => function($data){
                if(( ($data->tipo_remito == 'A') || ($data->tipo_remito == 'B') || ($data->tipo_remito == 'R') ) && ($data->facturado_a == 'Vertiente')){
                    return $data->total_guia;
                } else {
                    return '';
                }
            },
            'footer' => number_format ($model->guias_logistica + (0.21 * $model->guias_logistica), 2 , "." ,  "," ),

        ],
        [
            'attribute' => 'total_guia',
            'label' => 'Guías Cliente',
            'format' => ['decimal',2],
            'value' => function($data){
                if( ( ($data->tipo_remito == 'A') || ($data->tipo_remito == 'B') ) && ($data->facturado_a == 'Cliente')){
                    return $data->total_guia;
                } else {
                    return '';
                }
            },
            'footer' => $model->importe_guias,
        ],
    ],
    ]); ?>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idliquidacion',
            [
                'attribute' => 'importe_bultos',
                'label' => 'IMPORTES RETIROS/ENTREGA',
                'format' => ['decimal', 2],

            ],
            [
                'attribute' => 'guias_logistica',
                'label' => 'GUÍAS LOGÍSTICA',
                'format' => ['decimal', 2],
            ],
            [
                'attribute' => 'total_contra',
                'label' => 'TOTAL CONTRA REEMBOLSO',
                'format' => ['decimal', 2],

            ],
            [
                'attribute' => 'subtotal',
                'label' => 'SUBTOTAL',
                'format' => ['decimal', 2],
            ],
            [
                'attribute' => 'total_iva',
                'label' => 'IVA 21%',
                'format' => ['decimal', 2],
            ],
            [
                'attribute' => 'importe_guias',
                'label' => 'GUÍAS CLIENTE',
                'format' => ['decimal', 2],
            ],
            [
                'attribute' => 'total',
                'format' => ['decimal', 2],
            ],
        ],
    ]) ?>

    </div>
</div>