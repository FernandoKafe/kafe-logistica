<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%vehiculo}}".
 *
 * @property int $idvehiculo
 * @property string $marca
 * @property string $modelo
 * @property string $patente
 *
 * @property RemitoPorcentaje[] $remitoPorcentajes
 */
class Vehiculo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%vehiculo}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idvehiculo'], 'required'],
            [['idvehiculo'], 'integer'],
            [['marca', 'modelo', 'patente'], 'string', 'max' => 45],
            [['idvehiculo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idvehiculo' => Yii::t('app', 'Idvehiculo'),
            'marca' => Yii::t('app', 'Marca'),
            'modelo' => Yii::t('app', 'Modelo'),
            'patente' => Yii::t('app', 'Patente'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemitoPorcentajes()
    {
        return $this->hasMany(RemitoPorcentaje::className(), ['vehiculo_idvehiculo' => 'idvehiculo']);
    }

    /**
     * Funcion que busca un vehiculo por modelo y patente
     * @param string $q modelo del Vehículo
     * @return [id, text] Arreglo de identificador, razon social y patente
     */
    public static function getVehiculoByModeloAndPatente($q){
        $query = self::find();
        $query->select(["idvehiculo AS id", "concat(modelo,' - ', patente) AS text"])
            ->where(['like', 'modelo', $q])
            ->orWhere(['like', 'patente', $q])
            ->limit(20);
        return $query->asArray()->all();
    }
}
