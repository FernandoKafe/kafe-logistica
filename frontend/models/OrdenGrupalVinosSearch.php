<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\OrdenGrupalVinos;

/**
 * OrdenGrupalVinosSearch represents the model behind the search form of `common\models\OrdenGrupalVinos`.
 */
class OrdenGrupalVinosSearch extends OrdenGrupalVinos
{
    /**
     * {@inheritdoc}
     */ 
    public function rules()
    {
        return [
            [['idorden_grupal_vinos', 'liquidacionvinocliente_idliquidacionvinocliente', 'liquidacionvinorepartidor_idliquidacionvinorepartidor', 'remito_numero'], 'integer'],
            [['fecha', 'numero', 'cliente_idcliente', 'repartidor_idrepartidor', 'creado', 'empresa', 'guia'], 'safe'],
        ]; 
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = OrdenGrupalVinos::find();/*->andWhere(['or', ['liquidacionvinocliente_idliquidacionvinocliente' => null],['liquidacionvinorepartidor_idliquidacionvinorepartidor' => null]]);

        $query->joinWith(['clienteIdcliente as cliente']);
        $query->joinWith(['repartidorIdrepartidor as repartidor']);*/
        $query->joinWith(['remitoVinos as remitoVinos', 'remitoVinos.sucursalIdsucursal.empresaIdempresa as empresa']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if($this->liquidacionvinocliente_idliquidacionvinocliente == 1){
            $query->andWhere(['IS NOT', 'liquidacionvinocliente_idliquidacionvinocliente', null]);
        }else if($this->liquidacionvinocliente_idliquidacionvinocliente == '0'){
            $query->andWhere(['liquidacionvinocliente_idliquidacionvinocliente' => null]);
        }

        if($this->liquidacionvinorepartidor_idliquidacionvinorepartidor == 1){
            $query->andWhere(['IS NOT', 'liquidacionvinorepartidor_idliquidacionvinorepartidor', null]);
        }else if($this->liquidacionvinorepartidor_idliquidacionvinorepartidor == '0'){
            $query->andWhere(['liquidacionvinorepartidor_idliquidacionvinorepartidor' => null]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idorden_grupal_vinos' => $this->idorden_grupal_vinos,
            'fecha' => $this->fecha,
            'creado' => $this->creado,
            'remitoVinos.numero_remito' => $this->remito_numero
        ]);

        $query->andFilterWhere(['like', 'numero', $this->numero]);
        $query->andFilterWhere(['like', 'cliente.razonsocial', $this->cliente_idcliente]);
        $query->andFilterWhere(['like', 'CONCAT(repartidor.apellido, ", ", repartidor.nombre)', $this->cliente_idcliente])
        ->andFilterWhere(['like', 'empresa.nombre', $this->empresa])
        ->andFilterWhere(['like', 'remitoVinos.guia_numero', $this->guia]);


        return $dataProvider;
    }
}
