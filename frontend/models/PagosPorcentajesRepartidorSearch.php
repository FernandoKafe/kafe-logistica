<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PagosPorcentajesRepartidor;

/**
 * PagosPorcentajesRepartidorSearch represents the model behind the search form of `common\models\PagosPorcentajesRepartidor`.
 */
class PagosPorcentajesRepartidorSearch extends PagosPorcentajesRepartidor
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idpagos_vinos_repartidor', 'extremo_inferior', 'extremo_superior'], 'integer'],
            [['monto'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PagosPorcentajesRepartidor::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idpagos_vinos_repartidor' => $this->idpagos_vinos_repartidor,
            'extremo_inferior' => $this->extremo_inferior,
            'monto' => $this->monto,
            'extremo_superior' => $this->extremo_superior,
        ]);

        return $dataProvider;
    }
}
