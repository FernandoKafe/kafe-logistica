<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\OrdenRetiroGrupal;
use yii\data\ActiveDataProvider;
use common\models\Remito;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\OrdenRetiroGrupal */
$idRemitoGrupal = Yii::$app->getRequest()->getQueryParam('ordenderetirogrupal_idordenderetirogrupal');
$idGrup = Yii::$app->getRequest()->getQueryParam('id');

$this->title = $model->numero;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Ordenes de Retiro Grupal'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><i class="fa fa-copy"></i> Orden Grupal Nº:<?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2>Detalle</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <p>
            <?= Html::a('Inicio', ['index'], ['class' => 'btn btn-primary']);?>
            <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->idordenderetirogrupal], ['class' => 'btn btn-success']) ?>
        </p>
        <?=
            DetailView::widget([
                //'dataProvider' => OrdenRetiroGrupalController::findAllRemitos(),
                'model' => $model,
                'attributes' => [
                //'idordenderetirogrupal',
                    'fecha',
                    'numero',
                    'clienteIdcliente.razonsocial',
                    [
                        'label' => 'Repartidor',
                        'value' => function($data){
                            return $data->repartidorIdrepartidor->apellido.", ".$data->repartidorIdrepartidor->nombre;
                        }
                    ]
                //'remitos'
                ],
            ]);
        ?>
    </div>
</div> 

<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Remitos asociados');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php
    if(isset($idRemitoGrupal)){
        $dataProvider = new ActiveDataProvider([
            'query' => Remito::find()->where(['ordenderetirogrupal_idordenderetirogrupal' => $idRemitoGrupal]),
        ]);
        $dataProvider->pagination->pageSize=20;
    }else{
        $dataProvider = new ActiveDataProvider([
            'query' => Remito::find()->where(['ordenderetirogrupal_idordenderetirogrupal' => $idGrup]),
        ]);
        $dataProvider->pagination->pageSize=20;
    }
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    'moduleId' => 'gridviewk',
    'toolbar' =>  [
        '{export}',
        '{toggleData}',
    ],
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
    ],
    'columns' => [
        [
            'attribute' => 'idremito',
            'label' => 'ID Remito',
        ],
        [
            'attribute' => 'cantidad_bultos',
            'label' => 'Nº Bultos'
        ],
        [
            'attribute' => 'sucursalIdsucursal',
            'label' => 'Empresa Destinataria',
            'value' => function($data){
                return $data->sucursalIdsucursal->empresaIdempresa->nombre.", ".$data->sucursalIdsucursal->direccion;
            }
        ],
        'valor_declarado',
        [
            'attribute' => 'empresatransporteIdempresatransporte.razonsocial',
            'label' => 'Empresa de transporte'],
        [
            'attribute' => 'formadepagoIdformadepago.nombre',
            'label' => 'Forma de pago'
        ],
        [
            'attribute' => 'estadoIdestado.nombre',
            'label' => 'Estado del envío'
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Acciones',
            'headerOptions' => ['style' => 'color:#337ab7'],
            'template' => '{view}{update}{delete}',
            'buttons' => [
              'view' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                'title' => Yii::t('app', 'Ver'),
                    ]);
              },

              'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                'title' => Yii::t('app', 'Actualizar'),
                    ]);
                },
                
                'delete' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                                'title' => Yii::t('app', 'Eliminar'),
                                'data' => [
                                    'confirm' => 'Se eliminará el remito con ID '.$model->idremito.'. ¿Desea continuar?',
                                    'method' => 'post',
                                ],
                    ]);
                },
                

            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'view') {
                    $url ='../remito/view?id='.$model->idremito.'&ordenderetirogrupal_idordenderetirogrupal='.$model->ordenderetirogrupal_idordenderetirogrupal.'&repartidor_idrepartidor='.$model->repartidor_idrepartidor;
                    return $url;
                }

                if ($action === 'update') {
                    $url ='../remito/update?id='.
                    $model->idremito.'&ordenderetirogrupal_idordenderetirogrupal='.
                    $model->ordenderetirogrupal_idordenderetirogrupal.'&repartidor_idrepartidor='.
                    $model->repartidor_idrepartidor.'&cliente='.
                    $model->cliente_idcliente;
                    return $url;
                }

                if ($action === 'delete') {
                    $url ='delete-remito?id='.$model->idremito;
                    return $url;
                }
            }
        ],
    ],
    ]); ?>

    </div>
</div>
