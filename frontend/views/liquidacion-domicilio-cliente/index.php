<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use \yii\helpers\ArrayHelper;
use common\models\EstadoLiquidacion;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\LiquidacionDomicilioClienteSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Liquidación Domicilio Clientes');
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><i class="fa fa-file"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2>Listado</h2>
        <div class="clearfix"></div> 
    </div>
    <div class="x_content">
        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a(Yii::t('app', 'Previa Liquidación Domicilio Cliente'), ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app', 'Previa Rendición Diaria'), ['create-rendicion', 'rendicion' => 1], ['class' => 'btn btn-success']) ?>
        </p>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'moduleId' => 'gridviewk',
            'toolbar' =>  [
                '{export}',
                '{toggleData}',
            ],
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
            ],
            'columns' => [
                [
                    'attribute' => 'cliente_idcliente',
                    'value' => function($data){
                        return $data->clienteIdcliente->razonsocial;
                    }
                ],
                [
                    'label' => 'Cliente Tipo',
                    'value' => function($data){
                        return $data->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio->tipo;
                    }
                ],
                'nombre',
                'fecha_inicio',
                'fecha_fin',
                [
                    'attribute'=>'estadoliquidacion_idestadoliquidacion',
                    'filter'=>ArrayHelper::map(EstadoLiquidacion::find()->asArray()->all(), 'idestado_liquidacion', 'nombre'),
                    'content' => function($data){
                        return $data->estadoliquidacionIdestadoliquidacion->nombre;
                    }
                ],
                [
                    'attribute' => 'total',
                    'format' => ['decimal', 2]
                ],
                [
                    'attribute' => 'creado',
                    'label' => 'Fecha y Hora creación',
                ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>