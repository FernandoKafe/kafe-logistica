<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "servicio".
 *
 * @property int $idservicio
 * @property string $nombre
 *
 * @property Remito[] $remitos
 */
class Servicio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'servicio';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'nombre'], 'required'],
            [['idservicio'], 'integer'],
            [['nombre'], 'string', 'max' => 45],
            [['idservicio'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idservicio' => Yii::t('app', 'Servicio'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemitos()
    {
        return $this->hasMany(Remito::className(), ['servicio_idservicio' => 'idservicio']);
    }
}
