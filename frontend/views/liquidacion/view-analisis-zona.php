<?php

$fileName = "ZONA-MES-CLIENTE|".$model->clienteIdcliente->razonsocial."_".$model->periodo_inicio;
$fileName = str_replace(' ', '-', $fileName);
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=".$fileName.".xls");
header("Pragma: no-cache");
header("Expires: 0");
setlocale(LC_TIME, 'es_ES.UTF-8');
if($post){
    $longitudPost = count($post);
    //echo '<pre>' , print_r($post) , '</pre>';
}
if($postDomicilio){
    $longitudDomicilio = count($postDomicilio);
    //print_r($postDomicilio);
}
if($postPorcentaje){
    $longitudPorcentaje = count($postPorcentaje);
    //print_r($postPorcentaje);
}
if($postVino){
    $longitudVino = count($postVino);
    //print_r($postVino);
}
$currentMonth = getdate();
?>
<table xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">  
    <caption style='border: solid 1px; font-weight: bold'><?= $model->clienteIdcliente->razonsocial."-".$model->periodo_inicio?></caption>
    <?php if($post){
            $zonaAnterior = "";
            $contador= 0;
            $totalColumns = 0;
            $meses = [];
            ?>
            <tr>
                <td style='border: solid 1px'></td>
            <?php 
            foreach($post as $elemento){ 
                foreach($elemento as $subelemento){
                    if(key($elemento) == "fecha" && !($currentMonth[mon] == $elemento[fecha] && $currentMonth[year] == $model->periodo_inicio)){
                        $mes = $subelemento;
                        if(empty($meses)){
                            array_push($meses, $mes);
                        }else{
                            if(!in_array($mes, $meses)){
                                array_push($meses, $mes);
                            }
                        }
                    }

                    if((key($elemento) == "nombre")){
                        $actual = $subelemento;
                        if($actual != $zonaAnterior){?>

                            <td style='border: solid 1px; text-align: center;' colspan=3><strong><?= $subelemento ?></strong></td>
                        <?php           
                            $zonaAnterior = $subelemento;
                            $totalColumns++;
                            $a = array('Columna', 'Zona');
                            $b = array($totalColumns, $elemento[nombre]);
                            $zonaColumna[$contador] = array_combine($a, $b);
                            $contador++;
                        }
                    }
                    next($elemento);
                }
            }
            sort($meses);
            //echo '<pre>' , print_r($zonaColumna) , '</pre>';
            //echo '<pre>' , print_r($meses) , '</pre>';
            //echo "totalColumns zonas: <pre>" , print_r($zonaColumna) , "</pre><br>";
            ?>
            </tr>
            <tr>
                <td style='border: solid 1px; font-weight: bold'>Mes</td>
                <?php for($i = 0; $i < $totalColumns; $i++){ ?>
                    <td style='border: solid 1px; text-align: center;'>BULTOS</td>
                    <td style='border: solid 1px; text-align: center;'>EXCEDENTES</td>
                    <td style='border: solid 1px; text-align: center;'>TOTAL</td>
                <?php } ?>
            </tr>


            <?php  foreach($meses as $mes){ ?>
                <tr>
                    <td style='border: solid 1px; font-weight: bold'>
                        <?php 
                            $fecha = '0000/'.($mes + 1).'/00';
                            $fecha = strftime("%B", strtotime($fecha));
                            $fecha = strtoupper($fecha);
                            echo $fecha; 
                        ?>
                    </td>  
                <?php 
                $currentColumn = 0;
                //Para cada mes recorro todo el array post
                for($checkElemento = 0; $checkElemento <= $longitudPost; $checkElemento++){  
                    //Si la fecha del actual elemento de post es igual al mes en curso, entra en el if
                    if($post[$checkElemento][fecha] == $mes){

                        //echo '<pre>' , print_r($post[$checkElemento]) , '</pre><br>';
                        //echo "Post Elemento Fecha= ".$post[$checkElemento][fecha]."---"."mes = ".$mes."<br>";

                        //si el nombre de la zona de zonaColumna es distinto al nombre de la zona del elemento actual
                        //entra al if.
                        if(($zonaColumna[$currentColumn][Zona] != $post[$checkElemento][nombre])){
                            //echo "Post Elemento Nombre= ".$post[$checkElemento][nombre]."---"."Columna nombre = ".$zonaColumna[$currentColumn][Zona]."<br>";
                            //resto almacena la cantidad 
                            $remainingColumnForPrint = $totalColumns - $currentColumn;
                            //echo "Totales = ".$totalColumns."<br>";
                            //echo "Restantes = ".$remainingColumnForPrint."<br>";
                            while($remainingColumnForPrint){
                                echo "<td style='border: solid 1px; text-align: center'>0</td>";
                                echo "<td style='border: solid 1px; text-align: center'>0</td>";
                                echo "<td style='border: solid 1px; text-align: center'>0</td>";
                                $remainingColumnForPrint--;
                                $currentColumn++;
                                ($zonaColumna[$currentColumn][Zona] != $post[$checkElemento][nombre]) ? ($remainingColumnForPrint = true) : ($remainingColumnForPrint = false);
                            }
                        }else if($zonaColumna[$currentColumn][Zona] == $post[$checkElemento][nombre]){ 
                            echo "<td style='border: solid 1px; text-align: center'>".$post[$checkElemento][cantidad_bultos]."</td>";
                            echo "<td style='border: solid 1px; text-align: center'>".$post[$checkElemento][volumen]."</td>";
                            echo "<td style='border: solid 1px; text-align: center'>".$post[$checkElemento][total]."</td>";

                            if($post[$checkElemento][cantidad_bultos]){
                                $cantidad_bultos[$zonaColumna[$currentColumn][Zona]] += $post[$checkElemento][cantidad_bultos];
                            }
                            if($post[$checkElemento][volumen]){
                                $volumen[$zonaColumna[$currentColumn][Zona]] += $post[$checkElemento][volumen];
                            }
                            if($post[$checkElemento][total]){
                                $total[$zonaColumna[$currentColumn][Zona]] += $post[$checkElemento][total];
                            }
                            if($post[$checkElemento][bruto]){
                                $bruto[$zonaColumna[$currentColumn][Zona]] += $post[$checkElemento][bruto];
                            }
                        } else {
                            echo "<td style='border: solid 1px; text-align: center'>0</td>";
                            echo "<td style='border: solid 1px; text-align: center'>0</td>";
                            echo "<td style='border: solid 1px; text-align: center'>0</td>";
                        }
                        $currentColumn++;
                    }
                } ?>
                </tr>
            <?php }
                ?>
            <tr> 
                <td style='border: solid 1px'><strong>TOTAL</strong></td>
                <?php 
                    $i = 0;
                foreach($zonaColumna as $zc){ ?>
                    <td style='border: solid 1px; text-align: center'><strong><?= $cantidad_bultos[$zonaColumna[$i][Zona]]?></strong></td>
                    <td style='border: solid 1px; text-align: center'><strong><?= $volumen[$zonaColumna[$i][Zona]]?></strong></td>
                    <td style='border: solid 1px; text-align: center'><strong><?= $total[$zonaColumna[$i][Zona]]?></strong></td>
                    
                <?php  $i++;  }
                ?>
            </tr>
    <?php } ?> 


<!-- Domicilio -->
    <?php if($postDomicilio){
            $zonaAnterior = "";
            $contador= 0;
            $totalColumns = 0;
            $meses = [];
            ?>
            <tr>
                <td style='border: solid 1px'></td>
            <?php 
            foreach($postDomicilio as $elemento){ 
                foreach($elemento as $subelemento){
                    if(key($elemento) == "fecha" && !($currentMonth[mon] == $elemento[fecha] && $currentMonth[year] == $model->periodo_inicio)){
                        $mes = $subelemento;
                        if(empty($meses)){
                            array_push($meses, $mes);
                        }else{
                            if(!in_array($mes, $meses)){
                                array_push($meses, $mes);
                            }
                        }
                    }

                    if((key($elemento) == "nombre")){
                        $actual = $subelemento;
                        if($actual != $zonaAnterior){?>

                            <td style='border: solid 1px; text-align: center;' colspan=2><strong><?= $subelemento ?></strong></td>
                        <?php           
                            $zonaAnterior = $subelemento;
                            $totalColumns++;
                            $a = array('Columna', 'Zona');
                            $b = array($totalColumns, $elemento[nombre]);
                            $zonaColumna[$contador] = array_combine($a, $b);
                            $contador++;
                        }
                    }
                    next($elemento);
                }
            }
            sort($meses);
            //echo '<pre>' , print_r($zonaColumna) , '</pre>';
            //echo '<pre>' , print_r($meses) , '</pre>';
            //echo "totalColumns zonas: <pre>" , print_r($zonaColumna) , "</pre><br>";
            ?>
            </tr>
            <tr>
                <td style='border: solid 1px; font-weight: bold'>Mes</td>
                <?php for($i = 0; $i < $totalColumns; $i++){ ?>
                    <td style='border: solid 1px; text-align: center;'>BULTOS</td>
                    <td style='border: solid 1px; text-align: center;'>TOTAL</td>
                <?php } ?>
            </tr>


            <?php  foreach($meses as $mes){ ?>
                <tr>
                    <td style='border: solid 1px; font-weight: bold'>
                        <?php $fecha = '0000/'.($mes + 1).'/00';
                        $fecha = strftime("%B", strtotime($fecha));
                        $fecha = strtoupper($fecha);
                        echo $fecha; ?>
                    </td>  
                <?php $currentColumn = 0;
                //Para cada mes recorro todo el array post
                for($checkElemento = 0; $checkElemento <= $longitudDomicilio; $checkElemento++){  
                    //Si la fecha del actual elemento de post es igual al mes en curso, entra en el if
                    if($postDomicilio[$checkElemento][fecha] == $mes){

                        //echo '<pre>' , print_r($post[$checkElemento]) , '</pre><br>';
                        //echo "Post Elemento Fecha= ".$post[$checkElemento][fecha]."---"."mes = ".$mes."<br>";

                        //si el nombre de la zona de zonaColumna es distinto al nombre de la zona del elemento actual
                        //entra al if.
                        if(($zonaColumna[$currentColumn][Zona] != $postDomicilio[$checkElemento][nombre])){
                            //echo "Post Elemento Nombre= ".$post[$checkElemento][nombre]."---"."Columna nombre = ".$zonaColumna[$currentColumn][Zona]."<br>";
                            //resto almacena la cantidad 
                            $remainingColumnForPrint = $totalColumns - $currentColumn;
                            //echo "Totales = ".$totalColumns."<br>";
                            //echo "Restantes = ".$remainingColumnForPrint."<br>";
                            while($remainingColumnForPrint){
                                echo "<td style='border: solid 1px; text-align: center'>0</td>";
                                echo "<td style='border: solid 1px; text-align: center'>0</td>";
                                $remainingColumnForPrint--;
                                $currentColumn++;
                                ($zonaColumna[$currentColumn][Zona] != $postDomicilio[$checkElemento][nombre]) ? ($remainingColumnForPrint = true) : ($remainingColumnForPrint = false);
                            }
                        }
                        if($zonaColumna[$currentColumn][Zona] == $postDomicilio[$checkElemento][nombre]){ 
                            echo "<td style='border: solid 1px; text-align: center'>".$postDomicilio[$checkElemento][cantidad_bultos]."</td>";
                            echo "<td style='border: solid 1px; text-align: center'>".$postDomicilio[$checkElemento][total]."</td>";

                            if($postDomicilio[$checkElemento][cantidad_bultos]){
                                $cantidad_bultos[$zonaColumna[$currentColumn][Zona]] += $postDomicilio[$checkElemento][cantidad_bultos];
                            }
                            if($postDomicilio[$checkElemento][volumen]){
                                $volumen[$zonaColumna[$currentColumn][Zona]] += $postDomicilio[$checkElemento][volumen];
                            }
                            if($postDomicilio[$checkElemento][total]){
                                $total[$zonaColumna[$currentColumn][Zona]] += $postDomicilio[$checkElemento][total];
                            }
                            if($postDomicilio[$checkElemento][bruto]){
                                $bruto[$zonaColumna[$currentColumn][Zona]] += $postDomicilio[$checkElemento][bruto];
                            }
                        } else {
                            echo "<td style='border: solid 1px; text-align: center'>0</td>";
                            echo "<td style='border: solid 1px; text-align: center'>0</td>";
                            echo "<td style='border: solid 1px; text-align: center'>0</td>";
                        }
                        $currentColumn++;
                    }
                } ?>
                </tr>
            <?php }
                ?>
            <tr> 
                <td style='border: solid 1px'><strong>TOTAL</strong></td>
                <?php 
                    $i = 0;
                foreach($zonaColumna as $zc){ ?>
                    <td style='border: solid 1px; text-align: center'><strong><?= $cantidad_bultos[$zonaColumna[$i][Zona]]?></strong></td>
                    <td style='border: solid 1px; text-align: center'><strong><?= $total[$zonaColumna[$i][Zona]]?></strong></td>
                    
                <?php  $i++;  }
                ?>
            </tr>
    <?php } ?>  

    <?php if($postVino){
            $zonaAnterior = "";
            $contador= 0;
            $totalColumns = 0;
            $meses = [];
            ?>
            <tr>
                <td style='border: solid 1px'></td>
            <?php 
            foreach($postVino as $elemento){ 
                foreach($elemento as $subelemento){
                    if(key($elemento) == "fecha" && !($currentMonth[mon] == $elemento[fecha] && $currentMonth[year] == $model->periodo_inicio)){
                        $mes = $subelemento;
                        if(empty($meses)){
                            array_push($meses, $mes);
                        }else{
                            if(!in_array($mes, $meses)){
                                array_push($meses, $mes);
                            }
                        }
                    }

                    if((key($elemento) == "nombre")){
                        $actual = $subelemento;
                        if($actual != $zonaAnterior){?>

                            <td style='border: solid 1px; text-align: center;' colspan=3><strong><?= $subelemento ?></strong></td>
                        <?php           
                            $zonaAnterior = $subelemento;
                            $totalColumns++;
                            $a = array('Columna', 'Zona');
                            $b = array($totalColumns, $elemento[nombre]);
                            $zonaColumna[$contador] = array_combine($a, $b);
                            $contador++;
                        }
                    }
                    next($elemento);
                }
            }
            sort($meses);
            //echo '<pre>' , print_r($zonaColumna) , '</pre>';
            //echo '<pre>' , print_r($meses) , '</pre>';
            //echo "totalColumns zonas: <pre>" , print_r($zonaColumna) , "</pre><br>";
            ?>
            </tr>
            <tr>
                <td style='border: solid 1px; font-weight: bold'>Mes</td>
                <?php for($i = 0; $i < $totalColumns; $i++){ ?>
                    <td style='border: solid 1px; text-align: center;'>CAJAS</td>
                    <td style='border: solid 1px; text-align: center;'>BOTELLAS</td>
                    <td style='border: solid 1px; text-align: center;'>TOTAL</td>
                <?php } ?>
            </tr>


            <?php  foreach($meses as $mes){ ?>
                <tr>
                    <td style='border: solid 1px; font-weight: bold'>
                        <?php $fecha = '0000/'.($mes + 1).'/00';
                        $fecha = strftime("%B", strtotime($fecha));
                        $fecha = strtoupper($fecha);
                        echo $fecha; ?>
                    </td>  
                <?php $currentColumn = 0;
                //Para cada mes recorro todo el array post
                for($checkElemento = 0; $checkElemento <= $longitudVino; $checkElemento++){  
                    //Si la fecha del actual elemento de post es igual al mes en curso, entra en el if
                    if($postVino[$checkElemento][fecha] == $mes){

                        //echo '<pre>' , print_r($post[$checkElemento]) , '</pre><br>';
                        //echo "Post Elemento Fecha= ".$post[$checkElemento][fecha]."---"."mes = ".$mes."<br>";

                        //si el nombre de la zona de zonaColumna es distinto al nombre de la zona del elemento actual
                        //entra al if.
                        if(($zonaColumna[$currentColumn][Zona] != $postVino[$checkElemento][nombre])){
                            //echo "Post Elemento Nombre= ".$post[$checkElemento][nombre]."---"."Columna nombre = ".$zonaColumna[$currentColumn][Zona]."<br>";
                            //resto almacena la cantidad 
                            $remainingColumnForPrint = $totalColumns - $currentColumn;
                            //echo "Totales = ".$totalColumns."<br>";
                            //echo "Restantes = ".$remainingColumnForPrint."<br>";
                            while($remainingColumnForPrint){
                                echo "<td style='border: solid 1px; text-align: center'>0</td>";
                                echo "<td style='border: solid 1px; text-align: center'>0</td>";
                                echo "<td style='border: solid 1px; text-align: center'>0</td>";
                                $remainingColumnForPrint--;
                                $currentColumn++;
                                ($zonaColumna[$currentColumn][Zona] != $postVino[$checkElemento][nombre]) ? ($remainingColumnForPrint = true) : ($remainingColumnForPrint = false);
                            }
                        }
                        if($zonaColumna[$currentColumn][Zona] == $postVino[$checkElemento][nombre]){ 
                            echo "<td style='border: solid 1px; text-align: center'>".$postVino[$checkElemento][cantidad_bultos]."</td>";
                            echo "<td style='border: solid 1px; text-align: center'>".$postVino[$checkElemento][volumen]."</td>";
                            echo "<td style='border: solid 1px; text-align: center'>".$postVino[$checkElemento][total]."</td>";

                            if($postVino[$checkElemento][cantidad_bultos]){
                                $cantidad_bultos[$zonaColumna[$currentColumn][Zona]] += $postVino[$checkElemento][cantidad_bultos];
                            }
                            if($postVino[$checkElemento][volumen]){
                                $volumen[$zonaColumna[$currentColumn][Zona]] += $postVino[$checkElemento][volumen];
                            }
                            if($postVino[$checkElemento][total]){
                                $total[$zonaColumna[$currentColumn][Zona]] += $postVino[$checkElemento][total];
                            }
                            if($postVino[$checkElemento][bruto]){
                                $bruto[$zonaColumna[$currentColumn][Zona]] += $postVino[$checkElemento][bruto];
                            }
                        } else {
                            echo "<td style='border: solid 1px; text-align: center'>0</td>";
                            echo "<td style='border: solid 1px; text-align: center'>0</td>";
                            echo "<td style='border: solid 1px; text-align: center'>0</td>";
                        }
                        $currentColumn++;
                    }
                } ?>
                </tr>
            <?php }
                ?>
            <tr> 
                <td style='border: solid 1px'><strong>TOTAL</strong></td>
                <?php 
                    $i = 0;
                foreach($zonaColumna as $zc){ ?>
                    <td style='border: solid 1px; text-align: center'><strong><?= $cantidad_bultos[$zonaColumna[$i][Zona]]?></strong></td>
                    <td style='border: solid 1px; text-align: center'><strong><?= $volumen[$zonaColumna[$i][Zona]]?></strong></td>
                    <td style='border: solid 1px; text-align: center'><strong><?= $total[$zonaColumna[$i][Zona]]?></strong></td>
                    
                <?php  $i++;  }
                ?>
            </tr>
    <?php } ?> 

</table>





















































<?php/* }else if($postDomicilio){?> <!--Foreach $post-->
            <tr>
                <th><td style='border: solid 1px'>MES</td></th>
                <th><td style='border: solid 1px'>CANTIDAD</td></th>
                <th><td style='border: solid 1px'>MONTO</td></th>
            </tr>
            <?php foreach($postDomicilio as $elemento){ ?>
            <tr>
                <?php foreach($elemento as $subelemento){ ?>
                    <td style='border: solid 1px'>
                    <?php   if(key($elemento) == "fecha"){
                                $subelemento = '0000/'.($subelemento + 1).'/00';
                                //echo date("F", mktime(0, 0, 0, $subelemento, 10));
                                $fecha = strftime("%B", strtotime($subelemento));
                                $fecha = strtoupper($fecha);
                                echo $fecha;
                            }else{
                                if(key($elemento) == "cantidad"){
                                    $cantidad += $subelemento;
                                }
                                if(key($elemento) == "monto"){
                                    $monto += $subelemento;
                                }
                                echo $subelemento;
                            }
                            next($elemento)
                    ?>
                </td>
            <?php } ?><!--Foreach $elemento-->
            </tr>
        <?php } ?>
            <tr>
                <td style='border: solid 1px'><strong>TOTAL</strong></td>
                <td style='border: solid 1px'><strong><?= $cantidad ?></strong></td>
                <td style='border: solid 1px'><strong>$<?= $monto ?></strong></td>
            </tr>
        <?php }else if($postPorcentaje){?> <!--Foreach $post-->
            <tr>
                <th><td style='border: solid 1px'>MES</td></th>
                <th><td style='border: solid 1px'>PEDIDOS</td></th>
                <th><td style='border: solid 1px'>ENTREGADOS</td></th>
            </tr>
            <?php foreach($postPorcentaje as $elemento){ ?>
            <tr>
                <?php foreach($elemento as $subelemento){ ?>
                    <td style='border: solid 1px'>
                    <?php   if(key($elemento) == "fecha"){
                                $subelemento = '0000/'.($subelemento + 1).'/00';
                                //echo date("F", mktime(0, 0, 0, $subelemento, 10));
                                $fecha = strftime("%B", strtotime($subelemento));
                                $fecha = strtoupper($fecha);
                                echo $fecha;
                            }else{
                                if(key($elemento) == "pedidos"){
                                    $pedidos += $subelemento;
                                }
                                if(key($elemento) == "entregados"){
                                    $entregados += $subelemento;
                                }
                                echo $subelemento;
                            }
                            next($elemento)
                    ?>
                </td>
            <?php } ?><!--Foreach $elemento-->
            </tr>
        <?php } ?>
            <tr>
                <td style='border: solid 1px'><strong>TOTAL</strong></td>
                <td style='border: solid 1px'><strong><?= $pedidos ?></strong></td>
                <td style='border: solid 1px'><strong><?= $entregados ?></strong></td>
            </tr>
        <?php }else if($postVino){?> <!--Foreach $post-->
            <tr>
                <th><td style='border: solid 1px'>MES</td></th>
                <th><td style='border: solid 1px'>CAJAS</td></th>
            </tr>
            <?php foreach($postVino as $elemento){ ?>
            <tr>
                <?php foreach($elemento as $subelemento){ ?>
                    <td style='border: solid 1px'>
                    <?php   if(key($elemento) == "fecha"){
                                $subelemento = '0000/'.($subelemento + 1).'/00';
                                //echo date("F", mktime(0, 0, 0, $subelemento, 10));
                                $fecha = strftime("%B", strtotime($subelemento));
                                $fecha = strtoupper($fecha);
                                echo $fecha;
                            }else{
                                if(key($elemento) == "cant_cajas"){
                                    $cajas += $subelemento;
                                }
                                echo $subelemento;
                            }
                            next($elemento)
                    ?>
                </td>
            <?php } ?><!--Foreach $elemento-->
            </tr>
        <?php } */?>