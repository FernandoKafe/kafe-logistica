<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Vehiculo */

$this->title = $model->idvehiculo;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Vehiculos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="zona-view">
    <h1>Vehiculo: <?= $model->patente." - ".$model->modelo." - ".$model->marca ?></h1>
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Detalle') ?></h2>
            <div class="clearfix"></div>
        </div>

        <p>
            <?= Html::a(Yii::t('app', 'Inicio'), ['index'], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->idvehiculo], ['class' => 'btn btn-success']) ?>
        </p>


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idvehiculo',
            'marca',
            'modelo',
            'patente',
        ],
    ]) ?>

</div>
</div>
