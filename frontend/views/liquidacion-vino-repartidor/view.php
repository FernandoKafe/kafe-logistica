<?php

use yii\data\ActiveDataProvider;
use yii\widgets\DetailView;
use kartik\grid\GridView;
use yii\helpers\Html;

use common\models\RemitoVino;
use common\models\OrdenGrupalVinos;
/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionVinoRepartidor */
$idLiquidacion = Yii::$app->getRequest()->getQueryParam('id');


$this->title = "Liquidación vinos: ".$model->repartidorIdrepartidor->apellido.", ".$model->repartidorIdrepartidor->nombre." - ".$model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion Vino Repartidors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-porcentaje-cliente-view">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Detalle') ?></h2>
            <div class="clearfix"></div>
        </div>
        <p>
            <?= Html::a(Yii::t('app', 'Guardar'), ['index'], ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app', 'PDF'), ['report', 'id' => $model->idliquidacion_vino_repartidor], ['class' => 'btn btn-warning']) ?>

        </p>

        <?= DetailView::widget([ 
            'model' => $model,
            'attributes' => [
                [
                    'attribute' => 'estadoliquidacion_idestadoliquidacion',
                    'value' => function($data){
                        return $data->estadoliquidacionIdestadoliquidacion->nombre;
                    }
                ],
                'nombre',
                'periodo_inicio',
                'periodo_fin',
                [
                    'attribute' => 'total',
                    'format' => ['decimal',2],
                ],
            ],
        ]) ?>
    </div>
</div>

<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Remitos liquidados');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php
    $ordengrupal = OrdenGrupalVinos::find()->where(['liquidacionvinorepartidor_idliquidacionvinorepartidor' => $idLiquidacion])->all();
    foreach($ordengrupal as $ogv){
        $remito = RemitoVino::find()
        ->where(['ordengrupalvinos_idordengrupalvinos' => $ogv->idorden_grupal_vinos]);
        $dataProvider = new ActiveDataProvider([
            'query' => $remito,
        ]);
        echo GridView::widget([
        'dataProvider' => $dataProvider,
        'moduleId' => 'gridviewk',
        'toolbar' =>  [
            '{export}',
            '{toggleData}',
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
        ],
        'columns' => [
            [
                'label' => 'Fecha',
                'value' => function($data){
                    return $data->ordengrupalvinosIdordengrupalvinos->fecha;
                }
            ],
            [
                'label' => 'Nº Planilla',
                'value' => function($data){
                    return $data->numero_remito;
                }
            ],
            [
                'label' => 'Cliente',
                'value' => function($data){
                    return $data->ordengrupalvinosIdordengrupalvinos->clienteIdcliente->razonsocial;
                }
            ],
            'cant_cajas',
            [   
                'attribute' => 'valor_caja',
                'label' => 'Valor por caja',
                'format' => ['decimal',2],
            ],
            [
                'label' => 'Cantidad a pagar',
                'value' => function($data){
                    return ($data->precio_reparto * $data->cant_cajas);
                },
                'format' => ['decimal',2],
            ]
        ],
        ]);
    }?>

    </div>
</div>