<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RemitoPorcentaje */

$this->title = Yii::t('app', 'Update Remito Porcentaje: ' . $model->idremito_porcentaje, [
    'nameAttribute' => '' . $model->idremito_porcentaje,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Remito Porcentajes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idremito_porcentaje, 'url' => ['view', 'id' => $model->idremito_porcentaje]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="remito-porcentaje-update">
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Formulario de modificación') ?></h2>
            <div class="clearfix"></div>
            </div>
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
