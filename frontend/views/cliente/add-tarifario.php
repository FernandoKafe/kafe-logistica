<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use \yii\helpers\ArrayHelper;
use yii\web\View;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\helpers\Url;

use common\models\Cliente;
use common\models\Tarifario;
use common\models\TarifarioVino;
use common\models\ClienteHasTipo;
use common\models\TarifarioDomicilio;

$clienteVino = ClienteHasTipo::find()->where(['cliente_idcliente' => $model->idcliente])->andwhere(['clientetipo_idclientetipo' => 4])->one();
$tarifariosVinos = ArrayHelper::map(TarifarioVino::find()->all(),'idtarifario_vino','nombre');
$clienteDomicilio = ClienteHasTipo::find()->where(['cliente_idcliente' => $model->idcliente])->andwhere(['clientetipo_idclientetipo' => 2])->one();
$tarifarioDomicilio = ArrayHelper::map(TarifarioDomicilio::find()->all(), 'idtarifario_domicilio', 'nombre');

$form = ActiveForm::begin([
    'id' => 'update-form',
    'action' => Url::to(['add-tarifario', 'id' => $model->idcliente]),
]);
if(isset($clienteVino) && !$clienteVino->clienteIdcliente->tarifariovinoIdtarifariovino){
?>
    <div>
        <?= $form->field($model, 'idcliente')->textInput()->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'tarifariovino_idtarifariovino')->dropDownList($tarifariosVinos) ?>
        <p>
            <?= Html::submitButton(Yii::t('app', 'Agregar'), ['class' => 'btn btn-success']) ?>
        </p>
    </div>
<?php
    }else if(isset($clienteDomicilio) && !$clienteDomicilio->clienteIdcliente->tarifariodomicilioIdtarifariodomicilio){
?>
        <div>
        <?= $form->field($model, 'idcliente')->textInput()->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'tarifariodomicilio_idtarifariodomicilio')->dropDownList($tarifarioDomicilio) ?>
        <p>
            <?= Html::submitButton(Yii::t('app', 'Agregar'), ['class' => 'btn btn-success']) ?>
        </p>
    </div>
<?php
    }
    ActiveForm::end();
?>