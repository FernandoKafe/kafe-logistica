<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\OrdenRetiroGrupal;

/* @var $this yii\web\View */
/* @var $model common\models\Remito */
$idRemitoGrupal = Yii::$app->getRequest()->getQueryParam('ordenderetirogrupal_idordenderetirogrupal');

$this->title = $model->numero_remito;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Remitos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="remito-view">
    <h1><i class="fa fa-file"></i> Remito Nº: <?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Detalle') ?></h2>
        <div class="clearfix"></div>
    </div>

    <p>
        <?= Html::a('Inicio', ['index'], ['class' => 'btn btn-primary']);?>
        <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 
        'id' => $model->idremito,
        'ordenderetirogrupal_idordenderetirogrupal' => $model->ordenderetirogrupal_idordenderetirogrupal],
        ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idremito',
            [
                'attribute' => 'numero_remito',
                'label' => 'Número de remito'
            ],
            [
                'attribute' => 'remitofecha',
                'label' => 'Fecha'
            ],
            [
                'attribute' => 'sucursalIdsucursal.empresaIdempresa.nombre',
                'label' => 'Empresa',
                'value' => function($data){
                    return $data->rol_sucursal.": ".$data->sucursalIdsucursal->empresaIdempresa->nombre.", ".$data->sucursalIdsucursal->nombre;
                }
            ],
            [
                'attribute' => 'terminal',
                'label' => 'Destino Remito',
                'value' => function($data){
                    if($data->terminal == 0){
                        return $data->sucursalIdsucursal->localidadIdlocalidad->nombre
                        .", ".$data->sucursalIdsucursal->localidadIdlocalidad->departamentoIddepartamento->nombre
                        .", ".$data->sucursalIdsucursal->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->nombre;
                    } else {
                        return "Terminal";
                    }
                }
            ],
            [
                'attribute' => 'cantidad_bultos',
                'label' => 'Cantidad de bultos'
            ],
            [
                'attribute' => 'peso',
                'label' => 'Peso'
            ],
            [
                'attribute' => 'volumen',
                'label' => 'Volumen'],
            [
                'attribute' => 'valor_declarado',
                'label' => 'Valor Declarado',
                'format' => ['decimal',2],
            ],
            [
                'attribute' => 'servicioIdservicio.nombre',
                'label' => 'Servicio'
            ],
            [
                'attribute' => 'flete',
                'label' => 'Flete'
            ], //Solucionar, se muestra un numero, con 0 fleteprovincial, 1 flete naciona
            [
                'attribute' => 'seguro',
                'label' => 'Seguro',
                'format' => ['decimal',2],
            ],
            [
                'attribute' => 'total_contrarembolso',
                'label' => 'TOTAL contrarembolso',
                'contentOptions' => [
                    'class' => 'red danger',],
                'format' => ['decimal',2],
            ],
            [
                'attribute' => 'formadepagoIdformadepago.nombre',
                'label' => 'Forma de Pago'
            ],
            [
                'attribute' => 'productoIdproducto.nombre',
                'label' => 'Producto'
            ],
            [
                'attribute' => 'repartidorIdrepartidor.apellido',
                'label' => 'Repartidor'
            ],
            [
                'attribute' => 'numero_guia',
                'label' => 'Número de Guía'
            ],

            'tipo_remito',
            [
                'attribute' => 'total_guia',
                'contentOptions' => [
                    'class' => 'red danger',
                ],

                'format' => ['decimal',2],
                'label' => 'TOTAL Guía',
                'contentOptions' => [
                    'class' => 'red danger',
                ]
            ],
            [
                'attribute' => 'estadoIdestado.nombre',
                'label' => 'Estado del envío'
            ],
            [
                'attribute' => 'empresatransporteIdempresatransporte.razonsocial',
                'label' => 'Empresa de transporte'
            ],
            [
                'attribute' => 'ordenderetirogrupalIdordenderetirogrupal.numero',
                'label' => 'Pertenece a la Orden Grupal Nº:'
            ],
        ],
    ]) ?>

</div>
</div>
