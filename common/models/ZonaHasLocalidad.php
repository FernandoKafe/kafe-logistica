<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "zona_has_localidad".
 *
 * @property int $zona_idzona
 * @property int $localidad_idlocalidad
 *
 * @property Localidad $localidadIdlocalidad
 * @property Zona $zonaIdzona
 */
class ZonaHasLocalidad extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zona_has_localidad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['zona_idzona', 'localidad_idlocalidad'], 'required'],
            [['zona_idzona', 'localidad_idlocalidad'], 'integer'],
            [['zona_idzona', 'localidad_idlocalidad'], 'unique', 'targetAttribute' => ['zona_idzona', 'localidad_idlocalidad']],
            [['localidad_idlocalidad'], 'exist', 'skipOnError' => true, 'targetClass' => Localidad::className(), 'targetAttribute' => ['localidad_idlocalidad' => 'idlocalidad']],
            [['zona_idzona'], 'exist', 'skipOnError' => true, 'targetClass' => Zona::className(), 'targetAttribute' => ['zona_idzona' => 'idzona']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'zona_idzona' => Yii::t('app', 'Zona'),
            'localidad_idlocalidad' => Yii::t('app', 'Localidad'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalidadIdlocalidad()
    {
        return $this->hasOne(Localidad::className(), ['idlocalidad' => 'localidad_idlocalidad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getZonaIdzona()
    {
        return $this->hasOne(Zona::className(), ['idzona' => 'zona_idzona']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getZonaByLocalidadId($localidadId)
    {
        return self::find()->where(['localidad_idlocalidad' => $localidadId])->one();
    }
}
