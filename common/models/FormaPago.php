<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "forma_pago".
 *
 * @property int $idformadepago
 * @property string $nombre
 *
 * @property Remito[] $remitos
 */
class FormaPago extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'forma_pago';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idformadepago' => 'Forma de Pago',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemitos()
    {
        return $this->hasMany(Remito::className(), ['formadepago_idformadepago' => 'idformadepago']);
    }
}
