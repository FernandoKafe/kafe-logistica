<?php

namespace frontend\controllers;

use Yii;
use common\models\Rendicion;
use frontend\models\RendiconSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\Remito;
use common\models\OrdenGrupalDomicilio;
use common\models\GuiaDomicilio;
use \yii\helpers\ArrayHelper;
use common\models\LiquidacionDomicilioCliente;
use common\models\Liquidacion;
use yii\data\ActiveDataProvider;


/**
 * RendicionController implements the CRUD actions for Rendicion model.
 */
class RendicionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Rendicion models.
     * @return mixed
     */
    public function actionIndex($tipo)
    {
        $searchModel = new RendiconSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams,$tipo);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Rendicion model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id, $tipo)
    {
        $model = $this->findModel($id);
        
        if($tipo == 0){
            
            $remito = Remito::find()->select('remitofecha, total_contrarembolso, facturado_a, tipo_remito, terminal,carga_idcarga, sucursal_idsucursal, empresatransporte_idempresatransporte, cliente_idcliente, numero_remito, numero_guia, rol_sucursal, cantidad_bultos, total_guia, ordenderetirogrupal_idordenderetirogrupal')
                ->where(['>=','remitofecha', $model->fecha_inicio])
                ->andFilterWhere(['<=','remitofecha', $model->fecha_fin])
                ->andFilterWhere(['servicio_idservicio' => 2])
                ->andFilterWhere(['cliente_idcliente' => $model->cliente_idcliente]);
            $dataProvider = new ActiveDataProvider([
                'query' => $remito,
            ]);

            $bultosSum = Liquidacion::getBultosSum($remito->all());
            $volumenesSum = Liquidacion::getVolumenesSum($remito->all());
            $contraSum = Liquidacion::getContraSum($remito->all());

            return $this->render('view', [
                'model' => $model,
                'remito' => $remito,
                'dataProvider' => $dataProvider,
                'bultosSum' => $bultosSum,
                'volumenesSum' => $volumenesSum,
                'contraSum' => $contraSum,
            ]);
        }
        
        if($tipo == 1){

            $ordenesGrupales = OrdenGrupalDomicilio::getAllOrdenesClienteDia($model->cliente_idcliente, $model->fecha_inicio, $model->fecha_fin);
            $ordenesGrupalesId = ArrayHelper::getColumn($ordenesGrupales,'idorden_grupal_domicilio');
            $remitos = GuiaDomicilio::find()->where(['IN', 'ordengrupaldomicilio_idordengrupaldomicilio', $ordenesGrupalesId])->andWhere(['rendido' => $id]);
            $dataProvider = new ActiveDataProvider([
                'query' => $remitos,
            ]);
            $remitosArr = $remitos->all();
            $sumaBultos = LiquidacionDomicilioCliente::getBultosSum($remitosArr);
            $rendicionComun = LiquidacionDomicilioCliente::getRendicionComunSum($remitosArr);
            $rendicionSprinter = LiquidacionDomicilioCliente::getRendicionSprinterSum($remitosArr); 
            $rendicionTotal = LiquidacionDomicilioCliente::getRendicionTotal($remitosArr);
            
            return $this->render('view', [
                'model' => $model,
                'ordenesGrupales' => $ordenesGrupales,
                'ordenesGrupalesId' => $ordenesGrupalesId,
                'dataProvider' => $dataProvider,
                'remitosArr' => $remitosArr,
                'sumaBultos' => $sumaBultos,
                'rendicionComun' => $rendicionComun,
                'rendicionSprinter' => $rendicionSprinter,
                'rendicionTotal' => $rendicionTotal,
                'remitos' => $remitos,
            ]); 

        }
        
    }

    /**
     * Creates a new Rendicion model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($tipo, $fecha_inicio = null, $fecha_fin = null, $cliente = null)
    {
        $model = new Rendicion();
        $model->fecha_inicio = $fecha_inicio;
        $model->fecha_fin = $fecha_fin;
        $model->cliente_idcliente = $cliente;

        if (($model->load(Yii::$app->request->post())) || ($fecha_inicio && $fecha_fin && $cliente)) {
            $model->save();
            if($tipo == 0) //Si es tipo 0, es una rendición de contra reembolos de remitos comunes
            {
                $remitos = Remito::find()
                ->where(['>=','remitofecha', $model->fecha_inicio])
                ->andFilterWhere(['<=','remitofecha', $model->fecha_fin])
                ->andFilterWhere(['servicio_idservicio' => 2])
                ->andFilterWhere(['cliente_idcliente' => $model->cliente_idcliente])
                ->andWhere(['rendido' => 0])->all();
                if($remitos){
                    foreach($remitos as $r){
                        $r->rendido = $model->idrendicion;
                        $r->save();
                        $model->monto += $r->total_contrarembolso;
                    }
                } 
                else
                {
                    $model->delete();
                    Yii::$app->session->setflash('error', "No hay remitos con contra rembolso para las fechas especificadas.");
                    return $this->render('create', [
                        'model' => $model,
                        'cliente' => $cliente,
                    ]);
                }
            }
            else if($tipo == 1)
            {
                $ordenesGrupales = OrdenGrupalDomicilio::getAllOrdenesClienteDia($model->cliente_idcliente, $model->fecha_inicio, $model->fecha_fin);
                $ordenesGrupalesId = ArrayHelper::getColumn($ordenesGrupales,'idorden_grupal_domicilio');
                $remitos = GuiaDomicilio::find()->where(['IN', 'ordengrupaldomicilio_idordengrupaldomicilio', $ordenesGrupalesId])->andWhere(['rendido' => 0])->all();
                if($remitos){
                    $model->monto = LiquidacionDomicilioCliente::getRendicionTotal($remitos);
                    foreach($remitos as $r){
                        $r->rendido = $model->idrendicion;
                        $r->save();
                    }
                }
                else
                {
                    $model->delete();
                    Yii::$app->session->setflash('error', "No hay guías con las que se pueda generar una rendición para las fechas especificadas.");
                    return $this->render('create', [
                        'model' => $model,
                        'cliente' => $cliente,
                    ]);
                }
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->idrendicion, 'tipo' => $tipo]);
        }

        return $this->render('create', [
            'model' => $model,
            'tipo' => $tipo,
            'cliente' => $cliente,
        ]);
    }

    /**
     * Updates an existing Rendicion model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->idrendicion]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Rendicion model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $tipo)
    {
        $model = $this->findModel($id);
        if($tipo == 0){
            $remitos = Remito::find()
                    ->where(['>=','remitofecha', $model->fecha_inicio])
                    ->andFilterWhere(['<=','remitofecha', $fecha->periodo_fin])
                    ->andFilterWhere(['servicio_idservicio' => 2])
                    ->andFilterWhere(['cliente_idcliente' => $model->cliente_idcliente])
                    ->andWhere(['rendido' => $id])->all();
        }else if($tipo == 1){
            $ordenesGrupales = OrdenGrupalDomicilio::getAllOrdenesClienteDia($model->cliente_idcliente, $model->fecha_inicio, $model->fecha_fin);
            $ordenesGrupalesId = ArrayHelper::getColumn($ordenesGrupales,'idorden_grupal_domicilio');
            $remitos = GuiaDomicilio::find()->where(['IN', 'ordengrupaldomicilio_idordengrupaldomicilio', $ordenesGrupalesId])->where(['rendido' => $id])->all();
        }
        if($remitos){
            foreach($remitos as $r){
                $r->rendido = 0;
                $r->save();
            }
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index', 'tipo' => $tipo]);
    }

    /**
     * Finds the Rendicion model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rendicion the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rendicion::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionReport($id, $tipo) {

        $model = $this->findModel($id);
        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('rendicion-pdf', ['model' => $model]);

        // setup kartik\mpdf\Pdf component
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;

        // return the pdf output as per the destination setting
        return $pdf->render();
    }
}
