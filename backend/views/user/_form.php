<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="user-form">


    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true])?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true])?>

    <?= $form->field($model, 'surname')->textInput(['maxlength' => true])?>

    <?= $form->field($model, 'dni')->textInput()?>

    <?= $form->field($model, 'email')->input('email')?>

    <?= $form->field($model, 'telephone')->textInput()?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true])?>

    <?php if($model->isNewRecord){ ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'repeat_password')->passwordInput() ?>

    <?php }else{
        echo $form->field($model, 'status')->dropDownList(\common\models\User::getAllStatus());
    }?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
