<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ClienteHasTipo */

$this->title = Yii::t('app', 'Update Cliente Has Tipo: ' . $model->cliente_idcliente, [
    'nameAttribute' => '' . $model->cliente_idcliente,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cliente Has Tipos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cliente_idcliente, 'url' => ['view', 'cliente_idcliente' => $model->cliente_idcliente, 'clientetipo_idclientetipo' => $model->clientetipo_idclientetipo]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="cliente-has-tipo-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
