<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "empresa".
 *
 * @property int $idempresa
 * @property string $nombre
 * @property string $telefono
 *
 * @property Remito[] $remitos
 * @property Remito[] $remitos0
 */
class Empresa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empresa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['telefono'], 'integer'],
            [['nombre'], 'unique'],
            [['nombre'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idempresa' => 'Empresa ID',
            'nombre' => 'Nombre Empresa',
            'telefono' => 'Teléfono',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemitos()
    {
        return $this->hasMany(Remito::className(), ['empresa_idempresaremite' => 'idempresa']);
    }

    /**
     * {@inheritdoc}
     */
    public static function findOne($id)
    {
        return Empresa::find()->where(['idempresa' => $id])->one();

    }
}
