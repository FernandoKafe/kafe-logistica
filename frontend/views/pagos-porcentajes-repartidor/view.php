<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\PagosPorcentajesRepartidor */

$this->title = $model->monto.' ARG';
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Pagos Porcentajes Repartidors'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pagos-porcentajes-repartidor-view">

    <h1><i class="fa fa-copy"></i> Monto: <?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
        <div class="x_title">
            <h2>Detalle</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <p>
                <?= Html::a('Inicio', ['index'], ['class' => 'btn btn-primary']);?>
                <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->idpagos_vinos_repartidor], ['class' => 'btn btn-success']) ?>
            </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                //'idpagos_vinos_repartidor',
                'extremo_inferior',
                'extremo_superior',
                [
                    'attribute' => 'monto',
                    'format' => ['decimal',2]
                ],
            ],
        ]) ?>
    </div>
</div>
