<?php

use yii\helpers\Html;
use common\models\TarifarioHasPrecio;
use common\models\Tarifario;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Remito */
$idTarifario = Yii::$app->getRequest()->getQueryParam('id');

$this->title = Yii::t('app', 'Agregar Precio a Tarifario');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Precios'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><i class="fa fa-download"></i> <?= Html::encode($this->title) ?></h1>

<div class="x_panel">
    <div class="x_content">
        <?php
            echo $this->render('view', [
                'model' => Tarifario::findOne($idTarifario),
                ])
        ?>
    </div>
</div>

<!--Añadir precio -->
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Agregar Precio');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

    <?= $this->render('../tarifario-has-precio/_form', [
        'model' => $model,
    ]) ?>
    </div>
</div>

