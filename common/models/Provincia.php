<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "provincia".
 *
 * @property int $idprovincia
 * @property string $nombre
 *
 * @property Departamento[] $departamentos
 */
class Provincia extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'provincia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idprovincia' => Yii::t('app', 'Provincia'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartamentos()
    {
        return $this->hasMany(Departamento::className(), ['provincia_idprovincia' => 'idprovincia']);
    }
}
