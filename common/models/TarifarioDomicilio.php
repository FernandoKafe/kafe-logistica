<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%tarifario_domicilio}}".
 *
 * @property int $idtarifario_domicilio
 * @property double $reparto
 * @property double $reparto_adicional
 * @property string $nombre
 * @property string $tipo
 * @property double $primer_bulto
 * @property double $bulto_adicional
 * @property double $sprinter
 *
 * @property Cliente[] $clientes
 */
class TarifarioDomicilio extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%tarifario_domicilio}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['reparto', 'reparto_adicional', 'primer_bulto', 'bulto_adicional', 'sprinter'], 'number'],
            [['tipo'], 'required'],
            [['tipo'], 'string'],
            [['nombre'], 'string', 'max' => 100],
            [['nombre'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idtarifario_domicilio' => Yii::t('app', 'ID Tarifario Domicilio'),
            'reparto' => Yii::t('app', '$Reparto'),
            'reparto_adicional' => Yii::t('app', '$Reparto Adicional'),
            'nombre' => Yii::t('app', 'Nombre'),
            'tipo' => Yii::t('app', 'Tipo'),
            'primer_bulto' => Yii::t('app', '$Primer Bulto'),
            'bulto_adicional' => Yii::t('app', '$Bulto Adicional'),
            'sprinter' => Yii::t('app', 'Sprinter'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClientes()
    {
        return $this->hasMany(Cliente::className(), ['tarifariodomicilio_idtarifariodomicilio' => 'idtarifario_domicilio']);
    }
}
