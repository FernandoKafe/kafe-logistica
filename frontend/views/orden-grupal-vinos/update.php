<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\OrdenGrupalVinos */

$this->title = Yii::t('app', 'Actualizar Orden Grupal Vinos: ' . $model->clienteIdcliente->razonsocial." - ".$model->numero, [
    'nameAttribute' => '' . $model->idorden_grupal_vinos,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orden Grupal Vinos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idorden_grupal_vinos, 'url' => ['view', 'id' => $model->idorden_grupal_vinos]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<h1><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario de modificación') ?></h2>
        <div class="clearfix"></div>
        </div>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div> 
</div>
