<?php

namespace frontend\controllers;
use Yii;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

//modelos
use common\models\Cheque;
use common\models\LiquidacionPorcentajeCliente;
use common\models\LpcHasRemitoporcentaje;
use common\models\RemitoPorcentaje;

//modelos frontend
use frontend\models\LiquidacionPorcentajeClienteSearch;


/**
 * LiquidacionPorcentajeClienteController implements the CRUD actions for LiquidacionPorcentajeCliente model.
 */
class LiquidacionPorcentajeClienteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LiquidacionPorcentajeCliente models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LiquidacionPorcentajeClienteSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LiquidacionPorcentajeCliente model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new LiquidacionPorcentajeCliente model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LiquidacionPorcentajeCliente();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            //recetea el total rendido 
            $model->combustible = 0;
            $model->comision = 0;
            $model->iva = 0;
            $model->neto_sin_iva = 0;
            $model->pallets = 0;
            $model->subtotal = 0;
            $model->total_cobrar = 0;
            $model->total_real = 0;
            $model->total_rendido = 0;
            $totalRendido= 0;
            $model->pedidos = 0;
            $model->total_faltante = 0;
            $model->total_devuelto = 0;
            $model->total_facturado = 0;
            $model->total_rendido_al_3 = 0;
            $model->total_rendido_al_5 = 0;
            //Crea un array con todos los porcentajes existentes en la fecha
            $porcentajes = RemitoPorcentaje::getPorcentajesFecha($model->cliente_idcliente, $model->periodo_inicio, $model->periodo_fin);
            foreach($porcentajes as $p){
                $model->total_rendido = 0;
                //Traer remitos porcentaje pertenecientes a cliente comprendidos entre fechas
                $remitos = RemitoPorcentaje::getRemitosPorcentajeFechaCliente($model->cliente_idcliente, $model->periodo_inicio, $model->periodo_fin, $p->porcentaje);
                $flag = 1;
                if($remitos){
                    foreach ($remitos as $r) {
                        //Crea nueva relación entre liquidación y remito porcentaje
                        $rel = new LpcHasRemitoporcentaje();
                        //Guarda en la relación el id de la liquidación
                        $rel->lpc_idlpc = $model->idliquidacion_porcentaje_cliente;
                        //Guarda en la relación el id del remito
                        $rel->rp_idrp = $r->idremito_porcentaje;
                        $rel->save(false);
                        //Agrega al total_rendido el dinero rendido de cada remito
                        $model->total_rendido += $r->dinero_rendido;
                        //Suma el conmbustible de cada remito
                        $model->combustible += $r->combustible;
                        //Suma los montos faltantes de cada remito
                        $model->total_faltante += $r->dinero_faltante;
                        if($r->pallets != 0){
                            $model->pallets += $r->pallets;
                        }
                        $model->pedidos += $r->cant_pedidos;
                        $model->total_devuelto += $r->dinero_devuelto;
                        $model->total_facturado += $r->dinero_facturado;
                        if($r->porcentaje == 3.5){
                            $model->total_rendido_al_3 += $r->dinero_rendido;
                        } else if ($r->porcentaje == 5.5){
                            $model->total_rendido_al_5 += $r->dinero_rendido;
                        }
                    }
                } else {
                    Yii::$app->session->setflash('error', "No se puede generar la liquidación, no hay remito para las fechas especificadas");
                    $model->delete();
                    return $this->render('create', [
                        'model' => $model,
                    ]);
                }
                $model->neto_sin_iva += $model->total_rendido / 1.21;
                $totalRendido += $model->total_rendido;
                $model->comision += (($model->total_rendido / 1.21) * ($p->porcentaje / 100));
            }
            $model->total_rendido = $totalRendido;
            $model->subtotal += $model->comision + $model->combustible + $model->pallets;
            $model->iva += $model->subtotal * 0.21;
            $model->total_real += $model->subtotal + $model->iva;
            $model->total_cobrar += $model->total_real - $model->total_faltante;
            $model->creado = date('Y-m-d G:i', time());
            if($model->estadoliquidacion_idestadoliquidacion != 1)
                $model->fecha_pago = date('Y-m-d G:i', time());
            $model->save();
            Yii::$app->session->setflash('success', "Liquidación porcentaje cliente creada con éxito."); 
            return $this->redirect(['view', 'id' => $model->idliquidacion_porcentaje_cliente]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new LiquidacionPorcentajeCliente model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionRecalculate($id)
    {
        $model = $this->findModel($id);
        //recetea el total rendido
        $model->combustible = 0;
        $model->comision = 0;
        $model->iva = 0;
        $model->neto_sin_iva = 0;
        $model->pallets = 0;
        $model->subtotal = 0;
        $model->total_cobrar = 0;
        $model->total_real = 0;
        $model->total_rendido = 0;
        $total_parcial = 0;
        $totalRendido= 0;
        $model->pedidos = 0; 
        $model->total_faltante = 0;
        $model->total_devuelto = 0;
        $model->total_facturado = 0;
        $model->total_rendido_al_3 = 0;
        $model->total_rendido_al_5 = 0;

        //Crea un array con todos los porcentajes existentes en la fecha
        $porcentajes = RemitoPorcentaje::getPorcentajesFecha($model->cliente_idcliente, $model->periodo_inicio, $model->periodo_fin);
        foreach($porcentajes as $p){
            $model->total_rendido = 0;
            //Traer remitos porcentaje pertenecientes a cliente comprendidos entre fechas
            $remitos = RemitoPorcentaje::getRemitosPorcentajeFechaCliente($model->cliente_idcliente, $model->periodo_inicio, $model->periodo_fin, $p->porcentaje);
            $flag = 1;
            if($remitos){
                foreach ($remitos as $r) { 
                    //Crea nueva relación entre liquidación y remito porcentaje
                    $rel = new LpcHasRemitoporcentaje();
                    //Guarda en la relación el id de la liquidación
                    $rel->lpc_idlpc = $model->idliquidacion_porcentaje_cliente;
                    //Guarda en la relación el id del remito
                    $rel->rp_idrp = $r->idremito_porcentaje;
                    $rel->save(false);
                    //Agrega al total_rendido el dinero rendido de cada remito
                    $model->total_rendido += $r->dinero_rendido;
                    //Suma el conmbustible de cada remito
                    $model->combustible += $r->combustible;
                    //Suma los montos faltantes de cada remito
                    $model->total_faltante += $r->dinero_faltante;
                    if($r->pallets != 0){
                        $model->pallets += $r->pallets;
                    }
                    $model->pedidos += $r->cant_pedidos;
                    $model->total_devuelto += $r->dinero_devuelto;
                    $model->total_facturado += $r->dinero_facturado;
                    if($r->porcentaje == 3.5){
                        $model->total_rendido_al_3 += $r->dinero_rendido;
                    } else if ($r->porcentaje == 5.5){
                        $model->total_rendido_al_5 += $r->dinero_rendido;
                    }
                    
                }
            } else {
                Yii::$app->session->setflash('error', "No se puede generar la liquidación, no hay remito para las fechas especificadas");
                $model->delete();
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
            $model->neto_sin_iva += $model->total_rendido / 1.21;
            $totalRendido += $model->total_rendido;
            $model->comision += (($model->total_rendido / 1.21) * ($p->porcentaje / 100));
        }
        $model->total_rendido = $totalRendido;
        $model->subtotal += $model->comision + $model->combustible + $model->pallets;
        $model->iva += $model->subtotal * 0.21;
        $model->total_real += $model->subtotal + $model->iva;
        $model->total_cobrar += $model->total_real - $model->total_faltante;
        $model->creado = date('Y-m-d G:i', time());
        $model->save();
        Yii::$app->session->setflash('success', "Liquidación porcentaje cliente creada con éxito."); 
        return $this->redirect(['view', 'id' => $model->idliquidacion_porcentaje_cliente]);
    }

    /**
     * Updates an existing LiquidacionPorcentajeCliente model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $estado_liquidacion_anterior = $model->estadoliquidacion_idestadoliquidacion;
        
        if(!$model->chequeIdcheque){
            $cheque = new Cheque();
        }else{
            $cheque = $model->chequeIdcheque;
        }

        if ($model->load(Yii::$app->request->post())) {
            if($model->estadoliquidacion_idestadoliquidacion != $estado_liquidacion_anterior && $model->estadoliquidacion_idestadoliquidacion != 1){
                $model->fecha_pago = date('Y-m-d G:i', time());
                if($model->liquidacionformapago_idliquidacionformapago == 1){
                    Yii::$app->session->setflash('error', "Por favor seleccione una forma de pago.");
                    return $this->redirect(['update', 'id' => $model->idliquidacion_porcentaje_cliente]);
                }
            }
            if($cheque->load(Yii::$app->request->post()) && $cheque->save()){
                $model->cheque_idcheque = $cheque->idcheque;
            }
            if($cheque->estado == "Rechazado"){
                $model->estadoliquidacion_idestadoliquidacion = 1;
                $model->pago_parcial = 0;
                $model->liquidacionformapago_idliquidacionformapago = 1;
            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->idliquidacion_porcentaje_cliente]);
        }

        return $this->render('update', [
            'model' => $model,
            'cheque' => $cheque,
        ]);
    }

    /**
     * Deletes an existing LiquidacionPorcentajeCliente model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $cheque = Cheque::find()->where(['idcheque' => $model->cheque_idcheque])->one();

        if(isset($cheque)){
            $cheque->delete();
        }

        $rel = LpcHasRemitoporcentaje::find()->where(['lpc_idlpc' => $model->idliquidacion_porcentaje_cliente])->all();
        foreach($rel as $l){
            $remito = RemitoPorcentaje::find()->where(['idremito_porcentaje' => $l->rp_idrp])->one();
            if(isset($remito)){
                $remito->liquidado_cliente = 0;
                $remito->save();
            }
            $l->delete();
        }
        $model->delete();
        return $this->redirect(['index']);
    }

    /*public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $cheque = Cheque::find()->where(['liquidacion_idliquidacion' => $model->idliquidacion])->one();
        if(isset($cheque)){
            $cheque->delete();
        }
        $rel = LiquidacionHasRemito::find()->where(['liquidacion_idliquidacion' => $model->idliquidacion])->all();
        foreach($rel as $l){
            $remito = Remito::find()->where(['idremito' => $l->remito_idremito])->one();
            if(isset($remito)){
                $remito->liquidado = 0;
                $remito->save();
            }
            $l->delete();
        }
        $model->delete();
        return $this->redirect(['index']);
    }*/

    /**
     * Finds the LiquidacionPorcentajeCliente model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return LiquidacionPorcentajeCliente the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = LiquidacionPorcentajeCliente::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    public function actionReport($id) {

        $model = $this->findModel($id);
        // get your HTML raw content without any layouts or scripts
        $content = $this->renderPartial('view-pdf', ['model' => $model]);

        // setup kartik\mpdf\Pdf component
        $pdf = Yii::$app->pdf;
        $pdf->content = $content;

        // return the pdf output as per the destination setting
        return $pdf->render(); 
    }

    public function actionLiquidate($id){

        $model = $this->findModel($id);
        $model->confirmada = 1;
        $model->save();
        $porcentajes = RemitoPorcentaje::getPorcentajesFecha($model->cliente_idcliente, $model->periodo_inicio, $model->periodo_fin);
        foreach($porcentajes as $p){
            $remitos = RemitoPorcentaje::getRemitosPorcentajeFechaCliente($model->cliente_idcliente, $model->periodo_inicio, $model->periodo_fin, $p->porcentaje);
            foreach($remitos as $r){
                //Marca al remito como liquidado
                $r->liquidado_cliente = 1;
                $r->save();
            }
        }
        return $this->redirect(['report', 'id' => $id]);
    }
}
