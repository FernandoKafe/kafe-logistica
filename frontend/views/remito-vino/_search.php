<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\RemitoVinoSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="remito-vino-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'idremito_vino') ?>

    <?= $form->field($model, 'cant_cajas') ?>

    <?= $form->field($model, 'valor_declarado') ?>

    <?= $form->field($model, 'total_facturacion') ?>

    <?= $form->field($model, 'numero_remito') ?>

    <?= $form->field($model, 'cant_botellas') ?>

    <?php // echo $form->field($model, 'sucursal_idsucursal') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
