<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%orden_grupal_vinos}}".
 *
 * @property int $idorden_grupal_vinos
 * @property string $fecha
 * @property string $numero
 * @property int $cliente_idcliente
 * @property int $repartidor_idrepartidor
 * @property int $liquidacionvinocliente_idliquidacionvinocliente 
 * @property int $liquidacionvinorepartidor_idliquidacionvinorepartidor  
 * @property string $creado
 *
 * @property Cliente $clienteIdcliente
 * @property LiquidacionVinoCliente $liquidacionvinoclienteIdliquidacionvinocliente 
 * @property LiquidacionVinoRepartidor $liquidacionvinorepartidorIdliquidacionvinorepartidor
 * @property Repartidor $repartidorIdrepartidor
 * @property RemitoVino[] $remitoVinos 
 */
class OrdenGrupalVinos extends \yii\db\ActiveRecord
{
    public $empresa;
    public $guia;
    public $remito_numero;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%orden_grupal_vinos}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() 
    {
        return [
            [['fecha', 'creado'], 'safe'],
            [['numero', 'cliente_idcliente', 'repartidor_idrepartidor'], 'required'],
            [['cliente_idcliente', 'repartidor_idrepartidor', 'liquidacionvinocliente_idliquidacionvinocliente', 'liquidacionvinorepartidor_idliquidacionvinorepartidor'], 'integer'],
            [['numero'], 'unique'],
            [['numero'], 'string', 'max' => 100],
            [['cliente_idcliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['cliente_idcliente' => 'idcliente']],
            [['repartidor_idrepartidor'], 'exist', 'skipOnError' => true, 'targetClass' => Repartidor::className(), 'targetAttribute' => ['repartidor_idrepartidor' => 'idrepartidor']],
            [['liquidacionvinocliente_idliquidacionvinocliente'], 'exist', 'skipOnError' => true, 'targetClass' => LiquidacionVinoCliente::className(), 'targetAttribute' => ['liquidacionvinocliente_idliquidacionvinocliente' => 'idliquidacion_vino_cliente']], 
            [['liquidacionvinorepartidor_idliquidacionvinorepartidor'], 'exist', 'skipOnError' => true, 'targetClass' => LiquidacionVinoRepartidor::className(), 'targetAttribute' => ['liquidacionvinorepartidor_idliquidacionvinorepartidor' => 'idliquidacion_vino_repartidor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idorden_grupal_vinos' => Yii::t('app', 'Idorden Grupal Vinos'),
            'fecha' => Yii::t('app', 'Fecha'),
            'numero' => Yii::t('app', 'Nº de retiro grupal'),
            'cliente_idcliente' => Yii::t('app', 'Cliente'),
            'repartidor_idrepartidor' => Yii::t('app', 'Repartidor'),
            'liquidacionvinocliente_idliquidacionvinocliente' => Yii::t('app', 'Liquidacionvinocliente Idliquidacionvinocliente'),
            'liquidacionvinorepartidor_idliquidacionvinorepartidor' => Yii::t('app', 'Liquidacionvinorepartidor Idliquidacionvinorepartidor'),
            'creado' => Yii::t('app', 'Creado'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClienteIdcliente()
    {
        return $this->hasOne(Cliente::className(), ['idcliente' => 'cliente_idcliente']);
    } 
 
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getLiquidacionvinoclienteIdliquidacionvinocliente() 
    { 
        return $this->hasOne(LiquidacionVinoCliente::className(), ['idliquidacion_vino_cliente' => 'liquidacionvinocliente_idliquidacionvinocliente']); 
    } 
  
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getLiquidacionvinorepartidorIdliquidacionvinorepartidor() 
    { 
        return $this->hasOne(LiquidacionVinoRepartidor::className(), ['idliquidacion_vino_repartidor' => 'liquidacionvinorepartidor_idliquidacionvinorepartidor']); 
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepartidorIdrepartidor()
    {
        return $this->hasOne(Repartidor::className(), ['idrepartidor' => 'repartidor_idrepartidor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemitoVinos()
    {
        return $this->hasMany(RemitoVino::className(), ['ordengrupalvinos_idordengrupalvinos' => 'idorden_grupal_vinos']); 
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getOrdenVinosClienteByFecha($idCliente, $fechaInicio, $fechaFin){
        return self::find()->where(['cliente_idcliente' => $idCliente])
        ->andWhere(['>=', 'fecha', $fechaInicio])
        ->andWhere(['<=', 'fecha', $fechaFin])
        ->andWhere(['liquidacionvinocliente_idliquidacionvinocliente' => null])
        ->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getOrdenVinosRepartidorByFecha($idRepartidor, $fechaInicio, $fechaFin){
        return self::find()->where(['repartidor_idrepartidor' => $idRepartidor])
        ->andWhere(['>=', 'fecha', $fechaInicio])
        ->andWhere(['<=', 'fecha', $fechaFin])
        ->andWhere(['liquidacionvinorepartidor_idliquidacionvinorepartidor' => null])
        ->all();
    }
}
