<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%pagos_porcentajes_repartidor}}".
 *
 * @property int $idpagos_vinos_repartidor
 * @property int $extremo_inferior
 * @property double $monto
 * @property int $extremo_superior
 */
class PagosPorcentajesRepartidor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%pagos_porcentajes_repartidor}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['extremo_inferior', 'monto', 'extremo_superior'], 'required'],
            [['extremo_inferior', 'extremo_superior'], 'integer'],
            [['monto'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idpagos_vinos_repartidor' => Yii::t('app', 'Idpagos Vinos Repartidor'),
            'extremo_inferior' => Yii::t('app', 'Extremo Inferior'),
            'monto' => Yii::t('app', 'Monto'),
            'extremo_superior' => Yii::t('app', 'Extremo Superior'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function getPagoByPorcentaje($porcentaje){
        if($porcentaje == 100){
            return self::find()->where(['extremo_superior' => $porcentaje])
            ->one();
        }else if($porcentaje == 0){
            return self::find()->where(['extremo_inferior' => $porcentaje])
            ->one();
        }else{
            return self::find()->where(['<=', 'extremo_inferior', $porcentaje])
            ->andFilterWhere(['>', 'extremo_superior', $porcentaje])
            ->one();
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function getById($id){
        return self::find()->where(['idpagos_vinos_repartidor' => $id])->one();
    }
}
