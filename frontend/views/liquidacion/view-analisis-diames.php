<?php

$fileName = "DIA-MES | ".$model->clienteIdcliente->razonsocial."_".$model->periodo_inicio." AL ".$model->periodo_fin;
$fileName = str_replace(' ', '-', $fileName);
header("Content-Type:   application/vnd.ms-excel; charset=utf-8");
header("Content-Disposition: attachment; filename=".$fileName.".xls");
header("Pragma: no-cache");
header("Expires: 0");
setlocale(LC_TIME, 'es_ES.UTF-8');
if($post){
    $longitud = count($post);
    //print_r($post);
}
if($postAnterior){
    $longitud = count($postAnterior);
    //echo "<pre>" , print_r($postAnterior) , "</pre>";
}
if($postDomicilio){
    $longitud = count($postDomicilio);
    //print_r($post);
}
if($postPorcentaje){
    $longitud = count($postPorcentaje);
    //print_r($post);
}
if($postVino){
    $longitud = count($postVino);
    //print_r($post);
}
//echo date("F", mktime(0, 0, 0, $subelemento, 10));
$fecha = strftime("%B", strtotime($model->periodo_inicio));
$fecha = strtoupper($fecha);
?>
<table xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">  
    <caption style='border: solid 1px; font-weight: bold;'><?= $model->clienteIdcliente->razonsocial."-".$model->periodo_inicio." AL ".$model->periodo_fin?></caption>
    <?php if($post){ ?>
            <tr>
                <td style='border: solid 1px; font-weight: bold;'>DIA</td>
                <td style='border: solid 1px; font-weight: bold;'>BULTOS</td>
                <td style='border: solid 1px; font-weight: bold;'>EXCEDENTES</td>
                <td style='border: solid 1px; font-weight: bold;'>$TOTAL</td>
            </tr>
            <?php foreach($post as $elemento){ ?>
            <tr>
            <?php foreach($elemento as $subelemento){ ?>
                
                    <?php   
                        if(key($elemento) == "fecha"){
                            $subelemento = '0000/'.($subelemento + 1).'/00';
                            //echo date("F", mktime(0, 0, 0, $subelemento, 10));
                            $fecha = strftime("%B", strtotime($subelemento));
                            $fecha = strtoupper($fecha);
                        }else{
                            if(key($elemento) == "cantidad_bultos"){
                                $cantidad_bultos += $subelemento;
                            }
                            if(key($elemento) == "volumen"){
                                $volumen += $subelemento;
                            }
                            if(key($elemento) == "total"){
                                $total += $subelemento;
                            }
                            if(key($elemento) == "bruto"){
                                $bruto += $subelemento;
                            }
                            if(key($elemento) == "dia"){
                                echo "<td style='border: solid 1px; font-weight: bold;'>".$subelemento."</td>";
                            }else{
                                echo "<td style='border: solid 1px;'>".$subelemento."</td>";
                            }
                        }
                        next($elemento)
                    ?>
                </td>
            <?php } ?><!--Foreach $elemento-->
            </tr>
        <?php } ?>
            <tr>
                <td style='border: solid 1px; font-weight: bold;'><strong>TOTAL</strong></td>
                <td style='border: solid 1px'><strong><?= $cantidad_bultos?></strong></td>
                <td style='border: solid 1px'><strong><?= $volumen?></strong></td>
                <td style='border: solid 1px'><strong><?= $total?></strong></td>
                <!--<td style='border: solid 1px'><strong><?//= $bruto?></strong></td>-->
            </tr>
        <?php } ?> 


        <?php if($postDomicilio){ ?>
            <tr>
                <td style='border: solid 1px; font-weight: bold;'>DIA</td>
                <td style='border: solid 1px; font-weight: bold;'>BULTOS</td>
                <td style='border: solid 1px; font-weight: bold;'>$TOTAL</td>
            </tr>
            <?php foreach($postDomicilio as $elemento){ ?>
            <tr>
            <?php foreach($elemento as $subelemento){ ?>
                
                    <?php   
                        if(key($elemento) == "fecha"){
                            $subelemento = '0000/'.($subelemento + 1).'/00';
                            //echo date("F", mktime(0, 0, 0, $subelemento, 10));
                            $fecha = strftime("%B", strtotime($subelemento));
                            $fecha = strtoupper($fecha);
                        }else{
                            if(key($elemento) == "cantidad_bultos"){
                                $cantidad_bultos += $subelemento;
                            }
                            if(key($elemento) == "volumen"){
                                $volumen += $subelemento;
                            }
                            if(key($elemento) == "total"){
                                $total += $subelemento;
                            }
                            if(key($elemento) == "bruto"){
                                $bruto += $subelemento;
                            }
                            if(key($elemento) == "dia"){
                                echo "<td style='border: solid 1px; font-weight: bold;'>".$subelemento."</td>";
                            }else if (key($elemento) != "volumen") {
                                echo "<td style='border: solid 1px;'>".$subelemento."</td>";
                            }
                        }
                        next($elemento)
                    ?>
                </td>
            <?php } ?><!--Foreach $elemento-->
            </tr>
        <?php } ?>
            <tr>
                <td style='border: solid 1px; font-weight: bold;'><strong>TOTAL</strong></td>
                <td style='border: solid 1px'><strong><?= $cantidad_bultos?></strong></td>
                <td style='border: solid 1px'><strong><?= $total?></strong></td>
                <!--<td style='border: solid 1px'><strong><?//= $bruto?></strong></td>-->
            </tr>
        <?php } ?>




        <?php if($postPorcentaje){ ?>
            <tr>
                <td style='border: solid 1px; font-weight: bold;'>DIA</td>
                <td style='border: solid 1px; font-weight: bold;'>PEDIDOS</td>
                <td style='border: solid 1px; font-weight: bold;'>FACTURADO</td>
            </tr>
            <?php foreach($postPorcentaje as $elemento){ ?>
            <tr>
            <?php foreach($elemento as $subelemento){ ?>
                
                    <?php   
                        if(key($elemento) == "fecha"){
                            $subelemento = '0000/'.($subelemento + 1).'/00';
                            //echo date("F", mktime(0, 0, 0, $subelemento, 10));
                            $fecha = strftime("%B", strtotime($subelemento));
                            $fecha = strtoupper($fecha);
                        }else{
                            if(key($elemento) == "cantidad_bultos"){
                                $cantidad_bultos += $subelemento;
                            }
                            if(key($elemento) == "volumen"){
                                $volumen += $subelemento;
                            }
                            if(key($elemento) == "total"){
                                $total += $subelemento;
                            }
                            if(key($elemento) == "bruto"){
                                $bruto += $subelemento;
                            }
                            if(key($elemento) == "dia"){
                                echo "<td style='border: solid 1px; font-weight: bold;'>".$subelemento."</td>";
                            }else if (key($elemento) != "volumen") {
                                echo "<td style='border: solid 1px;'>".$subelemento."</td>";
                            }
                        }
                        next($elemento)
                    ?>
                </td>
            <?php } ?><!--Foreach $elemento-->
            </tr>
        <?php } ?>
            <tr>
                <td style='border: solid 1px; font-weight: bold;'><strong>TOTAL</strong></td>
                <td style='border: solid 1px'><strong><?= $cantidad_bultos?></strong></td>
                <td style='border: solid 1px'><strong><?= $total?></strong></td>
            </tr>
        <?php } ?>


        <?php if($postVino){ ?>
            <tr>
                <td style='border: solid 1px; font-weight: bold;'>DIA</td>
                <td style='border: solid 1px; font-weight: bold;'>CAJAS</td>
                <td style='border: solid 1px; font-weight: bold;'>BOTELLAS</td>
                <td style='border: solid 1px; font-weight: bold;'>$TOTAL</td>
            </tr>
            <?php foreach($postVino as $elemento){ ?>
            <tr>
            <?php foreach($elemento as $subelemento){ ?>
                
                    <?php   
                        if(key($elemento) == "fecha"){
                            $subelemento = '0000/'.($subelemento + 1).'/00';
                            //echo date("F", mktime(0, 0, 0, $subelemento, 10));
                            $fecha = strftime("%B", strtotime($subelemento));
                            $fecha = strtoupper($fecha);
                        }else{
                            if(key($elemento) == "cantidad_bultos"){
                                $cantidad_bultos += $subelemento;
                            }
                            if(key($elemento) == "volumen"){
                                $volumen += $subelemento;
                            }
                            if(key($elemento) == "total"){
                                $total += $subelemento;
                            }
                            if(key($elemento) == "bruto"){
                                $bruto += $subelemento;
                            }
                            if(key($elemento) == "dia"){
                                echo "<td style='border: solid 1px; font-weight: bold;'>".$subelemento."</td>";
                            }else{
                                echo "<td style='border: solid 1px;'>".$subelemento."</td>";
                            }
                        }
                        next($elemento)
                    ?>
                </td>
            <?php } ?><!--Foreach $elemento-->
            </tr>
        <?php } ?>
            <tr>
                <td style='border: solid 1px; font-weight: bold;'><strong>TOTAL</strong></td>
                <td style='border: solid 1px'><strong><?= $cantidad_bultos?></strong></td>
                <td style='border: solid 1px'><strong><?= $volumen?></strong></td>
                <td style='border: solid 1px'><strong><?= $total?></strong></td>
                <!--<td style='border: solid 1px'><strong><?//= $bruto?></strong></td>-->
            </tr>
        <?php } ?> 

</table>
<!--
<br><br><br>
<table xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40">  
    
    <?php /*
        $fecha = $postAnterior[0][mes];
        $fecha = '0000/'.($fecha).'/00';
        //echo date("F", mktime(0, 0, 0, $subelemento, 10));
        $fecha = strftime("%B", strtotime($fecha));
        $fecha = strtoupper($fecha);
    ?>
    <caption style='text-align:center;border: solid 1px; font-weight: bold;'><?="TOTALES MES ANTERIOR"?></caption>
    <?php if($postAnterior){ ?>
            <tr>
                <td><?= "" ?></td>
                <td style='text-align:center;border: solid 1px; font-weight: bold;'>TOTAL BULTOS</td>
                <td style='text-align:center;border: solid 1px; font-weight: bold;'>$TOTAL</td>
            </tr>
            <tr>
                <td style='text-align:center;border: solid 1px; font-weight: bold;'>TOTAL</td>
                <?php
                    foreach($postAnterior as $elemento){
                        foreach($elemento as $subelemento){
                            if(key($elemento) != "mes" && key($elemento) != "anio")
                                echo "<td style='text-align:center;border: solid 1px'>".$subelemento."</td>";
                            next($elemento);
                        }
                    }
                ?>
            </tr>
        <?php } */?>       
</table>-->