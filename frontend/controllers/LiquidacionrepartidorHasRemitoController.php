<?php

namespace frontend\controllers;

use Yii;
use common\models\LiquidacionrepartidorHasRemito;
use frontend\models\LiquidacionrepartidorHasRemitoSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * LiquidacionrepartidorHasRemitoController implements the CRUD actions for LiquidacionrepartidorHasRemito model.
 */
class LiquidacionrepartidorHasRemitoController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all LiquidacionrepartidorHasRemito models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new LiquidacionrepartidorHasRemitoSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single LiquidacionrepartidorHasRemito model.
     * @param integer $liquidacionrepartidor_idliquidacionrepartidor
     * @param integer $remito_idremito
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($liquidacionrepartidor_idliquidacionrepartidor, $remito_idremito)
    {
        return $this->render('view', [
            'model' => $this->findModel($liquidacionrepartidor_idliquidacionrepartidor, $remito_idremito),
        ]);
    }

    /**
     * Creates a new LiquidacionrepartidorHasRemito model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new LiquidacionrepartidorHasRemito();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'liquidacionrepartidor_idliquidacionrepartidor' => $model->liquidacionrepartidor_idliquidacionrepartidor, 'remito_idremito' => $model->remito_idremito]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing LiquidacionrepartidorHasRemito model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $liquidacionrepartidor_idliquidacionrepartidor
     * @param integer $remito_idremito
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($liquidacionrepartidor_idliquidacionrepartidor, $remito_idremito)
    {
        $model = $this->findModel($liquidacionrepartidor_idliquidacionrepartidor, $remito_idremito);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'liquidacionrepartidor_idliquidacionrepartidor' => $model->liquidacionrepartidor_idliquidacionrepartidor, 'remito_idremito' => $model->remito_idremito]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing LiquidacionrepartidorHasRemito model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $liquidacionrepartidor_idliquidacionrepartidor
     * @param integer $remito_idremito
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($liquidacionrepartidor_idliquidacionrepartidor, $remito_idremito)
    {
        $this->findModel($liquidacionrepartidor_idliquidacionrepartidor, $remito_idremito)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the LiquidacionrepartidorHasRemito model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $liquidacionrepartidor_idliquidacionrepartidor
     * @param integer $remito_idremito
     * @return LiquidacionrepartidorHasRemito the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($liquidacionrepartidor_idliquidacionrepartidor, $remito_idremito)
    {
        if (($model = LiquidacionrepartidorHasRemito::findOne(['liquidacionrepartidor_idliquidacionrepartidor' => $liquidacionrepartidor_idliquidacionrepartidor, 'remito_idremito' => $remito_idremito])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
