<?php

use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

use common\models\OrdenGrupalDomicilio;

/* @var $this yii\web\View */
/* @var $model common\models\Remito */
$idOrdenGrupal = Yii::$app->getRequest()->getQueryParam('idOrdenGrupal');

$this->title = Yii::t('app', 'Agregar guías a Orden Grupal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Gu´as'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<h1><i class="fa fa-download"></i> <?= Html::encode($this->title) ?></h1>

<div class="x_panel">
    <div class="x_content">
        <?php
            echo $this->render('view', [
                'model' => OrdenGrupalDomicilio::findOne($idOrdenGrupal),
                ]);
        ?>
    </div>
</div>

<!--Añadir remito -->
<?php
    $ordenGrupal = OrdenGrupalDomicilio::findOne($idOrdenGrupal);
    if(!$ordenGrupal->liquidacioncliente_idliquidacioncliente && !$ordenGrupal->liquidacionrepartidor_idliquidacionrepartidor){
?>
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Agregar guías');?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

        <?= $this->render('../guia-domicilio/_form', [
            'model' => $model,
        ]) ?>
        </div>
    </div>
<?php }else{ ?>
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Agregar guías');?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">

       <h4>No se pueden agregar gúias. La orden ya está liquidada.</h4>
        </div>
    </div>
<?php }?>