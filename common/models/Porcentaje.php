<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%porcentaje}}".
 *
 * @property int $idporcentaje
 * @property double $cantidad
 *
 * @property LiquidacionPorcentajeCliente[] $liquidacionPorcentajeClientes
 * @property RemitoPorcentaje[] $remitoPorcentajes
 */
class Porcentaje extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%porcentaje}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cantidad'], 'required'],
            [['cantidad'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idporcentaje' => Yii::t('app', 'Idporcentaje'),
            'cantidad' => Yii::t('app', 'cantidad'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacionPorcentajeClientes()
    {
        return $this->hasMany(LiquidacionPorcentajeCliente::className(), ['porcentaje_idporcentaje' => 'idporcentaje']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemitoPorcentajes()
    {
        return $this->hasMany(RemitoPorcentaje::className(), ['porcentaje_idporcentaje' => 'idporcentaje']);
    }
}
