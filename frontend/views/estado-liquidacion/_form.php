<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Liquidacion */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="liquidacion-form">

    <?php $form = ActiveForm::begin(); ?>

    <!--<?//= $form->field($model, 'idestado_liquidacion')->textInput() ?>-->

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
