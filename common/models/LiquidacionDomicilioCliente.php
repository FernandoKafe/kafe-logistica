<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%liquidacion_domicilio_cliente}}".
 *
 * @property int $idliquidacion_domicilio_cliente
 * @property string $fecha_inicio
 * @property string $fecha_fin
 * @property double $total
 * @property string $nombre
 * @property double $pago_parcial
 * @property double $total_b
 * @property double $neto_b
 * @property double $total_iva
 * @property double $total_ar
 * @property double $importe_bultos
 * @property int $borrado
 * @property int $cliente_idcliente
 * @property int $estadoliquidacion_idestadoliquidacion
 * @property int $liquidacionformapago_idliquidacionformapago
 * @property int $cheque_idcheque
 * @property int $cantidad_bultos
 * @property string $creado
 * @property int $confirmada
 * @property string $fecha_pago
 * @property double $rendicion_comun 
 * @property double $rendicion_sprinter 
 * @property double $reparto 
 *
 * @property Cheque $chequeIdcheque
 * @property Cliente $clienteIdcliente
 * @property EstadoLiquidacion $estadoliquidacionIdestadoliquidacion
 * @property LiquidacionFormaPago $liquidacionformapagoIdliquidacionformapago
 * @property OrdenGrupalDomicilio[] $ordenGrupalDomicilios 
 */
class LiquidacionDomicilioCliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%liquidacion_domicilio_cliente}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_inicio', 'fecha_fin', 'nombre', 'cliente_idcliente'], 'required'],
            [['fecha_inicio', 'fecha_fin', 'creado', 'fecha_pago'], 'safe'],
            [['total', 'pago_parcial', 'total_b', 'neto_b', 'total_iva', 'total_ar', 'importe_bultos', 'rendicion_comun', 'rendicion_sprinter', 'reparto'], 'number'],
            [['cliente_idcliente', 'estadoliquidacion_idestadoliquidacion', 'liquidacionformapago_idliquidacionformapago', 'cheque_idcheque', 'borrado', 'cantidad_bultos', 'confirmada'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['cheque_idcheque'], 'exist', 'skipOnError' => true, 'targetClass' => Cheque::className(), 'targetAttribute' => ['cheque_idcheque' => 'idcheque']],
            [['cliente_idcliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['cliente_idcliente' => 'idcliente']],
            [['estadoliquidacion_idestadoliquidacion'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoLiquidacion::className(), 'targetAttribute' => ['estadoliquidacion_idestadoliquidacion' => 'idestado_liquidacion']],
            [['liquidacionformapago_idliquidacionformapago'], 'exist', 'skipOnError' => true, 'targetClass' => LiquidacionFormaPago::className(), 'targetAttribute' => ['liquidacionformapago_idliquidacionformapago' => 'idliquidacion_forma_pago']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idliquidacion_domicilio_cliente' => Yii::t('app', 'Idliquidacion Domicilio Cliente'),
            'fecha_inicio' => Yii::t('app', 'Desde'),
            'fecha_fin' => Yii::t('app', 'Hasta'),
            'total' => Yii::t('app', 'TOTAL'),
            'nombre' => Yii::t('app', 'Periodo'),
            'pago_parcial' => Yii::t('app', 'Pago Parcial'),
            'total_b' => Yii::t('app', 'TOTAL B'),
            'neto_b' => Yii::t('app', 'Neto B'),
            'total_iva' => Yii::t('app', 'TOTAL IVA'),
            'total_ar' => Yii::t('app', 'TOTAL guías A, R y otras'),
            'importe_bultos' => Yii::t('app', 'Importe bultos'),
            'borrado' => Yii::t('app', 'Borrado'),
            'cantidad_bultos' => Yii::t('app', 'Bultos totales'),
            'cliente_idcliente' => Yii::t('app', 'Cliente'),
            'estadoliquidacion_idestadoliquidacion' => Yii::t('app', 'Estado de liquidación'),
            'liquidacionformapago_idliquidacionformapago' => Yii::t('app', 'Liquidacion forma de pago'),
            'cheque_idcheque' => Yii::t('app', 'Cheque'),
            'creado' => Yii::t('app', 'Creado'),
            'confirmada' => Yii::t('app', 'Confirmada'), 
            'fecha_pago' => Yii::t('app', 'Fecha de pago'),
            'rendicion_comun' => Yii::t('app', 'Rendición Común'),
            'rendicion_sprinter' => Yii::t('app', 'Rendición Sprinter'),
            'reparto' => Yii::t('app', 'Reparto'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChequeIdcheque()
    {
        return $this->hasOne(Cheque::className(), ['idcheque' => 'cheque_idcheque']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClienteIdcliente()
    {
        return $this->hasOne(Cliente::className(), ['idcliente' => 'cliente_idcliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoliquidacionIdestadoliquidacion()
    {
        return $this->hasOne(EstadoLiquidacion::className(), ['idestado_liquidacion' => 'estadoliquidacion_idestadoliquidacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacionformapagoIdliquidacionformapago()
    {
        return $this->hasOne(LiquidacionFormaPago::className(), ['idliquidacion_forma_pago' => 'liquidacionformapago_idliquidacionformapago']);
    }

    // Function that return the sum of packages
    public static function getBultosSum($remitos) {
        $cant_bultos = 0;
        if($remitos){
            foreach($remitos as $r){
                $cant_bultos += $r->cantidad;
            }
            return $cant_bultos;
        }    
    }

    //Function that return the sum of amounts of guias tipo A
    public static function getMontoTipoBSum($remitos) {
        $suma = 0;
        if($remitos){
            foreach($remitos as $r){
                if($r->guia_tipo == "B")
                        $suma += $r->monto;
            }
        }    
        return $suma;
    }

    //Function that return the sum of amounts of guias tipo A
    public static function getMontoTipoOtrasSum($remitos) {
        $suma = 0;
        if($remitos){
            foreach($remitos as $r){
                if(($r->guia_tipo != "B"))
                        $suma += $r->monto;
            }
        }    
        return $suma;
    }

    //Function that return the sum of amounts of guias tipo A
    public static function getRendicionComunSum($remitos) {
        $suma = 0;
        if($remitos){
            foreach($remitos as $r){
                /*$remitosOrden = GuiaDomicilio::find()->where(['ordengrupaldomicilio_idordengrupaldomicilio' => $r->ordengrupaldomicilio_idordengrupaldomicilio])->orderBy(['sprinter' => SORT_DESC])->all();
                if(($remitosOrden[0]['sprinter'] != 1))*/
                if($r->sprinter != 1)
                        $suma += $r->monto;
            }
        }    
        return $suma;
    }

    //Function that return the sum of amounts of guias tipo A
    public static function getRendicionSprinterSum($remitos) {
        $suma = 0;
        if($remitos){
            foreach($remitos as $r){
                /*$remitosOrden = GuiaDomicilio::find()->where(['ordengrupaldomicilio_idordengrupaldomicilio' => $r->ordengrupaldomicilio_idordengrupaldomicilio])->orderBy(['sprinter' => SORT_DESC])->all();
                if(($remitosOrden[0]['sprinter'] == 1))*/
                if($r->sprinter == 1)
                        $suma += $r->monto;
            }
        }    
        return $suma;
    }

    //Function that return the sum of amounts of guias tipo A
    public static function getRendicionTotal($remitos) {
        $suma = 0;
        $fechaAnterior = 0;
        if($remitos){
            foreach($remitos as $r){
                if(($r->sprinter == 1) && ($fechaAnterior != $r->ordengrupaldomicilioIdordengrupaldomicilio->fecha))
                        $suma += $r->monto;
                if(($r->sprinter == 0))
                    $suma += $r->monto;
                $fechaAnterior = $r->ordengrupaldomicilioIdordengrupaldomicilio->fecha;
            }
        }    
        return $suma;
    }

    //Function that return the sum of amounts of guias tipo A
    public static function getRepartoSum($remitos) {
        $suma = 0;
        $remitoAnteriorOrden = 0;
        $sprinterFlag = 0;
        if($remitos){
            foreach($remitos as $r){
                $tarifario = TarifarioDomicilio::find()->where(['idtarifario_domicilio' => $r->ordengrupaldomicilioIdordengrupaldomicilio->clienteIdcliente->tarifariodomicilio_idtarifariodomicilio])->one();
                if($r->sprinter || ($sprinterFlag && $r->ordengrupaldomicilio_idordengrupaldomicilio == $remitoAnteriorOrden)){
                    if($r->sprinter){
                        $suma += $tarifario->sprinter;
                        $sprinterFlag = 1;
                    }else{
                        $suma += 0;
                    }
                }else{
                    if(($r->cantidad == 1) && (!$r->sprinter)){
                        $suma += $tarifario->primer_bulto;
                    }else if(($r->cantidad > 1) && (!$r->sprinter)){
                        $suma += ($tarifario->primer_bulto + (($r->cantidad - 1) * $tarifario->bulto_adicional));
                    }
                    $sprinterFlag = 0;
                }
                $remitoAnteriorOrden = $r->ordengrupaldomicilio_idordengrupaldomicilio;
            }
        }    
        return $suma;
    }
}
