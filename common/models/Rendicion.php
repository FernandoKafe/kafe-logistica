<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%rendicion}}".
 *
 * @property int $idrendicion
 * @property string $fecha_inicio
 * @property string $fecha_fin
 * @property double $monto
 * @property int $cliente_idcliente
 *
 * @property Cliente $clienteIdcliente
 */
class Rendicion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%rendicion}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_inicio', 'fecha_fin'], 'safe'],
            [['monto'], 'number'],
            [['cliente_idcliente'], 'required'],
            [['cliente_idcliente'], 'integer'],
            [['cliente_idcliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['cliente_idcliente' => 'idcliente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idrendicion' => Yii::t('app', 'Idrendicion'),
            'fecha_inicio' => Yii::t('app', 'Desde'),
            'fecha_fin' => Yii::t('app', 'Hasta'),
            'monto' => Yii::t('app', 'Monto'),
            'cliente_idcliente' => Yii::t('app', 'Cliente'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClienteIdcliente()
    {
        return $this->hasOne(Cliente::className(), ['idcliente' => 'cliente_idcliente']);
    }
}
