<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use \common\models\Localidad;
use \common\models\Empresa;
use \common\models\EmpresaTransporte;
use \common\models\Repartidor;
use \common\models\Estado;
use \common\models\Servicio;
use \common\models\FormaPago;
use \common\models\Producto;
use \yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use common\models\Sucursal;
use common\models\Carga;
use common\models\Cliente;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;
use yii\web\View;
use \common\models\Cheque;
use \common\models\LiquidacionFormaPago;



$script = <<< 'SCRIPT'
    $('#remito-servicio_idservicio').change(function(){
      if(this.value != 2){
        $('#remito-total_contrarembolso').attr('disabled', 'disabled');
      }else{
        $('#remito-total_contrarembolso').removeAttr('disabled');
      }
    });

    $('#remito-flete').change(function(){
      var x = document.getElementById("formulario_guia");
      if(this.value != "Local"){
        x.style.display = "";
      }else{
        x.style.display = "none";
      }
    });

    $('#remito-contra_pago').change(function(){
      var x = document.getElementById("cheque-div");
      if(this.value == 3){
        x.style.display = "block";
      }else{
        x.style.display = "none";
      }
    });

    $('#liquidacion-estadoliquidacion_idestadoliquidacion').change(function(){
      var x = document.getElementById("liquidacion_forma_pago");
      if(this.value != 1){
        x.style.display = "block";
      }else{
        x.style.display = "none";
      }
    });
SCRIPT;
$this->registerJs($script, View::POS_END);
/* @var $this yii\web\View */
/* @var $model common\models\Remito */
/* @var $form yii\widgets\ActiveForm */


$idRemitoGrupal = Yii::$app->getRequest()->getQueryParam('ordenderetirogrupal_idordenderetirogrupal');
$idRepartidor = Yii::$app->getRequest()->getQueryParam('repartidor_idrepartidor');
$idRemito = Yii::$app->getRequest()->getQueryParam('id');
$fechaGrupal = Yii::$app->getRequest()->getQueryParam('fecha');
$clienteOrden = Yii::$app->getRequest()->getQueryParam('cliente');
$selectedValueFormaPago = 3;
$selectedValueEntrega = 3;
?>

<div class="form-horizontal form-label-left">
  <?php $form = ActiveForm::begin(); ?>
  <div class="row col-md-12 col-xs-12">
    <h5><strong><i>IMPORTANTE:</i></strong> Los campos con un asterisco rojo (<span class="red">*</span>) son obligatorios</h5>
    
    <div class="hide"><?= $form->field($model, 'idremito')->hiddenInput()->label(false)?></div>
    
    <div class="hide"><?=  $form->errorSummary($model); ?></div>

    <div class="row" style="border: 1px solid">
      <?php
      if($idRemitoGrupal == 1){
        echo "<div class='col-md-4'>";
        if($clienteOrden)
          $model->cliente_idcliente = $clienteOrden;
        $cliente = empty($model->clienteIdcliente) ? '' : $model->clienteIdcliente->razonsocial;
        echo $form->field($model, 'cliente_idcliente')->widget(Select2::classname(), [
          'initValueText' => $cliente,
          'language' => 'es',
          'options' => ['placeholder' => 'Seleccionar Cliente ...'],
          'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 1,
            'ajax' => [
              'url' => Url::to(['/cliente/listado-for-tipo']),
              'dataType' => 'json',
              'data' => new JsExpression('function(params) {return {q:params.term, id:null, cliente_tipo:1}; }')
            ],
          ],
        ])->label('<a href="../cliente/create" target="_blank"><i class="fa fa-plus green"></i></a> Cliente');
        echo "</div>";
      } else {
        if($clienteOrden){
          $model->cliente_idcliente = $clienteOrden;
        }else{
          $model->cliente_idcliente = $model->ordenderetirogrupalIdordenderetirogrupal->cliente_idcliente;
        }
        echo $form->field($model, 'cliente_idcliente')->label(false)->hiddenInput();
      }
    ?>

      <?php
      //Si vienen seteados el repartidor y la fecha entonces los campos Nº de remito y fecha se ocultan
      //y se setean por defecto con los valores de las variables seteadas
      if(isset($idRepartidor) && isset($fechaGrupal) && ($idRemitoGrupal != 1)){
        $model->remitofecha = $fechaGrupal; 
      ?>

      <div class="hide">
        <?php
        echo $form->field($model, 'numero_remito')->textInput()->label(false)->hiddenInput();
        echo $form->field($model, 'remitofecha')->textInput()->label(false)->hiddenInput();
        ?>
      </div>

      <?php }else{?>
        <div class="col-md-4">
          <?php
          if($fechaGrupal){
            $model->remitofecha = $fechaGrupal;
          }else if(!$fechaGrupal && !$model->remitofecha){
              $model->remitofecha = date("Y-m-d",strtotime("-1 day"));
          } 
          echo $form->field($model, 'remitofecha')->widget(DatePicker::classname(), [
            'options' => [
              'placeholder' => 'aaaa-mm-dd',
              'value' => $fechaGrupal,
            ],
            'pluginOptions' => [
              'autoclose'=>true,
              'format' => 'yyyy-mm-dd',
              'endDate' => "0d"
            ],
            'language' => Yii::$app->language,
            'type' => DatePicker::TYPE_COMPONENT_PREPEND
          ])->label('Fecha de Remito');
          ?>
        </div>
        <div class="col-md-4"><?= $form->field($model, 'numero_remito')->textInput() ?></div>

      <?php } ?>

          <div class="col-md-6">
          <?php
            $sucursalRecibe = empty($model->sucursalIdsucursal) ? '' :
              $model->sucursalIdsucursal->empresaIdempresa->nombre
              .", ".$model->sucursalIdsucursal->nombre;

            echo $form->field($model, 'sucursal_idsucursal')->widget(Select2::classname(), [
              'initValueText' => $sucursalRecibe,
              'language' => 'es',
              'options' => ['placeholder' => 'Seleccionar Empresa ...'],
              'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                  'url' => Url::to(['/sucursal/listado']),
                  'dataType' => 'json',
                  'data' => new JsExpression('function(params) {return {q:params.term}; }')
                ],
              ],
            ])->label('<a href="../empresa/create" target="_blank"><i class="fa fa-plus green"></i></a> Empresa&nbsp&nbsp(<a href="../sucursal/create" target="_blank"><i class="fa fa-plus green"></i></a>Sucursal)');
            ?>
          </div>

          <div class="col-md-6">
          <?= $form->field($model, 'rol_sucursal')->dropDownList([
              'Recibe' => 'Recibe',
              'Remite' => 'Remite', ])->label("Empresa: Recibe o Remite?");
          ?>
          </div>

          <div class="col-md-12"><?= $form->field($model, 'terminal')->checkbox()?></div>
      </div>
      <!--<div class="col-md-6"><?//= $form->field($model, 'sucursal_idsucursal')->dropDownList($sucursal, ['prompt' => 'Seleccionar empresa...']) ?></div>-->
      <br>
      <div class="row" style="border: 1px solid">

        <div class="col-md-3"><?= $form->field($model, 'cantidad_bultos')->textInput() ?></div>

        <div class="col-md-3"><?= $form->field($model, 'carga_idcarga')->dropDownList($carga)?></div>

        <div class="col-md-3"><?= $form->field($model, 'valor_declarado')->textInput() ?></div>

        <div class="col-md-3"><?= $form->field($model, 'peso')->textInput() ?></div>

        <div class="col-md-3"><?= $form->field($model, 'volumen')->textInput() ?></div>

        <div class="col-md-3"><?= $form->field($model, 'flete_pagado')->textInput() ?></div>

        <div class="col-md-3"><?= $form->field($model, 'seguro')->textInput() ?></div>  

        <div class="col-md-3"><?= $form->field($model, 'formadepago_idformadepago')->dropDownList($formaPago, ['options' => [$selectedValueFormaPago => ['Selected'=>'selected']]])?></div>
        
        <?php if($idRemitoGrupal != 1) {
        $model->repartidor_idrepartidor = $idRepartidor;
        $model->producto_idproducto = 1?>
        <div class="hide">
          <?= $form->field($model, 'repartidor_idrepartidor') ?>
        </div>

        <?php } else {?>
          <div class="col-md-3">
            <?php
              if($idRepartidor){
                $model->repartidor_idrepartidor = $idRepartidor;
              }
              $repartidorEntrega = empty($model->repartidorIdrepartidor) ? '' :
              $model->repartidorIdrepartidor->apellido
              .", ".$model->repartidorIdrepartidor->nombre;

              echo $form->field($model, 'repartidor_idrepartidor')->widget(Select2::classname(), [
                'initValueText' => $repartidorEntrega,
                'language' => 'es',
                'options' => ['placeholder' => 'Seleccionar Repartidor ...'],
                'pluginOptions' => [
                  'allowClear' => true,
                  'minimumInputLength' => 3,
                  'ajax' => [
                    'url' => Url::to(['/repartidor/listado']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) {return {q:params.term}; }')
                  ],
                ],
              ])->label('<a href="../repartidor/create" target="_blank"><i class="fa fa-plus green"></i></a> Repartidor');
            ?>
          </div>

        <?php } ?>

        <div class="col-md-3">
          <?= $form->field($model, 'flete')->dropDownList([
              'Local' => 'Local',
              'Provincial' => 'Provincial',
              'Nacional' => 'Nacional', ])
          ?>
        </div>
            
        <div class="col-md-3"><?= $form->field($model, 'estado_idestado')->dropDownList($estado, ['options' => [$selectedValueEntrega => ['Selected'=>'selected']]])->label('Estado del envío') ?></div>

        <div class="col-md-12"><?= $form->field($model, 'observacion')->textArea() ?></div>

        <div class="hide">
        <?php
          $model->ordenderetirogrupal_idordenderetirogrupal = $idRemitoGrupal;
          echo $form->field($model, 'ordenderetirogrupal_idordenderetirogrupal')->textInput()->hiddenInput();
        ?>
      </div>

      <?php
        if($model->flete == ""){
          $formularioShow = "style='display: none'";
        } else {
          $formularioShow = "";
        }
      ?>

      <div class="x_panel" <?php echo $formularioShow?> id="formulario_guia">
        <div class="x_title">
          <h2>Guía</h2>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">

          <div class="col-md-8">
            <?php
              $empresaTransporte = empty($model->empresatransporteIdempresatransporte) ? '' :
              $model->empresatransporteIdempresatransporte->razonsocial;

              echo $form->field($model, 'empresatransporte_idempresatransporte')->widget(Select2::classname(), [
                'initValueText' => $empresaTransporte,
                'language' => 'es',
                'options' => ['placeholder' => 'Seleccionar Empresa de Transporte ...'],
                'pluginOptions' => [
                  'allowClear' => true,
                  'minimumInputLength' => 3,
                  'ajax' => [
                    'url' => Url::to(['/empresa-transporte/listado']),
                    'dataType' => 'json',
                    'data' => new JsExpression('function(params) {return {q:params.term}; }')
                  ],
                ],
              ])->label('<a href="../empresa-transporte/create" target="_blank"><i class="fa fa-plus green"></i></a> Empresa de transporte');
            ?>
          </div>

        <div class="col-md-4"><?= $form->field($model, 'numero_guia')->textInput(['maxlength' => true]) ?></div> 

        <div class="col-md-4"><?= $form->field($model, 'tipo_remito')->dropDownList([ 'No Aplica' => 'No Aplica', 'A' => 'A', 'B' => 'B', 'R' => 'R', ]) ?></div>

        <div class="col-md-4"><?= $form->field($model, 'total_guia')->textInput()->label('$TOTAL Guía') ?></div>

        <div class="col-md-4">
          <?= $form->field($model, 'facturado_a')->dropDownList([
            'Cliente' => 'Cliente',
            'Vertiente' => 'Vertiente'])
          ?>
        </div>
        </div>
      </div>

      </div><!--Row end-->
      <br>
      <div class="row" style="border: 1px solid;">
        <div class="col-md-4"><?= $form->field($model, 'servicio_idservicio')->dropDownList($servicio)->label('Servicio (Contra.)') ?></div>
        <?php
          if($model->servicio_idservicio == 2){
            $disabled = false;
          }else{
            $disabled = true;
          }
        ?>
        <div class="col-md-4"><?= $form->field($model, 'total_contrarembolso')->textInput(['disabled'=>$disabled]) ?></div>
        
        <div class="col-md-4"><?= $form->field($model, 'contra_pago')->dropDownList($contraPago)?></div>

        <div class="col-md-0"><?= $form->field($model, 'producto_idproducto')->dropDownList($producto)->hiddenInput()->label(false) ?></div>
          


        <?php
            if($model->contra_pago != 3){
              $chequeShow = "style='display:none'";
            } else {
              $chequeShow = '';
            }

            echo "<div class='cheque-form' ".$chequeShow." id='cheque-div'>"; ?>

              <div class="x_panel">
                <div class="x_title">
                  <h2><?= Yii::t('app', 'Formulario cheque') ?></h2>
                  <div class="clearfix"></div>
                </div>

                <?php 
                  if(!isset($cheque)){
                    $cheque = new Cheque();
                    //$cheque->liquidacion_idliquidacion = $idLiquidacion;
                  }
                ?>

                <div class="col-md-6 col-xs-12"><?= $form->field($cheque, 'numero')->textInput(['maxlength' => true]) ?></div>

                <div class="col-md-6 col-xs-12"><?= $form->field($cheque, 'banco')->textInput(['maxlength' => true]) ?></div>

                <div class="col-md-4 col-xs-12"><?= $form->field($cheque, 'monto')->textInput() ?></div>

                <div class="col-md-4 col-xs-12">
                <?=
                  $form->field($cheque, 'fecha')->widget(DatePicker::classname(), [
                    'options' => ['placeholder' => 'aaaa-mm-dd'],
                    'pluginOptions' => [
                      'autoclose'=>true,
                      'format' => 'yyyy-mm-dd',
                      'endDate' => "0d"
                    ],
                    'language' => Yii::$app->language,
                    'type' => DatePicker::TYPE_COMPONENT_PREPEND
                  ]);
                ?>  
                </div>

                <div class="col-md-4 col-xs-12"><?= $form->field($cheque, 'estado')->dropDownList([ 'Cobrado' => 'Cobrado', 'Rechazado' => 'Rechazado', ], ['prompt' => '']) ?></div>

                <div class="col-md-12 col-xs-12"><?= $form->field($cheque, 'observacion')->textarea(['rows' => 3]) ?></div>

              </div>
            </div>

      </div>
      <br>
      <div class="form-group col-md-12">
        <?php
          if($idRemitoGrupal == 1){
            echo Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);
            echo Html::submitButton('Guardar', ['class' => 'btn btn-success']);
          } else {
            echo Html::submitButton('Agregar', ['class' => 'btn btn-success']);
          }
        ?>
      </div>
      <?php ActiveForm::end(); ?>
  </div>
</div>