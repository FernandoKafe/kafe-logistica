<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%liquidacion_forma_pago}}".
 *
 * @property int $idliquidacion_forma_pago
 * @property string $nombre
 *
 * @property Liquidacion[] $liquidacions
 */
class LiquidacionFormaPago extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%liquidacion_forma_pago}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idliquidacion_forma_pago'], 'required'],
            [['idliquidacion_forma_pago'], 'integer'],
            [['nombre'], 'string', 'max' => 45],
            [['idliquidacion_forma_pago'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idliquidacion_forma_pago' => Yii::t('app', 'Idliquidacion Forma Pago'),
            'nombre' => Yii::t('app', 'Nombre'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacions()
    {
        return $this->hasMany(Liquidacion::className(), ['liquidacionformapago_idliquidacionformapago' => 'idliquidacion_forma_pago']);
    }
}
