<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Repartidor;
use \yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use common\models\EstadoLiquidacion;
use yii\web\View;


$script = <<< 'SCRIPT'
    $('#liquidacionrepartidor-estadoliquidacion_idestadoliquidacion').change(function(){
      if(this.value != 2){
        $('#liquidacionrepartidor-pago_parcial').attr('disabled', 'disabled');
      }else{
        $('#liquidacionrepartidor-pago_parcial').removeAttr('disabled');
      }
    });
SCRIPT;
$this->registerJs($script, View::POS_END);
/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionRepartidor */
/* @var $form yii\widgets\ActiveForm */
$repartidores=ArrayHelper::map(Repartidor::find()->orderBy('apellido')->asArray()->all(), 'idrepartidor', 'nombre');
$idLiquidacionrepartidor = Yii::$app->getRequest()->getQueryParam('id');
$estado = ArrayHelper::map(EstadoLiquidacion::find()->all(),'idestado_liquidacion','nombre');


?>

<div class="liquidacion-repartidor-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="hide"><?=  $form->errorSummary($model); ?></div>

    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'repartidor_idrepartidor')->dropdownList($repartidores)->label('Repartidor') ?></div>

    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'periodo')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-3  col-xs-12">
      <?=
      $form->field($model, 'periodo_inicio')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'aaaa-mm-dd'],
        'pluginOptions' => [
          'autoclose'=>true,
          'format' => 'yyyy-mm-dd'
        ],
        'language' => Yii::$app->language,
        'type' => DatePicker::TYPE_COMPONENT_PREPEND
      ])->label('Desde');
      ?>
    </div>

    <div class="col-md-3  col-xs-12">
      <?=
      $form->field($model, 'periodo_fin')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'aaaa-mm-dd'],
        'pluginOptions' => [
          'autoclose'=>true,
          'format' => 'yyyy-mm-dd',
          'endDate' => "0d"
        ],
        'language' => Yii::$app->language,
        'type' => DatePicker::TYPE_COMPONENT_PREPEND
      ])->label('Hasta');
      ?>
    </div>

    <?php if(isset($idLiquidacionrepartidor)){ ?>
      <?php
        if($model->estadoliquidacion_idestadoliquidacion == 3){
          $disabledEstado = true;
        }else{
          $disabledEstado = false;
        }

        if($model->estadoliquidacion_idestadoliquidacion != 2){
          $disabledParcial = true;
        }else{
          $disabledParcial = false;
        }
      ?>
      <div class="col-md-3 col-xs-12"><?= $form->field($model, 'estadoliquidacion_idestadoliquidacion')->dropdownList($estado, ['options' => [1 => ['disabled' => $disabledEstado], 2 => ['disabled' => $disabledEstado]]]) ?></div>
      <div class="col-md-3 col-xs-12"><?= $form->field($model, 'pago_parcial')->textInput(['disabled'=>$disabledParcial]) ?></div>
    <?php }?>

    <div class="form-group col-md-12">
      <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);?>

      <?php if(isset($idLiquidacionrepartidor)){ ?>
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
      <?php }else{ ?>
        <?= Html::submitButton(Yii::t('app', 'Crear'), ['class' => 'btn btn-success']) ?>
      <?php } ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
