<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LiquidacionPorcentajeCliente;

/**
 * LiquidacionPorcentajeClienteSearch represents the model behind the search form of `common\models\LiquidacionPorcentajeCliente`.
 */
class LiquidacionPorcentajeClienteSearch extends LiquidacionPorcentajeCliente
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idliquidacion_porcentaje_cliente', 'cheque_idcheque', 'confirmada'], 'integer'],
            [['periodo', 'periodo_inicio', 'periodo_fin', 'cliente_idcliente', 'estadoliquidacion_idestadoliquidacion', 'liquidacionformapago_idliquidacionformapago', 'creado', 'fecha_pago', 'pedidos'], 'safe'],
            [['total_rendido', 'neto_sin_iva', 'combustible', 'comision', 'subtotal', 'iva', 'total_real', 'total_cobrar', 'pago_parcial', 'pallets', 'total_facturado', 'total_devuelto', 'total_faltante', 'total_rendido_al_3', 'total_rendido_al_5'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LiquidacionPorcentajeCliente::find()->where(['confirmada' => 1]);
        $query->joinWith(['clienteIdcliente as cliente']);
        $query->joinWith(['estadoliquidacionIdestadoliquidacion as eatado']);
        $query->joinWith(['liquidacionformapagoIdliquidacionformapago as formaPago']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idliquidacion_porcentaje_cliente' => $this->idliquidacion_porcentaje_cliente,
            'total_rendido' => $this->total_rendido,
            'neto_sin_iva' => $this->neto_sin_iva,
            'combustible' => $this->combustible,
            'comision' => $this->comision,
            'subtotal' => $this->subtotal,
            'iva' => $this->iva,
            'total_real' => $this->total_real,
            'total_cobrar' => $this->total_cobrar,
            'periodo_inicio' => $this->periodo_inicio,
            'periodo_fin' => $this->periodo_fin,
            'pago_parcial' => $this->pago_parcial,
            'creado' => $this->creado,
            'confirmada' => $this->confirmada,
            'fecha_pago' => $this->fecha_pago,
            'pedidos' => $this->pedidos, 
            'total_facturado' => $this->total_facturado, 
            'total_devuelto' => $this->total_devuelto, 
            'pallets' => $this->pallets, 
            'total_faltante' => $this->total_faltante, 
            'total_rendido_al_3' => $this->total_rendido_al_3,
            'total_rendido_al_5' => $this->total_rendido_al_5
        ]);

        $query->andFilterWhere(['like', 'periodo', $this->periodo])
        ->andFilterWhere(['like', 'cliente.razonsocial', $this->cliente_idcliente])
        ->andFilterWhere(['like', 'estado.nombre', $this->estadoliquidacion_idestadoliquidacion])
        ->andFilterWhere(['like', 'formaPago.nombre', $this->liquidacionformapago_idliquidacionformapago]);

        return $dataProvider;
    }
}
