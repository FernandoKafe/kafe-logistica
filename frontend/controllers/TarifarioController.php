<?php

namespace frontend\controllers;

use Yii;
use common\models\Tarifario;
use common\models\TarifarioHasPrecio;
use frontend\models\TarifarioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter; 
use yii\web\Response;


/**
 * TarifarioController implements the CRUD actions for Tarifario model.
 */
class TarifarioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Tarifario models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TarifarioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Tarifario model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Tarifario model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Tarifario();

        if ($model->load(Yii::$app->request->post())) {
            $tarifario = Tarifario::find()->where(['nombre' => $model->nombre])->one();
            if($tarifario){
                Yii::$app->session->setflash(
                    'error',
                    "Ya existe un tarifario: ".$model->nombre."!");
                    return $this->redirect(Yii::$app->request->referrer);
            }
            $model->save();
            return $this->redirect(['add-precio', 'id' => $model->idtarifario]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    /**
     * Updates an existing Tarifario model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['add-precio', 'id' => $model->idtarifario]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Tarifario model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Tarifario model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Tarifario the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Tarifario::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * @param integer $id idcomprobante
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAddPrecio()
    {
        $model = new TarifarioHasPrecio();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['add-precio',
                    'id' => $model->tarifario_idtarifario,
                ]);
        }

        return $this->render('add-precio', [
            'model' => $model,
        ]);
    }

    /**
     * Listado de Tarifarios para Select2 Ajax
     * @param String|$q Texto de busqueda
     * @param int|$id   Iedntificador de tarifario
     * @return string|\yii\web\Response
     */
    public function actionListado($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $out['results'] = Tarifario::getTarifarioCompleto($q);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Tarifario::findOne($id)->idtarifario];
        }
        return $out;
    }
}
