<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "empresa_transporte".
 *
 * @property int $idempresatransporte
 * @property string $razonsocial
 * @property string $cuit
 *
 * @property Remito[] $remitos
 */
class EmpresaTransporte extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empresa_transporte';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['razonsocial'], 'required'],
            [['razonsocial'], 'unique'],
            [['razonsocial', 'cuit'], 'string', 'max' => 45],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idempresatransporte' => 'Empresa de Transporte',
            'razonsocial' => 'Razón Social',
            'cuit' => 'CUIT',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemitos()
    {
        return $this->hasMany(Remito::className(), ['empresatransporte_idempresatransporte' => 'idempresatransporte']);
    }

    /**
     * Función que busca un empleado con nombre completo
     * @param string $q 
     * @return [id, text] Arreglo de identificador y razon social
     */
    public static function getEmpresaTransporte($q){
        $query = self::find();
        $query->select(['idempresatransporte AS id', 'razonsocial AS text',])
            ->where(['like', 'razonsocial', $q])
            ->limit(20);
        return $query->asArray()->all();
    }
}
