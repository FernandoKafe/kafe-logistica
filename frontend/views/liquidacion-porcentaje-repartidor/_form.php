<?php

use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use kartik\date\DatePicker;
use yii\helpers\Html;

use common\models\EstadoLiquidacion;
use common\models\Repartidor;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionPorcentajeRepartidor */
/* @var $form yii\widgets\ActiveForm */
$repartidores=ArrayHelper::map(Repartidor::find()->orderBy('apellido')->asArray()->all(), 'idrepartidor', 'nombre');
$idLiquidacionrepartidor = Yii::$app->getRequest()->getQueryParam('id');
$estado = ArrayHelper::map(EstadoLiquidacion::find()->all(),'idestado_liquidacion','nombre');

?> 

<div class="liquidacion-porcentaje-repartidor-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model)?>

    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'repartidor_idrepartidor')->dropdownList($repartidores)->label('Repartidor') ?></div>

    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'periodo')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-3 col-xs-12">
        <?=
          $form->field($model, 'fecha_inicio')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'aaaa-mm-dd'],
            'pluginOptions' => [
              'autoclose'=>true,
              'format' => 'yyyy-mm-dd'
            ],
            'language' => Yii::$app->language,
            'type' => DatePicker::TYPE_COMPONENT_PREPEND
          ]);
        ?>  
    </div>

    <div class="col-md-3 col-xs-12">
        <?=
          $form->field($model, 'fecha_fin')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'aaaa-mm-dd'],
            'pluginOptions' => [
              'autoclose'=>true,
              'format' => 'yyyy-mm-dd',
              'endDate' => "0d"
            ],
            'language' => Yii::$app->language,
            'type' => DatePicker::TYPE_COMPONENT_PREPEND
          ]);
        ?>  
    </div>

    <?php
        if(isset($idLiquidacionrepartidor)){ 
    ?>
            <div class="col-md-3 col-xs-12"><?= $form->field($model, 'estadoliquidacion_idestadoliquidacion')->dropdownList($estado)?></div>
    <?php
        }
    ?>

    <div class="hide"><?= $form->field($model, 'estadoliquidacion_idestadoliquidacion')->dropdownList($estado)?></div>

   <div class="form-group col-md-12">
      <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);?>

      <?php if(isset($idLiquidacionrepartidor)){ ?>
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
      <?php }else{ ?>
        <?= Html::submitButton(Yii::t('app', 'Crear'), ['class' => 'btn btn-success']) ?>
      <?php } ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
