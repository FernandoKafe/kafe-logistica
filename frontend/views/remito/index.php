<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use \yii\helpers\ArrayHelper;
use common\models\Estado;

$this->title = Yii::t('app', 'Remitos');
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><i class="fa fa-file"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2>Listado</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php Pjax::begin(); ?>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Crear Remito individual',
            ['create', 'ordenderetirogrupal_idordenderetirogrupal' => 1],
            ['class' => 'btn btn-success'])
        ?>
    </p> 
 
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'moduleId' => 'gridviewk',
        'toolbar' =>  [
            '{export}',
            '{toggleData}',
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
        ],
        'columns' => [
            [
                'attribute' => 'idremito',
                'label' => 'ID',
            ],
            [
                'attribute' => 'numero_remito',
                'label' => 'Remito Nº'
            ],
            [
                'attribute' => 'ordenderetirogrupal_idordenderetirogrupal',
                'label' => 'Orden Grupal Nº',
                'content' => function($data){
                    return $data->ordenderetirogrupalIdordenderetirogrupal->numero;
                }
            ],
            [
                'attribute' => 'remitofecha',
                'label' => 'Fecha' 
            ],
            [
                'attribute' => 'numero_guia',
                'label' => 'Guía Nº'
            ],
            [
                'attribute' => 'cliente_idcliente',
                'format' => 'text',//raw, html
                'label' => 'Cliente',
                'content' => function($data){
                    return $data->clienteIdcliente->razonsocial;
                }
            ],
            [
                'attribute' => 'sucursal_idsucursal',
                'format' => 'text',//raw, html
                'content' => function($data){
                    return $data->sucursalIdsucursal->empresaIdempresa->nombre;
                }
            ],
            [
                'attribute' => 'repartidor_idrepartidor',
                'value' => function($data){
                    return $data->repartidorIdrepartidor->apellido.", ".$data->repartidorIdrepartidor->nombre;
                }
            ],
            [
                'attribute'=>'liquidado',
                'label' => 'Liquidado Cliente',
                'format' => 'boolean',
            ],
            [
                'attribute'=>'liquidado_repartidor',
                'label' => 'Liquidado Repartidor',
                'format' => 'boolean',
            ],
            [
                'attribute'=>'estado_idestado',
                'filter'=> $estados,
                'content' => function($data){
                    return $data->estadoIdestado->nombre;
                }
            ],
            [
                'attribute' => 'creado',
                'label' => 'Fecha y Hora creación',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Acciones',
                'headerOptions' => ['style' => 'color:#337ab7'],
                'template' => '{view}{update}{delete}',
                'buttons' => [
                  'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url, [
                                    'title' => Yii::t('app', 'Ver'),
                        ]);
                  },

                  'update' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                    'title' => Yii::t('app', 'Actualizar'),
                        ]);
                    }, 

                    'update' => function ($url, $model) {
                          return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                                      'title' => Yii::t('app', 'Actualizar'),
                          ]);
                      },

                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url ='view?id='.$model->idremito.'&ordenderetirogrupal_idordenderetirogrupal='.$model->ordenderetirogrupal_idordenderetirogrupal;
                        return $url;
                    }

                    if ($action === 'update') {
                        $url ='update?id='.$model->idremito
                        .'&ordenderetirogrupal_idordenderetirogrupal='.$model->ordenderetirogrupal_idordenderetirogrupal
                        .'&repartidor_idrepartidor='.$model->repartidor_idrepartidor;
                        return $url;
                    }
                     if ($action === 'delete') {
                        $url ='delete?id='.$model->idremito;
                        return $url;
                    }
                }
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>
</div>
