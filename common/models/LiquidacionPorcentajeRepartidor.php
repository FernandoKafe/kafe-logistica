<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%liquidacion_porcentaje_repartidor}}".
 *
 * @property int $idliquidacion_porcentaje_repartidor
 * @property double $total
 * @property string $fecha_inicio
 * @property string $fecha_fin
 * @property string $periodo
 * @property int $repartidor_idrepartidor
 * @property string $periodo_fin
 * @property int $estadoliquidacion_idestadoliquidacion
 * @property string $creado
 * @property int liquidacionunica
 *
 * @property EstadoLiquidacion $estadoliquidacionIdestadoliquidacion
 * @property Repartidor $repartidorIdrepartidor
 * @property LiquidacionporcentajerepartidorHasRemito[] $liquidacionporcentajerepartidorHasRemitos
 */
class LiquidacionPorcentajeRepartidor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%liquidacion_porcentaje_repartidor}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['total'], 'number'],
            [['fecha_inicio', 'fecha_fin', 'creado'], 'safe'],
            [['repartidor_idrepartidor', 'estadoliquidacion_idestadoliquidacion'], 'required'],
            [['repartidor_idrepartidor', 'estadoliquidacion_idestadoliquidacion', 'liquidacionunica'], 'integer'],
            [['periodo'], 'string', 'max' => 100],
            [['estadoliquidacion_idestadoliquidacion'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoLiquidacion::className(), 'targetAttribute' => ['estadoliquidacion_idestadoliquidacion' => 'idestado_liquidacion']],
            [['repartidor_idrepartidor'], 'exist', 'skipOnError' => true, 'targetClass' => Repartidor::className(), 'targetAttribute' => ['repartidor_idrepartidor' => 'idrepartidor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idliquidacion_porcentaje_repartidor' => Yii::t('app', 'Id liquidación % Repartidor'),
            'total' => Yii::t('app', 'TOTAL'),
            'fecha_inicio' => Yii::t('app', 'Fecha Inicio'),
            'fecha_fin' => Yii::t('app', 'Fecha Fin'),
            'periodo' => Yii::t('app', 'Periodo'),
            'repartidor_idrepartidor' => Yii::t('app', 'Repartidor'),
            'estadoliquidacion_idestadoliquidacion' => Yii::t('app', 'Estado liquidación'),
            'creado' => Yii::t('app', 'Creado'),
            'liquidacionunica' => Yii::t('app', 'Liquidación única'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoliquidacionIdestadoliquidacion()
    {
        return $this->hasOne(EstadoLiquidacion::className(), ['idestado_liquidacion' => 'estadoliquidacion_idestadoliquidacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepartidorIdrepartidor()
    {
        return $this->hasOne(Repartidor::className(), ['idrepartidor' => 'repartidor_idrepartidor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacionporcentajerepartidorHasRemitos()
    {
        return $this->hasMany(LiquidacionporcentajerepartidorHasRemito::className(), ['lpr_idlpr' => 'idliquidacion_porcentaje_repartidor']);
    }

    public static function createLiquidacion($model, $liquidacionUnica){
        $model->save();
        //Traer todo los remitos porcentaje asociados al repartidor comprendidos entre fecha_inicio y fecha_fin
        $remitos = RemitoPorcentaje::getRemitosPorcentajeFechaRepartidor($model->repartidor_idrepartidor, $model->fecha_inicio, $model->fecha_fin);
        $flag = 0;
        if($remitos){
            foreach($remitos as $r){
                $pago = PagosPorcentajesRepartidor::getPagoByPorcentaje(($r->cant_entregados * 100) / $r->cant_pedidos);
                if(!$pago){
                    Yii::$app->session->setFlash('error', 'En tarifario porcentaje faltan especificar montos de pago para el porcentaje:'.(($r->cant_entregados * 100) / $r->cant_pedidos));
                    return false;
                }else{
                    $flag = 1;
                }
            }
        }else{
            $model->delete();
            return false;            
        }
        //Entra al if si existen remitos asociados sino muestra mensaje de error
        if($remitos){
            // Recorre cada remito
            $primerMonto = 0;
            $primerPorcentaje = 0;
            $acumularPorcentaje = 0;
            $fechaAnterior = 0;
            $contadorB = 1;
            $model->total = 0;
            $faltante = 0;
            $actual = 1;
            foreach($remitos as $r){ 
                // Crea una nueva relación para relacionar remito con liquidación
                $rel = new LiquidacionporcentajerepartidorHasRemito();
                // Guarda el ID de la liquidación en la relación
                $rel->lpr_idlpr = $model->idliquidacion_porcentaje_repartidor;
                //Guarda el ID del remito en la liquidación
                $rel->remitoporcentaje_idremitoporcentaje = $r->idremito_porcentaje;
                //Persiste la relación en la BD
                $rel->save();
                // Se almacena en la variable $pago el monto a pagar en relación al porcentaje de entrega
                
                if($r->fecha != $remitos[$actual -1]->fecha  && $r->fecha != $remitos[$actual + 1]->fecha ){
                    $r->porcentaje_entregas = $porcentaje_entregas = (($r->cant_entregados * 100) / $r->cant_pedidos);
                    $r->reparto_seleccionado = PagosPorcentajesRepartidor::getPagoByPorcentaje($porcentaje_entregas/($contadorB))->monto;
                    $r->se_paga = 1;
                }
                if($r->fecha != $remitos[$actual - 1]->fecha  && $r->fecha === $remitos[$actual + $contadorB]->fecha ) {
                    $r->porcentaje_entregas = $porcentaje_entregas = (($r->cant_entregados * 100) / $r->cant_pedidos);
                    while($r->fecha != $remitos[$actual - 1]->fecha  && $r->fecha == $remitos[$actual + $contadorB]->fecha ){
                        $porcentaje_entregas += (($remitos[$actual + $contadorB]->cant_entregados * 100) / $remitos[$actual + $contadorB]->cant_pedidos);
                        $contadorB++;
                    }

                    $r->porcentaje_entregas = ($porcentaje_entregas / $contadorB);
                    $r->se_paga = 1;
                    $r->reparto_seleccionado = PagosPorcentajesRepartidor::getPagoByPorcentaje($r->porcentaje_entregas)->monto;
                    $contadorB = 1;
                }
                if($r->fecha == $remitos[$actual - 1]->fecha){
                    $r->se_paga = 0;
                    $r->porcentaje_entregas = $remitos[$actual - 1]->porcentaje_entregas;
                    $r->reparto_seleccionado = $remitos[$actual - 1]->reparto_seleccionado;
                }
                $porcentaje_entregas=0;
                $actual++;
                //Entra al IF si el remito actual tiene la misma fecha que el anterior.
                if($r->se_paga == 1){
                    $model->total += $r->reparto_seleccionado;
                }
                $faltante += $r->dinero_faltante;
                //El remito se marca cómo liquidado al repartidor
                $r->liquidado_repartidor = 1;
                $pago = PagosPorcentajesRepartidor::getById(1);
                $r->reparto_uno = $pago->monto;
                $pago = PagosPorcentajesRepartidor::getById(2);
                $r->reparto_dos = $pago->monto;
                $pago = PagosPorcentajesRepartidor::getById(3);
                $r->reparto_tres = $pago->monto;
                $pago = PagosPorcentajesRepartidor::getById(4);
                $r->reparto_cuatro = $pago->monto;
                // Se persiste el remito en la BD
                $r->save(false);
            }
            $model->total = $model->total - $faltante;
            $model->liquidacionunica = $liquidacionUnica;
            //Se persiste la liquidación en la BD
            $model->creado = date('Y-m-d G:i', time());
            $model->save();
            return $model;
        } 
    }

    public static function deleteLiquidacion($liquidacionUnica){
        $model = self::find()->where(['fecha_inicio' => $liquidacionUnica->fecha_inicio])
        ->andWhere(['fecha_fin' => $liquidacionUnica->fecha_fin])
        ->andWhere(['periodo' => $liquidacionUnica->nombre])
        ->andWhere(['repartidor_idrepartidor' => $liquidacionUnica->repartidor_idrepartidor])->one();

        $rel = LiquidacionporcentajerepartidorHasRemito::find()->where(['lpr_idlpr' => $model->idliquidacion_porcentaje_repartidor])->all();
        if($rel){
            foreach($rel as $r){
                $remito = RemitoPorcentaje::find()->where(['idremito_porcentaje' => $r->remitoporcentaje_idremitoporcentaje])->one();
                if($remito){
                    $remito->liquidado_repartidor = 0;
                    $remito->save();
                }
                $r->delete();
            }
        }

        if($model){
            $model->delete();
            return 0;
        }else{
            return 1;
        }
    }

    public static function getAllFromUnica($idUnica){
        return self::find()->select('idliquidacion_porcentaje_repartidor')->where(['liquidacionunica' => $idUnica])->one();
    }
}
