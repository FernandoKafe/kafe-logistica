<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TarifarioHasPrecio;

/**
 * TarifarioHasPrecioSearch represents the model behind the search form of `common\models\TarifarioHasPrecio`.
 */
class TarifarioHasPrecioSearch extends TarifarioHasPrecio
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['carga_idcarga', 'precio', 'zona_idzona', 'tarifario_idtarifario', 'precio_reparto', 'precio_adicional', 'precio_reparto_adicional', 'porcentaje_contra'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TarifarioHasPrecio::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'carga_idcarga' => $this->carga_idcarga,
            'precio' => $this->precio,
            'zona_idzona' => $this->zona_idzona,
            'tarifario_idtarifario' => $this->tarifario_idtarifario,
            'precio_repartio' => $this->precio_reparto,
            'precio_adicional' => $this->precio_adicional,
            'precio_reparto_adicional' => $this->precio_reparto_adicional,
            'porcentaje_contra' => $this->porcentaje_contra,
        ]);

        return $dataProvider;
    }
}
