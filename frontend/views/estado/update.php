<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Estado */

$this->title = Yii::t('app', 'Update Estado: ' . $model->nombre);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Estados'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idestado, 'url' => ['view', 'id' => $model->idestado]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<h1><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario de modificación') ?></h2>
        <div class="clearfix"></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
