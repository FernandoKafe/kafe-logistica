<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use \yii\helpers\ArrayHelper;
use yii\web\View;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\helpers\Url;

use common\models\LiquidacionFormaPago;
use \common\models\Cliente;
use \common\models\Cheque;
use common\models\EstadoLiquidacion;

$script = <<< 'SCRIPT'
    $('#liquidaciondomiciliocliente-estadoliquidacion_idestadoliquidacion').change(function(){
      if(this.value == 2 || this.value == 4){
        $('#liquidaciondomiciliocliente-pago_parcial').removeAttr('disabled');
      }else{
        $('#liquidaciondomiciliocliente-pago_parcial').attr('disabled', 'disabled');
      }
    });

    $('#liquidaciondomiciliocliente-liquidacionformapago_idliquidacionformapago').change(function(){
      var x = document.getElementById("cheque-div");
      if(this.value == 3){
        x.style.display = "block";
      }else{
        x.style.display = "none";
      }
    });

    $('#liquidaciondomiciliocliente-estadoliquidacion_idestadoliquidacion').change(function(){
      var x = document.getElementById("liquidacion_forma_pago");
      if(this.value != 1){
        x.style.display = "block";
      }else{
        x.style.display = "none";
      }
    });
SCRIPT;
$this->registerJs($script, View::POS_END);
/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionDomicilioCliente */
/* @var $form yii\widgets\ActiveForm */
$idLiquidacion = Yii::$app->getRequest()->getQueryParam('id');
$rendicion = Yii::$app->getRequest()->getQueryParam('rendicion');
$cliente = ArrayHelper::map(Cliente::find()->all(),'idcliente','razonsocial');
$estado = ArrayHelper::map(EstadoLiquidacion::find()->all(),'idestado_liquidacion','nombre');
$formaPago = ArrayHelper::map(LiquidacionFormaPago::find()->all(),'idliquidacion_forma_pago','nombre');
?>

<div class="liquidacion-domicilio-cliente-form">  

    <?php 
        if(!$rendicion){
            $form = ActiveForm::begin(); 
        }else {
            $form = ActiveForm::begin([
                'action' => Url::to(['create-rendicion', 'rendicion' => 1]),
            ]);
        }
    ?>

    <div class="hide"><?=  $form->errorSummary($model); ?></div>

    <!--<?//= $form->field($model, 'idliquidacion')->textInput() ?>-->
    <div class="col-md-3 col-xs-12">
        <?= $form->field($model, 'cliente_idcliente')->widget(Select2::classname(), [
                'initValueText' => $cliente,
                'language' => 'es',
                'options' => ['placeholder' => 'Seleccionar Cliente ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 1,
                    'ajax' => [
                        'url' => Url::to(['/cliente/listado-for-tipo']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) {return {q:params.term, id:null, cliente_tipo:2}; }')
                    ],
                ],
            ])->label('<a href="../cliente/create" target="_blank"><i class="fa fa-plus green"></i></a> Cliente');?>
    </div>

    <?php
    if(!$rendicion){
    ?>
        <div class="col-md-3 col-xs-12"><?= $form->field($model, 'nombre')->textInput() ?></div>
    <?php } ?> 
    <div class="col-md-3  col-xs-12">
    <?=
    $form->field($model, 'fecha_inicio')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'aaaa-mm-dd'],
        'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd'
        ],
        'language' => Yii::$app->language,
        'type' => DatePicker::TYPE_COMPONENT_PREPEND
    ])->label('Desde');
    ?>
    </div>

    <div class="col-md-3  col-xs-12">
    <?php
        echo $form->field($model, 'fecha_fin')->widget(DatePicker::classname(), [
        'options' => ['placeholder' => 'aaaa-mm-dd'],
        'pluginOptions' => [
        'autoclose'=>true,
        'format' => 'yyyy-mm-dd'
        ],
        'language' => Yii::$app->language,
        'type' => DatePicker::TYPE_COMPONENT_PREPEND
    ])->label('Hasta');
    ?>
    </div>

    <?php if(isset($idLiquidacion)){ ?>

    <?php
    if($model->estadoliquidacion_idestadoliquidacion == 3){
        $disabledEstado = true;
    }else{
        $disabledEstado = false;
    }
    if($model->estadoliquidacion_idestadoliquidacion == 2 || $model->estadoliquidacion_idestadoliquidacion == 4){
        $disabledParcial = false;
    }else{
        $disabledParcial = true;
    }
    ?>
    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'estadoliquidacion_idestadoliquidacion')->dropdownList($estado, ['options' => [1 => ['disabled' => $disabledEstado], 2 => ['disabled' => $disabledEstado]]]) ?></div>

    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'pago_parcial')->textInput(['disabled'=>$disabledParcial])->label('Entrega monto') ?></div>
    <?php
    if($model->estadoliquidacion_idestadoliquidacion != 1){
        $pagoShow = '';
    } else {
        $pagoShow = "style='display:none'";
    }
    echo "<div class='col-md-3 col-xs-12' ".$pagoShow." id='liquidacion_forma_pago'>".$form->field($model, 'liquidacionformapago_idliquidacionformapago')->dropdownList($formaPago)."</div>" ?>

    <?php
    if($model->liquidacionformapago_idliquidacionformapago != 3){
        $chequeShow = "style='display:none'";
    } else {
        $chequeShow = '';
    }

    echo "<div class='cheque-form' ".$chequeShow." id='cheque-div'>"; ?>

    <div class="x_panel">
        <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario cheque') ?></h2>
        <div class="clearfix"></div>
        </div>

        <?php 
        if(!isset($cheque)){
            $cheque = new Cheque();
            //$cheque->liquidacion_idliquidacion = $idLiquidacion;
        }
        
        ?>

        <div class="col-md-6 col-xs-12"><?= $form->field($cheque, 'numero')->textInput(['maxlength' => true]) ?></div>

        <div class="col-md-6 col-xs-12"><?= $form->field($cheque, 'banco')->textInput(['maxlength' => true]) ?></div>

        <div class="col-md-4 col-xs-12"><?= $form->field($cheque, 'monto')->textInput() ?></div>

        <div class="col-md-4 col-xs-12">
        <?=
        $form->field($cheque, 'fecha')->widget(DatePicker::classname(), [
            'options' => ['placeholder' => 'aaaa-mm-dd'],
            'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'yyyy-mm-dd',
            'endDate' => "0d"
            ],
            'language' => Yii::$app->language,
            'type' => DatePicker::TYPE_COMPONENT_PREPEND
        ]);
        ?>  
        </div>

        <div class="col-md-4 col-xs-12"><?= $form->field($cheque, 'estado')->dropDownList([ 'Cobrado' => 'Cobrado', 'Rechazado' => 'Rechazado', ], ['prompt' => '']) ?></div>

        <div class="col-md-12 col-xs-12"><?= $form->field($cheque, 'observacion')->textarea(['rows' => 3]) ?></div>

        </div>
    </div>
    </div>
    <?php } ?>

    <div class="form-group col-md-12">
      <?php
        echo Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);
        if(isset($idLiquidacion)){
            echo Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']);
        }else{
            echo Html::submitButton(Yii::t('app', 'Crear'), ['class' => 'btn btn-success']);
        } ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
