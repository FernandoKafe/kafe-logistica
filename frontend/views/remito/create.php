<?php

use yii\helpers\Html;
use common\models\OrdenRetiroGrupal;


/* @var $this yii\web\View */
/* @var $model common\models\Remito */
$idRemitoGrupal = Yii::$app->getRequest()->getQueryParam('ordenderetirogrupal_idordenderetirogrupal');

if($idRemitoGrupal != 1){
    $this->title = Yii::t('app', 'Agregar remitos a Orden Grupal');
}else{
    $this->title = Yii::t('app', 'Crear Remito');
}
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Remitos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><i class="fa fa-download"></i> <?= Html::encode($this->title) ?></h1>

<?php if($idRemitoGrupal != 1){ ?>
    <div class="x_panel">
        <div class="x_title">
            <h2><?= Yii::t('app', 'Encabezado') ?></h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <?= $this->render('/orden-retiro-grupal/view', [
                'model' => OrdenRetiroGrupal::findOne($idRemitoGrupal),
                ])
            ?>
        </div>
    </div>
<?php } ?>

<div class="x_panel">
    <div class="x_title">
        <h2><?php
        if($idRemitoGrupal == 1)
            echo Yii::t('app', 'Formulario');
        ?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">

    <?= $this->render('_form', [
        'model' => $model,
        'empresa' => $empresa,
        'repartidor' => $repartidor,
        'servicio' => $servicio,
        'formaPago' => $formaPago,
        'producto' => $producto,
        'sucursal' => $sucursal,
        'estado' => $estado,
        'carga' => $carga,
        'contraPago' => $contraPago,
    ]) ?>
    </div>
</div>