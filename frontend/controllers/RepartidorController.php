<?php

namespace frontend\controllers;

use Yii;
use common\models\Repartidor;
use frontend\models\RepartidorSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use common\models\RepartidorHasVehiculo;


/**
 * RepartidorController implements the CRUD actions for Repartidor model.
 */
class RepartidorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Repartidor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RepartidorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Repartidor model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Repartidor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Repartidor();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['add-vehiculo', 'id' => $model->idrepartidor]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Repartidor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            return $this->redirect(['add-vehiculo', 'id' => $model->idrepartidor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Repartidor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Repartidor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Repartidor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Repartidor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
     * Listado de Repartidores para Select2 Ajax
     * @param String|$q Texto de busqueda
     * @param int|$id   Iedntificador de repartidor
     * @return string|\yii\web\Response
     */
    public function actionListado($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $out['results'] = Repartidor::getRepartidorCompleto($q);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Repartidor::findOne($id)->idrepartidor];
        }
        return $out;
    }

    /**
     * Add vehícuki to an existing Repartidor.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id idcliente
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAddVehiculo($id)
    {
        $model = new RepartidorHasVehiculo();

        if ($model->load(Yii::$app->request->post())) {
            $vehiculos = RepartidorHasVehiculo::find()->where(['repartidor_idrepartidor' => $id])->andWhere(['<=', 'desde', $model->desde])->andWhere(['>=', 'hasta', $model->desde])->all();
            if($vehiculos){
                Yii::$app->session->setflash('error', "Un repartidor no puede usar dos vehículos el mismo días.");
            }else{
                $model->save();
            }
            return $this->redirect(['add-vehiculo',
                    'id' => $id,
                ]);
        }
        return $this->render('add-vehiculo', [
            'model' => $model,
        ]);
    }
}
