<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use kartik\depdrop\DepDrop;
use \common\models\Provincia;
use \common\models\Departamento;



/* @var $this yii\web\View */
/* @var $model common\models\Localidad */
/* @var $form yii\widgets\ActiveForm */
$dataProvincia=ArrayHelper::map(Provincia::find()->orderBy('nombre')->asArray()->all(), 'idprovincia', 'nombre');

$idLocalidad = Yii::$app->getRequest()->getQueryParam('id');

?>

<div class="localidad-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
      $provincia = new Provincia();
      $dataDepartamentos = [];
      if(isset($idLocalidad)){
        $provincia->idprovincia = $model->departamentoIddepartamento->provinciaIdprovincia->idprovincia;
        $dataDepartamentos = ArrayHelper::map(Departamento::find()->where(['provincia_idprovincia' => $provincia->idprovincia])->orderBy('nombre')->all(), 'iddepartamento', 'nombre');
        //$model = Localidad::model()->findByAttributes(array('provincia_idprovincia'=>$provincia,));
      }

      echo  $form->field($provincia, 'idprovincia')->dropDownList($dataProvincia,
              ['prompt'=>'- Seleccionar Provincia -',
              'id' => 'provinciaDestino'])->label('Provincia');

      echo  $form->field($model, 'departamento_iddepartamento')->widget(DepDrop::classname(), [
              //'data'=> $initLocalidad,
              'data' => $dataDepartamentos,
              'language' => 'es',
              'options' => ['id'=>'departamento'],
              'type' => DepDrop::TYPE_SELECT2,
                'pluginOptions'=>[
                  'depends' => ['provinciaDestino'],
                  'placeholder' => '- Seleccionar Departamento -',
                  'url' => \yii\helpers\Url::to(['direccion/departamento-provincia']),
                  'loadingText' => 'Cargando Departamentos...'
              ],
            ])->label('<a href="../departamento/create" target="_blank"><i class="fa fa-plus green"></i></a> Departamento');
    ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'cp')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);?>
        <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
