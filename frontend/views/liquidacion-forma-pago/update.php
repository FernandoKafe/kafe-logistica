<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionFormaPago */

$this->title = Yii::t('app', 'Update Liquidacion Forma Pago: ' . $model->idliquidacion_forma_pago, [
    'nameAttribute' => '' . $model->idliquidacion_forma_pago,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion Forma Pagos'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idliquidacion_forma_pago, 'url' => ['view', 'id' => $model->idliquidacion_forma_pago]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="liquidacion-forma-pago-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
