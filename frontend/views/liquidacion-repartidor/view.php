<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use frontend\models\LiquidacionrepartidorHasRemitoSearch;
use common\models\LiquidacionrepartidorHasRemito;
use common\models\Remito;
use yii\data\ActiveDataProvider;
use kartik\grid\GridView;
use common\models\TarifarioHasPrecio;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionRepartidor */
$idLiquidacion = Yii::$app->getRequest()->getQueryParam('id');


$this->title = $model->repartidorIdrepartidor->apellido.", ".$model->repartidorIdrepartidor->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidación Repartidor'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="localidad-view">
    <h1>Repartidor: <?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Detalle') ?></h2>
        <div class="clearfix"></div>
    </div>

    <p>
        <?= Html::a(Yii::t('app', 'Guardar'), ['index'], ['class' => 'btn btn-success']) ?>
        <?= Html::a(Yii::t('app', 'PDF'), ['report', 'id' => $model->idliquidacion_repartidor], ['class' => 'btn btn-warning']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'idliquidacion_repartidor',
            [
                'attribute' => 'repartidor_idrepartidor',
                'value' => function($data){
                    return $data->repartidorIdrepartidor->apellido.", ".$data->repartidorIdrepartidor->nombre;
                }
            ],
            'periodo',
            [
                'attribute' => 'estadoliquidacion_idestadoliquidacion',
                'value' => function($data){
                    return $data->estadoliquidacionIdestadoliquidacion->nombre;
                }
            ],
            'periodo_inicio',
            'periodo_fin',
            [
                'attribute' => 'total',
                'format' => ['decimal', 2],
            ],
        ],
    ]) ?>
    <!--<p>
        <?//= Html::a(Yii::t('app', 'Resumen mensual'), ['summary', 'id' => $model->idliquidacion_repartidor], ['class' => 'btn btn-danger']) ?>
    </p>-->
</div>
</div>

<!--Remitos Asociados-->
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Remitos liquidados');?></h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php
    $remito = Remito::find()->joinWith('liquidacionrepartidorHasRemitos')->where(['liquidacionrepartidor_idliquidacionrepartidor' => $idLiquidacion]);
    $dataProvider = new ActiveDataProvider([
        'query' => $remito,
    ]);
    $dataProvider->setSort([
        'attributes' => [
            'remitofecha' => [
                'asc' => ['remitofecha' => SORT_ASC],
                'desc' => ['remitofecha' => SORT_DESC],
                'default' => SORT_ASC
            ],
        ],
        'defaultOrder' => [
            'remitofecha' => SORT_ASC
        ]
    ]);
    echo GridView::widget([
    'dataProvider' => $dataProvider,
    'moduleId' => 'gridviewk',
    'toolbar' =>  [
        '{export}',
        '{toggleData}',
    ],
    'panel' => [
        'type' => GridView::TYPE_PRIMARY,
    ],
    'columns' => [
        [
            'attribute' => 'remitofecha',
            'label' => 'Fecha',
        ],
        [
            'attribute' => 'numero_guia',
            'label' => 'Guía',
            'value' => function($data){
                return $data->numero_guia.' - '.$data->tipo_remito;
            }
        ],
        [
            'attribute' => 'numero_remito',
            'label' => 'Remito',
        ],
        [
            'attribute' => 'clienteIdcliente',
            'label' => 'Cliente',
            'value' => function($data){
                return $data->clienteIdcliente->razonsocial;
            }
        ],
        [
            'attribute' => 'cantidad_bultos',
            'label' => 'Bultos'
        ],
        [
            'attribute' => 'precio_reparto',
            'label' => 'Reparto',
            'format' => ['decimal',2],
        ],
        [
            'attribute' => 'volumen',
        ],
        [
            'attribute' => 'precio_reparto_adicional',
            'label' => 'Reparto adicional',
            'format' => ['decimal',2],
        ],
        /*[
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Acciones',
            'headerOptions' => ['style' => 'color:#337ab7'],
            'template' => '{add}{remove}{view}{update}',
            'buttons' => [
                'remove' => function ($url, $model) {
                    if($model->liquidado == 1){
                        return Html::a('<i class="fa fa-times red"> Remover</i>', $url, [
                                'title' => Yii::t('app', 'Ver'),
                                ]);
                        }
                },
                'add' => function ($url, $model) {
                    if($model->liquidado == 0){
                        return Html::a('<i class="fa fa-check green"> Agregar</i>', $url, [
                              'title' => Yii::t('app', 'Ver'),
                              ]);
                    }
                },
                'view' => function ($url, $model) {
                        return Html::a('<i class="fa fa-bulleye"></i>', $url, [
                              'title' => Yii::t('app', 'Ver'),
                              ]);
                },
                'update' => function ($url, $model) {
                        return Html::a('<i class="fa fa-edit"></i>', $url, [
                              'title' => Yii::t('app', 'Ver'),
                              ]);
                },

            ],
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'remove') {
                    $url ='../remito/remove-liquidado?idRemito='.$model->idremito.'&idLiquidacion='.$model->liquidacionHasRemito->liquidacion_idliquidacion;
                    return $url;
                }
            },
            'urlCreator' => function ($action, $model, $key, $index) {
                if ($action === 'add') {
                    $url ='../remito/add-liquidado?idRemito='.$model->idremito.'&idLiquidacion='.$model->liquidacionHasRemito->liquidacion_idliquidacion;
                    return $url;
                }
            }
        ],*/
    ],
    ]); ?>

    </div>
</div>