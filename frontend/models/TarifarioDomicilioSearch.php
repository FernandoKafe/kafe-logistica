<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\TarifarioDomicilio;

/**
 * TarifarioDomicilioSearch represents the model behind the search form of `common\models\TarifarioDomicilio`.
 */
class TarifarioDomicilioSearch extends TarifarioDomicilio
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idtarifario_domicilio'], 'integer'],
            [['reparto', 'reparto_adicional', 'primer_bulto', 'bulto_adicional', 'sprinter'], 'number'],
            [['nombre', 'tipo'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TarifarioDomicilio::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idtarifario_domicilio' => $this->idtarifario_domicilio,
            'reparto' => $this->reparto,
            'reparto_adicional' => $this->reparto_adicional,
            'primer_bulto' => $this->primer_bulto,
            'bulto_adicional' => $this->bulto_adicional,
            'sprinter' => $this->sprinter,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'tipo', $this->tipo]);

        return $dataProvider;
    }
}
