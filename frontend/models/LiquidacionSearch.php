<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Liquidacion;

/**
 * LiquidacionSearch represents the model behind the search form of `common\models\Liquidacion`.
 */
class LiquidacionSearch extends Liquidacion
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idliquidacion', 'estadoliquidacion_idestadoliquidacion', 'cheque_idcheque', 'confirmada'], 'integer'],
            [['guias_logistica', 'importe_bultos', 'importe_guias', 'iva', 'subtotal', 'total_bultos', 'total_iva', 'total', 'pago_parcial', 'total_contra', 'confirmada', 'bultos', 'volumen'], 'number'],
            [['periodo_inicio', 'son_pesos', 'periodo_fin', 'periodo', 'cliente_idcliente', 'liquidacionformapago_idliquidacionformapago', 'creado', 'fecha_pago'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */ 
    public function search($params)
    {
        $query = Liquidacion::find()->where(['confirmada' => 1]);
        $query->joinWith(['clienteIdcliente as cliente']);
        $query->joinWith(['estadoliquidacionIdestadoliquidacion as estado']);
        $query->joinWith(['liquidacionformapagoIdliquidacionformapago as forma_pago']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idliquidacion' => $this->idliquidacion,
            'guias_logistica' => $this->guias_logistica,
            'importe_bultos' => $this->importe_bultos,
            'importe_guias' => $this->importe_guias,
            'iva' => $this->iva,
            'periodo_inicio' => $this->periodo_inicio,
            'subtotal' => $this->subtotal,
            'total_bultos' => $this->total_bultos,
            'total_iva' => $this->total_iva,
            'total' => $this->total,
            'periodo_fin' => $this->periodo_fin,
            'pago_parcial' => $this->pago_parcial,
            'total_contra' => $this->total_contra,
            'cheque_idcheque' => $this->cheque_idcheque, 
            'creado' => $this->creado,
            'confirmada' => $this->confirmada,
            'fecha_pago' => $this->fecha_pago,
            'bultos' => $this->bultos, 
            'volumen' => $this->volumen, 
        ]);

        $query->andFilterWhere(['like', 'son_pesos', $this->son_pesos])
              ->andFilterWhere(['like', 'periodo', $this->periodo])
              ->andFilterWhere(['like', 'cliente.razonsocial', $this->cliente_idcliente])
              ->andFilterWhere(['like', 'estado.idestado_liquidacion', $this->estadoliquidacion_idestadoliquidacion])
              ->andFilterWhere(['like', 'forma_pago.nombre', $this->liquidacionformapago_idliquidacionformapago]);

        return $dataProvider;
    }
}
