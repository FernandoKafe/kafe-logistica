<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use \yii\helpers\ArrayHelper;
use common\models\EstadoLiquidacion;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\LiquidacionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Liquidaciones clientes');
$this->params['breadcrumbs'][] = $this->title; 
?>
<h1><i class="fa fa-file"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2>Listado</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <p>
            <?= Html::a(Yii::t('app', 'Previa Liquidación Cliente'), ['create'], ['class' => 'btn btn-success']) ?>
            <?= Html::a(Yii::t('app', 'Previa Planilla Contra Reembolso'), ['create-rendicion', 'rendicion' => 1], ['class' => 'btn btn-success']) ?>
        </p>

            <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'moduleId' => 'gridviewk',
            'toolbar' =>  [
                '{export}',
                '{toggleData}',
            ],
            'panel' => [
                'type' => GridView::TYPE_PRIMARY,
            ],
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'idliquidacion',
                    'label' => 'Nº liquidación'
                ],
                [
                    'attribute' => 'cliente_idcliente',
                    'value' => function($data){
                        return $data->clienteIdcliente->razonsocial;
                    }
                ],
                [
                    'label' => 'Tipo',
                    'value' => function($data){
                        return $data->clienteIdcliente->tarifarioIdtarifario->tipo;
                    }
                ],
                //'importe_bultos',
                //'guias_logistica',
                //'importe_guias',
                //'iva',
                'periodo',
                //'son_pesos:ntext',
                //'subtotal',
                //'total_iva',
                [
                    'attribute'=>'estadoliquidacion_idestadoliquidacion',
                    'filter'=>ArrayHelper::map(EstadoLiquidacion::find()->asArray()->all(), 'idestado_liquidacion', 'nombre'),
                    'content' => function($data){
                        return $data->estadoliquidacionIdestadoliquidacion->nombre;
                    }
                ],
                [
                    'attribute' => 'total',
                    'format' => ['decimal',2]
                ],
                /*[
                    'label' => 'Saldo',
                    'format' => ['decimal',2],
                    'contentOptions' => function($model, $key, $index, $column){
                        return ['style' => 'color:'.(($model->pago_parcial != 0) && $model->pago_parcial - $model->total < 0 ? 'green' : 'red')];
                    },
                    'value' => function($data){
                            return $data->pago_parcial - $data->total;
                    },
                ],*/
                [
                    'attribute' => 'creado',
                    'label' => 'Fecha y Hora creación',
                ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

    </div>
</div>

