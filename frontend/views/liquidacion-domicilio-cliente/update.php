<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionDomicilioCliente */

$this->title = Yii::t('app', 'Actualizar Liquidación Domicilio Cliente: ' . $model->idliquidacion_domicilio_cliente, [
    'nameAttribute' => '' . $model->idliquidacion_domicilio_cliente,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidacion Domicilio Clientes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->idliquidacion_domicilio_cliente, 'url' => ['view', 'id' => $model->idliquidacion_domicilio_cliente]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<h1><i class="fa fa-pencil"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario de modificación') ?></h2>
        <div class="clearfix"></div>
    </div>
    <?= $this->render('_form', [
        'model' => $model,
        'cheque' => $cheque
    ]) ?>

</div>
