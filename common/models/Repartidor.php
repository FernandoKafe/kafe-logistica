<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%repartidor}}".
 *
 * @property int $idrepartidor
 * @property string $nombre
 * @property string $apellido
 * @property int $dni
 * @property int $localidad_idlocalidad
 * @property string $direccion
 * @property string $hijos
 * @property string $estado_civil
 * @property string $nacimiento
 * @property string $grupo_sanguineo
 * @property double $cuil 
 * @property double $telefono 
 * @property double $celular
 * @property string $nacionalidad
 * @property string $estudios
 * @property string $email
 * @property string $puesto 
 *
 * @property LiquidacionRepartidor[] $liquidacionRepartidors
 * @property OrdenRetiroGrupal[] $ordenRetiroGrupals
 * @property Remito[] $remitos
 * @property Localidad $localidadIdlocalidad
 */
class Repartidor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%repartidor}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellido', 'dni'], 'required'],
            [['dni', 'localidad_idlocalidad', ], 'integer', 'max' => 999999999],
            [['nacimiento'], 'safe'],
            [['cuil', 'telefono', 'celular'], 'number', 'max' => 99999999999], 
            [['dni'], 'unique'],
            [['nombre', 'apellido', 'direccion', 'hijos', 'estado_civil', 'grupo_sanguineo', 'nacionalidad', 'estudios', 'email', 'puesto'], 'string', 'max' => 45],
            [['localidad_idlocalidad'], 'exist', 'skipOnError' => true, 'targetClass' => Localidad::className(), 'targetAttribute' => ['localidad_idlocalidad' => 'idlocalidad']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idrepartidor' => Yii::t('app', 'Idrepartidor'),
            'nombre' => Yii::t('app', 'Nombre'),
            'apellido' => Yii::t('app', 'Apellido'),
            'dni' => Yii::t('app', 'Dni'),
            'localidad_idlocalidad' => Yii::t('app', 'Localidad'),
            'direccion' => Yii::t('app', 'Dirección'),
            'hijos' => Yii::t('app', 'Hijos'),
            'estado_civil' => Yii::t('app', 'Estado Civil'),
            'nacimiento' => Yii::t('app', 'Nacimiento'),
            'grupo_sanguineo' => Yii::t('app', 'Grupo Sanguineo'),
            'cuil' => Yii::t('app', 'Cuil'),
            'telefono' => Yii::t('app', 'Teléfono'),
            'celular' => Yii::t('app', 'Celular'),
            'nacionalidad' => Yii::t('app', 'Nacionalidad'),
            'estudios' => Yii::t('app', 'Estudios'),
            'email' => Yii::t('app', 'e-mail'),
            'puesto' => Yii::t('app', 'Puesto'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacionRepartidors()
    {
        return $this->hasMany(LiquidacionRepartidor::className(), ['repartidor_idrepartidor' => 'idrepartidor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenRetiroGrupals()
    {
        return $this->hasMany(OrdenRetiroGrupal::className(), ['repartidor_idrepartidor' => 'idrepartidor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemitos()
    {
        return $this->hasMany(Remito::className(), ['repartidor_idrepartidor' => 'idrepartidor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalidadIdlocalidad()
    {
        return $this->hasOne(Localidad::className(), ['idlocalidad' => 'localidad_idlocalidad']);
    }

    /**
     * Función que busca un empleado con nombre completo
     * @param string $q 
     * @return [id, text] Arreglo de identificador y razon social
     */
    public static function getRepartidorCompleto($q){
        $query = self::find();
        $query->select(['idrepartidor AS id', 'concat(repartidor.apellido, ", ", repartidor.nombre) AS text',])
            ->where(['like', 'repartidor.nombre', $q])
            ->orWhere(['like', 'repartidor.apellido', $q])
            ->limit(20);
        return $query->asArray()->all();
    }
}
