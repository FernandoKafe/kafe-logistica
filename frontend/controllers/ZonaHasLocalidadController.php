<?php

namespace frontend\controllers;

use Yii;
use common\models\ZonaHasLocalidad;
use frontend\models\ZonaHasLocalidadSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ZonaHasLocalidadController implements the CRUD actions for ZonaHasLocalidad model.
 */
class ZonaHasLocalidadController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all ZonaHasLocalidad models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ZonaHasLocalidadSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ZonaHasLocalidad model.
     * @param integer $zona_idzona
     * @param integer $localidad_idlocalidad
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($zona_idzona, $localidad_idlocalidad)
    {
        return $this->render('view', [
            'model' => $this->findModel($zona_idzona, $localidad_idlocalidad),
        ]);
    }

    /**
     * Creates a new ZonaHasLocalidad model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ZonaHasLocalidad();

        if ($model->load(Yii::$app->request->post())) {
            $localidadZona = ZonaHasLocalidad::find()->where(['localidad_idlocalidad' => $model->localidad_idlocalidad])->one();
            if($localidadZona){
                Yii::$app->session->setflash('error', "La localidad ya pertenece a la ".$model->zonaIdzona->nombre);
                return $this->render('zona/addLocalidad', [
                    'model' => $model,
                ]);
            }
            return $this->redirect(['view', 'zona_idzona' => $model->zona_idzona, 'localidad_idlocalidad' => $model->localidad_idlocalidad]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ZonaHasLocalidad model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $zona_idzona
     * @param integer $localidad_idlocalidad
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($zona_idzona, $localidad_idlocalidad)
    {
        $model = $this->findModel($zona_idzona, $localidad_idlocalidad);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'zona_idzona' => $model->zona_idzona, 'localidad_idlocalidad' => $model->localidad_idlocalidad]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ZonaHasLocalidad model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $zona_idzona
     * @param integer $localidad_idlocalidad
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($zona_idzona, $localidad_idlocalidad)
    {
        $this->findModel($zona_idzona, $localidad_idlocalidad)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ZonaHasLocalidad model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $zona_idzona
     * @param integer $localidad_idlocalidad
     * @return ZonaHasLocalidad the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($zona_idzona, $localidad_idlocalidad)
    {
        if (($model = ZonaHasLocalidad::findOne(['zona_idzona' => $zona_idzona, 'localidad_idlocalidad' => $localidad_idlocalidad])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
