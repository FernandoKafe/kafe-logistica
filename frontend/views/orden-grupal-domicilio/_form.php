<?php

use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\select2\Select2;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\OrdenGrupalDomicilio */
/* @var $form yii\widgets\ActiveForm */
$idOrden = Yii::$app->getRequest()->getQueryParam('id');
$cliente = empty($model->clienteIdcliente) ? '' : $model->clienteIdcliente->razonsocial;
$repartidor = empty($model->repartidorIdrepartidor) ? '' : $model->repartidorIdrepartidor->apellido.", ".$model->repartidorIdrepartidor->apellido;
?>

<div class="orden-grupal-domicilio-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-3 col-xs-12"><?= $form->field($model, 'numero_orden')->textInput(['maxlength' => true]) ?></div>

    <div class="col-md-3 col-xs-12">
        <?php
            if(!$idOrden){
                $model->fecha = date("Y-m-d",strtotime("-1 day"));
            }
            echo $form->field($model, 'fecha')->widget(DatePicker::classname(), [
                'options' => ['placeholder' => 'aaaa-mm-dd'],
                'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'yyyy-mm-dd',
                'endDate' => "0d",
                ],
                'language' => Yii::$app->language,
                'type' => DatePicker::TYPE_COMPONENT_PREPEND
            ]);
        ?>
    </div>

    <div class="col-md-3 col-xs-12">
        <?= $form->field($model, 'cliente_idcliente')->widget(Select2::classname(), [
                'initValueText' => $cliente,
                'language' => 'es',
                'options' => ['placeholder' => 'Seleccionar Cliente ...'],
                'pluginOptions' => [
                    'allowClear' => true,
                    'minimumInputLength' => 1,
                    'ajax' => [
                        'url' => Url::to(['/cliente/listado-for-tipo']),
                        'dataType' => 'json',
                        'data' => new JsExpression('function(params) { return {q:params.term, id:null, cliente_tipo:2}; }')
                    ],
                ],
            ])->label('<a href="../cliente/create" target="_blank"><i class="fa fa-plus green"></i></a> Cliente');
        ?>
    </div>

    <div class="col-md-3 col-xs-12">
        <?= $form->field($model, 'repartidor_idrepartidor')->widget(Select2::classname(), [
              'initValueText' => $repartidor,
              'language' => 'es',
              'options' => ['placeholder' => 'Seleccionar Repartidor ...'],
              'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                  'url' => Url::to(['/repartidor/listado']),
                  'dataType' => 'json',
                  'data' => new JsExpression('function(params) {return {q:params.term}; }')
                ],
              ],
            ])->label('<a href="../repartidor/create" target="_blank"><i class="fa fa-plus green"></i></a> Repartidor');
        ?>
    </div>

    <?php if(!isset($idOrden)){?>
            <div class="col-md-12 form-group">
                <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary'])?>
                <?= Html::submitButton(Yii::t('app', 'Crear'), ['class' => 'btn btn-success']) ?>
            </div>
    <?php }else{ ?>
            <div class="col-md-12 form-group">
                <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary'])?>
                <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
            </div>
    <?php } ?>

    <?php ActiveForm::end(); ?>

</div>
