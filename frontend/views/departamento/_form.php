<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

use \common\models\Provincia;

/* @var $this yii\web\View */
/* @var $model common\models\Departamento */
/* @var $form yii\widgets\ActiveForm */
$dataProvincia=ArrayHelper::map(Provincia::find()->orderBy('nombre')->asArray()->all(), 'idprovincia', 'nombre');

?>

<div class="departamento-form">

    <?php $form = ActiveForm::begin(); ?> 

    <?=  $form->errorSummary($model); ?>
    
    <div class="col-md-6"><?= $form->field($model, 'provincia_idprovincia')->dropDownList($dataProvincia) ?></div>

    <div class="col-md-6"><?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?></div>

    <div class="form-group col-md-12">
        <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);?>
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
