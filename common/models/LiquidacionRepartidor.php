<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%liquidacion_repartidor}}".
 *
 * @property int $idliquidacion_repartidor
 * @property int $repartidor_idrepartidor
 * @property double $total
 * @property string $periodo
 * @property int $estadoliquidacion_idestadoliquidacion
 * @property string $periodo_inicio
 * @property string $periodo_fin
 * @property string $creado
 * @property int liquidacionunica
 *
 * @property EstadoLiquidacion $estadoliquidacionIdestadoliquidacion
 * @property Repartidor $repartidorIdrepartidor
 * @property LiquidacionrepartidorHasRemito[] $liquidacionrepartidorHasRemitos
 * @property Remito[] $remitoIdremitos
 */
class LiquidacionRepartidor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%liquidacion_repartidor}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['repartidor_idrepartidor', 'periodo', 'periodo_inicio', 'periodo_fin'], 'required'],
            [['repartidor_idrepartidor', 'estadoliquidacion_idestadoliquidacion', 'liquidacionunica'], 'integer'],
            [['total'], 'number'],
            [['periodo_inicio', 'periodo_fin', 'creado'], 'safe'],
            [['periodo'], 'string', 'max' => 100],
            [['estadoliquidacion_idestadoliquidacion'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoLiquidacion::className(), 'targetAttribute' => ['estadoliquidacion_idestadoliquidacion' => 'idestado_liquidacion']],
            [['repartidor_idrepartidor'], 'exist', 'skipOnError' => true, 'targetClass' => Repartidor::className(), 'targetAttribute' => ['repartidor_idrepartidor' => 'idrepartidor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idliquidacion_repartidor' => Yii::t('app', 'Nº de Liquidación'),
            'repartidor_idrepartidor' => Yii::t('app', 'Repartidor'),
            'total' => Yii::t('app', 'Total'),
            'periodo' => Yii::t('app', 'Periodo'),
            'estadoliquidacion_idestadoliquidacion' => Yii::t('app', 'Estado'),
            'periodo_inicio' => Yii::t('app', 'Periodo Inicio'),
            'periodo_fin' => Yii::t('app', 'Periodo Fin'),
            'creado' => Yii::t('app', 'Creado'),
            'liquidacionunica' => Yii::t('app', 'Liquidación única'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoliquidacionIdestadoliquidacion()
    {
        return $this->hasOne(EstadoLiquidacion::className(), ['idestado_liquidacion' => 'estadoliquidacion_idestadoliquidacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepartidorIdrepartidor()
    {
        return $this->hasOne(Repartidor::className(), ['idrepartidor' => 'repartidor_idrepartidor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacionrepartidorHasRemitos()
    {
        return $this->hasMany(LiquidacionrepartidorHasRemito::className(), ['liquidacionrepartidor_idliquidacionrepartidor' => 'idliquidacion_repartidor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemitoIdremitos()
    {
        return $this->hasMany(Remito::className(), ['idremito' => 'remito_idremito'])->viaTable('{{%liquidacionrepartidor_has_remito}}', ['liquidacionrepartidor_idliquidacionrepartidor' => 'idliquidacion_repartidor']);
    } 

    public static function getTotal($cliente, $fieldName)
    {
        $total = 0;

        foreach ($cliente as $c) {
            $total += $c[$fieldName];
        }

    return $total;
    }

    public static function createLiquidacion($model, $liquidacionUnica){
        if ($model) {
            $remitos = Remito::getRemitosOrdenFechaByRepartidor($model->repartidor_idrepartidor, $model->periodo_inicio, $model->periodo_fin);
            $flag = 1;
            if($remitos){
                foreach ($remitos as $r) {
                    if($r->terminal == 0){
                        $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, $r->sucursalIdsucursal->localidadIdlocalidad->zonaHasLocalidad->zona_idzona);
                    } else {
                        $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, 5);
                    }
                    if(!$tarHasPrecio){
                        $clienteHuerfano = $r->clienteIdcliente->razonsocial;
                        $tarifarioHuerfano = $r->clienteIdcliente->tarifarioIdtarifario->nombre;
                        $cargaHuerfana = $r->cargaIdcarga->nombre;
                        $zonaHuerfana = $r->sucursalIdsucursal->localidadIdlocalidad->zonaHasLocalidad->zonaIdzona->nombre;
                        $remitoError = $r->idremito;
                        $flag = -1;
                        break;
                    }
                }
                if($flag != -1  && $model->save()){
                    $fechaAnterior = $clienteAnterior = null;
                    foreach ($remitos as $r) {
                        if($r->terminal == 0){
                            $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, $r->sucursalIdsucursal->localidadIdlocalidad->zonaHasLocalidad->zona_idzona);
                        } else {
                            $tarHasPrecio = TarifarioHasPrecio::getTarifarioHasPrecio($r->carga_idcarga, $r->clienteIdcliente->tarifario_idtarifario, 5);
                        }
                        $rel = new LiquidacionrepartidorHasRemito();
                        $rel->liquidacionrepartidor_idliquidacionrepartidor = $model->idliquidacion_repartidor;
                        $rel->remito_idremito = $r->idremito;
                        $rel->total = 0;
                        $r->liquidado_repartidor = 1;
                        if(!isset($r->precio) || !isset($r->precio_adicional) || !isset($r->precio_reparto) || !isset($r->precio_reparto_adicional) || !isset($r->porcentaje_contra))
                        {
                            if(!isset($r->precio))
                                $r->precio = $tarHasPrecio->precio;
                            if(!isset($r->precio_adicional) )
                                $r->precio_adicional = $tarHasPrecio->precio_adicional;
                            if(!isset($r->precio_reparto))
                                $r->precio_reparto = $tarHasPrecio->precio_reparto;
                            if(!isset($r->precio_reparto_adicional))
                                $r->precio_reparto_adicional = $tarHasPrecio->precio_reparto_adicional;
                            if(!isset($r->porcentaje_contra))
                                $r->porcentaje_contra = $tarHasPrecio->porcentaje_contra;
                        }

                        if($r->clienteIdcliente->tarifarioIdtarifario->tipo == "Tipo A"){
                            $rel->total += $tarHasPrecio->precio_reparto;
                            $rel->total += (($r->cantidad_bultos + $r->volumen - 1)  * $tarHasPrecio->precio_reparto_adicional);
                            $fechaAnterior = $r->remitofecha;
                            $clienteAnterior = $r->cliente_idcliente;
                        }else if($r->clienteIdcliente->tarifarioIdtarifario->tipo == "Tipo B"){
                            if($fechaAnterior == $r->remitofecha && $clienteAnterior == $r->cliente_idcliente){
                                $rel->total += (($r->cantidad_bultos + $r->volumen) * $tarHasPrecio->precio_reparto_adicional);
                            }else{
                                $rel->total += ($tarHasPrecio->precio_reparto + (($r->cantidad_bultos + $r->volumen - 1) * $tarHasPrecio->precio_reparto_adicional));
                            }
                            $fechaAnterior = $r->remitofecha;
                            $clienteAnterior = $r->cliente_idcliente;
                        }
                        $r->save(false);
                        $rel->save(false);
                        $model->total += $rel->total;
                    }
                    $model->liquidacionunica = $liquidacionUnica;
                    $model->creado = date('Y-m-d G:i', time());
                    $model->save(false);
                    return $model;
                }else{
                    $mensaje = "Completar datos de zona ".$zonaHuerfana." en tarifario ".$tarifarioHuerfano."!";
                    Yii::$app->session->setflash('error', $mensaje);
                    return ["error", $remitoError, $mensaje];
                    //info azul
                    //warning amarrillo
                    //success verde
                    //error rojo

                }
            }else{
                return false;
            }
        } 
    }

    public static function deleteLiquidacion($liquidacionUnica){
        $model = self::find()->where(['periodo_inicio' => $liquidacionUnica->fecha_inicio])
        ->andWhere(['periodo_fin' => $liquidacionUnica->fecha_fin])
        ->andWhere(['periodo' => $liquidacionUnica->nombre])
        ->andWhere(['repartidor_idrepartidor' => $liquidacionUnica->repartidor_idrepartidor])->one();

        $liquidacionHasRemito =  LiquidacionrepartidorHasRemito::find()->where(['liquidacionrepartidor_idliquidacionrepartidor' => $model->idliquidacion_repartidor])->all();
        foreach($liquidacionHasRemito as $lhr){
            $post = Yii::$app->db->createCommand('UPDATE remito SET liquidado_repartidor=:liquidado_repartidor WHERE idremito=:idremito')
            ->bindValue(':liquidado_repartidor', 0)
            ->bindValue(':idremito', $lhr->remito_idremito)
            ->execute();
            $lhr->delete();
        }
        if($model){
            $model->delete();
            return 0;
        }else {
            return 1;
        }
    }

    public static function getAllFromUnica($idUnica){
        return self::find()->select('idliquidacion_repartidor')->where(['liquidacionunica' => $idUnica])->one();
    }
}
