<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Localidad;

/**
 * LocalidadSearch represents the model behind the search form of `common\models\Localidad`.
 */
class LocalidadSearch extends Localidad
{
    public function attributes()
    {
        // add related fields to searchable attributes
        return array_merge(parent::attributes(), ['departamentoIddepartamento.provincia_idprovincia']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'idlocalidad', 'departamento_iddepartamento', 'nombre', 'cp', 'departamentoIddepartamento.provincia_idprovincia'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Localidad::find();

        $query->joinWith(['departamentoIddepartamento as departamento', 'departamentoIddepartamento.provinciaIdprovincia as provincia']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'departamento_iddepartamento' => $this->departamento_iddepartamento,
        ]);

        $query->andFilterWhere(['like', 'cp', $this->cp]);

        $query->andFilterWhere(['like', 'CONCAT(localidad.nombre, " - ", departamento.nombre, " - ", provincia.nombre )', $this->idlocalidad]);
        $query->andFilterWhere(['like', 'provincia.idprovincia', $this->getAttribute('departamentoIddepartamento.provincia_idprovincia')]);


        return $dataProvider;
    }
}
