<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%liquidacion_vino_cliente}}".
 *
 * @property int $idliquidacion_vino_cliente
 * @property double $total
 * @property string $periodo_inicio
 * @property string $periodo_fin
 * @property string $nombre
 * @property int $estadoliquidacion_idestadoliquidacion
 * @property int $liquidacionformapago_idliquidacionformapago
 * @property int $cliente_idcliente
 * @property int $cheque_idcheque 
 * @property double $pago_parcial 
 * @property double $total_guias
 * @property double $total_cobrar
 * @property string $creado
 * @property int $confirmada
 * @property string $fecha_pagoo
 * @property int $cajas 
 * @property double $valor_cajas 
 * @property double $total_seguro 
 *
 * @property LiquidacionFormaPago $liquidacionformapagoIdliquidacionformapago
 * @property Cliente $clienteIdcliente 
 * @property EstadoLiquidacion $estadoliquidacionIdestadoliquidacion
 * @property RemitoVino[] $remitoVinos
 * @property Cheque $chequeIdcheque
 */
class LiquidacionVinoCliente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%liquidacion_vino_cliente}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['total', 'pago_parcial', 'pago_parcial', 'total_guias', 'total_cobrar', 'valor_cajas', 'total_seguro'], 'number'],
            [['cliente_idcliente', 'periodo_inicio', 'periodo_fin', 'nombre'], 'required'],
            [['periodo_inicio', 'periodo_fin', 'creado', 'fecha_pago'], 'safe'],
            [['estadoliquidacion_idestadoliquidacion', 'liquidacionformapago_idliquidacionformapago', 'cliente_idcliente', 'cheque_idcheque', 'confirmada', 'cajas'], 'integer'],
            [['nombre'], 'string', 'max' => 100],
            [['liquidacionformapago_idliquidacionformapago'], 'exist', 'skipOnError' => true, 'targetClass' => LiquidacionFormaPago::className(), 'targetAttribute' => ['liquidacionformapago_idliquidacionformapago' => 'idliquidacion_forma_pago']],
            [['estadoliquidacion_idestadoliquidacion'], 'exist', 'skipOnError' => true, 'targetClass' => EstadoLiquidacion::className(), 'targetAttribute' => ['estadoliquidacion_idestadoliquidacion' => 'idestado_liquidacion']],
            [['cliente_idcliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['cliente_idcliente' => 'idcliente']],
            [['cheque_idcheque'], 'exist', 'skipOnError' => true, 'targetClass' => Cheque::className(), 'targetAttribute' => ['cheque_idcheque' => 'idcheque']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idliquidacion_vino_cliente' => Yii::t('app', 'Idliquidacion Vino Cliente'),
            'total' => Yii::t('app', 'TOTAL'),
            'periodo_inicio' => Yii::t('app', 'Periodo Inicio'),
            'periodo_fin' => Yii::t('app', 'Periodo Fin'),
            'nombre' => Yii::t('app', 'Periodo Nombre'),
            'estadoliquidacion_idestadoliquidacion' => Yii::t('app', 'Estado liquidación'),
            'liquidacionformapago_idliquidacionformapago' => Yii::t('app', 'Forma de pago'),
            'cliente_idcliente' => Yii::t('app', 'Cliente'),
            'cheque_idcheque' => Yii::t('app', 'Cheque'),
            'pago_parcial' => Yii::t('app', 'Pago Parcial'),
            'total_guias' => Yii::t('app', 'TOTAL Guias'), 
            'total_cobrar' => Yii::t('app', 'TOTAL Cobrar'),
            'creado' => Yii::t('app', 'Creado'),
            'confirmada' => Yii::t('app', 'Confirmada'),
            'fecha_pago' => Yii::t('app', 'Fecha de pago'),
            'valor_cajas' => Yii::t('app', 'Valor Cajas'),
            'total_seguro' => Yii::t('app', 'Total Seguro'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChequeIdcheque()
    {
        return $this->hasOne(Cheque::className(), ['idcheque' => 'cheque_idcheque']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacionformapagoIdliquidacionformapago()
    {
        return $this->hasOne(LiquidacionFormaPago::className(), ['idliquidacion_forma_pago' => 'liquidacionformapago_idliquidacionformapago']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClienteIdcliente()
    {
        return $this->hasOne(Cliente::className(), ['idcliente' => 'cliente_idcliente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoliquidacionIdestadoliquidacion()
    {
        return $this->hasOne(EstadoLiquidacion::className(), ['idestado_liquidacion' => 'estadoliquidacion_idestadoliquidacion']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRemitoVinos()
    {
        return $this->hasMany(RemitoVino::className(), ['liquidacionvinocliente_idliquidacionvinocliente' => 'idliquidacion_vino_cliente']);
    }

    public static function getCajasSum($remitoVinos){
        $cant_cajas = 0;
        if($remitoVinos){
            foreach($remitoVinos as $r){
                $cant_cajas += $r->cant_cajas;
            }
            return $cant_cajas;
        }    
    }
    
    public static function getValorCaja($remitoVinos, $idCliente){
        $totalValorCajaSeguro = 0;
        $cliente = Cliente::find()->where(['idcliente' => $idCliente])->one();
        $tv = TarifarioVino::find()->where(['idtarifario_vino' => $cliente->tarifariovino_idtarifariovino])->one();
        if($remitoVinos){
            foreach($remitoVinos as $r){
                $totalValorCajaSeguro += (($r->cant_cajas * $tv->precio_caja));
            }
            return $totalValorCajaSeguro;
        }    
    }

    public static function getSeguroSum($remitoVinos, $idCliente){
        $totalValorCajaSeguro = 0;
        $cliente = Cliente::find()->where(['idcliente' => $idCliente])->one();
        if($remitoVinos){
            foreach($remitoVinos as $r){
                $totalValorCajaSeguro += (($r->valor_declarado / 2) * 0.01);
            }
            return $totalValorCajaSeguro;
        }    
    }
}
