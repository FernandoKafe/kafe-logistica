<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\OrdenGrupalVinosSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="orden-grupal-vinos-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'idorden_grupal_vinos') ?>

    <?= $form->field($model, 'fecha') ?>

    <?= $form->field($model, 'numero') ?>

    <?= $form->field($model, 'cliente_idcliente') ?>

    <?= $form->field($model, 'repartidor_idrepartidor') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
