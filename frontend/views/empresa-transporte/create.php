<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\EmpresaTransporte */

$this->title = 'Crear Empresa de Transporte';
$this->params['breadcrumbs'][] = ['label' => 'Empresas de Transporte', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cliente-create">
<h1><i class="fa fa-download"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario') ?></h2>
        <div class="clearfix"></div>
    </div>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
</div>
