<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\PagosPorcentajesRepartidorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Pagos Porcentajes Repartidors');
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><i class="fa fa-copy"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2>Listado</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Creatr monto'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'idpagos_vinos_repartidor',
            [
                'attribute' => 'extremo_inferior',
                'value' => function($data){
                    if($data->extremo_inferior == 0){
                        return 'Mayor que '.$data->extremo_inferior.' %';
                    }else{
                        return 'Mayor o igual que '.$data->extremo_inferior.' %';
                    }
                }
            ],
            [
                'attribute' => 'extremo_superior',
                'value' => function($data){
                    if(!$data->extremo_inferior == 100){
                        return 'Menor que '.$data->extremo_superior.' %';
                    }else{
                        return 'Menor o igual que '.$data->extremo_superior.' %';
                    }
                }
            ],
            [
                'attribute' => 'monto',
                'format' => ['decimal',2]
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
</div>
