<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LiquidacionPorcentajeCliente */

$this->title = Yii::t('app', 'Crear Liquidación % para Cliente');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Crear Liquidación por % para Cliente'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h1><i class="fa fa-download"></i> <?= Html::encode($this->title) ?></h1>
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Formulario') ?></h2>
        <div class="clearfix"></div>
    </div>

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
