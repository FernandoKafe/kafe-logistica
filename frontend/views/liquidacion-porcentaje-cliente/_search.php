<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\LiquidacionPorcentajeClienteSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="liquidacion-porcentaje-cliente-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'idliquidacion_porcentaje_cliente') ?>

    <?= $form->field($model, 'periodo') ?>

    <?= $form->field($model, 'total_rendido') ?>

    <?= $form->field($model, 'neto_sin_iva') ?>

    <?= $form->field($model, 'combustible') ?>

    <?php // echo $form->field($model, 'comision') ?>

    <?php // echo $form->field($model, 'subtotal') ?>

    <?php // echo $form->field($model, 'iva') ?>

    <?php // echo $form->field($model, 'total_real') ?>

    <?php // echo $form->field($model, 'total_cobrar') ?>

    <?php // echo $form->field($model, 'periodo_inicio') ?>

    <?php // echo $form->field($model, 'periodo_fin') ?>

    <?php // echo $form->field($model, 'cliente_idcliente') ?>

    <?php // echo $form->field($model, 'porcentaje_idporcentaje') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
