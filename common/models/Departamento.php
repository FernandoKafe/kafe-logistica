<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "departamento".
 *
 * @property int $iddepartamento
 * @property int $provincia_idprovincia
 * @property string $nombre
 *
 * @property Provincia $provinciaIdprovincia
 * @property Localidad[] $localidads
 */
class Departamento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'departamento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
           [['provincia_idprovincia', 'nombre'], 'required'],
           [['iddepartamento', 'provincia_idprovincia'], 'integer'],
           [['nombre'], 'string', 'max' => 100],
           [['iddepartamento'], 'unique'],
            [['provincia_idprovincia'], 'exist', 'skipOnError' => true, 'targetClass' => Provincia::className(), 'targetAttribute' => ['provincia_idprovincia' => 'idprovincia']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'iddepartamento' => Yii::t('app', 'Departamento'),
            'provincia_idprovincia' => Yii::t('app', 'Provincia'),
            'nombre' => Yii::t('app', 'Nombre Departamento'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvinciaIdprovincia()
    {
        return $this->hasOne(Provincia::className(), ['idprovincia' => 'provincia_idprovincia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocalidads()
    {
        return $this->hasMany(Localidad::className(), ['departamento_iddepartamento' => 'iddepartamento']);
    }
}
