<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\Url;
use kartik\depdrop\DepDrop;
use \common\models\CondicionFiscal;
use \common\models\Tarifario;
use common\models\Sucursal;
use common\models\Localidad;
use \common\models\Provincia;
use \common\models\Departamento;
use common\models\ClienteTipo;
use common\models\Cliente;
use common\models\ClienteHasTipo;

/* @var $this yii\web\View */
/* @var $model common\models\Cliente */
/* @var $form yii\widgets\ActiveForm */
$idCliente = Yii::$app->getRequest()->getQueryParam('id');
if(isset($idCliente)){
    $new = false;
  }else{
    $localidad = new Localidad();
    $new = true; 
  }


$condicionfiscal = ArrayHelper::map(CondicionFiscal::getAllTipo(),'idcondicionfiscal','nombre');
$tarifario = ArrayHelper::map(Tarifario::getAllTipo(), 'idtarifario', 'nombre');
$sucursal = new Sucursal();
$dataProvincia=ArrayHelper::map(Provincia::find()->orderBy('nombre')->asArray()->all(), 'idprovincia', 'nombre');

//$clienteTipo = ArrayHelper::map(ClienteTipo::find()->orderBy('idcliente_tipo')->asArray()->all(), 'idcliente_tipo', 'nombre');

?>

<div class="x_panel">
    <div class="x_content">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'razonsocial')->widget(\yii\jui\AutoComplete::classname(), [
            'clientOptions' => [
                'source' => ArrayHelper::getColumn(Cliente::find()->orderBy(['razonsocial'=>SORT_ASC])->all(),'razonsocial'),
            ],
            'options'=>['class' => 'form-control']
        ]) ?>

        <?= $form->field($model, 'cuit')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'telefono')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'mail')->textInput() ?>

        <?php
          $dataDepartamentos = [];
          $dataLocalidades = [];
          $departamento = new Departamento();
          $provincia = new Provincia();

          if(!$new && isset($model->localidad_idlocalidad)){
            $provincia->idprovincia = $model->localidadIdlocalidad->departamentoIddepartamento->provinciaIdprovincia->idprovincia;
            $dataDepartamentos =  ArrayHelper::map(Departamento::find()
              ->select(['iddepartamento as id','nombre as name'])
              ->where(['provincia_idprovincia' => $provincia->idprovincia])
              ->orderBy('nombre ASC')
              ->asArray()
              ->all(), 'id', 'name');
            $departamento->iddepartamento = $model->localidadIdlocalidad->departamentoIddepartamento->iddepartamento;
            
            $dataLocalidades = ArrayHelper::map(Localidad::find()
            ->select(['idlocalidad as id','nombre as name'])
            ->where(['departamento_iddepartamento' => $departamento->iddepartamento])
            ->orderBy('nombre ASC')
            ->asArray()
            ->all(), 'id', 'name');
          }

          echo $form->field($provincia, 'idprovincia')->dropDownList($dataProvincia,
          ['prompt'=>'-Seleccionar-',
            'id' => 'provinciaDestino'])->label('Provincia');

          echo  $form->field($departamento, 'iddepartamento')->widget(DepDrop::classname(), [
            //'data'=> $initLocalidad,
            'data' => $dataDepartamentos,
            'language' => 'es',
            'options' => ['id'=>'departamento'],
            'type' => DepDrop::TYPE_SELECT2,
                'pluginOptions'=>[
                'depends' => ['provinciaDestino'],
                'placeholder' => '- Seleccionar Departamento -',
                'url' => \yii\helpers\Url::to(['direccion/departamento-provincia']),
                'loadingText' => 'Cargando Departamentos...'
              ],
          ])->label('<a href="../departamento/create" target="_blank"><i class="fa fa-plus green"></i></a> Departamento');
          
          echo $form->field($sucursal, 'localidad_idlocalidad')->widget(DepDrop::classname(), [
            //'data'=> $initLocalidad,
            'data' => $dataLocalidades,
            'language' => 'es',
            'options' => ['id'=>'localidad'],
            'type' => DepDrop::TYPE_SELECT2,
              'pluginOptions'=>[
              'depends' => ['departamento'],
              'placeholder' => '- Seleccionar Localidad -',
              'url' => \yii\helpers\Url::to(['direccion/departamento-localidad']),
              'loadingText' => 'Cargando Localidades...'
            ],
          ])->label('<a href="../localidad/create" target="_blank"><i class="fa fa-plus green"></i></a> Localidad');
        ?>

        <?= $form->field($model, 'domicilio')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'condicionfiscal_idcondicionfiscal')->dropDownList($condicionfiscal)->label('Condicón fiscal') ?>

        <!--<?//= $form->field($model, 'clientetipo_idclientetipo')->dropDownList($clienteTipo) ?>-->

          <?php
            $tarifario = empty($model->tarifarioIdtarifario) ? '' :
            $model->tarifarioIdtarifario->nombre;

            echo $form->field($model, 'tarifario_idtarifario')->widget(Select2::classname(), [
              'initValueText' => $tarifario,
              'language' => 'es',
              'options' => ['placeholder' => 'Seleccionar Tarifario ...'],
              'pluginOptions' => [
                'allowClear' => true,
                'minimumInputLength' => 3,
                'ajax' => [
                  'url' => Url::to(['/tarifario/listado']),
                  'dataType' => 'json',
                  'data' => new JsExpression('function(params) {return {q:params.term}; }')
                ],
              ],
            ])->label('<a href="../tarifario/create" target="_blank"><i class="fa fa-plus green"></i></a> Tarifario');
          ?>

        <div class="form-group col-md-12">
            <?= Html::a('Volver', ['index'], ['class' => 'btn btn-primary']);?>
            <?= Html::submitButton(Yii::t('app', 'Guardar'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
