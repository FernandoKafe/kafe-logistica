<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Repartidor;

/**
 * RepartidorSearch represents the model behind the search form of `common\models\Repartidor`.
 */
class RepartidorSearch extends Repartidor
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [ 
            [['idrepartidor', 'dni', 'localidad_idlocalidad', ], 'integer'],
            [['cuil', 'telefono', 'celular'], 'number'],
            [['nombre', 'apellido', 'direccion', 'hijos', 'estado_civil', 'nacimiento', 'grupo_sanguineo', 'nacionalidad', 'estudios', 'email', 'puesto'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Repartidor::find();

        $query->joinWith(['localidadIdlocalidad as localidad']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        } 

        // grid filtering conditions
        $query->andFilterWhere([
            'idrepartidor' => $this->idrepartidor,
            'dni' => $this->dni,
            'localidad_idlocalidad' => $this->localidad_idlocalidad,
            'nacimiento' => $this->nacimiento,
            'cuil' => $this->cuil,
            'telefono' => $this->telefono,
            'celular' => $this->celular,
        ]);

        $query->andFilterWhere(['like', 'nombre', $this->nombre])
            ->andFilterWhere(['like', 'apellido', $this->apellido])
            ->andFilterWhere(['like', 'direccion', $this->direccion])
            ->andFilterWhere(['like', 'hijos', $this->hijos])
            ->andFilterWhere(['like', 'estado_civil', $this->estado_civil])
            ->andFilterWhere(['like', 'grupo_sanguineo', $this->grupo_sanguineo])
            ->andFilterWhere(['like', 'nacionalidad', $this->nacionalidad])
            ->andFilterWhere(['like', 'estudios', $this->estudios])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'puesto', $this->puesto]);

        $query->andFilterWhere(['like', 'localidad.nombre', $this->localidad_idlocalidad]);


        return $dataProvider;
    }
}
