<?php

namespace frontend\controllers;

use Yii;
use common\models\OrdenGrupalDomicilio;
use frontend\models\OrdenGrupalDomicilioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\GuiaDomicilio;

/**
 * OrdenGrupalDomicilioController implements the CRUD actions for OrdenGrupalDomicilio model.
 */
class OrdenGrupalDomicilioController extends Controller
{ 
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrdenGrupalDomicilio models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdenGrupalDomicilioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrdenGrupalDomicilio model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OrdenGrupalDomicilio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OrdenGrupalDomicilio();

        if ($model->load(Yii::$app->request->post())) {
            if($model->clienteIdcliente->tarifariodomicilio_idtarifariodomicilio){
                $model->creado = date('Y-m-d G:i', time());
                $model->save();
                return $this->redirect([
                'add-guia',
                'idOrdenGrupal' => $model->idorden_grupal_domicilio]);
            }else{
                Yii::$app->session->setflash('error', "Asignar un tarifario domicilio al cliente antes de crear la orden grupal.");
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing OrdenGrupalDomicilio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'add-guia',
                'idOrdenGrupal' => $model->idorden_grupal_domicilio,
                ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OrdenGrupalDomicilio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if($model->liquidacioncliente_idliquidacioncliente || $model->liquidacionrepartidor_idliquidacionrepartidor){
            Yii::$app->session->setflash('error', "No se puede eliminar la orden por que esta liquidada.");
        }else{
            $guias = GuiaDomicilio::find()->where(['ordengrupaldomicilio_idordengrupaldomicilio' => $id])->all();
            if($guias){
                foreach($guias as $g){
                    $g->delete();
                }
            }
            $model->delete();
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the OrdenGrupalDomicilio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrdenGrupalDomicilio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrdenGrupalDomicilio::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

    /**
    * Add Remitos to an existing OrdenRetiroGrupal model.
    * If update is successful, the browser will be redirected to the 'view' page.
    * @param integer $id idcomprobante
    * @return mixed
    * @throws NotFoundHttpException if the model cannot be found
    */
   public function actionAddGuia()
   {
       $model = new GuiaDomicilio();
       $model->ordengrupaldomicilio_idordengrupaldomicilio = $_GET['idOrdenGrupal'];

       if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $model->creado = date('Y-m-d G:i', time());
            if($model->save()){
                Yii::$app->session->setflash('success', "Guía creada con éxito.");
            }
            return $this->redirect([
               'add-guia',
               'idOrdenGrupal' => $model->ordengrupaldomicilio_idordengrupaldomicilio,
            ]);
       }

       return $this->render('add-guia', [
           'model' => $model,
       ]);
   }

   /**
    * Displays a single OrdenGrupalDomicilio model.
    * @param integer $id
    * @return mixed
    * @throws NotFoundHttpException if the model cannot be found
    */
   public function actionRendicion($id)
   {
       return $this->render('rendicion', [
           'model' => $this->findModel($id),
       ]);
   }
}
