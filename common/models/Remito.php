<?php

namespace common\models;

use phpDocumentor\Reflection\Types\Self_;
use Yii;

/**
 * This is the model class for table "remito".
 *
 * @property int $idremito
 * @property int $numero_remito
 * @property string $remitofecha
 * @property int $sucursal_idsucursal
 * @property int $carga_idcarga
 * @property int $cantidad_bultos
 * @property double $peso
 * @property double $volumen
 * @property double $valor_declarado
 * @property int $servicio_idservicio
 * @property string $flete
 * @property string $rol_sucursal
 * @property string $facturado_a
 * @property double $seguro
 * @property double $flete_pagado
 * @property double $total_contrarembolso
 * @property int $formadepago_idformadepago
 * @property int $producto_idproducto
 * @property int $repartidor_idrepartidor
 * @property string $numero_guia
 * @property double $total_guia
 * @property int $estado_idestado
 * @property int $cliente_idcliente
 * @property int $empresatransporte_idempresatransporte
 * @property int $ordenderetirogrupal_idordenderetirogrupal
 * @property string $tipo_remito 
 * @property string $observacion
 * @property int $liquidado_repartidor
 * @property int $liquidado
 * @property int $terminal
 * @property int $contra_pago
 * @property int $cheque_idcheque
 * @property string $creado
 * @property double $precio
 * @property double $precio_adicional
 * @property double $precio_reparto
 * @property double $precio_reparto_adicional
 * @property double $porcentaje_contra
 * @property string $tipo
 *
 * @property LiquidacionHasRemito[] $liquidacionHasRemitos 
 * @property LiquidacionrepartidorHasRemito[] $liquidacionrepartidorHasRemitos 
 * @property LiquidacionRepartidor[] $liquidacionrepartidorIdliquidacionrepartidors 
 * @property EmpresaTransporte $empresatransporteIdempresatransporte
 * @property Estado $estadoIdestado
 * @property LiquidacionHasRemito $liquidacionHasRemito
 * @property FormaPago $formadepagoIdformadepago
 * @property FormaPago $contraPago
 * @property Cheque $chequeIdcheque
 * @property OrdenRetiroGrupal $ordenderetirogrupalIdordenderetirogrupal
 * @property Producto $productoIdproducto
 * @property Repartidor $repartidorIdrepartidor
 * @property Servicio $servicioIdservicio
 * @property Sucursal $sucursalIdsucursal
 * @property Carga $cargaIdcarga
 * @property Cliente $clienteIdcliente
 */
class Remito extends \yii\db\ActiveRecord
{
    public $bultos_terminal;
    public $excedentes_terminal;
    public $terminal_exc;
    public $max_idremito;
    public $min_idremito;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'remito';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['numero_remito', 'carga_idcarga', 'cliente_idcliente', 'sucursal_idsucursal', 'cantidad_bultos', 'servicio_idservicio', 'formadepago_idformadepago', 'producto_idproducto', 'repartidor_idrepartidor', 'estado_idestado', 'empresatransporte_idempresatransporte', 'ordenderetirogrupal_idordenderetirogrupal', 'liquidado_repartidor', 'terminal', 'contra_pago', 'cheque_idcheque'], 'integer'],
            [['remitofecha', 'sucursal_idsucursal', 'cliente_idcliente', 'cantidad_bultos', 'formadepago_idformadepago', 'repartidor_idrepartidor', 'ordenderetirogrupal_idordenderetirogrupal', 'valor_declarado', 'carga_idcarga'], 'required'],
            [['remitofecha', 'creado'], 'safe'],
            [['peso', 'volumen', 'valor_declarado', 'seguro', 'total_contrarembolso', 'total_guia', 'flete_pagado', 'precio', 'precio_adicional', 'precio_reparto', 'precio_reparto_adicional', 'porcentaje_contra'], 'number'],
            [['flete','tipo', 'rol_sucursal', 'facturado_a','tipo_remito', 'observacion'], 'string'],
            [['numero_guia'], 'string', 'max' => 100],
            [['empresatransporte_idempresatransporte'], 'exist', 'skipOnError' => true, 'targetClass' => EmpresaTransporte::className(), 'targetAttribute' => ['empresatransporte_idempresatransporte' => 'idempresatransporte']],
            [['estado_idestado'], 'exist', 'skipOnError' => true, 'targetClass' => Estado::className(), 'targetAttribute' => ['estado_idestado' => 'idestado']],
            [['formadepago_idformadepago'], 'exist', 'skipOnError' => true, 'targetClass' => FormaPago::className(), 'targetAttribute' => ['formadepago_idformadepago' => 'idformadepago']],
            [['ordenderetirogrupal_idordenderetirogrupal'], 'exist', 'skipOnError' => true, 'targetClass' => OrdenRetiroGrupal::className(), 'targetAttribute' => ['ordenderetirogrupal_idordenderetirogrupal' => 'idordenderetirogrupal']],
            [['producto_idproducto'], 'exist', 'skipOnError' => true, 'targetClass' => Producto::className(), 'targetAttribute' => ['producto_idproducto' => 'idproducto']],
            [['repartidor_idrepartidor'], 'exist', 'skipOnError' => true, 'targetClass' => Repartidor::className(), 'targetAttribute' => ['repartidor_idrepartidor' => 'idrepartidor']],
            [['servicio_idservicio'], 'exist', 'skipOnError' => true, 'targetClass' => Servicio::className(), 'targetAttribute' => ['servicio_idservicio' => 'idservicio']],
            [['sucursal_idsucursal'], 'exist', 'skipOnError' => true, 'targetClass' => Sucursal::className(), 'targetAttribute' => ['sucursal_idsucursal' => 'idsucursal']],
            [['cliente_idcliente'], 'exist', 'skipOnError' => true, 'targetClass' => Cliente::className(), 'targetAttribute' => ['cliente_idcliente' => 'idcliente']],
            [['carga_idcarga'], 'exist', 'skipOnError' => true, 'targetClass' => Carga::className(), 'targetAttribute' => ['carga_idcarga' => 'idcarga']],
            [['contra_pago'], 'exist', 'skipOnError' => true, 'targetClass' => LiquidacionFormaPago::className(), 'targetAttribute' => ['contra_pago' => 'idliquidacion_forma_pago']],
            [['cheque_idcheque'], 'exist', 'skipOnError' => true, 'targetClass' => Cheque::className(), 'targetAttribute' => ['cheque_idcheque' => 'idcheque']], 
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'idremito' => Yii::t('app', 'Remito'),
            'numero_remito' => Yii::t('app', 'Nº de Remito'),
            'remitofecha' => Yii::t('app', 'Fecha del Remito'),
            'sucursal_idsucursal' => Yii::t('app', 'Empresa'),
            'cantidad_bultos' => Yii::t('app', 'Cantidad de Bultos'),
            'peso' => Yii::t('app', 'Peso'),
            'volumen' => Yii::t('app', 'Excedente'),
            'valor_declarado' => Yii::t('app', 'Valor Declarado'),
            'servicio_idservicio' => Yii::t('app', 'Servicio'),
            'flete' => Yii::t('app', 'Flete'),
            'rol_sucursal' => Yii::t('app', 'Empresa'),
            'facturado_a' => Yii::t('app', 'Facturada a nombre de:'),
            'seguro' => Yii::t('app', 'Seguro'),
            'total_contrarembolso' => Yii::t('app', 'Total Contra Reembolso'),
            'formadepago_idformadepago' => Yii::t('app', 'Forma de Pago'),
            'producto_idproducto' => Yii::t('app', 'Producto'),
            'repartidor_idrepartidor' => Yii::t('app', 'Repartidor'),
            'numero_guia' => Yii::t('app', 'Número de Guía'),
            'total_guia' => Yii::t('app', 'TOTAL Guía'),
            'estado_idestado' => Yii::t('app', 'Estado'),
            'empresatransporte_idempresatransporte' => Yii::t('app', 'Empresa de Transporte'),
            'ordenderetirogrupal_idordenderetirogrupal' => Yii::t('app', 'Orden de Retiro Grupal'),
            'tipo_remito' => Yii::t('app', 'Tipo Guía'),
            'observacion' => Yii::t('app', 'Observación'),
            'carga_idcarga' => Yii::t('app', 'Carga'),
            'cliente_idcliente' => Yii::t('app', 'Cliente'),
            'liquidado_repartidor' => Yii::t('app', 'Liquidado Repartidor'),
            'terminal' => Yii::t('app', 'Destino terminal'),
            'flete_pagado' => Yii::t('app', 'Flete Pagado'), 
            'contra_pago' => Yii::t('app', 'Contra Pago'),
            'cheque_idcheque' => Yii::t('app', 'Cheque Idcheque'),
            'creado' => Yii::t('app', 'Creado'), 
            'precio' => Yii::t('app', 'Bulto'),
            'precio_adicional' => Yii::t('app', 'Bulto Adicional'),
            'precio_reparto' => Yii::t('app', 'Reparto'),
            'precio_reparto_adicional' => Yii::t('app', 'Reparto Adicional'),
            'porcentaje_contra' => Yii::t('app', 'Porcentaje Contra'),
            'tipo' => Yii::t('app', 'Tipo'),
        ];
    }

   /**
    * @return \yii\db\ActiveQuery
    */
    public function getContraPago() 
    { 
        return $this->hasOne(LiquidacionFormaPago::className(), ['idliquidacion_forma_pago' => 'contra_pago']); 
    } 

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChequeIdcheque()
    {
        return $this->hasOne(Cheque::className(), ['idcheque' => 'cheque_idcheque']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmpresatransporteIdempresatransporte()
    {
        return $this->hasOne(EmpresaTransporte::className(), ['idempresatransporte' => 'empresatransporte_idempresatransporte']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstadoIdestado()
    {
        return $this->hasOne(Estado::className(), ['idestado' => 'estado_idestado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLiquidacionHasRemito()
    {
        return $this->hasOne(LiquidacionHasRemito::className(), ['remito_idremito' => 'idremito']);
    }

    /**
	* @return \yii\db\ActiveQuery
	*/
    public function getLiquidacionrepartidorHasRemitos() {
        return $this->hasMany(LiquidacionrepartidorHasRemito::className(), ['remito_idremito' => 'idremito']);
    } 
		 
    /** 
     * @return \yii\db\ActiveQuery 
     */ 
    public function getLiquidacionrepartidorIdliquidacionrepartidors() 
    { 
        return $this->hasMany(LiquidacionRepartidor::className(), ['idliquidacion_repartidor' => 'liquidacionrepartidor_idliquidacionrepartidor'])->viaTable('{{%liquidacionrepartidor_has_remito}}', ['remito_idremito' => 'idremito']); 
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFormadepagoIdformadepago()
    {
        return $this->hasOne(FormaPago::className(), ['idformadepago' => 'formadepago_idformadepago']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrdenderetirogrupalIdordenderetirogrupal()
    {
        return $this->hasOne(OrdenRetiroGrupal::className(), ['idordenderetirogrupal' => 'ordenderetirogrupal_idordenderetirogrupal']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductoIdproducto()
    {
        return $this->hasOne(Producto::className(), ['idproducto' => 'producto_idproducto']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepartidorIdrepartidor()
    {
        return $this->hasOne(Repartidor::className(), ['idrepartidor' => 'repartidor_idrepartidor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicioIdservicio()
    {
        return $this->hasOne(Servicio::className(), ['idservicio' => 'servicio_idservicio']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSucursalIdsucursal()
    {
        return $this->hasOne(Sucursal::className(), ['idsucursal' => 'sucursal_idsucursal']);
    }

     /**
     * @return \yii\db\ActiveQuery
     */
    public function getCargaIdcarga()
    {
        return $this->hasOne(Carga::className(), ['idcarga' => 'carga_idcarga']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
   public function getClienteIdcliente()
   {
       return $this->hasOne(Cliente::className(), ['idcliente' => 'cliente_idcliente']);
   }

    /**
     * Funcion para realizar todas las validaciones necesarias
     * @return bool
     */
    public function validateAll(){
        if((($this->servicioIdservicio->nombre == "Contra Reembolso") && ($this->total_contrarembolso == ""))||(($this->total_contrarembolso != "") && ($this->servicioIdservicio->nombre != "Contra Reembolso")) ){
            $this->addError('total_contrarembolso','Debe completar el TOTAL Contra Reembolso.');
            return false;
        }
        return true;
    }

    /**
     * @return \yii\db\AvtiveQuery 
     */
    static public function getRemitosFechaCliente($clienteId, $fechaInicio, $fechaFin){
        return self::find()
        ->select('terminal, carga_idcarga, cliente_idcliente, sucursal_idsucursal, numero_remito, idremito, volumen, cantidad_bultos, remitofecha, servicio_idservicio, total_contrarembolso, tipo_remito, facturado_a, total_guia, flete_pagado, ,empresatransporte_idempresatransporte, numero_guia, rol_sucursal, ordenderetirogrupal_idordenderetirogrupal')
        ->where(['cliente_idcliente' => $clienteId])
        ->andWhere(['>=', 'remitofecha', $fechaInicio])
        ->andWhere(['<=', 'remitofecha', $fechaFin])
        ->andWhere([ 'liquidado' => 0])
        ->andWhere(['!=', 'estado_idestado', 4])
        ->andWhere(['formadepago_idformadepago' => 3])
        ->orderBy(['remitofecha' => SORT_ASC]) 
        ->all();
    }

    /**
     * @return \yii\db\AvtiveQuery 
     */
    static public function updateRemitosFechaCliente($clienteId, $fechaInicio, $fechaFin){

        $params = [':cliente' => $clienteId, ':initDate' => $fechaInicio, ':endDate' => $fechaFin, ':estado' => 0, ':estadoA' => 4, ':estadoB' => 3];
        Yii::$app->db->createCommand('UPDATE remito SET liquidado=1 WHERE cliente_idcliente=:cliente AND remitofecha>=:initDate  AND remitofecha<=:endDate AND liquidado=:estado AND estado_idestado!=:estadoA AND formadepago_idformadepago=:estadoB')->bindValues($params)->execute();
    }

    /*Function tha get all remitos between two dates of one repartidor
    /**
     * @return \yii\db\AvtiveQuery
     */
    static public function getRemitosOrdenFechaByRepartidor($repartidorId, $fechaInicio, $fechaFin){
        return self::find()
        ->where(['repartidor_idrepartidor' => $repartidorId])
        ->andWhere(['>=', 'remitofecha', $fechaInicio])
        ->andWhere(['<=', 'remitofecha', $fechaFin])
        ->andWhere([ 'liquidado_repartidor' => 0])
        ->andWhere(['!=', 'estado_idestado', 4])
            ->orderBy(['cliente_idcliente' => SORT_ASC, 'remitofecha' => SORT_ASC])
            ->all();
    }

    /**
     * @return \yii\db\AvtiveQuery
     */
    static public function getSumBultosByRepartidorForCliente($repartidorId, $fechaInicio, $fechaFin){
        return self::find()
        ->select([
            'SUM(cantidad_bultos) AS cantidad_bultos',
            'SUM(volumen) AS volumen',
            'cliente_idcliente',
            'sucursal_idsucursal'])
        ->where(['repartidor_idrepartidor' => $repartidorId])
        ->andWhere(['>=', 'remitofecha', $fechaInicio])
        ->andWhere(['<=', 'remitofecha', $fechaFin])
        ->andWhere([ 'liquidado_repartidor' => 1])
        ->andWhere(['!=', 'estado_idestado', 4])
        ->groupBy(['cliente_idcliente'])->all();
    }

    /**
     * @return \yii\db\AvtiveQuery
     */
    static public function getRemitosLiquidadosByRepartidor($repartidorId, $fechaInicio, $fechaFin){
        return self::find()
        ->where(['repartidor_idrepartidor' => $repartidorId])
        ->andWhere(['>=', 'remitofecha', $fechaInicio])
        ->andWhere(['<=', 'remitofecha', $fechaFin])
        ->andWhere([ 'liquidado_repartidor' => 1])
        ->orderBy(['remitofecha' => SORT_ASC])->all();
    }



    /**
     * @return \yii\db\AvtiveQuery
     */
    static public function testIfFirstOfDate($fecha, $cliente){
        return self::find()
        ->where(['cliente_idcliente' => $cliente])
        ->andWhere(['remitofecha' => $fecha])
        ->andWhere([ 'estado_idestado' => 3])
        ->orderBy(['remitofecha' => SORT_ASC])->one();
    }

    /**
     * @return \yii\db\AvtiveQuery
     */
    static public function countRemitos($cliente_idcliente, $periodo_inicio, $periodo_fin){
        return  self::find()
                ->where(['cliente_idcliente' => $cliente_idcliente])
                ->andWhere(['>=', 'remitofecha', $periodo_inicio])
                ->andWhere(['<=', 'remitofecha', $periodo_fin])
                ->count();
    }



    /**
     * @return \yii\db\AvtiveQuery
     */
    static public function remitosSinZona($cliente_idcliente, $periodo_inicio, $periodo_fin){
        return  self::find()
                ->joinWith(['sucursalIdsucursal as sucursal', 
                            'sucursalIdsucursal.localidadIdlocalidad as localidad'])
                ->where(['cliente_idcliente' => $cliente_idcliente])
                ->andWhere(['>=', 'remitofecha', $periodo_inicio])
                ->andWhere(['<=', 'remitofecha', $periodo_fin])
                ->andWhere('0 = (SELECT count(*) FROM zon_has_localidad WHERE localidad_idlocalidad = localidad.idlpcalidad)')
                ->all();
    }



    /**
     * @return \yii\db\AvtiveQuery
     */
    static public function remitosView($idLiquidacion){
        return self::find()->select('idremito, remitofecha, facturado_a, tipo_remito, volumen, terminal, carga_idcarga, sucursal_idsucursal, empresatransporte_idempresatransporte, cliente_idcliente, numero_remito, numero_guia, rol_sucursal, cantidad_bultos, total_guia, ordenderetirogrupal_idordenderetirogrupal, precio, precio_adicional, precio_reparto, precio_reparto_adicional, porcentaje_contra')
        ->joinWith('liquidacionHasRemito')
        ->where(['liquidacion_idliquidacion' => $idLiquidacion])
        ->orderBy(['remitofecha' => SORT_ASC, 'idremito' => SORT_ASC]);
    }

    /**
     * Funcion que retorna la cantidad de remitos de un liquidacion agrupado por cliente y precio de reparto
     * @param $idliquidacion_repartidor
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getCantidadRemitosLiquidacionComun($idliquidacion_repartidor){
        $remitosRepartidorSubQuery = self::find()
            ->select('
                    cliente_idcliente,
                    remitofecha,
                    min(idremito) min_idremito,
                    tipo,
                    terminal,
                    sum(case when terminal = 1 then 1 else 0 end) bultos_terminal,
                    sum(case when terminal = 0 then 1 else 0 end) cantidad_bultos, 
                    sum(case when terminal = 1 and volumen is not null then volumen + cantidad_bultos - 1 when terminal = 1 then cantidad_bultos - 1 else 0 end) excedentes_terminal,
                    sum(case when terminal = 0 and volumen is not null then volumen + cantidad_bultos - 1 when terminal = 0 then cantidad_bultos - 1 else 0 end) volumen,
                    precio_reparto,
                    precio_reparto_adicional
                    ')
            ->joinWith('liquidacionrepartidorHasRemitos')
            ->where([
                'liquidacionrepartidor_idliquidacionrepartidor' => $idliquidacion_repartidor])
            ->groupBy([ 'cliente_idcliente','remitofecha', 'precio_reparto', 'precio_reparto_adicional'])
            ->orderBy(['cliente_idcliente' => SORT_ASC, 'remitofecha' => SORT_ASC, 'min_idremito' => SORT_ASC]);
        $arrayTodoBultos = self::find()->select('
        cliente_idcliente, 
        case when tipo = \'Tipo B\' and terminal = 1 then count(*) when tipo = \'Tipo B\' then 0 else sum(bultos_terminal) end bultos_terminal, 
        case when tipo = \'Tipo B\' and terminal = 0 then count(*) when tipo = \'Tipo B\' then 0 else sum(cantidad_bultos) end cantidad_bultos,
        case when tipo = \'Tipo B\' and terminal = 1 then sum(bultos_terminal + excedentes_terminal) - count(*) when tipo = \'Tipo B\' then sum(excedentes_terminal + bultos_terminal) else sum(excedentes_terminal) end excedentes_terminal,
        case when tipo = \'Tipo B\' and terminal = 0 then sum(cantidad_bultos + volumen) - count(*) when tipo = \'Tipo B\' then sum(cantidad_bultos + volumen) else sum(volumen) end volumen,
        precio_reparto,precio_reparto_adicional  
        ')->from(['tabla' => $remitosRepartidorSubQuery])->groupBy(['cliente_idcliente','precio_reparto']);
        echo $arrayTodoBultos->createCommand()->getRawSql();
        return $arrayTodoBultos->all();
    }


    /**
     * Funcion que retorna todos los remitos de una liquidacion de repartido agrupado por cliente
     * @param $idliquidacion_repartidor
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getRemitosPorLiquidacionByCliente($idliquidacion_repartidor){
        return self::find()
            ->select('cliente_idcliente, remitofecha, terminal')
            ->joinWith('liquidacionrepartidorHasRemitos')
            ->where(['liquidacionrepartidor_idliquidacionrepartidor' => $idliquidacion_repartidor])
            ->groupBy([ 'cliente_idcliente'])
            ->orderBy(['cliente_idcliente' => SORT_ASC])->all();
    }

    /**
     * Funcion que retorna la sumatoria de todos los remitos de una liquidacion agrupados por cliente y fecha
     * @param $idliquidacion_repartidor
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getSumatoriaRemitosComunesPorLiquidacionByCliente($idliquidacion_repartidor){
        $remitosRepartidorSubQuery = self::find()
            ->select('
                    cliente_idcliente,
                    remitofecha,
                    min(idremito) min_idremito,
                    tipo,
                    terminal,
                    sum(case when terminal = 1 then 1 else 0 end) bultos_terminal,
                    sum(case when terminal = 0 then 1 else 0 end) cantidad_bultos, 
                    sum(case when terminal = 1 and volumen is not null then volumen + cantidad_bultos - 1 when terminal = 1 then cantidad_bultos - 1 else 0 end) excedentes_terminal,
                    sum(case when terminal = 0 and volumen is not null then volumen + cantidad_bultos - 1 when terminal = 0 then cantidad_bultos - 1 else 0 end) volumen,
                    precio_reparto,
                    precio_reparto_adicional
                    ')
            ->joinWith('liquidacionrepartidorHasRemitos')
            ->where([
                'liquidacionrepartidor_idliquidacionrepartidor' => $idliquidacion_repartidor])
            ->groupBy([ 'cliente_idcliente','remitofecha'])
            ->orderBy(['cliente_idcliente' => SORT_ASC, 'remitofecha' => SORT_ASC, 'min_idremito' => SORT_ASC]);
        $arrayTodoBultos = self::find()->select('
        cliente_idcliente, 
        remitofecha,
        case when tipo = \'Tipo B\' and terminal = 1 then 1 when tipo = \'Tipo B\' then 0 else sum(bultos_terminal) end terminal, 
        case when tipo = \'Tipo B\' and terminal = 0 then 1 when tipo = \'Tipo B\' then 0 else sum(cantidad_bultos) end cantidad_bultos,
        case when tipo = \'Tipo B\' and terminal = 1 then sum(bultos_terminal + excedentes_terminal) - 1 when tipo = \'Tipo B\' then sum(excedentes_terminal + bultos_terminal) else sum(excedentes_terminal) end terminal_exc,
        case when tipo = \'Tipo B\' and terminal = 0 then sum(cantidad_bultos + volumen) - 1 when tipo = \'Tipo B\' then sum(cantidad_bultos + volumen) else sum(volumen) end volumen,
        precio_reparto,precio_reparto_adicional  
        ')->from(['tabla' => $remitosRepartidorSubQuery])->groupBy(['cliente_idcliente','remitofecha']);
        return $arrayTodoBultos->all();

    }

    /**
     * Funcion que retorna la sumatoria de bultos, volumen y neto por mes de un año y cliente determinado
     * @param $cliente_idcliente
     * @param $periodo_inicio
     * @return array [fecha, bultos, volumen, neto]
     * @throws \yii\db\Exception
     */
    public static function getSumatoriaRemitosPorClienteAnio($cliente_idcliente, $periodo_inicio){
            return Yii::$app->db->createCommand(
                'SELECT 
        MONTH(remitofecha) fecha,
        SUM(cantidad_bultos) bultos,
        SUM(volumen) volumen,
        ROUND( SUM(cantidad_bultos * CASE
            WHEN precio IS NOT NULL THEN precio
            ELSE 0
        END + CASE
            WHEN volumen IS NOT NULL THEN volumen
            ELSE 0
        END * CASE
            WHEN precio_adicional IS NOT NULL THEN precio_adicional
            ELSE 0
        END) / 1.21, 2) neto
    FROM
        remito
    WHERE
        cliente_idcliente = :cliente_idcliente
            AND YEAR(remitofecha) = :periodo_inicio
    GROUP BY MONTH(remitofecha)')
                ->bindValue(':cliente_idcliente', $cliente_idcliente)
                ->bindValue(':periodo_inicio', $periodo_inicio)
                ->queryAll();
    }
}
