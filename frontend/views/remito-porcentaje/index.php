<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel frontend\models\RemitoPorcentajeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Remitos por Porcentajes');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="remito-porcentaje-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="x_panel">
    <div class="x_title">
        <h2>Listado</h2>
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
    <?php Pjax::begin(); ?>
    <?php //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Crear Remito por Porcentaje'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'moduleId' => 'gridviewk',
        'toolbar' =>  [
            '{export}',
            '{toggleData}',
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
        ],
        'columns' => [
            'fecha',
            'numero_planilla',
            [
                'attribute' => 'cliente_idcliente',
                'value' => function($data){
                    return $data->clienteIdcliente->razonsocial;
                }
            ],
            [
                'attribute' => 'repartidor_idrepartidor',
                'value' => function($data){
                    return $data->repartidorIdrepartidor->apellido.", ".$data->repartidorIdrepartidor->nombre;
                }
            ],
            //'cant_pedidos',
            //'cant_entregados',
            //'noquiso_nopidio',
            //'cant_anulado',
            //'cant_sin_dinero',
            //'cant_regresado',
            //'cant_faltante',
            //'cant_mal_armado',
            //'cant_duplicado',
            [
                'attribute'=>'liquidado_cliente',
                'label' => 'Liquidado Cliente',
                'format' => 'boolean',
            ],
            [
                'attribute'=>'liquidado_repartidor',
                'label' => 'Liquidado Repartidor',
                'format' => 'boolean',
            ],
            [
                'attribute' => 'dinero_facturado',
                'format' => ['decimal',2]
            ],
            [
                'attribute' => 'dinero_rendido',
                'format' => ['decimal',2]
            ],
            [
                'attribute' => 'dinero_devuelto',
                'format' => ['decimal',2]
            ],
            [
                'attribute' => 'creado',
                'label' => 'Fecha y Hora creación',
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
        </div>
    </div>
</div>
