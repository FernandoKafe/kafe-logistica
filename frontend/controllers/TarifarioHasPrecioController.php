<?php

namespace frontend\controllers;

use Yii;
use common\models\TarifarioHasPrecio;
use frontend\models\TarifarioHasPrecioSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * TarifarioHasPrecioController implements the CRUD actions for TarifarioHasPrecio model.
 */
class TarifarioHasPrecioController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TarifarioHasPrecio models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TarifarioHasPrecioSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TarifarioHasPrecio model.
     * @param integer $carga_idcarga
     * @param integer $zona_idzona
     * @param integer $tarifario_idtarifario 
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($carga_idcarga, $zona_idzona, $tarifario_idtarifario)
    {
        return $this->render('view', [
            'model' => $this->findModel($carga_idcarga, $zona_idzona, $tarifario_idtarifario),
        ]);
    }

    /**
     * Creates a new TarifarioHasPrecio model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TarifarioHasPrecio();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'carga_idcarga' => $model->carga_idcarga, 'zona_idzona' => $model->zona_idzona, 'tarifario_idtarifario' => $model->tarifario_idtarifario]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing TarifarioHasPrecio model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $carga_idcarga
     * @param integer $zona_idzona
     * @param integer $tarifario_idtarifario
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($carga_idcarga, $zona_idzona, $tarifario_idtarifario)
    {
        $model = $this->findModel($carga_idcarga, $zona_idzona, $tarifario_idtarifario);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'carga_idcarga' => $model->carga_idcarga, 'zona_idzona' => $model->zona_idzona, 'tarifario_idtarifario' => $model->tarifario_idtarifario]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing TarifarioHasPrecio model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $carga_idcarga
     * @param integer $zona_idzona
     * @param integer $tarifario_idtarifario
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($carga_idcarga, $zona_idzona, $tarifario_idtarifario)
    {
        $this->findModel($carga_idcarga, $zona_idzona, $tarifario_idtarifario)->delete();

        return $this->redirect(['tarifario/view?id='.$tarifario_idtarifario]);
    }

    /**
     * Finds the TarifarioHasPrecio model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $carga_idcarga
     * @param integer $zona_idzona
     * @param integer $tarifario_idtarifario
     * @return TarifarioHasPrecio the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($carga_idcarga, $zona_idzona, $tarifario_idtarifario)
    {
        if (($model = TarifarioHasPrecio::findOne(['carga_idcarga' => $carga_idcarga, 'zona_idzona' => $zona_idzona, 'tarifario_idtarifario' => $tarifario_idtarifario])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
