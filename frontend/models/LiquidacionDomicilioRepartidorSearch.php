<?php

namespace frontend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\LiquidacionDomicilioRepartidor;

/**
 * LiquidacionDomicilioRepartidorSearch represents the model behind the search form of `common\models\LiquidacionDomicilioRepartidor`.
 */
class LiquidacionDomicilioRepartidorSearch extends LiquidacionDomicilioRepartidor
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['idliquidacion_domicilio_repartidor', 'liquidacionunica'], 'integer'],
            [['fecha_inicio', 'fecha_fin', 'nombre', 'repartidor_idrepartidor', 'estadoliquidacion_idestadoliquidacion', 'creado'], 'safe'],
            [['total'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = LiquidacionDomicilioRepartidor::find()->where(['borrado' => 0]);
        $query->joinWith(['repartidorIdrepartidor as repartidor']);
        $query->joinWith(['estadoliquidacionIdestadoliquidacion as estado']);



        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'idliquidacion_domicilio_repartidor' => $this->idliquidacion_domicilio_repartidor,
            'fecha_inicio' => $this->fecha_inicio,
            'fecha_fin' => $this->fecha_fin,
            'total' => $this->total,
            'creado' => $this->creado,
            'liquidacionunica' => $this->liquidacionunica,
        ]);

        $query->andFilterWhere(['like', 'liquidacion_domicilio_repartidor.nombre', $this->nombre]);
        $query->andFilterWhere(['like', 'CONCAT(repartidor.apellido, ", ", repartidor.nombre)', $this->repartidor_idrepartidor]);
        $query->andFilterWhere(['like', 'estado.nombre', $this->estadoliquidacion_idestadoliquidacion]);

        return $dataProvider;
    }
}
