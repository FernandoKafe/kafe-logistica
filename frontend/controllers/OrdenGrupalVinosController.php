<?php

namespace frontend\controllers;

use Yii;
use common\models\OrdenGrupalVinos;
use frontend\models\OrdenGrupalVinosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use common\models\RemitoVino;

/**
 * OrdenGrupalVinosController implements the CRUD actions for OrdenGrupalVinos model.
 */
class OrdenGrupalVinosController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrdenGrupalVinos models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdenGrupalVinosSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrdenGrupalVinos model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new OrdenGrupalVinos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new OrdenGrupalVinos();

        if ($model->load(Yii::$app->request->post())) {
            if($model->clienteIdcliente->tarifariovino_idtarifariovino){
                $model->creado = date('Y-m-d G:i', time());
                $model->save();
                return $this->redirect([
                'add-remito',
                'idOrdenGrupal' => $model->idorden_grupal_vinos]);
            }else{
                Yii::$app->session->setflash('error', "Asignar un tarifario vino al cliente antes de crear la orden grupal.");
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]); 
    }

    /**
     * Updates an existing OrdenGrupalVinos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect([
                'add-remito',
                'idOrdenGrupal' => $model->idorden_grupal_vinos,
                ]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OrdenGrupalVinos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if(!$model->liquidacionvinocliente_idliquidacionvinocliente && !$model->liquidacionvinorepartidor_idliquidacionvinorepartidor){
            $remitosVinos = RemitoVino::find()->where(['ordengrupalvinos_idordengrupalvinos' => $model->idorden_grupal_vinos])->all();
            foreach($remitosVinos as $r){
                $r->delete();
            }
            $model->delete();
        }else{
            Yii::$app->session->setflash('error', "Imposible eliminar. Orden ya liquidada a repartidor o cliente.");
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the OrdenGrupalVinos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrdenGrupalVinos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrdenGrupalVinos::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }

     /**
     * Add Remitos to an existing OrdenRetiroGrupal model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id idcomprobante
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAddRemito()
    {
        $model = new RemitoVino();
        $model->ordengrupalvinos_idordengrupalvinos = $_GET['idOrdenGrupal'];

        if ($model->load(Yii::$app->request->post())) {
            $model->creado = date('Y-m-d G:i', time());
            if($model->save()){
                Yii::$app->session->setflash('success', "Remito vino creado con éxito.");
            }
            return $this->redirect([
                'add-remito',
                'idOrdenGrupal' => $model->ordengrupalvinos_idordengrupalvinos,
            ]);
        }

        return $this->render('add-remito', [
            'model' => $model,
        ]);
    }
}
