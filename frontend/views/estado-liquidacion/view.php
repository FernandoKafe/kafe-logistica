<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Liquidacion */

$this->title = $model->nombre;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Liquidación'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="liquidacion-view">
<div class="x_panel">
    <div class="x_title">
        <h2><?= Yii::t('app', 'Detalle') ?></h2>
        <div class="clearfix"></div>
    </div>
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Inicio'), ['index', 'id' => $model->idestado_liquidacion], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Actualizar'), ['update', 'id' => $model->idestado_liquidacion], ['class' => 'btn btn-success']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'idliquidacion',
            'nombre',
        ],
    ]) ?>

</div> 
</div> 
